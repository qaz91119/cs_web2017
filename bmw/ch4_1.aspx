﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ch4_1.aspx.cs" Inherits="CS_ch4_1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            這是第四章的第一個練習:<br />
            <br />
            現在下面文字方塊輸入資料，並點按確定按鈕。<br />
            <asp:TextBox ID="TB1" runat="server" BackColor="Lime" ForeColor="Blue" Height="20px" TextMode="MultiLine" Width="121px" AutoPostBack="True" OnTextChanged="TB1_TextChanged">凸凸凸凸凸凸凸凸凸凸凸凸凸凸凸</asp:TextBox>
            <br />
            <br />
            <asp:Button ID="btn1" runat="server" OnClick="btn1_Click" Text="確定" />
            <br />
            <br />
            <asp:Label ID="LB1" runat="server" Text="顯示區"></asp:Label>
            <br />
            <br />
        </div>
    </form>
</body>
</html>
