﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default3.aspx.cs" Inherits="作業_第八組_莫_Default3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Member_Account" DataSourceID="SqlDataSource1">
                <Columns>
                    <asp:BoundField DataField="Member_ID" HeaderText="Member_ID" SortExpression="Member_ID" />
                    <asp:BoundField DataField="Member_Account" HeaderText="Member_Account" ReadOnly="True" SortExpression="Member_Account" />
                    <asp:BoundField DataField="Member_Password" HeaderText="Member_Password" SortExpression="Member_Password" />
                    <asp:BoundField DataField="Member_Name" HeaderText="Member_Name" SortExpression="Member_Name" />
                    <asp:BoundField DataField="E_mail" HeaderText="E_mail" SortExpression="E_mail" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:eightConnectionString %>" SelectCommand="SELECT * FROM [Member]"></asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
