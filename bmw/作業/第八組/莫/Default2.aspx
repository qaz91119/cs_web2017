﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="作業_第八組_莫_Default2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:FormView ID="FormView1" runat="server" AllowPaging="True" DataKeyNames="Discussion_ID" DataSourceID="SqlDataSource1">
                <EditItemTemplate>
                    Discussion_ID:
                    <asp:Label ID="Discussion_IDLabel1" runat="server" Text='<%# Eval("Discussion_ID") %>' />
                    <br />
                    Member_Account:
                    <asp:TextBox ID="Member_AccountTextBox" runat="server" Text='<%# Bind("Member_Account") %>' />
                    <br />
                    Book_ID:
                    <asp:TextBox ID="Book_IDTextBox" runat="server" Text='<%# Bind("Book_ID") %>' />
                    <br />
                    Book_Name:
                    <asp:TextBox ID="Book_NameTextBox" runat="server" Text='<%# Bind("Book_Name") %>' />
                    <br />
                    Discussion_Content:
                    <asp:TextBox ID="Discussion_ContentTextBox" runat="server" Text='<%# Bind("Discussion_Content") %>' />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="更新" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
                </EditItemTemplate>
                <InsertItemTemplate>
                    Discussion_ID:
                    <asp:TextBox ID="Discussion_IDTextBox" runat="server" Text='<%# Bind("Discussion_ID") %>' />
                    <br />
                    Member_Account:
                    <asp:TextBox ID="Member_AccountTextBox" runat="server" Text='<%# Bind("Member_Account") %>' />
                    <br />
                    Book_ID:
                    <asp:TextBox ID="Book_IDTextBox" runat="server" Text='<%# Bind("Book_ID") %>' />
                    <br />
                    Book_Name:
                    <asp:TextBox ID="Book_NameTextBox" runat="server" Text='<%# Bind("Book_Name") %>' />
                    <br />
                    Discussion_Content:
                    <asp:TextBox ID="Discussion_ContentTextBox" runat="server" Text='<%# Bind("Discussion_Content") %>' />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="插入" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
                </InsertItemTemplate>
                <ItemTemplate>
                    Discussion_ID:
                    <asp:Label ID="Discussion_IDLabel" runat="server" Text='<%# Eval("Discussion_ID") %>' />
                    <br />
                    Member_Account:
                    <asp:Label ID="Member_AccountLabel" runat="server" Text='<%# Bind("Member_Account") %>' />
                    <br />
                    Book_ID:
                    <asp:Label ID="Book_IDLabel" runat="server" Text='<%# Bind("Book_ID") %>' />
                    <br />
                    Book_Name:
                    <asp:Label ID="Book_NameLabel" runat="server" Text='<%# Bind("Book_Name") %>' />
                    <br />
                    Discussion_Content:
                    <asp:Label ID="Discussion_ContentLabel" runat="server" Text='<%# Bind("Discussion_Content") %>' />
                    <br />
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="編輯" />
                    &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="刪除" />
                    &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="新增" />
                </ItemTemplate>
            </asp:FormView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:eightConnectionString %>" DeleteCommand="DELETE FROM [Discussion_board] WHERE [Discussion_ID] = @Discussion_ID" InsertCommand="INSERT INTO [Discussion_board] ([Discussion_ID], [Member_Account], [Book_ID], [Book_Name], [Discussion_Content]) VALUES (@Discussion_ID, @Member_Account, @Book_ID, @Book_Name, @Discussion_Content)" SelectCommand="SELECT * FROM [Discussion_board]" UpdateCommand="UPDATE [Discussion_board] SET [Member_Account] = @Member_Account, [Book_ID] = @Book_ID, [Book_Name] = @Book_Name, [Discussion_Content] = @Discussion_Content WHERE [Discussion_ID] = @Discussion_ID">
                <DeleteParameters>
                    <asp:Parameter Name="Discussion_ID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Discussion_ID" Type="Int32" />
                    <asp:Parameter Name="Member_Account" Type="String" />
                    <asp:Parameter Name="Book_ID" Type="Int32" />
                    <asp:Parameter Name="Book_Name" Type="String" />
                    <asp:Parameter Name="Discussion_Content" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Member_Account" Type="String" />
                    <asp:Parameter Name="Book_ID" Type="Int32" />
                    <asp:Parameter Name="Book_Name" Type="String" />
                    <asp:Parameter Name="Discussion_Content" Type="String" />
                    <asp:Parameter Name="Discussion_ID" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
