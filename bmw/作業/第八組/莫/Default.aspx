﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="作業_第八組_莫_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="Book_ID" DataSourceID="SqlDataSource1">
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="Book_ID" HeaderText="Book_ID" ReadOnly="True" SortExpression="Book_ID" />
                    <asp:BoundField DataField="Member_Account" HeaderText="Member_Account" SortExpression="Member_Account" />
                    <asp:BoundField DataField="Book_Name" HeaderText="Book_Name" SortExpression="Book_Name" />
                    <asp:BoundField DataField="Author" HeaderText="Author" SortExpression="Author" />
                    <asp:BoundField DataField="Lastest_Update" HeaderText="Lastest_Update" SortExpression="Lastest_Update" />
                    <asp:BoundField DataField="Book_Status" HeaderText="Book_Status" SortExpression="Book_Status" />
                    <asp:BoundField DataField="Book_Picture" HeaderText="Book_Picture" SortExpression="Book_Picture" />
                    <asp:BoundField DataField="Book_Type" HeaderText="Book_Type" SortExpression="Book_Type" />
                    <asp:BoundField DataField="Book_Introduction" HeaderText="Book_Introduction" SortExpression="Book_Introduction" />
                    <asp:BoundField DataField="URL" HeaderText="URL" SortExpression="URL" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:eightConnectionString %>" SelectCommand="SELECT * FROM [Book]"></asp:SqlDataSource>
            <br />
            <br />
            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="Discussion_ID" DataSourceID="SqlDataSource2">
                <Columns>
                    <asp:BoundField DataField="Discussion_ID" HeaderText="Discussion_ID" ReadOnly="True" SortExpression="Discussion_ID" />
                    <asp:BoundField DataField="Member_Account" HeaderText="Member_Account" SortExpression="Member_Account" />
                    <asp:BoundField DataField="Book_ID" HeaderText="Book_ID" SortExpression="Book_ID" />
                    <asp:BoundField DataField="Book_Name" HeaderText="Book_Name" SortExpression="Book_Name" />
                    <asp:BoundField DataField="Discussion_Content" HeaderText="Discussion_Content" SortExpression="Discussion_Content" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:eightConnectionString %>" SelectCommand="SELECT * FROM [Discussion_board] WHERE ([Discussion_ID] = @Discussion_ID)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="GridView1" Name="Discussion_ID" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
