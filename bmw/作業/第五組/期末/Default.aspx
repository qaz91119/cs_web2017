﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="bmw_作業_第五組_期末_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/作業/第五組/期末/Default2.aspx">疫苗注意事項</asp:HyperLink>
            <br />
            問題.....<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/作業/第五組/期末/Default3.aspx">詳見內文</asp:HyperLink>
            <br />
            <br />
            <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataSourceID="SqlDataSource1" Height="50px" OnPageIndexChanging="DetailsView1_PageIndexChanging1" Width="125px">
                <Fields>
                    <asp:BoundField DataField="article_read" HeaderText="瀏覽次數" SortExpression="article_read" />
                    <asp:BoundField DataField="article_like" HeaderText="按讚" SortExpression="article_like" />
                </Fields>
            </asp:DetailsView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:fiveConnectionString %>" SelectCommand="SELECT [article_like], [article_read] FROM [article]"></asp:SqlDataSource>
            <br />
            <br />
            <br />
            <asp:DetailsView ID="DetailsView2" runat="server" AllowPaging="True" AutoGenerateRows="False" DataKeyNames="article_ID" DataSourceID="SqlDataSource2" Height="50px" Width="125px">
                <Fields>
                    <asp:BoundField DataField="article_ID" HeaderText="article_ID" ReadOnly="True" SortExpression="article_ID" />
                    <asp:TemplateField HeaderText="article_title" SortExpression="article_title">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("article_title") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("article_title") %>'></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl='<%# Eval("article_ID", "Default2.aspx?article_ID={0}") %>' Text='<%# Eval("article_title") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="article_class" HeaderText="article_class" SortExpression="article_class" />
                    <asp:BoundField DataField="article_content" HeaderText="article_content" SortExpression="article_content" />
                    <asp:BoundField DataField="article_date" HeaderText="article_date" SortExpression="article_date" />
                    <asp:BoundField DataField="article_like" HeaderText="article_like" SortExpression="article_like" />
                    <asp:BoundField DataField="article_read" HeaderText="article_read" SortExpression="article_read" />
                    <asp:BoundField DataField="M_account" HeaderText="M_account" SortExpression="M_account" />
                    <asp:CommandField ShowInsertButton="True" />
                </Fields>
            </asp:DetailsView>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:fiveConnectionString %>" DeleteCommand="DELETE FROM [article] WHERE [article_ID] = @article_ID" InsertCommand="INSERT INTO [article] ([article_ID], [article_title], [article_class], [article_content], [article_date], [article_like], [article_read], [M_account]) VALUES (@article_ID, @article_title, @article_class, @article_content, @article_date, @article_like, @article_read, @M_account)" SelectCommand="SELECT * FROM [article]" UpdateCommand="UPDATE [article] SET [article_title] = @article_title, [article_class] = @article_class, [article_content] = @article_content, [article_date] = @article_date, [article_like] = @article_like, [article_read] = @article_read, [M_account] = @M_account WHERE [article_ID] = @article_ID">
                <DeleteParameters>
                    <asp:Parameter Name="article_ID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="article_ID" Type="Int32" />
                    <asp:Parameter Name="article_title" Type="String" />
                    <asp:Parameter Name="article_class" Type="String" />
                    <asp:Parameter Name="article_content" Type="String" />
                    <asp:Parameter Name="article_date" Type="DateTime" />
                    <asp:Parameter Name="article_like" Type="Decimal" />
                    <asp:Parameter Name="article_read" Type="Decimal" />
                    <asp:Parameter Name="M_account" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="article_title" Type="String" />
                    <asp:Parameter Name="article_class" Type="String" />
                    <asp:Parameter Name="article_content" Type="String" />
                    <asp:Parameter Name="article_date" Type="DateTime" />
                    <asp:Parameter Name="article_like" Type="Decimal" />
                    <asp:Parameter Name="article_read" Type="Decimal" />
                    <asp:Parameter Name="M_account" Type="String" />
                    <asp:Parameter Name="article_ID" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <br />
            <br />
            <br />
            <br />
            <br />
            <asp:FormView ID="FormView1" runat="server" AllowPaging="True" DataKeyNames="article_ID" DataSourceID="SqlDataSource3">
                <EditItemTemplate>
                    article_ID:
                    <asp:DynamicControl ID="article_IDDynamicControl" runat="server" DataField="article_ID" Mode="ReadOnly" />
                    <br />
                    article_title:
                    <asp:DynamicControl ID="article_titleDynamicControl" runat="server" DataField="article_title" Mode="Edit" />
                    <br />
                    article_class:
                    <asp:DynamicControl ID="article_classDynamicControl" runat="server" DataField="article_class" Mode="Edit" />
                    <br />
                    article_content:
                    <asp:DynamicControl ID="article_contentDynamicControl" runat="server" DataField="article_content" Mode="Edit" />
                    <br />
                    article_date:
                    <asp:DynamicControl ID="article_dateDynamicControl" runat="server" DataField="article_date" Mode="Edit" />
                    <br />
                    article_like:
                    <asp:DynamicControl ID="article_likeDynamicControl" runat="server" DataField="article_like" Mode="Edit" />
                    <br />
                    article_read:
                    <asp:DynamicControl ID="article_readDynamicControl" runat="server" DataField="article_read" Mode="Edit" />
                    <br />
                    M_account:
                    <asp:DynamicControl ID="M_accountDynamicControl" runat="server" DataField="M_account" Mode="Edit" />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="更新" ValidationGroup="Insert" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
                </EditItemTemplate>
                <InsertItemTemplate>
                    article_ID:
                    <asp:DynamicControl ID="article_IDDynamicControl" runat="server" DataField="article_ID" Mode="Insert" ValidationGroup="Insert" />
                    <br />
                    article_title:
                    <asp:DynamicControl ID="article_titleDynamicControl" runat="server" DataField="article_title" Mode="Insert" ValidationGroup="Insert" />
                    <br />
                    article_class:
                    <asp:DynamicControl ID="article_classDynamicControl" runat="server" DataField="article_class" Mode="Insert" ValidationGroup="Insert" />
                    <br />
                    article_content:
                    <asp:DynamicControl ID="article_contentDynamicControl" runat="server" DataField="article_content" Mode="Insert" ValidationGroup="Insert" />
                    <br />
                    article_date:
                    <asp:DynamicControl ID="article_dateDynamicControl" runat="server" DataField="article_date" Mode="Insert" ValidationGroup="Insert" />
                    <br />
                    article_like:
                    <asp:DynamicControl ID="article_likeDynamicControl" runat="server" DataField="article_like" Mode="Insert" ValidationGroup="Insert" />
                    <br />
                    article_read:
                    <asp:DynamicControl ID="article_readDynamicControl" runat="server" DataField="article_read" Mode="Insert" ValidationGroup="Insert" />
                    <br />
                    M_account:
                    <asp:DynamicControl ID="M_accountDynamicControl" runat="server" DataField="M_account" Mode="Insert" ValidationGroup="Insert" />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="插入" ValidationGroup="Insert" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
                </InsertItemTemplate>
                <ItemTemplate>
                    <br />
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="編輯" />
&nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text='<%# Eval("article_ID") %>' />
                </ItemTemplate>
            </asp:FormView>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:fiveConnectionString %>" DeleteCommand="DELETE FROM [article] WHERE [article_ID] = @original_article_ID AND (([article_title] = @original_article_title) OR ([article_title] IS NULL AND @original_article_title IS NULL)) AND (([article_class] = @original_article_class) OR ([article_class] IS NULL AND @original_article_class IS NULL)) AND (([article_content] = @original_article_content) OR ([article_content] IS NULL AND @original_article_content IS NULL)) AND (([article_date] = @original_article_date) OR ([article_date] IS NULL AND @original_article_date IS NULL)) AND (([article_like] = @original_article_like) OR ([article_like] IS NULL AND @original_article_like IS NULL)) AND (([article_read] = @original_article_read) OR ([article_read] IS NULL AND @original_article_read IS NULL)) AND (([M_account] = @original_M_account) OR ([M_account] IS NULL AND @original_M_account IS NULL))" InsertCommand="INSERT INTO [article] ([article_ID], [article_title], [article_class], [article_content], [article_date], [article_like], [article_read], [M_account]) VALUES (@article_ID, @article_title, @article_class, @article_content, @article_date, @article_like, @article_read, @M_account)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [article]" UpdateCommand="UPDATE [article] SET [article_title] = @article_title, [article_class] = @article_class, [article_content] = @article_content, [article_date] = @article_date, [article_like] = @article_like, [article_read] = @article_read, [M_account] = @M_account WHERE [article_ID] = @original_article_ID AND (([article_title] = @original_article_title) OR ([article_title] IS NULL AND @original_article_title IS NULL)) AND (([article_class] = @original_article_class) OR ([article_class] IS NULL AND @original_article_class IS NULL)) AND (([article_content] = @original_article_content) OR ([article_content] IS NULL AND @original_article_content IS NULL)) AND (([article_date] = @original_article_date) OR ([article_date] IS NULL AND @original_article_date IS NULL)) AND (([article_like] = @original_article_like) OR ([article_like] IS NULL AND @original_article_like IS NULL)) AND (([article_read] = @original_article_read) OR ([article_read] IS NULL AND @original_article_read IS NULL)) AND (([M_account] = @original_M_account) OR ([M_account] IS NULL AND @original_M_account IS NULL))">
                <DeleteParameters>
                    <asp:Parameter Name="original_article_ID" Type="Int32" />
                    <asp:Parameter Name="original_article_title" Type="String" />
                    <asp:Parameter Name="original_article_class" Type="String" />
                    <asp:Parameter Name="original_article_content" Type="String" />
                    <asp:Parameter Name="original_article_date" Type="DateTime" />
                    <asp:Parameter Name="original_article_like" Type="Decimal" />
                    <asp:Parameter Name="original_article_read" Type="Decimal" />
                    <asp:Parameter Name="original_M_account" Type="String" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="article_ID" Type="Int32" />
                    <asp:Parameter Name="article_title" Type="String" />
                    <asp:Parameter Name="article_class" Type="String" />
                    <asp:Parameter Name="article_content" Type="String" />
                    <asp:Parameter Name="article_date" Type="DateTime" />
                    <asp:Parameter Name="article_like" Type="Decimal" />
                    <asp:Parameter Name="article_read" Type="Decimal" />
                    <asp:Parameter Name="M_account" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="article_title" Type="String" />
                    <asp:Parameter Name="article_class" Type="String" />
                    <asp:Parameter Name="article_content" Type="String" />
                    <asp:Parameter Name="article_date" Type="DateTime" />
                    <asp:Parameter Name="article_like" Type="Decimal" />
                    <asp:Parameter Name="article_read" Type="Decimal" />
                    <asp:Parameter Name="M_account" Type="String" />
                    <asp:Parameter Name="original_article_ID" Type="Int32" />
                    <asp:Parameter Name="original_article_title" Type="String" />
                    <asp:Parameter Name="original_article_class" Type="String" />
                    <asp:Parameter Name="original_article_content" Type="String" />
                    <asp:Parameter Name="original_article_date" Type="DateTime" />
                    <asp:Parameter Name="original_article_like" Type="Decimal" />
                    <asp:Parameter Name="original_article_read" Type="Decimal" />
                    <asp:Parameter Name="original_M_account" Type="String" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <br />
        </div>
    </form>
</body>
</html>
