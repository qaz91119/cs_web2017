﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="bmw_作業_第五組_期末_Default2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body style="text-align: center">
    <form id="form1" runat="server">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="article_ID" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="article_ID" HeaderText="article_ID" ReadOnly="True" SortExpression="article_ID" />
                <asp:BoundField DataField="article_title" HeaderText="article_title" SortExpression="article_title" />
                <asp:BoundField DataField="article_class" HeaderText="article_class" SortExpression="article_class" />
                <asp:BoundField DataField="article_content" HeaderText="article_content" SortExpression="article_content" />
                <asp:BoundField DataField="article_date" HeaderText="article_date" SortExpression="article_date" />
                <asp:BoundField DataField="article_like" HeaderText="article_like" SortExpression="article_like" />
                <asp:BoundField DataField="article_read" HeaderText="article_read" SortExpression="article_read" />
                <asp:BoundField DataField="M_account" HeaderText="M_account" SortExpression="M_account" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:fiveConnectionString %>" SelectCommand="SELECT * FROM [article] WHERE ([article_ID] = @article_ID)">
            <SelectParameters>
                <asp:QueryStringParameter Name="article_ID" QueryStringField="article_ID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <br />
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="message_id" DataSourceID="SqlDataSource2">
            <Columns>
                <asp:BoundField DataField="message_id" HeaderText="message_id" ReadOnly="True" SortExpression="message_id" />
                <asp:BoundField DataField="message_content" HeaderText="message_content" SortExpression="message_content" />
                <asp:BoundField DataField="message_date" HeaderText="message_date" SortExpression="message_date" />
                <asp:BoundField DataField="message_like" HeaderText="message_like" SortExpression="message_like" />
                <asp:BoundField DataField="article_ID" HeaderText="article_ID" SortExpression="article_ID" />
                <asp:BoundField DataField="M_account" HeaderText="M_account" SortExpression="M_account" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:fiveConnectionString %>" SelectCommand="SELECT * FROM [messagebox] WHERE ([article_ID] = @article_ID)">
            <SelectParameters>
                <asp:QueryStringParameter Name="article_ID" QueryStringField="article_ID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
    </form>
</body>
</html>
