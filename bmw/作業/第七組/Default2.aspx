﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="作業_第七組_Default2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <br />
            <br />
            <asp:FormView ID="FormView1" runat="server" AllowPaging="True" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="CS_id" DataSourceID="SqlDataSource1" GridLines="Horizontal">
                <EditItemTemplate>
                    CS_id:
                    <asp:Label ID="CS_idLabel1" runat="server" Text='<%# Eval("CS_id") %>' />
                    <br />
                    CS_title:
                    <asp:TextBox ID="CS_titleTextBox" runat="server" Text='<%# Bind("CS_title") %>' />
                    <br />
                    CS_time:
                    <asp:TextBox ID="CS_timeTextBox" runat="server" Text='<%# Bind("CS_time") %>' />
                    <br />
                    CS_summary:
                    <asp:TextBox ID="CS_summaryTextBox" runat="server" Text='<%# Bind("CS_summary") %>' />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="更新" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
                </EditItemTemplate>
                <EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                <FooterTemplate>
                    頁數總攬
                </FooterTemplate>
                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                <HeaderTemplate>
                    每日行程表
                </HeaderTemplate>
                <InsertItemTemplate>
                    CS_title:
                    <asp:TextBox ID="CS_titleTextBox" runat="server" Text='<%# Bind("CS_title") %>' />
                    <br />
                    CS_time:
                    <asp:TextBox ID="CS_timeTextBox" runat="server" Text='<%# Bind("CS_time") %>' />
                    <br />
                    CS_summary:
                    <asp:TextBox ID="CS_summaryTextBox" runat="server" Text='<%# Bind("CS_summary") %>' />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="插入" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
                </InsertItemTemplate>
                <ItemTemplate>
                    編號:
                    <asp:Label ID="CS_idLabel" runat="server" Text='<%# Eval("CS_id") %>' />
                    <br />
                    標題:
                    <asp:Label ID="CS_titleLabel" runat="server" Text='<%# Bind("CS_title") %>' />
                    <br />
                    時間:
                    <asp:Label ID="CS_timeLabel" runat="server" Text='<%# Bind("CS_time") %>' />
                    <br />
                    頁數總覽 <br />
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="編輯" />
                    &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="刪除" />
                    &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="新增" />
                </ItemTemplate>
                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
            </asp:FormView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testConnectionString %>" DeleteCommand="DELETE FROM [Calendar_Schedule] WHERE [CS_id] = @CS_id" InsertCommand="INSERT INTO [Calendar_Schedule] ([CS_title], [CS_time], [CS_summary]) VALUES (@CS_title, @CS_time, @CS_summary)" SelectCommand="SELECT [CS_id], [CS_title], [CS_time], [CS_summary] FROM [Calendar_Schedule]" UpdateCommand="UPDATE [Calendar_Schedule] SET [CS_title] = @CS_title, [CS_time] = @CS_time, [CS_summary] = @CS_summary WHERE [CS_id] = @CS_id">
                <DeleteParameters>
                    <asp:Parameter Name="CS_id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="CS_title" Type="String" />
                    <asp:Parameter Name="CS_time" Type="DateTime" />
                    <asp:Parameter Name="CS_summary" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="CS_title" Type="String" />
                    <asp:Parameter Name="CS_time" Type="DateTime" />
                    <asp:Parameter Name="CS_summary" Type="String" />
                    <asp:Parameter Name="CS_id" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
    </form>
</body>
</html>
