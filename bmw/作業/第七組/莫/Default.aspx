﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="member_id" DataSourceID="SqlDataSource1" OnRowCommand="Gridview1_RowCommand" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
            <Columns>
                <asp:ButtonField CommandName="myinsert" Text="按鈕" />
                <asp:BoundField DataField="member_id" HeaderText="member_id" InsertVisible="False" ReadOnly="True" SortExpression="member_id" />
                <asp:BoundField DataField="account" HeaderText="account" SortExpression="account" />
                <asp:BoundField DataField="passwood" HeaderText="passwood" SortExpression="passwood" />
                <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
                <asp:BoundField DataField="nickname" HeaderText="nickname" SortExpression="nickname" />
                <asp:BoundField DataField="gender" HeaderText="gender" SortExpression="gender" />
            </Columns>
            <EmptyDataTemplate>
                <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="member_id" DataSourceID="SqlDataSource1" DefaultMode="Insert" Height="50px" Width="125px">
                    <Fields>
                        <asp:BoundField DataField="member_id" HeaderText="member_id" InsertVisible="False" ReadOnly="True" SortExpression="member_id" />
                        <asp:BoundField DataField="account" HeaderText="account" SortExpression="account" />
                        <asp:BoundField DataField="passwood" HeaderText="passwood" SortExpression="passwood" />
                        <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
                        <asp:BoundField DataField="nickname" HeaderText="nickname" SortExpression="nickname" />
                        <asp:BoundField DataField="gender" HeaderText="gender" SortExpression="gender" />
                        <asp:CommandField ShowInsertButton="True" />
                    </Fields>
                </asp:DetailsView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:aaaConnectionString %>" DeleteCommand="DELETE FROM [members] WHERE [member_id] = @member_id" InsertCommand="INSERT INTO [members] ([account], [passwood], [email], [nickname], [gender]) VALUES (@account, @passwood, @email, @nickname, @gender)" SelectCommand="SELECT * FROM [members]" UpdateCommand="UPDATE [members] SET [account] = @account, [passwood] = @passwood, [email] = @email, [nickname] = @nickname, [gender] = @gender WHERE [member_id] = @member_id">
                    <DeleteParameters>
                        <asp:Parameter Name="member_id" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="account" Type="String" />
                        <asp:Parameter Name="passwood" Type="String" />
                        <asp:Parameter Name="email" Type="String" />
                        <asp:Parameter Name="nickname" Type="String" />
                        <asp:Parameter Name="gender" Type="String" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="account" Type="String" />
                        <asp:Parameter Name="passwood" Type="String" />
                        <asp:Parameter Name="email" Type="String" />
                        <asp:Parameter Name="nickname" Type="String" />
                        <asp:Parameter Name="gender" Type="String" />
                        <asp:Parameter Name="member_id" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </EmptyDataTemplate>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:aaaConnectionString %>" DeleteCommand="DELETE FROM [members] WHERE [member_id] = @member_id" InsertCommand="INSERT INTO [members] ([account], [passwood], [email], [nickname], [gender]) VALUES (@account, @passwood, @email, @nickname, @gender)" SelectCommand="SELECT * FROM [members]" UpdateCommand="UPDATE [members] SET [account] = @account, [passwood] = @passwood, [email] = @email, [nickname] = @nickname, [gender] = @gender WHERE [member_id] = @member_id">
            <DeleteParameters>
                <asp:Parameter Name="member_id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="account" Type="String" />
                <asp:Parameter Name="passwood" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="nickname" Type="String" />
                <asp:Parameter Name="gender" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="account" Type="String" />
                <asp:Parameter Name="passwood" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="nickname" Type="String" />
                <asp:Parameter Name="gender" Type="String" />
                <asp:Parameter Name="member_id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</body>
</html>
