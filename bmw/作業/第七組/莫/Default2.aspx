﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="Default2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="member_id" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:CommandField ShowEditButton="True" ShowSelectButton="True" />
                <asp:BoundField DataField="member_id" HeaderText="member_id" InsertVisible="False" ReadOnly="True" SortExpression="member_id" />
                <asp:BoundField DataField="account" HeaderText="account" SortExpression="account" />
                <asp:BoundField DataField="passwood" HeaderText="passwood" SortExpression="passwood" />
                <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
                <asp:BoundField DataField="nickname" HeaderText="nickname" SortExpression="nickname" />
                <asp:BoundField DataField="gender" HeaderText="gender" SortExpression="gender" />
            </Columns>
            <SelectedRowStyle BackColor="#6666FF" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:aaaConnectionString %>" DeleteCommand="DELETE FROM [members] WHERE [member_id] = @member_id" InsertCommand="INSERT INTO [members] ([account], [passwood], [email], [nickname], [gender]) VALUES (@account, @passwood, @email, @nickname, @gender)" SelectCommand="SELECT * FROM [members]" UpdateCommand="UPDATE [members] SET [account] = @account, [passwood] = @passwood, [email] = @email, [nickname] = @nickname, [gender] = @gender WHERE [member_id] = @member_id">
            <DeleteParameters>
                <asp:Parameter Name="member_id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="account" Type="String" />
                <asp:Parameter Name="passwood" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="nickname" Type="String" />
                <asp:Parameter Name="gender" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="account" Type="String" />
                <asp:Parameter Name="passwood" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="nickname" Type="String" />
                <asp:Parameter Name="gender" Type="String" />
                <asp:Parameter Name="member_id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="member_id" DataSourceID="SqlDataSource2">
            <Columns>
                <asp:BoundField DataField="member_id" HeaderText="member_id" InsertVisible="False" ReadOnly="True" SortExpression="member_id" />
                <asp:BoundField DataField="account" HeaderText="account" SortExpression="account" />
                <asp:BoundField DataField="passwood" HeaderText="passwood" SortExpression="passwood" />
                <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
                <asp:BoundField DataField="nickname" HeaderText="nickname" SortExpression="nickname" />
                <asp:BoundField DataField="gender" HeaderText="gender" SortExpression="gender" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:aaaConnectionString %>" DeleteCommand="DELETE FROM [members] WHERE [member_id] = @member_id" InsertCommand="INSERT INTO [members] ([account], [passwood], [email], [nickname], [gender]) VALUES (@account, @passwood, @email, @nickname, @gender)" SelectCommand="SELECT * FROM [members] WHERE ([member_id] = @member_id)" UpdateCommand="UPDATE [members] SET [account] = @account, [passwood] = @passwood, [email] = @email, [nickname] = @nickname, [gender] = @gender WHERE [member_id] = @member_id">
            <DeleteParameters>
                <asp:Parameter Name="member_id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="account" Type="String" />
                <asp:Parameter Name="passwood" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="nickname" Type="String" />
                <asp:Parameter Name="gender" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="GridView1" Name="member_id" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="account" Type="String" />
                <asp:Parameter Name="passwood" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="nickname" Type="String" />
                <asp:Parameter Name="gender" Type="String" />
                <asp:Parameter Name="member_id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</body>
</html>
