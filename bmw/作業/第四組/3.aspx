﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="3.aspx.cs" Inherits="bmw_作業_第四組_3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title></title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <strong>多個 Button，共用「同一個事件」</strong> ----&nbsp; <span class="style1"><strong>參數 sender</strong></span>的用法<br />
        <br />
        <br />
        3+2=5<asp:Button ID="Button1" runat="server" CommandArgument="3+2=5" 
            Text="Button1" onclick="Button1_Click" />
        <br />
        <br />
        1+4=5<asp:Button ID="Button2" runat="server" CommandArgument="1+4=5" 
            Text="Button2" onclick="Button1_Click" />
        <br />
        <br /> 
        0+5=5<asp:Button ID="Button3" runat="server" CommandArgument="0+5=5" 
            Text="Button3" onclick="Button1_Click" />
        <br />
        <br />
        兩個按鈕，<strong>共用<span class="style1">同一個 Button1_Click</span>事件</strong></div>
    </form>
</body>
</html>