﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="作業_第四組_末_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="product_Id" DataSourceID="SqlDataSource1">
                <Columns>
                    <asp:CommandField ShowEditButton="True" />
                    <asp:BoundField DataField="product_Id" HeaderText="product_Id" ReadOnly="True" SortExpression="product_Id" />
                    <asp:BoundField DataField="product_categorys" HeaderText="product_categorys" SortExpression="product_categorys" />
                    <asp:BoundField DataField="product_Name" HeaderText="product_Name" SortExpression="product_Name" />
                    <asp:BoundField DataField="product_price" HeaderText="product_price" SortExpression="product_price" />
                    <asp:BoundField DataField="product_stock" HeaderText="product_stock" SortExpression="product_stock" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:fourConnectionString %>" DeleteCommand="DELETE FROM [Products] WHERE [product_Id] = @original_product_Id AND (([product_categorys] = @original_product_categorys) OR ([product_categorys] IS NULL AND @original_product_categorys IS NULL)) AND (([product_Name] = @original_product_Name) OR ([product_Name] IS NULL AND @original_product_Name IS NULL)) AND (([product_price] = @original_product_price) OR ([product_price] IS NULL AND @original_product_price IS NULL)) AND (([product_stock] = @original_product_stock) OR ([product_stock] IS NULL AND @original_product_stock IS NULL))" InsertCommand="INSERT INTO [Products] ([product_Id], [product_categorys], [product_Name], [product_price], [product_stock]) VALUES (@product_Id, @product_categorys, @product_Name, @product_price, @product_stock)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Products]" UpdateCommand="UPDATE [Products] SET [product_categorys] = @product_categorys, [product_Name] = @product_Name, [product_price] = @product_price, [product_stock] = @product_stock WHERE [product_Id] = @original_product_Id AND (([product_categorys] = @original_product_categorys) OR ([product_categorys] IS NULL AND @original_product_categorys IS NULL)) AND (([product_Name] = @original_product_Name) OR ([product_Name] IS NULL AND @original_product_Name IS NULL)) AND (([product_price] = @original_product_price) OR ([product_price] IS NULL AND @original_product_price IS NULL)) AND (([product_stock] = @original_product_stock) OR ([product_stock] IS NULL AND @original_product_stock IS NULL))">
                <DeleteParameters>
                    <asp:Parameter Name="original_product_Id" Type="Int32" />
                    <asp:Parameter Name="original_product_categorys" Type="String" />
                    <asp:Parameter Name="original_product_Name" Type="String" />
                    <asp:Parameter Name="original_product_price" Type="Int32" />
                    <asp:Parameter Name="original_product_stock" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="product_Id" Type="Int32" />
                    <asp:Parameter Name="product_categorys" Type="String" />
                    <asp:Parameter Name="product_Name" Type="String" />
                    <asp:Parameter Name="product_price" Type="Int32" />
                    <asp:Parameter Name="product_stock" Type="Int32" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="product_categorys" Type="String" />
                    <asp:Parameter Name="product_Name" Type="String" />
                    <asp:Parameter Name="product_price" Type="Int32" />
                    <asp:Parameter Name="product_stock" Type="Int32" />
                    <asp:Parameter Name="original_product_Id" Type="Int32" />
                    <asp:Parameter Name="original_product_categorys" Type="String" />
                    <asp:Parameter Name="original_product_Name" Type="String" />
                    <asp:Parameter Name="original_product_price" Type="Int32" />
                    <asp:Parameter Name="original_product_stock" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <br />
            <br />
            <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="product_Id" DataSourceID="SqlDataSource2" Height="50px" Width="125px">
                <Fields>
                    <asp:BoundField DataField="product_Id" HeaderText="product_Id" ReadOnly="True" SortExpression="product_Id" />
                    <asp:BoundField DataField="product_categorys" HeaderText="product_categorys" SortExpression="product_categorys" />
                    <asp:BoundField DataField="product_Name" HeaderText="product_Name" SortExpression="product_Name" />
                    <asp:BoundField DataField="product_price" HeaderText="product_price" SortExpression="product_price" />
                    <asp:BoundField DataField="product_stock" HeaderText="product_stock" SortExpression="product_stock" />
                    <asp:CommandField ShowInsertButton="True" />
                </Fields>
            </asp:DetailsView>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:fourConnectionString %>" DeleteCommand="DELETE FROM [Products] WHERE [product_Id] = @original_product_Id AND (([product_categorys] = @original_product_categorys) OR ([product_categorys] IS NULL AND @original_product_categorys IS NULL)) AND (([product_Name] = @original_product_Name) OR ([product_Name] IS NULL AND @original_product_Name IS NULL)) AND (([product_price] = @original_product_price) OR ([product_price] IS NULL AND @original_product_price IS NULL)) AND (([product_stock] = @original_product_stock) OR ([product_stock] IS NULL AND @original_product_stock IS NULL))" InsertCommand="INSERT INTO [Products] ([product_Id], [product_categorys], [product_Name], [product_price], [product_stock]) VALUES (@product_Id, @product_categorys, @product_Name, @product_price, @product_stock)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Products]" UpdateCommand="UPDATE [Products] SET [product_categorys] = @product_categorys, [product_Name] = @product_Name, [product_price] = @product_price, [product_stock] = @product_stock WHERE [product_Id] = @original_product_Id AND (([product_categorys] = @original_product_categorys) OR ([product_categorys] IS NULL AND @original_product_categorys IS NULL)) AND (([product_Name] = @original_product_Name) OR ([product_Name] IS NULL AND @original_product_Name IS NULL)) AND (([product_price] = @original_product_price) OR ([product_price] IS NULL AND @original_product_price IS NULL)) AND (([product_stock] = @original_product_stock) OR ([product_stock] IS NULL AND @original_product_stock IS NULL))">
                <DeleteParameters>
                    <asp:Parameter Name="original_product_Id" Type="Int32" />
                    <asp:Parameter Name="original_product_categorys" Type="String" />
                    <asp:Parameter Name="original_product_Name" Type="String" />
                    <asp:Parameter Name="original_product_price" Type="Int32" />
                    <asp:Parameter Name="original_product_stock" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="product_Id" Type="Int32" />
                    <asp:Parameter Name="product_categorys" Type="String" />
                    <asp:Parameter Name="product_Name" Type="String" />
                    <asp:Parameter Name="product_price" Type="Int32" />
                    <asp:Parameter Name="product_stock" Type="Int32" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="product_categorys" Type="String" />
                    <asp:Parameter Name="product_Name" Type="String" />
                    <asp:Parameter Name="product_price" Type="Int32" />
                    <asp:Parameter Name="product_stock" Type="Int32" />
                    <asp:Parameter Name="original_product_Id" Type="Int32" />
                    <asp:Parameter Name="original_product_categorys" Type="String" />
                    <asp:Parameter Name="original_product_Name" Type="String" />
                    <asp:Parameter Name="original_product_price" Type="Int32" />
                    <asp:Parameter Name="original_product_stock" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <br />
            <br />
            <asp:DetailsView ID="DetailsView2" runat="server" AllowPaging="True" AutoGenerateRows="False" DataKeyNames="member_Id" DataSourceID="SqlDataSource3" Height="50px" Width="125px">
                <Fields>
                    <asp:BoundField DataField="member_Id" HeaderText="member_Id" ReadOnly="True" SortExpression="member_Id" />
                    <asp:BoundField DataField="member_name" HeaderText="member_name" SortExpression="member_name" />
                    <asp:BoundField DataField="member_birthday" HeaderText="member_birthday" SortExpression="member_birthday" />
                    <asp:BoundField DataField="member_phone" HeaderText="member_phone" SortExpression="member_phone" />
                    <asp:BoundField DataField="member_email" HeaderText="member_email" SortExpression="member_email" />
                    <asp:BoundField DataField="member_address" HeaderText="member_address" SortExpression="member_address" />
                    <asp:BoundField DataField="member_account" HeaderText="member_account" SortExpression="member_account" />
                    <asp:BoundField DataField="member_password" HeaderText="member_password" SortExpression="member_password" />
                    <asp:CommandField ShowEditButton="True" ShowInsertButton="True" />
                </Fields>
            </asp:DetailsView>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:fourConnectionString %>" DeleteCommand="DELETE FROM [Member] WHERE [member_Id] = @member_Id" InsertCommand="INSERT INTO [Member] ([member_Id], [member_name], [member_birthday], [member_phone], [member_email], [member_address], [member_account], [member_password]) VALUES (@member_Id, @member_name, @member_birthday, @member_phone, @member_email, @member_address, @member_account, @member_password)" SelectCommand="SELECT * FROM [Member]" UpdateCommand="UPDATE [Member] SET [member_name] = @member_name, [member_birthday] = @member_birthday, [member_phone] = @member_phone, [member_email] = @member_email, [member_address] = @member_address, [member_account] = @member_account, [member_password] = @member_password WHERE [member_Id] = @member_Id">
                <DeleteParameters>
                    <asp:Parameter Name="member_Id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="member_Id" Type="Int32" />
                    <asp:Parameter Name="member_name" Type="String" />
                    <asp:Parameter Name="member_birthday" Type="DateTime" />
                    <asp:Parameter Name="member_phone" Type="String" />
                    <asp:Parameter Name="member_email" Type="String" />
                    <asp:Parameter Name="member_address" Type="String" />
                    <asp:Parameter Name="member_account" Type="String" />
                    <asp:Parameter Name="member_password" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="member_name" Type="String" />
                    <asp:Parameter Name="member_birthday" Type="DateTime" />
                    <asp:Parameter Name="member_phone" Type="String" />
                    <asp:Parameter Name="member_email" Type="String" />
                    <asp:Parameter Name="member_address" Type="String" />
                    <asp:Parameter Name="member_account" Type="String" />
                    <asp:Parameter Name="member_password" Type="String" />
                    <asp:Parameter Name="member_Id" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
