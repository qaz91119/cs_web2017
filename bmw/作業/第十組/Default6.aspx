﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default6.aspx.cs" Inherits="Default6" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
    使用最基本、最原始的Web控制項，從頭開始打造一個資料查詢的畫面
    <hr />
    
        <br />
        日期：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        （日期格式-- yyyy/MM/dd）<br />
        <br />
        標題：<asp:TextBox ID="TextBox2" runat="server" Width="334px"></asp:TextBox>
        <br />
        <p style="margin:50px"><asp:Button ID="Button1" runat="server" Text="查詢" onclick="Button1_Click" /></p>
    </div>
    
    
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:testConnectionString %>" SelectCommand="SELECT [CS_title], [CS_time], [CS_summary] FROM [Calendar_Schedule]" >
        
    </asp:SqlDataSource>
    
    
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="CS_title" HeaderText="標題" SortExpression="CS_title" />
                <asp:BoundField DataField="CS_time" HeaderText="時間" SortExpression="CS_time" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
    
    
        <div>
    
            <span class="style3">
         <br />
         <br />
            </span></div>   
        </div>
    </form>
</body>
</html>
