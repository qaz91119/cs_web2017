<%@ Page Language="C#" AutoEventWireup="true" CodeFile="4_Manual_Insert.aspx.cs" Inherits="Ch06_4_Manual_Insert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>使用最基本、最原始的Web控制項，從頭開始打造一個資料輸入（新增）的畫面</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    使用最基本、最原始的Web控制項，從頭開始打造一個資料輸入（新增）的畫面
    <hr />
    
        <br />
        日期：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        （日期格式-- yyyy/MM/dd）<br />
        <br />
        標題：<asp:TextBox ID="TextBox2" runat="server" Width="334px"></asp:TextBox>
        <br />
        <br />
    </div>
        <P style="margin:50px"><asp:Button ID="Button1" runat="server" Text="Submit / 新增一筆資料" 
            onclick="Button1_Click" /></P>
    
    
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:testConnectionString %>" 
        
        InsertCommand="INSERT INTO [Calendar_Schedule] ([CS_time], [CS_title], [CS_summary]) VALUES (@CS_time, @CS_title, @CS_summary)" DeleteCommand="DELETE FROM [Calendar_Schedule] WHERE [CS_id] = @CS_id" SelectCommand="SELECT [CS_id], Convert(char(10),CS_time,111) as CS_time, [CS_title], [CS_summary] FROM [Calendar_Schedule]" UpdateCommand="UPDATE [Calendar_Schedule] SET [CS_time] = @CS_time, [CS_title] = @CS_title, [CS_summary] = @CS_summary WHERE [CS_id] = @CS_id" >
        <DeleteParameters>
            <asp:Parameter Name="CS_id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="TextBox1" Name="CS_time" PropertyName="Text" Type="DateTime" />
            <asp:ControlParameter ControlID="TextBox2" Name="CS_title" PropertyName="Text" Type="String" />
            <asp:Parameter Name="CS_summary" DefaultValue="??" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="CS_time" Type="DateTime" />
            <asp:Parameter Name="CS_title" Type="String" />
            <asp:Parameter Name="CS_id" Type="Int32" />
        </UpdateParameters>
        
    </asp:SqlDataSource>
    
    
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="CS_id" DataSourceID="SqlDataSource1" AllowPaging="True" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="CS_id" HeaderText="編號" InsertVisible="False" ReadOnly="True" SortExpression="CS_id" />
                <asp:BoundField DataField="CS_time" HeaderText="時間" SortExpression="CS_time" />
                <asp:BoundField DataField="CS_title" HeaderText="標題" SortExpression="CS_title" />
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
    
    
        <div>
    
            <span class="style3">
         <br />
         <br />
            </span></div>   
    </form>
 
</body>
</html>
