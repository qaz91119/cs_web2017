﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="作業_第十組_末_Default2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:FormView ID="FormView1" runat="server" AllowPaging="True" DataKeyNames="cid" DataSourceID="SqlDataSource1">
                <EditItemTemplate>
                    cid:
                    <asp:Label ID="cidLabel1" runat="server" Text='<%# Eval("cid") %>' />
                    <br />
                    cname:
                    <asp:TextBox ID="cnameTextBox" runat="server" Text='<%# Bind("cname") %>' />
                    <br />
                    cprice:
                    <asp:TextBox ID="cpriceTextBox" runat="server" Text='<%# Bind("cprice") %>' />
                    <br />
                    ccontent:
                    <asp:TextBox ID="ccontentTextBox" runat="server" Text='<%# Bind("ccontent") %>' />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="更新" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
                </EditItemTemplate>
                <InsertItemTemplate>
                    cname:
                    <asp:TextBox ID="cnameTextBox" runat="server" Text='<%# Bind("cname") %>' />
                    <br />
                    cprice:
                    <asp:TextBox ID="cpriceTextBox" runat="server" Text='<%# Bind("cprice") %>' />
                    <br />
                    ccontent:
                    <asp:TextBox ID="ccontentTextBox" runat="server" Text='<%# Bind("ccontent") %>' />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="插入" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
                </InsertItemTemplate>
                <ItemTemplate>
                    cid:
                    <asp:Label ID="cidLabel" runat="server" Text='<%# Eval("cid") %>' />
                    <br />
                    cname:
                    <asp:Label ID="cnameLabel" runat="server" Text='<%# Bind("cname") %>' />
                    <br />
                    cprice:
                    <asp:Label ID="cpriceLabel" runat="server" Text='<%# Bind("cprice") %>' />
                    <br />
                    ccontent:
                    <asp:Label ID="ccontentLabel" runat="server" Text='<%# Bind("ccontent") %>' />
                    <br />
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="編輯" />
                    &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="刪除" />
                    &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="新增" />
                </ItemTemplate>
            </asp:FormView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:tenConnectionString %>" DeleteCommand="DELETE FROM [commodity] WHERE [cid] = @cid" InsertCommand="INSERT INTO [commodity] ([cname], [cprice], [ccontent]) VALUES (@cname, @cprice, @ccontent)" SelectCommand="SELECT * FROM [commodity]" UpdateCommand="UPDATE [commodity] SET [cname] = @cname, [cprice] = @cprice, [ccontent] = @ccontent WHERE [cid] = @cid">
                <DeleteParameters>
                    <asp:Parameter Name="cid" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="cname" Type="String" />
                    <asp:Parameter Name="cprice" Type="String" />
                    <asp:Parameter Name="ccontent" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="cname" Type="String" />
                    <asp:Parameter Name="cprice" Type="String" />
                    <asp:Parameter Name="ccontent" Type="String" />
                    <asp:Parameter Name="cid" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
