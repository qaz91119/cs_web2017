﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="作業_第十組_末_Default" %>

<!DOCTYPE html>



<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            
            <asp:FormView ID="FormView1" runat="server" AllowPaging="True" DataKeyNames="uid" DataSourceID="SqlDataSource1">
                <EditItemTemplate>
                    uname:
                    <asp:TextBox ID="unameTextBox" runat="server" Text='<%# Bind("uname") %>' />
                    <br />
                    upassword:
                    <asp:TextBox ID="upasswordTextBox" runat="server" Text='<%# Bind("upassword") %>' />
                    <br />
                    uid:
                    <asp:Label ID="uidLabel1" runat="server" Text='<%# Eval("uid") %>' />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="更新" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
                </EditItemTemplate>
                <InsertItemTemplate>
                    uname:
                    <asp:TextBox ID="unameTextBox" runat="server" Text='<%# Bind("uname") %>' />
                    <br />
                    upassword:
                    <asp:TextBox ID="upasswordTextBox" runat="server" Text='<%# Bind("upassword") %>' />
                    <br />

                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="插入" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
                </InsertItemTemplate>
                <ItemTemplate>
                    uname:
                    <asp:Label ID="unameLabel" runat="server" Text='<%# Bind("uname") %>' />
                    <br />
                    upassword:
                    <asp:Label ID="upasswordLabel" runat="server" Text='<%# Bind("upassword") %>' />
                    <br />
                    uid:
                    <asp:Label ID="uidLabel" runat="server" Text='<%# Eval("uid") %>' />
                    <br />
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="編輯" />
                    &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="刪除" />
                    &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="新增" />
                </ItemTemplate>
            </asp:FormView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:tenConnectionString %>" DeleteCommand="DELETE FROM [member] WHERE [uid] = @uid" InsertCommand="INSERT INTO [member] ([uname], [upassword]) VALUES (@uname, @upassword)" SelectCommand="SELECT * FROM [member]" UpdateCommand="UPDATE [member] SET [uname] = @uname, [upassword] = @upassword WHERE [uid] = @uid">
                <DeleteParameters>
                    <asp:Parameter Name="uid" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="uname" Type="String" />
                    <asp:Parameter Name="upassword" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="uname" Type="String" />
                    <asp:Parameter Name="upassword" Type="String" />
                    <asp:Parameter Name="uid" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            
        </div>
    </form>
</body>
</html>
