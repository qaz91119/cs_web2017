﻿<%@ Page Language="C#" %>

<!DOCTYPE html>

<script runat="server">

    protected void Button1_Click(object sender, EventArgs e)
    {
        
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="auto-style1">
                <center>
            <asp:ListView ID="ListView1" runat="server" DataKeyNames="post_No" DataSourceID="SqlDataSource1" GroupItemCount="3">
                <AlternatingItemTemplate>
                    <td runat="server" style="">post_No:
                        <asp:Label ID="post_NoLabel" runat="server" Text='<%# Eval("post_No") %>' />
                        <br />
                        member_account:
                        <asp:Label ID="member_accountLabel" runat="server" Text='<%# Eval("member_account") %>' />
                        <br />
                        title:
                        <asp:Label ID="titleLabel" runat="server" Text='<%# Eval("title") %>' />
                        <br />
                        content:
                        <asp:Label ID="contentLabel" runat="server" Text='<%# Eval("content") %>' />
                        <br />
                        likecount:
                        <asp:Label ID="likecountLabel" runat="server" Text='<%# Eval("likecount") %>' />
                        <br />
                    </td>
                </AlternatingItemTemplate>
                <EditItemTemplate>
                    <td runat="server" style="">post_No:
                        <asp:Label ID="post_NoLabel1" runat="server" Text='<%# Eval("post_No") %>' />
                        <br />
                        member_account:
                        <asp:TextBox ID="member_accountTextBox" runat="server" Text='<%# Bind("member_account") %>' />
                        <br />
                        title:
                        <asp:TextBox ID="titleTextBox" runat="server" Text='<%# Bind("title") %>' />
                        <br />
                        content:
                        <asp:TextBox ID="contentTextBox" runat="server" Text='<%# Bind("content") %>' />
                        <br />
                        likecount:
                        <asp:TextBox ID="likecountTextBox" runat="server" Text='<%# Bind("likecount") %>' />
                        <br />
                        <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="更新" />
                        <br />
                        <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="取消" />
                        <br />
                    </td>
                </EditItemTemplate>
                <EmptyDataTemplate>
                    <table runat="server" style="">
                        <tr>
                            <td>未傳回資料。</td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <EmptyItemTemplate>
                    <td runat="server" />
                </EmptyItemTemplate>
                <GroupTemplate>
                    <tr id="itemPlaceholderContainer" runat="server">
                        <td id="itemPlaceholder" runat="server"></td>
                    </tr>
                </GroupTemplate>
                <InsertItemTemplate>
                    <td runat="server" style="">member_account:
                        <asp:TextBox ID="member_accountTextBox" runat="server" Text='<%# Bind("member_account") %>' />
                        <br />
                        title:
                        <asp:TextBox ID="titleTextBox" runat="server" Text='<%# Bind("title") %>' />
                        <br />
                        content:
                        <asp:TextBox ID="contentTextBox" runat="server" Text='<%# Bind("content") %>' />
                        <br />
                        likecount:
                        <asp:TextBox ID="likecountTextBox" runat="server" Text='<%# Bind("likecount") %>' />
                        <br />
                        <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="插入" />
                        <br />
                        <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="清除" />
                        <br />
                    </td>
                </InsertItemTemplate>
                <ItemTemplate>
                    <td runat="server" style="">post_No:
                        <asp:Label ID="post_NoLabel" runat="server" Text='<%# Eval("post_No") %>' />
                        <br />
                        member_account:
                        <asp:Label ID="member_accountLabel" runat="server" Text='<%# Eval("member_account") %>' />
                        <br />
                        title:
                        <asp:Label ID="titleLabel" runat="server" Text='<%# Eval("title") %>' />
                        <br />
                        content:
                        <asp:Label ID="contentLabel" runat="server" Text='<%# Eval("content") %>' />
                        <br />
                        likecount:
                        <asp:Label ID="likecountLabel" runat="server" Text='<%# Eval("likecount") %>' />
                        <br />
                    </td>
                </ItemTemplate>
                <LayoutTemplate>
                    <table runat="server">
                        <tr runat="server">
                            <td runat="server">
                                <table id="groupPlaceholderContainer" runat="server" border="0" style="">
                                    <tr id="groupPlaceholder" runat="server">
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server" style="">
                                <asp:DataPager ID="DataPager1" runat="server" PageSize="12">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" />
                                    </Fields>
                                </asp:DataPager>
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <SelectedItemTemplate>
                    <td runat="server" style="">post_No:
                        <asp:Label ID="post_NoLabel" runat="server" Text='<%# Eval("post_No") %>' />
                        <br />
                        member_account:
                        <asp:Label ID="member_accountLabel" runat="server" Text='<%# Eval("member_account") %>' />
                        <br />
                        title:
                        <asp:Label ID="titleLabel" runat="server" Text='<%# Eval("title") %>' />
                        <br />
                        content:
                        <asp:Label ID="contentLabel" runat="server" Text='<%# Eval("content") %>' />
                        <br />
                        likecount:
                        <asp:Label ID="likecountLabel" runat="server" Text='<%# Eval("likecount") %>' />
                        <br />
                    </td>
                </SelectedItemTemplate>
            </asp:ListView>
            </div>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:nineConnectionString %>" SelectCommand="SELECT * FROM [Post]"></asp:SqlDataSource>
        
            <br />
&nbsp;<asp:Button ID="Button1" runat="server" PostBackUrl="m09-4.aspx" Text="上一頁" />
        </div>
        </center>
    </form>
</body>
</html>
