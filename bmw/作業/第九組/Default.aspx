﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="作業_第九組_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <br />
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1">
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                    <asp:BoundField DataField="city_name" HeaderText="city_name" SortExpression="city_name" />
                    <asp:BoundField DataField="district_name" HeaderText="district_name" SortExpression="district_name" />
                    <asp:BoundField DataField="street_name" HeaderText="street_name" SortExpression="street_name" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testConnectionString %>" DeleteCommand="DELETE FROM [Address_All] WHERE [id] = @id" InsertCommand="INSERT INTO [Address_All] ([city_name], [district_name], [street_name]) VALUES (@city_name, @district_name, @street_name)" SelectCommand="SELECT * FROM [Address_All]" UpdateCommand="UPDATE [Address_All] SET [city_name] = @city_name, [district_name] = @district_name, [street_name] = @street_name WHERE [id] = @id">
                <DeleteParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="city_name" Type="String" />
                    <asp:Parameter Name="district_name" Type="String" />
                    <asp:Parameter Name="street_name" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="city_name" Type="String" />
                    <asp:Parameter Name="district_name" Type="String" />
                    <asp:Parameter Name="street_name" Type="String" />
                    <asp:Parameter Name="id" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <br />
            <br />
            <br />
            <br />
            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource2">
                <Columns>
                    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                    <asp:BoundField DataField="city_name" HeaderText="city_name" SortExpression="city_name" />
                    <asp:BoundField DataField="district_name" HeaderText="district_name" SortExpression="district_name" />
                    <asp:BoundField DataField="street_name" HeaderText="street_name" SortExpression="street_name" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:testConnectionString %>" SelectCommand="SELECT * FROM [Address_All] WHERE ([id] = @id)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="GridView1" Name="id" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
    </form>
</body>
</html>
