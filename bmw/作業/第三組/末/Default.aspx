﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="作業_第三組_末_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="orderID" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4">
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="orderID" HeaderText="訂單編號" ReadOnly="True" SortExpression="orderID" />
                    <asp:BoundField DataField="order_date" HeaderText="訂單時間" SortExpression="order_date" />
                    <asp:BoundField DataField="order_total" HeaderText="訂單金額" SortExpression="order_total" />
                    <asp:BoundField DataField="order_time" HeaderText="訂單時間" SortExpression="order_time" />
                    <asp:BoundField DataField="customerID" HeaderText="客戶編號" SortExpression="customerID" />
                </Columns>
                <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                <RowStyle BackColor="White" ForeColor="#330099" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                <SortedAscendingCellStyle BackColor="#FEFCEB" />
                <SortedAscendingHeaderStyle BackColor="#AF0101" />
                <SortedDescendingCellStyle BackColor="#F6F0C0" />
                <SortedDescendingHeaderStyle BackColor="#7E0000" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:cakeConnectionString %>" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Orders]">
            </asp:SqlDataSource>
            <br />
            <br />
            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="costomerID" DataSourceID="SqlDataSource2" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4">
                <Columns>
                    <asp:BoundField DataField="costomerID" HeaderText="客戶編號" ReadOnly="True" SortExpression="costomerID" />
                    <asp:BoundField DataField="customer_name" HeaderText="客戶姓名" SortExpression="customer_name" />
                    <asp:BoundField DataField="customer_address" HeaderText="客戶地址" SortExpression="customer_address" />
                    <asp:BoundField DataField="customer_phone" HeaderText="客戶電話" SortExpression="customer_phone" />
                    <asp:BoundField DataField="customer_email" HeaderText="客戶信箱" SortExpression="customer_email" />
                </Columns>
                <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                <RowStyle BackColor="White" ForeColor="#330099" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                <SortedAscendingCellStyle BackColor="#FEFCEB" />
                <SortedAscendingHeaderStyle BackColor="#AF0101" />
                <SortedDescendingCellStyle BackColor="#F6F0C0" />
                <SortedDescendingHeaderStyle BackColor="#7E0000" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:cakeConnectionString %>" SelectCommand="SELECT * FROM [Customers] WHERE ([costomerID] = @costomerID)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="GridView1" Name="costomerID" PropertyName="SelectedValue" Type="Decimal" />
                </SelectParameters>
            </asp:SqlDataSource>
            <br />
            <br />
            <asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" AutoGenerateRows="False" DataKeyNames="costomerID" DataSourceID="SqlDataSource3" Height="50px" Width="125px">
                <Fields>
                    <asp:BoundField DataField="costomerID" HeaderText="客戶編號" ReadOnly="True" SortExpression="costomerID" />
                    <asp:BoundField DataField="customer_name" HeaderText="客戶姓名" SortExpression="customer_name" />
                    <asp:BoundField DataField="customer_address" HeaderText="客戶地址" SortExpression="customer_address" />
                    <asp:BoundField DataField="customer_phone" HeaderText="客戶電話" SortExpression="customer_phone" />
                    <asp:BoundField DataField="customer_email" HeaderText="客戶信箱" SortExpression="customer_email" />
                    <asp:CommandField ShowDeleteButton="True" ShowInsertButton="True" />
                </Fields>
            </asp:DetailsView>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:cakeConnectionString %>" DeleteCommand="DELETE FROM [Customers] WHERE [costomerID] = @original_costomerID AND (([customer_name] = @original_customer_name) OR ([customer_name] IS NULL AND @original_customer_name IS NULL)) AND (([customer_address] = @original_customer_address) OR ([customer_address] IS NULL AND @original_customer_address IS NULL)) AND (([customer_phone] = @original_customer_phone) OR ([customer_phone] IS NULL AND @original_customer_phone IS NULL)) AND (([customer_email] = @original_customer_email) OR ([customer_email] IS NULL AND @original_customer_email IS NULL))" InsertCommand="INSERT INTO [Customers] ([costomerID], [customer_name], [customer_address], [customer_phone], [customer_email]) VALUES (@costomerID, @customer_name, @customer_address, @customer_phone, @customer_email)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Customers]" UpdateCommand="UPDATE [Customers] SET [customer_name] = @customer_name, [customer_address] = @customer_address, [customer_phone] = @customer_phone, [customer_email] = @customer_email WHERE [costomerID] = @original_costomerID AND (([customer_name] = @original_customer_name) OR ([customer_name] IS NULL AND @original_customer_name IS NULL)) AND (([customer_address] = @original_customer_address) OR ([customer_address] IS NULL AND @original_customer_address IS NULL)) AND (([customer_phone] = @original_customer_phone) OR ([customer_phone] IS NULL AND @original_customer_phone IS NULL)) AND (([customer_email] = @original_customer_email) OR ([customer_email] IS NULL AND @original_customer_email IS NULL))">
                <DeleteParameters>
                    <asp:Parameter Name="original_costomerID" Type="Decimal" />
                    <asp:Parameter Name="original_customer_name" Type="String" />
                    <asp:Parameter Name="original_customer_address" Type="String" />
                    <asp:Parameter Name="original_customer_phone" Type="String" />
                    <asp:Parameter Name="original_customer_email" Type="String" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="costomerID" Type="Decimal" />
                    <asp:Parameter Name="customer_name" Type="String" />
                    <asp:Parameter Name="customer_address" Type="String" />
                    <asp:Parameter Name="customer_phone" Type="String" />
                    <asp:Parameter Name="customer_email" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="customer_name" Type="String" />
                    <asp:Parameter Name="customer_address" Type="String" />
                    <asp:Parameter Name="customer_phone" Type="String" />
                    <asp:Parameter Name="customer_email" Type="String" />
                    <asp:Parameter Name="original_costomerID" Type="Decimal" />
                    <asp:Parameter Name="original_customer_name" Type="String" />
                    <asp:Parameter Name="original_customer_address" Type="String" />
                    <asp:Parameter Name="original_customer_phone" Type="String" />
                    <asp:Parameter Name="original_customer_email" Type="String" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <br />
            <asp:ListView ID="ListView1" runat="server" DataKeyNames="costomerID" DataSourceID="SqlDataSource4" InsertItemPosition="LastItem">
                <AlternatingItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="刪除" />
                            <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="編輯" />
                        </td>
                        <td>
                            <asp:Label ID="costomerIDLabel" runat="server" Text='<%# Eval("costomerID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="customer_nameLabel" runat="server" Text='<%# Eval("customer_name") %>' />
                        </td>
                        <td>
                            <asp:Label ID="customer_addressLabel" runat="server" Text='<%# Eval("customer_address") %>' />
                        </td>
                        <td>
                            <asp:Label ID="customer_phoneLabel" runat="server" Text='<%# Eval("customer_phone") %>' />
                        </td>
                        <td>
                            <asp:Label ID="customer_emailLabel" runat="server" Text='<%# Eval("customer_email") %>' />
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <EditItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="更新" />
                            <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="取消" />
                        </td>
                        <td>
                            <asp:Label ID="costomerIDLabel1" runat="server" Text='<%# Eval("costomerID") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="customer_nameTextBox" runat="server" Text='<%# Bind("customer_name") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="customer_addressTextBox" runat="server" Text='<%# Bind("customer_address") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="customer_phoneTextBox" runat="server" Text='<%# Bind("customer_phone") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="customer_emailTextBox" runat="server" Text='<%# Bind("customer_email") %>' />
                        </td>
                    </tr>
                </EditItemTemplate>
                <EmptyDataTemplate>
                    <table runat="server" style="">
                        <tr>
                            <td>未傳回資料。</td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <InsertItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="插入" />
                            <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="清除" />
                        </td>
                        <td>
                            <asp:TextBox ID="costomerIDTextBox" runat="server" Text='<%# Bind("costomerID") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="customer_nameTextBox" runat="server" Text='<%# Bind("customer_name") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="customer_addressTextBox" runat="server" Text='<%# Bind("customer_address") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="customer_phoneTextBox" runat="server" Text='<%# Bind("customer_phone") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="customer_emailTextBox" runat="server" Text='<%# Bind("customer_email") %>' />
                        </td>
                    </tr>
                </InsertItemTemplate>
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="刪除" />
                            <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="編輯" />
                        </td>
                        <td>
                            <asp:Label ID="costomerIDLabel" runat="server" Text='<%# Eval("costomerID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="customer_nameLabel" runat="server" Text='<%# Eval("customer_name") %>' />
                        </td>
                        <td>
                            <asp:Label ID="customer_addressLabel" runat="server" Text='<%# Eval("customer_address") %>' />
                        </td>
                        <td>
                            <asp:Label ID="customer_phoneLabel" runat="server" Text='<%# Eval("customer_phone") %>' />
                        </td>
                        <td>
                            <asp:Label ID="customer_emailLabel" runat="server" Text='<%# Eval("customer_email") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table runat="server">
                        <tr runat="server">
                            <td runat="server">
                                <table id="itemPlaceholderContainer" runat="server" border="0" style="">
                                    <tr runat="server" style="">
                                        <th runat="server"></th>
                                        <th runat="server">costomerID</th>
                                        <th runat="server">customer_name</th>
                                        <th runat="server">customer_address</th>
                                        <th runat="server">customer_phone</th>
                                        <th runat="server">customer_email</th>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server" style="">
                                <asp:DataPager ID="DataPager1" runat="server">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" />
                                    </Fields>
                                </asp:DataPager>
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <SelectedItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="刪除" />
                            <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="編輯" />
                        </td>
                        <td>
                            <asp:Label ID="costomerIDLabel" runat="server" Text='<%# Eval("costomerID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="customer_nameLabel" runat="server" Text='<%# Eval("customer_name") %>' />
                        </td>
                        <td>
                            <asp:Label ID="customer_addressLabel" runat="server" Text='<%# Eval("customer_address") %>' />
                        </td>
                        <td>
                            <asp:Label ID="customer_phoneLabel" runat="server" Text='<%# Eval("customer_phone") %>' />
                        </td>
                        <td>
                            <asp:Label ID="customer_emailLabel" runat="server" Text='<%# Eval("customer_email") %>' />
                        </td>
                    </tr>
                </SelectedItemTemplate>
            </asp:ListView>
            <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:cakeConnectionString %>" DeleteCommand="DELETE FROM [Customers] WHERE [costomerID] = @costomerID" InsertCommand="INSERT INTO [Customers] ([costomerID], [customer_name], [customer_address], [customer_phone], [customer_email]) VALUES (@costomerID, @customer_name, @customer_address, @customer_phone, @customer_email)" SelectCommand="SELECT * FROM [Customers]" UpdateCommand="UPDATE [Customers] SET [customer_name] = @customer_name, [customer_address] = @customer_address, [customer_phone] = @customer_phone, [customer_email] = @customer_email WHERE [costomerID] = @costomerID">
                <DeleteParameters>
                    <asp:Parameter Name="costomerID" Type="Decimal" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="costomerID" Type="Decimal" />
                    <asp:Parameter Name="customer_name" Type="String" />
                    <asp:Parameter Name="customer_address" Type="String" />
                    <asp:Parameter Name="customer_phone" Type="String" />
                    <asp:Parameter Name="customer_email" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="customer_name" Type="String" />
                    <asp:Parameter Name="customer_address" Type="String" />
                    <asp:Parameter Name="customer_phone" Type="String" />
                    <asp:Parameter Name="customer_email" Type="String" />
                    <asp:Parameter Name="costomerID" Type="Decimal" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <br />
            <br />
        </div>
    </form>
</body>
</html>
