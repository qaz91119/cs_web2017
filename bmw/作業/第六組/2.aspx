<%@ Page Language="C#" AutoEventWireup="true" CodeFile="2.aspx.cs" Inherits="Ch03_WebControls_CheckBoxList_1" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>點選下面的 CheckBoxList（複選），答對了才能得分！</title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
            font-weight: bold;
            background-color: #FFFF00;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <b>點選下面的 CheckBoxList（複選），</b><span class="style1">答對了才能得分！<br />
        </span><br />
        <br />
        為了趕時間，<asp:Label ID="Label1" runat="server" ></asp:Label>
        &nbsp;
        <asp:CheckBoxList ID="CheckBoxList1" runat="server" >
            <asp:ListItem Value="0">可以超速行駛</asp:ListItem>
            <asp:ListItem Value="0">不必遵守交通規則</asp:ListItem>
            <asp:ListItem Value="25">&lt;b&gt;乃要依規定駕駛&lt;/b&gt;</asp:ListItem>
        </asp:CheckBoxList>
    
    <hr />
        <br />
        號 牌遺失不報請公路主管機關補發，經舉發後奶不辦理而行駛者，處以(複選)<asp:Label ID="Label2" runat="server" ></asp:Label>
        &nbsp;
        <asp:CheckBoxList ID="CheckBoxList2" runat="server">
            <asp:ListItem Value="25">&lt;b&gt;罰鍰&lt;/b&gt;</asp:ListItem>
            <asp:ListItem Value="0">吊銷執照</asp:ListItem>
            <asp:ListItem Value="25">&lt;b&gt;禁止其行駛&lt;/b&gt;</asp:ListItem>
        </asp:CheckBoxList>    
    
    <hr />
        <br />
        防衛駕駛就是<asp:Label ID="Label3" runat="server"></asp:Label>
        &nbsp;
        <asp:CheckBoxList ID="CheckBoxList3" runat="server">
            <asp:ListItem Value="25">&lt;b&gt;能盡力採取和合理慎行為加以防止車禍發生&lt;/b&gt;</asp:ListItem>
            <asp:ListItem Value="3">優良駕駛技術</asp:ListItem>
            <asp:ListItem Value="0">良好生活習慣</asp:ListItem>
        </asp:CheckBoxList>        
    <hr />
    
        <br />
        <asp:Button ID="Button1" runat="server" Text="計算總分" onclick="Button1_Click" />
    
    &nbsp;&nbsp;&nbsp;&nbsp;
        「總分」：<asp:Label ID="Label_summary" runat="server" style="color: #FF0000"></asp:Label>
    
    </div>
    </form> 
</body>
</html>