﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="3.aspx.cs" Inherits="作業_第六組_3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>

</head>

     <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>jQuery UI Datepicker - Default functionality</title>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   <link rel="stylesheet" href="/resources/demos/style.css">
   <script src="https://code.jquery.com/jquery-1.12.4.js"   ></script>
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <script>
       $(function () {
           $("#TextBox6").datepicker();
       });
  </script>

<body>
    <form id="form1" runat="server">
        <div>
            <br />
            姓名 :&nbsp;
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <br />
            生日 :
            <asp:TextBox ID="TextBox6" runat="server" OnTextChanged="TextBox6_TextChanged"></asp:TextBox>
            <br />
            <br />
            檢查日期 :
            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <br />
            <br />
            性別 :<br />
&nbsp;&nbsp;&nbsp;
            <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                <asp:ListItem>男孩</asp:ListItem>
                <asp:ListItem>女孩</asp:ListItem>
            </asp:RadioButtonList>
            <br />
            血型 :
           
            <input id="checkbox" type="checkbox" />A型
            <input id="checkbox" type="checkbox" />B型
            <input id="checkbox" type="checkbox" />O型
            <input id="checkbox" type="checkbox" />AB型
                    
            <br />
            <br />
            身高 :
&nbsp;&nbsp;&nbsp;<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            &nbsp;&nbsp;&nbsp;公分&nbsp;&nbsp;&nbsp; &nbsp; 體重 :&nbsp;
            <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
&nbsp;公斤<br />
            <br />
            腰圍 :
            <br />
           <asp:RadioButton ID="radiobutton" Text="50cm以下" runat="server"  GroupName="cm" />
            <br/>
            <asp:RadioButton ID="radiobutton1" Text="51~60cm" runat="server" GroupName="cm" />
            <br/>
            <asp:RadioButton ID="radiobutton2" Text="61~70cm" runat="server" GroupName="cm" />
            <br/>
            <asp:RadioButton ID="radiobutton3" Text="71~80cm" runat="server"  GroupName="cm"/>
            <br/>
            <asp:RadioButton ID="radiobutton5" Text="80~90cm" runat="server" GroupName="cm" />
            <br/>
            <asp:RadioButton ID="radiobutton4" Text="90cm以上" runat="server"  GroupName="cm"/>
            <br />
            <br />
            是否有以下疾病 :
            <br />
            <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                <asp:ListItem>心臟病</asp:ListItem>
                <asp:ListItem>高血壓</asp:ListItem>
                <asp:ListItem>糖尿病</asp:ListItem>
                <asp:ListItem>蠶豆症</asp:ListItem>
                <asp:ListItem>B型肝炎</asp:ListItem>
                <asp:ListItem>無</asp:ListItem>
            </asp:CheckBoxList>
            <br/>
        </div>
    </form>
</body>
</html>
  
