﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _20171212 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        Response.Write("<br>GridView1.SelectedIndex--" + GridView1.SelectedIndex);
        Response.Write("<br>GridView1.SelectedIndex--" + GridView1.SelectedRow.RowIndex);
        Response.Write("<br>");
        Response.Write("<br>GridView1.SelectedValue(資料庫主索引建)--" + GridView1.SelectedValue);
        Response.Write("<br>GridView1.SelectedDataKey.Value(資料庫主索引建)--" + GridView1.SelectedDataKey.Value);

        //GridView1.EditIndex = GridView1.SelectedIndex;

    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridView1.SelectedIndex = e.NewSelectedIndex;

    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridView1.SelectedIndex = -1;

    }
}