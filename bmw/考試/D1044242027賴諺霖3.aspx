﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="D1044242027賴諺霖3.aspx.cs" Inherits="考試_D1044242027賴諺霖" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>訂單主表 :</h2>
            <br />
            <br />
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="OrderID" DataSourceID="SqlDataSource1">
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="OrderID" HeaderText="訂單編號" InsertVisible="False" ReadOnly="True" SortExpression="OrderID" />
                    <asp:BoundField DataField="CustomerID" HeaderText="客戶編號" SortExpression="CustomerID" />
                    <asp:BoundField DataField="OrderDate" HeaderText="下定日期" SortExpression="OrderDate" />
                    <asp:BoundField DataField="RequiredDate" HeaderText="出貨日期" SortExpression="RequiredDate" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:NorthwindConnectionString %>" SelectCommand="SELECT [OrderID], [CustomerID], [OrderDate], [RequiredDate] FROM [Orders]"></asp:SqlDataSource>
            <br />
            <h2>訂單明細表 :</h2>
            <br />
            <br />
            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="OrderID,ProductID" DataSourceID="SqlDataSource2">
                <Columns>
                    <asp:BoundField DataField="OrderID" HeaderText="訂單編號" ReadOnly="True" SortExpression="OrderID" />
                    <asp:BoundField DataField="ProductID" HeaderText="產品編號" ReadOnly="True" SortExpression="ProductID" />
                    <asp:BoundField DataField="UnitPrice" HeaderText="單價" SortExpression="UnitPrice" />
                    <asp:BoundField DataField="Quantity" HeaderText="訂購數量" SortExpression="Quantity" />
                    <asp:BoundField DataField="Discount" HeaderText="折扣" SortExpression="Discount" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:NorthwindConnectionString %>" SelectCommand="SELECT * FROM [Order Details] WHERE ([OrderID] = @OrderID)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="GridView1" Name="OrderID" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
    </form>
</body>
</html>
