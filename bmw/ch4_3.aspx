﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ch4_3.aspx.cs" Inherits="CS_ch4_3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <p>
            不同按鈕控制項的練習:</p>
        <p>
            <asp:Button ID="Button1" runat="server" Text="連接到yahoo" OnClick="Button1_Click" />
        </p>
        <p>
            <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="http://www.pchome.tw">連接到網路家庭</asp:LinkButton>
        </p>
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/CS_03_04_05/企鵝老師.gif" />
    </form>
</body>
</html>
