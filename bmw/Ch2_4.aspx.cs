﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Ch2_4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string s = TextBox1.Text; //從文字方塊 參考Ch2_3
        string[] spChars = { "!", "@", "#", "%" };
        for (int i = 0; i < spChars.Length; i++) 
        {
            if (s.IndexOf(spChars[i]) >= 0)
            {                                  
                Response.Write("字串" + s + "中有" + spChars[i] + "符號<br/>");
                Response.End(); 
                
            }
        }
    }
}