﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Book_Sample_Ch10_GridView_Update_3_DetailsView_Add : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        //**** .FindControl()方法 ****
        TextBox TB = (TextBox)DetailsView1.FindControl("TextBox1");

        Calendar CA = (Calendar)DetailsView1.FindControl("Calendar1");


        //**** 以下就是上集「第三章」Calendar的第一個範例的作法。
        TB.Text = CA.SelectedDate.ToShortDateString();
    }
}