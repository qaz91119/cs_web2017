﻿
Partial Class VS2008_Book_Sample_test_GridView_GridView_Delete_MultiRow_5_Page
    Inherits System.Web.UI.Page

    Dim non_Record As String = "-1"

    '================================================(start)
    '== 以重構的手法，將程式裡面重複使用的 Session變成一個 Property
    '================================================
    '== 優點：避免 Session的變數寫錯字而不自知。
    '== 以後要更換成 Cookie或是 ViewState也容易

    Public Property WantToDelete_ID() As String
        Get
            Return Session("delete_ID").ToString()
        End Get
        Set(ByVal value As String)
            Session("delete_ID") = value
        End Set
    End Property
    '=================================================(end)



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            '-- 第一次執行，清空所有要刪除的ID值。
            WantToDelete_ID = non_Record   '-- Session 如果沒有預設值的話，會出現錯誤。
        End If
    End Sub


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Checkbox_Process()
        '== 重複的、大量的程式，就抽離出去，獨自成為一個函數、副程式。
    End Sub


    '== GridView 分頁
    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        '== 在此不用作 DataBinding。
        '      因為HTML畫面裡面， GridView已經有設定 DataSourceID。
    End Sub


    '**** 請看上集，第十一章 *******************************************
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        '-- 參考資料： [習題]GridView樣版內部，改用CheckBox/Radio/DropDownList（單/複選）控制項，取代TextBox
        '-- 請看我的BLOG與習題 -- http://www.dotblogs.com.tw/mis2000lab/archive/2008/12/26/gridview_template_radiobuttonlist_1225.aspx

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim myCheckbox As CheckBox = e.Row.FindControl("CheckBox1")
            Dim myID As Label = e.Row.FindControl("Label1")
            Dim myID_no_str2 As String = "A" & myID.Text & "，"  '--幫ID編號加上一個A字首

            If InStr(1, WantToDelete_ID, myID_no_str2) > 0 Then
                '-- 檢查一下，如果文章編號已經記錄在 Session裡面了，那麼 CheckBox就要被勾選。
                myCheckbox.Checked = True
            Else
                myCheckbox.Checked = False
            End If
        End If

    End Sub



    '******** 自訂的副程式與函數 **********************************************
    Sub Checkbox_Process()
        For i As Integer = 0 To (GridView1.Rows.Count - 1)
            Dim myCheckbox As CheckBox = GridView1.Rows(i).FindControl("CheckBox1")
            Dim myID As Label = GridView1.Rows(i).FindControl("Label1")
            Dim myID_no_str1 As String = "A" & myID.Text  '--幫ID編號加上一個A字首

            If myCheckbox.Checked = True Then
                '====================
                '==  被點選的某一筆記錄。 ==
                '====================
                WantToDelete_ID_Add(myID_no_str1)
                '== 加入「您想刪除的ID字串」裡面。
            Else
                '=====================================================
                '== 「沒有」被點選的某一筆記錄。 必須從 Session裡面刪除（以空字串代替）==
                '=====================================================

                WantToDelete_ID_Remove(myID_no_str1)
                '== 從「您想刪除的ID字串」裡面，移除這筆ID。
            End If
        Next

        If WantToDelete_ID = non_Record Then
            Label2.Text = "您尚未點選任何一筆資料（沒有刪除任何一筆）"
        Else
            Label2.Text = Replace(WantToDelete_ID, "A", "").ToString()
            '== 您可以使用這些文章的ID來進行SQL指令「刪除」的動作 ==
        End If
    End Sub


    '== 加入「您想刪除的ID字串」裡面。
    Sub WantToDelete_ID_Add(ByVal myID_no_str As String)

        If WantToDelete_ID = non_Record Then
            '-- 使用者點選某一筆記錄後，原本的預設值 delete_ID = "-1" 就要取消。
            WantToDelete_ID = ""
        End If

        If InStr(1, WantToDelete_ID, myID_no_str) = 0 Then
            '-- 檢查一下，新增的文章編號，才加入。
            '-- 如果相同的文章編號已經記錄在 Session()了，就不要重複記憶！
            WantToDelete_ID = WantToDelete_ID & myID_no_str & "，"
        End If
    End Sub


    '== 從「您想刪除的ID字串」裡面，移除這筆ID。
    Sub WantToDelete_ID_Remove(ByVal myID_no_str As String)

        If WantToDelete_ID <> non_Record Then   '--已經有資料在內
            If InStr(1, WantToDelete_ID, myID_no_str) > 0 Then
                Dim replace_str As String = myID_no_str & "，"
                WantToDelete_ID = Replace(WantToDelete_ID, replace_str, "")
            End If
        End If

    End Sub

End Class
