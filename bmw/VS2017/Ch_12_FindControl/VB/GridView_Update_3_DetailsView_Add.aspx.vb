﻿
Partial Class Book_Sample_Ch10_Program_GridView_Update_3_DetailsView_Add
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        '****** 請注意！Page_Load事件後面有「Handles」關鍵字！！******
    End Sub


    '****** 重點！！   VB的作法，必須在「新增樣版」裡面，設定Calendar的閃電圖示，才能設定事件！！
    Protected Sub Calendar1_SelectionChanged(sender As Object, e As EventArgs)
        '**** .FindControl()方法 ****
        Dim TB As TextBox = DetailsView1.FindControl("TextBox1")

        Dim CA As Calendar = DetailsView1.FindControl("Calendar1")


        '**** 以下就是上集「第三章」Calendar的第一個範例的作法。
        TB.Text = CA.SelectedDate.ToShortDateString()
    End Sub





End Class
