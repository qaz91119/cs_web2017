﻿
Partial Class Book_Sample_Ch10_Program_GridView_Delete_MultiRow_ALL_MSDN_JavaScript
    Inherits System.Web.UI.Page

    Private Sub GridView1_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowCreated
        ' 把放在 **表頭樣板（HeaderTemplate）** 裡面的CheckBox控制項，加入這段JavaScript。
        ' 事先在畫面上寫好這段 JavaScript程式，function名為 ConnectGridSelectAll。

        If (e.Row.RowType = DataControlRowType.Header) Then
            Dim CB As CheckBox = e.Row.FindControl("CheckBox2_Header")
            CB.Attributes.Add("onclick", "ConnectGridSelectAll('GridView1');")
            ' 加入HTML標籤的屬性或是JavaScript時，請用.Attributes.Add()方法。
            ' GridView1這個ID名稱，請您依照您畫面上的GridView做必要修改。
        End If
    End Sub

    '*** 使用這兩個事件都可以做出相同效果。******************************************

    Private Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        '' 把放在 **表頭樣板（HeaderTemplate）** 裡面的CheckBox控制項，加入這段JavaScript。
        '' 事先在畫面上寫好這段 JavaScript程式，function名為 ConnectGridSelectAll。

        'If (e.Row.RowType = DataControlRowType.Header) Then
        '    Dim CB As CheckBox = e.Row.FindControl("CheckBox2_Header")
        '    CB.Attributes.Add("onclick", "ConnectGridSelectAll('GridView1');")
        '    ' 加入HTML標籤的屬性或是JavaScript時，請用.Attributes.Add()方法。
        '    ' GridView1這個ID名稱，請您依照您畫面上的GridView做必要修改。
        'End If
    End Sub

End Class
