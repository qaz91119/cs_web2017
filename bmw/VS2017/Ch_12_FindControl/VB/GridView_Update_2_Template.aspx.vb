﻿
Partial Class VS2010_Book_Sample_test_GridView_GridView_Update_2_Template
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridView1.RowUpdating

        '==抓取「編輯」模式裡面，使用者修改後的欄位值。
        Dim lb As New Label
        lb = CType(GridView1.Rows(e.RowIndex).FindControl("Label1"), Label)
        Response.Write("id -- " & lb.Text)

        Response.Write("<br> 對應 test資料表的主索引鍵 --" & GridView1.DataKeys(e.RowIndex).Value.ToString())

        Dim tb As New TextBox
        '-- ***************************************        
        '-- 重點來了!! 以下兩行程式碼跟上一個範例不同!!
        '-- ***************************************
        '-- 以下兩種寫法，請您任選其一
        
        tb = CType(GridView1.Rows(e.RowIndex).FindControl("TextBox1"), TextBox)
        '-- tb = CType(GridView1.Rows(e.RowIndex).Cells(2).FindControl("TextBox1"), TextBox)
        
        Response.Write("<br> title -- " & tb.Text)

        Response.End()  '==因為本範例的 GridView搭配 SqlDataSource，不寫這一行會出現錯誤。
    End Sub

End Class
