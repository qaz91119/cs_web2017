﻿
Partial Class VS2008_Book_Sample_test_GridView_GridView_Delete_MultiRow_4_Page
    Inherits System.Web.UI.Page

    Dim non_Record As String = "-1"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Session("delete_ID") = non_Record   '-- Session()如果沒有預設值的話，會出現錯誤。
        End If
    End Sub


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Checkbox_Process()
    End Sub


    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex

        '== 在此不用作 DataBinding。
        '      因為HTML畫面裡面， GridView已經有設定 DataSourceID。
    End Sub




    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        '-- 參考資料： [習題]GridView樣版內部，改用CheckBox/Radio/DropDownList（單/複選）控制項，取代TextBox
        '-- 請看我的BLOG與習題 -- http://www.dotblogs.com.tw/mis2000lab/archive/2008/12/26/gridview_template_radiobuttonlist_1225.aspx

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim myCheckbox As CheckBox = e.Row.FindControl("CheckBox1")
            Dim myID As Label = e.Row.FindControl("Label1")
            Dim myID_no_str As String = "A" & myID.Text & "，"  '--幫ID編號加上一個A字首

            If InStr(1, Session("delete_ID"), myID_no_str) > 0 Then
                '-- 檢查一下，如果文章編號已經記錄在 Session裡面了，那麼 CheckBox就要被勾選。
                myCheckbox.Checked = True
            Else
                myCheckbox.Checked = False
            End If
        End If

    End Sub


    Sub Checkbox_Process()
        For i As Integer = 0 To (GridView1.Rows.Count - 1)
            Dim myCheckbox As CheckBox = GridView1.Rows(i).FindControl("CheckBox1")
            Dim myID As Label = GridView1.Rows(i).FindControl("Label1")
            Dim myID_no_str As String = "A" & myID.Text  '--幫ID編號加上一個A字首

            If myCheckbox.Checked = True Then
                '====================
                '==  被點選的某一筆資料。 ==
                '====================
                If Session("delete_ID") = non_Record Then
                    '-- 使用者點選某一筆資料後，原本的預設值 Session("delete_ID") = "-1" 就要取消。
                    Session("delete_ID") = ""
                End If
                If InStr(1, Session("delete_ID"), myID_no_str) = 0 Then
                    '-- 檢查一下，新增的文章編號，才加入。
                    '-- 如果相同的文章編號已經記錄在 Session()了，就不要重複記憶！
                    Session("delete_ID") = Session("delete_ID") & myID_no_str & "，"
                End If

            Else

                '=====================================================
                '== 「沒有」被點選的某一筆資料。 必須從 Session()裡面刪除（以空字串代替）==
                '=====================================================
                If Session("delete_ID") <> non_Record Then   '--已經有資料在內
                    If InStr(1, Session("delete_ID"), myID_no_str) > 0 Then
                        Dim replace_str As String = myID_no_str & "，"
                        Session("delete_ID") = Replace(Session("delete_ID"), replace_str, "")
                    End If
                End If

            End If
        Next

        If Session("delete_ID") = non_Record Then
            Label2.Text = "您尚未點選任何一筆資料（沒有刪除任何一筆）"
        Else
            Label2.Text = Replace(Session("delete_ID"), "A", "").ToString()
            '== 您可以使用這些文章的ID來進行SQL指令「刪除」的動作 ==
        End If
    End Sub
End Class
