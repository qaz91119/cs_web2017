﻿
Partial Class VS2010_Book_Sample_Ch10_Program_GridView_Update_2_Easy
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Dim lb As Label = CType(GridView1.Rows(0).FindControl("Label1"), Label)
            '-- 程式第一次執行（Page_Load事件），抓取第一列(Rows(0))紀錄 title欄位的值。
            '-- VB語法可簡寫為 Dim lb As Label = GridView1.Rows(0).FindControl("Label1")

            Response.Write(lb.Text)
        End If
    End Sub


    Protected Sub GridView1_SelectedIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GridView1.SelectedIndexChanging
        Dim lb As Label = CType(GridView1.Rows(e.NewSelectedIndex).FindControl("Label1"), Label)
        Response.Write(lb.Text)
    End Sub

End Class
