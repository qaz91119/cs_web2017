﻿
Partial Class Book_Sample_Ch10_Program_GridView_Update_0_Concept_2
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        Response.Write("<font color=blue><h3>第一個GridView</h3>")

        For i As Integer = 0 To 2
            If GridView1.Rows(1).Cells(i).HasControls() Then
                Response.Write("第二列的" & i & "個格子(Cells)有" & GridView1.Rows(1).Cells(i).Controls.Count & "個子控制項(Controls)<br />")

                Dim lb As LiteralControl = GridView2.Rows(1).Cells(2).Controls(0)
                Response.Write(".....第二列的第三個格子(Cells)有" & lb.Text & "<br />")
                '--任何不在伺服器上處理的 HTML 標記或文字字串，都會被視為 LiteralControl 物件。
                '--參考網址  http://msdn.microsoft.com/zh-tw/library/system.web.ui.control.controls%28v=vs.80%29
            Else
                Response.Write("<br /><br />第二列的" & i & "個格子(Cells) is No Controls")
            End If
        Next

        Response.Write("<br><br>====GridView1的第二列有幾個子控制項？==GridView1.Rows(1).Controls.Count====" & GridView1.Rows(1).Controls.Count & "====<br /><br />")
        Response.Write("</font>")


        '======================================================
        Label3.Text = "<h3>第二個GridView（轉成樣版）</h3>"

        For j As Integer = 0 To 2
            If GridView2.Rows(1).Cells(j).HasControls() Then

                Label3.Text &= "<hr />第二列的" & j & "個格子(Cells)有" & GridView2.Rows(1).Cells(j).Controls.Count & "個子控制項(Controls)<br />"
            Else
                Label3.Text &= "第二列的" & j & "個格子(Cells) is No Controls<br /><br />"
            End If
        Next

        Dim lb0 As LiteralControl = GridView2.Rows(1).Cells(2).Controls(0)
        Label3.Text &= "<br />.....第二列的第三個格子(Cells)，.Controls[0]集合有" & lb0.Text & "<br />"
        '--任何不在伺服器上處理的 HTML 標記或文字字串，都會被視為 LiteralControl 物件。
        '--參考網址  http://msdn.microsoft.com/zh-tw/library/system.web.ui.control.controls%28v=vs.80%29

        Dim lb1 As LiteralControl = GridView2.Rows(1).Cells(2).Controls(1)
        Label3.Text &= "<br />.....第二列的第三個格子(Cells)，.Controls[1]集合有" & lb1.Text & "<br />"

        Dim lb2 As LiteralControl = GridView2.Rows(1).Cells(2).Controls(2)
        Label3.Text &= "<br />.....第二列的第三個格子(Cells)，.Controls[2]集合有" & lb2.Text & "<br />"

        '== 本範例來說，子控制項最多只有三個。超過三個就會報錯！
        'Dim lb3 As LiteralControl = GridView2.Rows(1).Cells(2).Controls(3)
        'Label3.Text &= "<br />.....第二列的第三個格子(Cells)，.Controls[3]集合有" & lb3.Text & "<br />"


        Label3.Text &= "<br><br>====GridView2的第二列有幾個子控制項？==GridView2.Rows(1).Controls.Count====" & GridView2.Rows(1).Controls.Count & "====<br /><br />"


        Dim lbFD As Label = GridView2.Rows(1).Cells(2).FindControl("Label2")
        Label3.Text &= "<br> .FindControl()方法 --" & lbFD.Text
    End Sub
End Class
