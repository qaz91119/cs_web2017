﻿<%@ Page Language="VB" Debug="true" AutoEventWireup="false" CodeFile="GridView_Update_1.aspx.vb" Inherits="VS2010_Book_Sample_test_GridView_GridView_Update_1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            background-color: #FFCCCC;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <strong>.Controls(index值)集合，稍有難度！</strong><br />
        <br />
        <br />
        自己撰寫程式之前，先練習一下能否抓到 GridView<span class="style1">「編輯模式」底下的 &quot;值&quot;？<br />
        </span>

        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" AutoGenerateEditButton="True" 
            DataKeyNames="id" DataSourceID="SqlDataSource1" PageSize="5">
            <Columns>
                <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" 
                    ReadOnly="True" SortExpression="id" />
                <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testConnectionString %>" 
            SelectCommand="SELECT [id], [title] FROM [test]"></asp:SqlDataSource>
    
        <br />
        請先設定好 GridView的<b>「DataKeyNames」屬性</b></div>
    </form>
</body>
</html>
