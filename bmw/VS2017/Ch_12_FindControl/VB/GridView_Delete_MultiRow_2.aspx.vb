﻿
Partial Class VS2008_Book_Sample_test_GridView_GridView_Delete_MultiRow_2
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Session("Page") = "0"   '-- 第一次執行頁面，一定在第一頁。
            Session("delete_ID") = "-1"   '-- Session()如果沒有預設值的話，會出現錯誤。
        End If
    End Sub


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        For i As Integer = 0 To (GridView1.Rows.Count - 1)
            Dim myCheckbox As CheckBox = GridView1.Rows(i).FindControl("CheckBox1")
            Dim myID As Label = GridView1.Rows(i).FindControl("Label1")

            If myCheckbox.Checked = True Then
                '==  被點選的某一筆資料。 ==
                '====================

                If Session("delete_ID").ToString() = "-1" Then
                    '-- 使用者點選某一筆資料後，原本的預設值 Session("delete_ID") = "-1" 就要取消。
                    Session("delete_ID") = ""
                End If
                If InStr(1, Session("delete_ID").ToString(), myID.Text) = 0 Then
                    '-- 檢查一下，如果相同的文章編號已經記錄在 Session()了，就不要重複記憶！
                    Session("delete_ID") = Session("delete_ID").ToString() & myID.Text & "，"
                End If
            Else
                '== 「沒有」被點選的某一筆資料。 必須從 Session()裡面刪除（以空字串代替）==
                '======================================================

                If Session("delete_ID").ToString() <> "-1" Then
                    If InStr(1, Session("delete_ID"), myID.Text) > 0 Then
                        Dim replace_str As String = myID.Text & "，"
                        Session("delete_ID") = Replace(Session("delete_ID"), replace_str, "")
                    End If
                End If
            End If
        Next

        If Session("delete_ID") = "-1" Then
            Label2.Text = "您尚未點選任何一筆資料（沒有刪除任何一筆）"
        Else
            Label2.Text = Session("delete_ID").ToString()
        End If
    End Sub


End Class
