﻿
Partial Class VS2010_Book_Sample_test_GridView_GridView_Update_1
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridView1.RowUpdating

        '==抓取「編輯」模式裡面，使用者修改後的欄位值。

        Response.Write("對應 test資料表的主索引鍵 --" & GridView1.DataKeys(e.RowIndex).Value.ToString())

        '========================================
        '== 如果您把 id欄位的HTML標籤，取消這一段 ReadOnly="True" 
        '== 便可以使用下面的程式碼了。
        'Dim tb1 As New TextBox
        'tb1 = CType(GridView1.Rows(e.RowIndex).Cells(1).Controls(0), TextBox)
        'Response.Write("<br> id -- " & tb1.Text)
        '========================================

        If GridView1.Rows(e.RowIndex).Cells(2).HasControls() Then  '==先判斷這個格子裡面，是否有「子控制項」？
            Dim tb As TextBox = CType(GridView1.Rows(e.RowIndex).Cells(2).Controls(0), TextBox)
            Response.Write("<br> title -- " & tb.Text)
        End If

        Response.End()  '==因為本範例的 GridView搭配 SqlDataSource，不寫這一行會出現錯誤。
    End Sub
End Class
