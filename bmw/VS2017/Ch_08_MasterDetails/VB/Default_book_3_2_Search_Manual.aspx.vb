﻿
Partial Class VS2010_Book_Sample_Default_book_3_2_Search_Manual
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '==按下搜尋的 Button按鈕之後，才讓 GridView1做 DataBinding 並顯示資料

        GridView1.DataSourceID = "SqlDataSource1"
    End Sub


    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        '==按下GridView1的「Select」的按鈕之後，才讓 GridView2做 DataBinding 並顯示資料

        GridView2.DataSourceID = "SqlDataSource2"
    End Sub


End Class
