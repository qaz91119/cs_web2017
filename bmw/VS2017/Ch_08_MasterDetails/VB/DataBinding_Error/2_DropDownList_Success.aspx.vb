﻿
Partial Class Book_Sample_Ch09_Program_DataBinding_Error_2_DropDownList_Success
    Inherits System.Web.UI.Page

    Protected Sub SqlDataSource2_Updated(sender As Object, e As SqlDataSourceStatusEventArgs) Handles SqlDataSource2.Updated
        DropDownList1.DataSourceID = "SqlDataSource1"
        ' 也可以寫成 DropDownList1.DataBind()

        ' 或是寫成
        ' DropDownList1.DataSource = SqlDataSource1   ' 注意！這裡的 SqlDataSource 沒有「雙引號」
        ' DropDownList1.DataBind()
    End Sub
End Class
