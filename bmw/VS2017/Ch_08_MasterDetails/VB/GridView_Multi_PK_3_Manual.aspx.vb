﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----


Partial Class test_GridView_GridView_Multi_PK_3
    Inherits System.Web.UI.Page

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        '====自己手寫的程式碼， DataAdapter / DataSet ====(Start)
        '----上面已經事先寫好 Imports System.Web.Configuration ----
        '----連結資料庫
        Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)

        Dim myAdapter As SqlDataAdapter
        myAdapter = New SqlDataAdapter("select * from test where id = " & GridView1.SelectedDataKey.Values(0) & " and title = '" & GridView1.SelectedDataKey.Values(1) & "'", Conn)

        Dim ds As New DataSet

        Try  '==== 以下程式，只放「執行期間」的指令！=================
            'Conn.Open()   '---- 不用寫，DataAdapter會自動開啟

            myAdapter.Fill(ds, "test")    '---- 這時候執行SQL指令。取出資料，放進 DataSet。

            GridView2.DataSource = ds     '----標準寫法 GridView2.DataSource = ds.Tables("test").DefaultView ----
            GridView2.DataBind()

        Catch ex As Exception
            Response.Write("<HR/> Exception Error Message----  " + ex.ToString())
        Finally
            '---- 不用寫，DataAdapter會自動關閉
            'If (Conn.State = ConnectionState.Open) Then
            '   Conn.Close()
            '   Conn.Dispose()
            'End If
        End Try
    End Sub
End Class
