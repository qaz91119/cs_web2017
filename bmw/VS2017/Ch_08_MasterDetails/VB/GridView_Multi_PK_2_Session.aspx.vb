﻿
Partial Class test_GridView_GridView_Multi_PK_2_Session
    Inherits System.Web.UI.Page

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        Session("s_id") = CInt(GridView1.SelectedDataKey.Values(0))    '--第一個索引鍵（整數型態）
        Session("s_title") = GridView1.SelectedDataKey.Values(1).ToString()    '--第二個索引鍵（字串型態）

        '-- 請比對一下 HTML碼裡面， SqlDataSource2的兩個參數
        '<SelectParameters>
        '    <asp:SessionParameter Name="id" SessionField="s_id" Type="Int32" />   註：整數型態
        '    <asp:SessionParameter Name="title" SessionField="s_title" Type="String" />  註：字串
        '</SelectParameters>

    End Sub


End Class
