﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="3_Button_Command_CodeBehind.aspx.vb" Inherits="VS2010_Book_Sample_Ch03_Program__Book_WebControls_3_Button_Command_CodeBehind" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Button控制項，CommandName屬性的範例（Code Behind）</title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
            background-color: #FFCC99;
        }
    </style>
</head>
<body>
   <form id="form2" runat="server">
  <h3>按下不同的按鈕，會出現不同的狀態<span class="style1">（Code Behind）</span></h3>
  <br /><br />
  <asp:Button id="Button1" runat="server"
   Text="排序（由大到小）"
   CommandName="Sort" CommandArgument="Ascending" />

      &nbsp;

  <asp:Button id="Button2" runat="server"
   Text="反排序（由小到大）"
   CommandName="Sort" CommandArgument="Descending" />
       <!-- 註解： 本範例的兩個按鈕，執行同一支 CommandBtn_Click() 這支程式 -->
      <br /><br />
  <asp:Label id="Message" runat="server"/>
   </form>
</body>
</html>
