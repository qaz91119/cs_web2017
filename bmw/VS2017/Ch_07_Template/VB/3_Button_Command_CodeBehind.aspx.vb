﻿
Partial Class VS2010_Book_Sample_Ch03_Program__Book_WebControls_3_Button_Command_CodeBehind
    Inherits System.Web.UI.Page

    Protected Sub Button1_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) _
        Handles Button1.Command, Button2.Command
        '==重點在此！！！^^^^^^^^^^^^^^^^^^^^^

        Select Case e.CommandName
            Case "Sort"
                ' –註解： CType(A, B) 把A 轉成B型態
                Sort_List(CType(e.CommandArgument, String))
            Case Else
                Message.Text = "你按下哪個按鈕？我不認得～."
        End Select
    End Sub


    Sub Sort_List(ByVal CommandArgument As String)
        Select Case CommandArgument
            Case "Ascending"
                Message.Text = "你按下「排序」的按鈕！CommandArgument為 Ascending "
            Case "Descending"
                Message.Text = "你按下「反排序」的按鈕！ CommandArgument為 Descending "
        End Select
    End Sub
End Class
