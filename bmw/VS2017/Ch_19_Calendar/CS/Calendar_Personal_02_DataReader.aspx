﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Calendar_Personal_02_DataReader.aspx.cs" Inherits="_Book_New_Samples_Calendar_Calendar_Personal_02_DataReader" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>個人行事曆（修正版，DataReader）針對SQL指令改善。</title>
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            background-color: #CCFFCC;
        }
        .style2
        {
            font-weight: bold;
            color: #FFFFFF;
            background-color: #009900;
        }
        .style3
        {
            color: #009900;
        }
        .style4
        {
            color: #FF0000;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            個人行事曆（<span class="style2">修正版，DataReader</span>）針對SQL指令改善。<br />
            <br />
            <span class="style4">SQL指令的部分，仍不夠好！</span><br class="style3" />
            <span class="style3">&quot;select * from Calendar_Schedule where cs_time &gt;= &#39;&quot; + 
        Session[&quot;myMonth_start&quot;] + &quot;&#39; and cs_time &lt; &#39;&quot; + Session[&quot;myMonth_end&quot;] + &quot;&#39;&quot;</span><br />
            <br />
            （建議把日期移至 2011/10~ 2012/1月之間，才有記錄。）<br />
            （需搭配資料庫 <span class="style1">Calendar_Schedule資料表</span>、ADO.NET程式）<br />
            <br />
            <br />
            <asp:Calendar ID="Calendar1" runat="server" BackColor="White"
                BorderColor="#3366CC" BorderWidth="1px" Caption="MIS2000 Lab.個人行事曆"
                CellPadding="5" CellSpacing="5" DayNameFormat="Shortest" Font-Names="Verdana"
                Font-Size="8pt" ForeColor="#003399" Height="280px" Width="350px" 
                OnDayRender="Calendar1_DayRender" OnDisposed="Calendar1_Disposed" OnInit="Calendar1_Init" OnVisibleMonthChanged="Calendar1_VisibleMonthChanged">
                <DayHeaderStyle BackColor="#99FF99" ForeColor="#336666" Height="1px" />
                <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <SelectedDayStyle BackColor="#66FF66" Font-Bold="True" ForeColor="#009900" />
                <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                <TitleStyle BackColor="#009900" BorderColor="#3366CC" BorderWidth="1px"
                    Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                <TodayDayStyle BackColor="#FF99CC" ForeColor="White" />
                <WeekendDayStyle BackColor="#FFFF99" />
            </asp:Calendar>

            <p>
                <strong>使用 Calendar的「Init」事件、「Dispose」事件、「VisibleMonthChanged」事件（當您變動月份時，才會重新整理DB資料）</strong>
            </p>

            <asp:Label ID="Label1" runat="server" Style="font-size: x-small"></asp:Label>
        </div>
    </form>
</body>
</html>
