﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Calendar_06_DayRender.aspx.cs" Inherits="_Book_New_Samples_Calendar_Calendar_06_DayRender" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .style1
        {
            background-color: #FFFF66;
        }
        .style2
        {
            background-color: #FF6699;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Calendar控制項的 &nbsp; <b>.OnDayRender 方法 </b>
            <br />
            <br />
            資料來源：微軟MSDN &nbsp; <a
                href="http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.ondayrender.aspx">http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.ondayrender.aspx</a>
            <br />
            <br />
            <h3>DayRender Event Example <span class="style2">#2</span></h3>
            <br />
            <br />
            請將日期移到<b> <span class="style1">2014/9/7 </span></b>以便觀看結果，謝謝。 
        <br />
            <br />
            <asp:Calendar ID="calendar1" runat="server" BackColor="White" BorderColor="White"
                BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black"
                Height="190px" NextPrevFormat="FullMonth" Width="350px" OnDayRender="calendar1_DayRender">
                <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
                <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333"
                    VerticalAlign="Bottom" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px"
                    Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
                <TodayDayStyle BackColor="#CCCCCC" />
                <WeekendDayStyle BackColor="#FFCCCC" ForeColor="Black" />
            </asp:Calendar>

        </div>
    </form>
</body>
</html>
