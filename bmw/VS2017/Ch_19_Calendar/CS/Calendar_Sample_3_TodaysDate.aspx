﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Calendar_Sample_3_TodaysDate.aspx.cs" Inherits="_Book_New_Samples_Calendar_Calendar_Sample_3_TodaysDate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <b>TodatDates屬性</b>&nbsp;
        <br />
            <a href="http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.todaysdate.aspx">http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.todaysdate.aspx</a>
            <br />
            <br />

            直接跨年月，跳到您想要的那一月份。<br />
            <br />
            年：<asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem>2013</asp:ListItem>
                <asp:ListItem>2012</asp:ListItem>
                <asp:ListItem>2011</asp:ListItem>
                <asp:ListItem>2010</asp:ListItem>
                <asp:ListItem>2009</asp:ListItem>
                <asp:ListItem>2008</asp:ListItem>
                <asp:ListItem>2007</asp:ListItem>
                <asp:ListItem>2006</asp:ListItem>
                <asp:ListItem>2005</asp:ListItem>
            </asp:DropDownList>
            <br />
            月：<asp:DropDownList ID="DropDownList2" runat="server">
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
                <asp:ListItem>4</asp:ListItem>
                <asp:ListItem>5</asp:ListItem>
                <asp:ListItem>6</asp:ListItem>
                <asp:ListItem>7</asp:ListItem>
                <asp:ListItem>8</asp:ListItem>
                <asp:ListItem>9</asp:ListItem>
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>11</asp:ListItem>
                <asp:ListItem>12</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
            <br />
            <br />
            <asp:Calendar ID="Calendar1" runat="server" BackColor="#FFFFCC"
                BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest"
                Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px"
                ShowGridLines="True" Width="220px">
                <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                <OtherMonthDayStyle ForeColor="#CC9966" />
                <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                <SelectorStyle BackColor="#FFCC66" />
                <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt"
                    ForeColor="#FFFFCC" />
                <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
            </asp:Calendar>
            <br />
            您點選的日期：<asp:Label ID="Label1" runat="server"
                Style="font-weight: 700; color: #990033"></asp:Label>
            <br />

        </div>
    </form>
</body>
</html>
