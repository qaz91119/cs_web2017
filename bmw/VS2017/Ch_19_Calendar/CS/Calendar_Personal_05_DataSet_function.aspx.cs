﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//----自己（宣告）寫的----
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
//----自己（宣告）寫的----


public partial class _Book_New_Samples_Calendar_Calendar_Personal_05_DataSet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //因為資料庫裡面，2011/10~2012/1這幾個月才有資料。所以直接時光跳躍。
            Calendar1.VisibleDate = new DateTime(2012, 1, 1);
        }
    }


    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {
        String CStitle = DBInit(e.Day.Date);

        //--新增一個 Label控制項到日曆裡面的 cell。
        if (CStitle != "")
        {   //-- 出現一個 Label控制項。
            Label LB = new Label();

            LB.Text = "<br />" + CStitle;
            e.Cell.Controls.Add(LB);
        }
    }


    protected String DBInit(DateTime inputDate)
    {
        //----(連結資料庫 字串)----
        SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString);
        String sqlstr = "Select CS_title From Calendar_Schedule Where CS_Time = '" + String.Format("{0:yyyy/M/d}", inputDate) + "'";
        Label1.Text += sqlstr + "<hr />";   //--檢查SQL指令正確否？
        
        //*** 因為 e.Day.Date產生的日期，不會自動補零。例如 2013/1/1
        //*** 在C#語法裡面，將會對應不到資料庫裡面的日期（2013/01/01）。VB語法會自動處理這個缺失。
        //*** 詳見 http://www.dotblogs.com.tw/mis2000lab/archive/2013/07/30/calendar_dayrender_datetime_formate_2013.aspx 
        
        //Conn.Open();  //-- DataAdapter會自動開關資料庫的連結。
        SqlDataAdapter da = new SqlDataAdapter(sqlstr, Conn);   //-- 執行SQL指令，取出資料
        DataSet ds = new DataSet();
        da.Fill(ds, "CS");

        if (ds.Tables["CS"].DefaultView.Count > 0)
            return ds.Tables["CS"].Rows[0][0].ToString();
        else
            return "";    //--找不到，回傳一個空字串。  
    }

}