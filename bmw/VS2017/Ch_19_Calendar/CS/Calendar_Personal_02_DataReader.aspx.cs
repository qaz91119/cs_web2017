﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//----自己（宣告）寫的----
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
//----自己（宣告）寫的----


public partial class _Book_New_Samples_Calendar_Calendar_Personal_02_DataReader : System.Web.UI.Page
{

    //----(連結資料庫 字串)----
    SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString);


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //因為資料庫裡面，2011/10~2012/1這幾個月才有資料。所以直接時光跳躍。
            Calendar1.VisibleDate = new DateTime(2012, 2, 1);


            //-- 第一次執行網頁。只列出目前這一個月的行程。
            Session["myMonth_start"] = System.DateTime.Now.Year.ToString() + "/" + System.DateTime.Now.Month.ToString() + "/1";

            if ((System.DateTime.Now.Month + 1) > 12)
                Session["myMonth_end"] = (System.DateTime.Now.Year + 1) + "/1/1";
            else
                Session["myMonth_end"] = System.DateTime.Now.Year.ToString() + "/" + (System.DateTime.Now.Month + 1) + "/1";
        }
    }


    //******************************************************** (start)
    protected void Calendar1_Init(object sender, EventArgs e)
    {
        //== 不放在 DayRender事件內，避免月曆出現每一天，就開關一次資料庫的連結。
        Conn.Open();   //---- 連結DB
    }


    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {
        SqlDataReader dr = null;
        String sqlstr = "select * from Calendar_Schedule where cs_time >= '" + Session["myMonth_start"] + "' and cs_time < '" + Session["myMonth_end"] + "'";
        Label1.Text += sqlstr + "<br />";   //--檢查SQL指令正確否？

        SqlCommand cmd = new SqlCommand(sqlstr, Conn);

        try     //==== 以下程式，只放「執行期間」的指令！===================
        {
            dr = cmd.ExecuteReader();   //---- 這時候執行SQL指令，取出資料

            if (dr.HasRows)   //== 這個月有資料，才呈現。
            {
                while (dr.Read())
                {
                    DateTime csTime = DateTime.Parse(dr["cs_time"].ToString());
                    //日期格式的轉換，請參閱 http://www.dotblogs.com.tw/mis2000lab/archive/2011/04/28/formatdatetime_vb_csharp_20110428.aspx

                    //*******************************************************(start)
                    //-- Cell 屬性表示正在呈現的儲存格，
                    //-- Day 屬性則表示要呈現在儲存格中的日期。 

                    //--新增一個 Label控制項到日曆裡面的 cell。
                    if (e.Day.Date.ToShortDateString() == String.Format("{0:yyyy/M/d}", csTime))
                    {   //*** 因為 e.Day.Date產生的日期，不會自動補零。例如 2013/1/1
                        //*** 在C#語法裡面，將會對應不到資料庫裡面的日期（2013/01/01）。VB語法會自動處理這個缺失。
                        //*** 詳見 http://www.dotblogs.com.tw/mis2000lab/archive/2013/07/30/calendar_dayrender_datetime_formate_2013.aspx

                        //-- 出現一個 Label控制項。
                        Label LB = new Label();
                        LB.Text = "<br />" + dr["cs_title"];

                        e.Cell.Controls.Add(LB);
                    }
                    //*******************************************************(end)
                }
            }
        }
        catch (Exception ex)
        {   //---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<hr />");
        }
        finally
        {   //---- Always call Close when done reading.
            if (dr != null)
            {
                cmd.Cancel();
                //----關閉DataReader之前，一定要先「取消」SqlCommand
                //參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
                dr.Close();
            }
        }
    }


    protected void Calendar1_Disposed(object sender, EventArgs e)
    {   //== 不放在 DayRender事件內，避免月曆出現每一天，就開關一次資料庫的連結。
        if (Conn.State == ConnectionState.Open)
        {
            Conn.Close();
            Conn.Dispose();  //---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
        }
    }
    //******************************************************** (end)


    protected void Calendar1_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
    {  //--參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.visiblemonthchanged.aspx

        Session["myMonth_start"] = e.NewDate.Year.ToString() + "/" + e.NewDate.Month.ToString() + "/1";
        //--指向其他月份的話，就組成那個月的第一日。

        if ((e.NewDate.Month + 1) > 12)
            Session["myMonth_end"] = (e.NewDate.Year + 1) + "/1/1";
        else
            Session["myMonth_end"] = e.NewDate.Year + "/" + (e.NewDate.Month + 1) + "/1";
    }
}