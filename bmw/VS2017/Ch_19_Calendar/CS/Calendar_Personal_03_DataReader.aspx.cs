﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//----自己（宣告）寫的----
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
//----自己（宣告）寫的----


public partial class _Book_New_Samples_Calendar_Calendar_Personal_03_DataReader : System.Web.UI.Page
{

    //----(連結資料庫 字串)----
    SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString);


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //因為資料庫裡面，2011/10~2012/1這幾個月才有資料。所以直接時光跳躍。
            Calendar1.VisibleDate = new DateTime(2012, 1, 1);


            //-- 第一次執行網頁。只列出目前這一個月的行程。
            Session["myMonth_start"] = System.DateTime.Now.Year.ToString() + "/" + System.DateTime.Now.Month.ToString() + "/1";

            if ((System.DateTime.Now.Month + 1) > 12)
                Session["myMonth_end"] = (System.DateTime.Now.Year + 1) + "/1/1";
            else
                Session["myMonth_end"] = System.DateTime.Now.Year.ToString() + "/" + (System.DateTime.Now.Month + 1) + "/1";
        }
    }


    //******************************************************** (start)
    protected void Calendar1_Init(object sender, EventArgs e)
    {
        //== 不放在 DayRender事件內，避免月曆出現每一天，就開關一次資料庫的連結。
        Conn.Open();   //---- 連結DB
    }


    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {
        SqlDataReader dr = null;
        //==========================================================
        String sqlstr = "Select CS_title From Calendar_Schedule Where CS_Time = '" + String.Format("{0:yyyy/MM/dd}", e.Day.Date.ToShortDateString()) + "'";
        //==========================================================
        Label1.Text += sqlstr + "<br />";   //--檢查SQL指令正確否？

        SqlCommand cmd = new SqlCommand(sqlstr, Conn);

        try     //==== 以下程式，只放「執行期間」的指令！===================
        {
            dr = cmd.ExecuteReader();   //---- 這時候執行SQL指令，取出資料

            if (dr.HasRows)   //== 這個月有資料，才呈現。
            {
                while (dr.Read())
                {
                    //*******************************************************(start)
                    //-- Cell 屬性表示正在呈現的儲存格，
                    //-- Day 屬性則表示要呈現在儲存格中的日期。 

                    //--新增一個 Label控制項到日曆裡面的 cell。

                        //-- 出現一個 Label控制項。
                        Label LB = new Label();
                        LB.Text = "<br />" + dr["cs_title"].ToString();

                        e.Cell.Controls.Add(LB);
                    //*******************************************************(end)
                }
            }
        }
        catch (Exception ex)
        {   //---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<hr />");
        }
        finally
        {   //---- Always call Close when done reading.
            if (dr != null)
            {
                cmd.Cancel();
                //----關閉DataReader之前，一定要先「取消」SqlCommand
                //參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
                dr.Close();
            }
        }
    }


    protected void Calendar1_Disposed(object sender, EventArgs e)
    {   //== 不放在 DayRender事件內，避免月曆出現每一天，就開關一次資料庫的連結。
        if (Conn.State == ConnectionState.Open)
        {
            Conn.Close();
            Conn.Dispose();  //---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
        }
    }
    //******************************************************** (end)

}