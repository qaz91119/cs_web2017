﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//----自己（宣告）寫的----
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
//----自己（宣告）寫的----


public partial class _Book_New_Samples_Calendar_Calendar_Personal_04_DataSet : System.Web.UI.Page
{
    //*************************
    //DataSet ds;
    DataTable dt;
    //*************************


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //因為資料庫裡面，2011/10~2012/1這幾個月才有資料。所以直接時光跳躍。
            Calendar1.VisibleDate = new DateTime(2012, 2, 1);


            //-- 第一次執行網頁。只列出目前這一個月的行程。
            Session["myMonth_start"] = System.DateTime.Now.Year.ToString() + "/" + System.DateTime.Now.Month.ToString() + "/1";

            if ((System.DateTime.Now.Month + 1) > 12)
            {
                Session["myMonth_end"] = (System.DateTime.Now.Year + 1) + "/1/1";
            }
            else
            {
                Session["myMonth_end"] = System.DateTime.Now.Year.ToString() + "/" + (System.DateTime.Now.Month + 1) + "/1";
            }
             
            DBInit();   //***自己寫的 DataSet程式***
        }
    }


    //******************************************************** (start)
    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {
        for(int i = 0; i < dt.Rows.Count; i++)
        {
            DateTime csTime = DateTime.Parse(dt.Rows[i]["CS_time"].ToString());
            //日期格式的轉換，請參閱 http://www.dotblogs.com.tw/mis2000lab/archive/2011/04/28/formatdatetime_vb_csharp_20110428.aspx

            //-- Cell 屬性表示正在呈現的儲存格，
            //-- Day 屬性則表示要呈現在儲存格中的日期。

            //--新增一個 Label控制項到日曆裡面的 cell。
                    if (e.Day.Date.ToShortDateString() == String.Format("{0:yyyy/M/d}", csTime))
                    {   //*** 因為 e.Day.Date產生的日期，不會自動補零。例如 2013/1/1
                        //*** 在C#語法裡面，將會對應不到資料庫裡面的日期（2013/01/01）。VB語法會自動處理這個缺失。
                        //*** 詳見 http://www.dotblogs.com.tw/mis2000lab/archive/2013/07/30/calendar_dayrender_datetime_formate_2013.aspx

                        //-- 出現一個 Label控制項。
                        Label LB = new Label();
                        LB.Text = "<br />" + dt.Rows[i]["CS_title"].ToString();

                        e.Cell.Controls.Add(LB);
                    }
        }
    }
    //******************************************************** (end)


    protected void Calendar1_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
    {   //--參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.visiblemonthchanged.aspx

        Session["myMonth_start"] = e.NewDate.Year.ToString() + "/" + e.NewDate.Month.ToString() + "/1";
        //--指向其他月份的話，就組成那個月的第一日。

        if ((e.NewDate.Month + 1) > 12)
        {
            Session["myMonth_end"] = (e.NewDate.Year + 1) + "/1/1";
        }
        else
        {
            Session["myMonth_end"] = e.NewDate.Year + "/" + (e.NewDate.Month + 1) + "/1";
        }

        DBInit();   //***自己寫的 DataSet程式***
    }


    //===================================
    protected void DBInit()
    {
        //----(連結資料庫 字串)----
        SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString);
        String sqlstr = "Select * From Calendar_Schedule Where cs_time >= '" + Session["myMonth_start"] + "' and cs_time < '" + Session["myMonth_end"] + "'";
        Label1.Text += sqlstr + "<hr />";   //--檢查SQL指令正確否？

        //Conn.Open();   //-- DataAdapter會自動開關資料庫的連結。
        SqlDataAdapter da = new SqlDataAdapter(sqlstr, Conn);  //-- 執行SQL指令，取出資料

        DataSet ds = new DataSet();
        da.Fill(ds, "CS");

        dt = ds.Tables["CS"];
    }

    
}