﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Book_New_Samples_Calendar_Calendar_05_DayRender : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void calendar1_DayRender(object sender, DayRenderEventArgs e)
    {
        //-- Cell 屬性表示正在呈現的儲存格，
        //-- Day 屬性則表示要呈現在儲存格中的日期。 
        
        if ((!e.Day.IsOtherMonth) && (!e.Day.IsWeekend))
        {
            //-- 把本月份、非週末假日的日期，通通改成黃色背景。
            e.Cell.BackColor = System.Drawing.Color.Yellow;
        }


        //--新增一個文字控制項（Literal）到日曆裡面的 cell。
        if (e.Day.Date.Day == 18)
        {
            //-- 本月的 18日，出現一個 Lietral控制項（註明為 "Holiday"）
            e.Cell.Controls.Add(new LiteralControl(char.ConvertFromUtf32((int)60) + "br" + char.ConvertFromUtf32((int)62) + char.ConvertFromUtf32((int)60) + "font color=red" + char.ConvertFromUtf32((int)62) + "Holiday" + char.ConvertFromUtf32((int)60) + "/font" + char.ConvertFromUtf32((int)62)));

            //-- VB的寫法，C#並沒有 ChrW()。需改寫如上面的 char.ConvertFromUtf32()方法。
            //   C#語法的寫法跟VB不同，請看 http://stackoverflow.com/questions/6060576/what-is-the-c-sharp-equivalent-of-chrwe-keycode
            //  e.Cell.Controls.Add(New LiteralControl(ChrW(60) & "br" & ChrW(62) & ChrW(60) & "font color=red" & ChrW(62) & "Holiday" & ChrW(60) & "/font" & ChrW(62)))       
        }

    }
}