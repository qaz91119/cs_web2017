﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//----自己（宣告）寫的----
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
//----自己（宣告）寫的----


public partial class _Book_New_Samples_Calendar_Calendar_Personal_06_DataReader_function : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //因為資料庫裡面，2013/10~2014/1這幾個月才有資料。所以直接時光跳躍。
            Calendar1.VisibleDate = new DateTime(2014, 1, 1);
        }
    }

    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {
        String inputDate = String.Format("{0:yyyy/M/d}", e.Day.Date);
        String CStitle = DBInit(inputDate);   //-- 比對DB。看看這個日期，是否當天有行程？
        //== DayRender事件裡面的e.Day.Date。型別：System.Web.UI.WebControls.CalendarDay。
        //== CalendarDay，包含要呈現的日期資訊。以yyyy/M/d格式呈現（如2014/1/1），
        //     並不會為了月、日的數字補上零（並非yyyy/MM/dd格式，如2014/01/01）。
        //     在C#語法中可能與資料庫裡面的日期格式（yyyy/MM/dd）對應不起來，需要寫程式另外處理。
        //     在VS 2013 / 2015執行，C#不會有這樣的錯誤。


        //--新增一個 Label控制項到日曆裡面的 cell。
        if (CStitle != "")
        {   ////-- 出現一個 Label控制項。
            //Label LB = new Label();
            //LB.Text = "<br />***" + CStitle;
            //e.Cell.Controls.Add(LB);
            ////-- 在這一個格子裡面，動態加入 Label控制項。

            //== 動態加入超連結的HyperLink控制項 ==
            HyperLink HL = new HyperLink();
            HL.Text = "<br />***" + CStitle;
            HL.NavigateUrl = "http://www.dotblogs.com.tw/mis2000lab/archive/2012/11/06/howto_calendar_personal_schedule.aspx";
            //-- 請輸入您想要的超連結網址（URL）。
            e.Cell.Controls.Add(HL);
        }
    }


    //===========================
    protected String DBInit(String inputDate)
    {   //----(連結資料庫 字串)----
        SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString);
        Conn.Open();

        String sqlstr = "Select CS_title From Calendar_Schedule Where CS_Time = '" + inputDate + "'";
        //Response.Write(sqlstr + "<br />");  //--檢查SQL指令正確否？
        SqlCommand cmd = new SqlCommand(sqlstr, Conn);
        SqlDataReader dr = cmd.ExecuteReader();
        String returnStr= null;
        if (dr.Read())
            returnStr = dr[0].ToString();
        else
            returnStr = "";

        cmd.Cancel();
        dr.Close();
        Conn.Close();

        return returnStr;        
    }

}