﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Calendar_05_DayRender.aspx.cs" Inherits="_Book_New_Samples_Calendar_Calendar_05_DayRender" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .style1
        {
            color: #0000FF;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Calendar控制項的 &nbsp; <b>.OnDayRender 方法 </b>
            <br />
            <br />
            資料來源：微軟MSDN &nbsp; <a
                href="http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.ondayrender.aspx">http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.ondayrender.aspx</a>
            <br />
            <br />
            <br />
            <h3>DayRender Event Example <span class="style1">#1</span></h3>

            <asp:Calendar ID="calendar1" runat="server" OnDayRender="calendar1_DayRender">
                <WeekendDayStyle BackColor="gray"></WeekendDayStyle>
            </asp:Calendar>

        </div>
    </form>
</body>
</html>
