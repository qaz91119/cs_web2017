﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//----自己（宣告）寫的----
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
//----自己（宣告）寫的----


public partial class _Book_New_Samples_Calendar_Calendar_Personal_01_Bad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //因為資料庫裡面，2011/10~2012/1這幾個月才有資料。所以直接時光跳躍。
            Calendar1.VisibleDate = new DateTime(2012, 1, 1);
        }
    }


    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {
        //=======微軟SDK文件的範本=======
        //----(連結資料庫 字串)----
        SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString);
        SqlDataReader dr = null;
        String sqlstr = "Select * From Calendar_Schedule";
        SqlCommand cmd = new SqlCommand(sqlstr, Conn);

        try     //==== 以下程式，只放「執行期間」的指令！===================
        {
            Conn.Open();   //---- 這時候才連結DB
            dr = cmd.ExecuteReader();   //---- 這時候執行SQL指令，取出資料

            while (dr.Read())
            {
                DateTime csTime = DateTime.Parse(dr["cs_time"].ToString());
                //日期格式的轉換，請參閱 http://www.dotblogs.com.tw/mis2000lab/archive/2011/04/28/formatdatetime_vb_csharp_20110428.aspx

                //*******************************************************(start)
                //-- Cell 屬性表示正在呈現的儲存格，
                //-- Day 屬性則表示要呈現在儲存格中的日期。 

                //--新增一個 Label控制項到日曆裡面的 cell。
                if (e.Day.Date.ToShortDateString() == String.Format("{0:yyyy/M/d}", csTime))
                {   //***因為 e.Day.Date產生的日期，不會自動補零。例如 2013/1/1
                    //***在C#語法裡面，將會對應不到資料庫裡面的日期（2013/01/01）。VB語法會自動處理這個缺失。
                    
                    //-- 出現一個 Label控制項。
                    Label LB = new Label();
                    LB.Text = "<br />" + dr["cs_title"].ToString();

                    e.Cell.Controls.Add(LB);
                }
                //*******************************************************(end)
            }
        }
        catch (Exception ex)
        {   //---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<hr />");
        }
        finally
        {  //---- Always call Close when done reading.
            if (dr != null)
            {
                cmd.Cancel();
                //----關閉DataReader之前，一定要先「取消」SqlCommand
                //參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
                dr.Close();
            }
            //---- Close the connection when done with it.
            if (Conn.State == ConnectionState.Open)
            {
                Conn.Close();
                Conn.Dispose();  //---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
            }
        }

    }
}