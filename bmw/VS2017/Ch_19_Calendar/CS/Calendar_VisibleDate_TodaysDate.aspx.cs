﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Book_New_Samples_Calendar_Calendar_VisibleDate_TodaysDate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        int myYear = Convert.ToInt32(DropDownList1.SelectedValue);
        int myMonth = Convert.ToInt32(DropDownList2.SelectedValue);

        Calendar1.VisibleDate = new DateTime(myYear, myMonth, 1);

        Calendar2.TodaysDate = new DateTime(myYear, myMonth, 1);
    }
}