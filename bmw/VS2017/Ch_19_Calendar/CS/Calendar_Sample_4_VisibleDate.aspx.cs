﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Book_New_Samples_Calendar_Calendar_Sample_4_VisibleDate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //--參考資料： http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.visibledate.aspx
        int myYear = Convert.ToInt32(DropDownList1.SelectedValue);
        int myMonth = Convert.ToInt32(DropDownList2.SelectedValue);

        Calendar1.VisibleDate = new DateTime(myYear, myMonth, 1);
        //-- 只會顯示「被選定的」月份！不包含日期喔！
        //-- 使用 VisibleDate屬性，以程式設計方式設定要顯示在 Calendar控制項中的「月份」。
    }
}