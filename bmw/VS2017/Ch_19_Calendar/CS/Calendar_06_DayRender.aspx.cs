﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Book_New_Samples_Calendar_Calendar_06_DayRender : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void calendar1_DayRender(object sender, DayRenderEventArgs e)
    {
        //-- Cell 屬性表示正在呈現的儲存格，
        //-- Day 屬性則表示要呈現在儲存格中的日期。 

        //--新增一個 HyperLink控制項到日曆裡面的 cell。
        if (e.Day.Date == new DateTime(2014, 9, 7))
        {
            //-- 請將日期移到 2014/9/7 以便觀看結果，謝謝。

            //-- 出現一個 HyperLink控制項。
            HyperLink hLink = new HyperLink();
            hLink.NavigateUrl = "http://www.dotblogs.com.tw/mis2000lab/";
            hLink.Text = "<br />購買MIS2000 Lab.新書";
            hLink.Target = "_blank";

            e.Cell.Controls.Add(hLink);
        }

        //--新增一個 HyperLink控制項到日曆裡面的 cell。
        if (e.Day.Date == new DateTime(2014, 9, 22))
        {
            //-- 請將日期移到 2014/9/7 以便觀看結果，謝謝。

            //-- 出現一個 HyperLink控制項。
            HyperLink hLink2 = new HyperLink();
            hLink2.NavigateUrl = "http://www.find.org.tw/";
            hLink2.Text = "<br /><br />資策會FIND網站";
            hLink2.Target = "_blank";

            e.Cell.Controls.Add(hLink2);
        }
        

    }
}