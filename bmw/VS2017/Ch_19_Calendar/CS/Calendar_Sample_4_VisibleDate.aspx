﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Calendar_Sample_4_VisibleDate.aspx.cs" Inherits="_Book_New_Samples_Calendar_Calendar_Sample_4_VisibleDate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <b>VisibleDate 屬性 </b>
            <br />
            <a href="http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.visibledate.aspx">http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.visibledate.aspx</a>
            <br />
            <br />
            <br />

            直接跨年月，跳到您想要的那一月份。<br />
            <br />
            年：<asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem>2013</asp:ListItem>
                <asp:ListItem>2012</asp:ListItem>
                <asp:ListItem>2011</asp:ListItem>
                <asp:ListItem>2010</asp:ListItem>
                <asp:ListItem>2009</asp:ListItem>
                <asp:ListItem>2008</asp:ListItem>
                <asp:ListItem>2007</asp:ListItem>
                <asp:ListItem>2006</asp:ListItem>
                <asp:ListItem>2005</asp:ListItem>
            </asp:DropDownList>
            <br />
            月：<asp:DropDownList ID="DropDownList2" runat="server">
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
                <asp:ListItem>4</asp:ListItem>
                <asp:ListItem>5</asp:ListItem>
                <asp:ListItem>6</asp:ListItem>
                <asp:ListItem>7</asp:ListItem>
                <asp:ListItem>8</asp:ListItem>
                <asp:ListItem>9</asp:ListItem>
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>11</asp:ListItem>
                <asp:ListItem>12</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
            <br />
            <br />
            只會顯示「被選定的」<span class="style1">月份</span>！<br />
            <asp:Calendar ID="Calendar1" runat="server" BackColor="White"
                BorderColor="Black"
                Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="250px"
                Width="330px" BorderStyle="Solid" CellSpacing="1" NextPrevFormat="ShortMonth">
                <DayHeaderStyle Font-Bold="True" Height="8pt" Font-Size="8pt"
                    ForeColor="#333333" />
                <DayStyle BackColor="#CCFF99" />
                <NextPrevStyle Font-Size="8pt" ForeColor="White" Font-Bold="True" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <SelectedDayStyle BackColor="Lime" ForeColor="White" />
                <TitleStyle BackColor="#009900" Font-Bold="True" Font-Size="12pt"
                    ForeColor="White" BorderStyle="Solid" Height="12pt" />
                <TodayDayStyle BackColor="#FFCCFF" ForeColor="Red" />
            </asp:Calendar>

        </div>
    </form>
</body>
</html>
