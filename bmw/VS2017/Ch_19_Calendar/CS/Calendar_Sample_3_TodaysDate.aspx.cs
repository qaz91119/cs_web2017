﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Book_New_Samples_Calendar_Calendar_Sample_3_TodaysDate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {

        //--參考網頁  http://msdn.microsoft.com/zh-tw/library/e057ff0d(VS.80).aspx
        //--呼叫日曆控制項中 SelectedDates 集合的 .Add()方法。
        //--您可以依任何順序加入日期，這是因為集合會為您排序所加入的日期。
        //--由於集合也強制唯一性，因此如果您加入的日期已存在，集合會予以忽略。 
        int myYear = Convert.ToInt32(DropDownList1.SelectedValue);
        int myMonth = Convert.ToInt32(DropDownList2.SelectedValue);

        Calendar1.SelectedDates.Add(new DateTime(myYear, myMonth, 10));
        Calendar1.SelectedDates.Add(new DateTime(myYear, myMonth, 15));
        Calendar1.SelectedDates.Add(new DateTime(myYear, myMonth, 20));

        //*******************************************************************
        //-- 設定「今天」的日期。日曆控制項就會跳到這個月份（當月的一日）了。
        Calendar1.TodaysDate = new DateTime(myYear, myMonth, 1);

        Label1.Text = Calendar1.SelectedDate.ToShortDateString();   //-- 當月的一日
    }
}