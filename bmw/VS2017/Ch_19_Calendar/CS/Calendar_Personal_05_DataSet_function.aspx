﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Calendar_Personal_05_DataSet_function.aspx.cs" Inherits="_Book_New_Samples_Calendar_Calendar_Personal_05_DataSet" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>DataSet 精簡的寫法</title>
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            background-color: #CC66FF;
        }
        .style2
        {
            font-weight: bold;
            color: #FFFF00;
            background-color: #993366;
        }
        .style3
        {
            color: #FF0000;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            個人行事曆（<span class="style2">修正版，DataSet</span>）<br />
            <br />
            <br />
            （建議把日期移至 2011/10~ 2012/1月之間，才有記錄。）<br />
            （需搭配資料庫 <span class="style1">Calendar_Schedule資料表</span>、ADO.NET程式）<br />
            <br />
            <br />
            <asp:Calendar ID="Calendar1" runat="server" BackColor="White"
                BorderColor="#3366CC" BorderWidth="1px" Caption="MIS2000 Lab.個人行事曆"
                CellPadding="5" CellSpacing="5" DayNameFormat="Shortest" Font-Names="Verdana"
                Font-Size="8pt" ForeColor="#003399" Height="280px" Width="350px" OnDayRender="Calendar1_DayRender">
                <DayHeaderStyle BackColor="#CC9900" ForeColor="#336666" Height="1px" />
                <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <SelectedDayStyle BackColor="#FFCCFF" Font-Bold="True" ForeColor="#009900" />
                <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                <TitleStyle BackColor="#996600" BorderColor="#3366CC" BorderWidth="1px"
                    Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                <TodayDayStyle BackColor="#FF99CC" ForeColor="White" />
                <WeekendDayStyle BackColor="#CC9900" />
            </asp:Calendar>

            <asp:Label ID="Label1" runat="server" Style="font-size: x-small"></asp:Label>
        </div>
    </form>
</body>
</html>
