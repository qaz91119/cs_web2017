﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Calendar_Personal_01_Bad.aspx.cs" Inherits="_Book_New_Samples_Calendar_Calendar_Personal_01_Bad" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>個人行事曆（簡單版，但有嚴重瑕疵！）</title>
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            background-color: #FF99CC;
        }
        .style2
        {
            color: #FFFFFF;
            font-weight: bold;
            background-color: #990000;
        }
        .style3
        {
            color: #FF0000;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            個人行事曆<span class="style2">（簡單版，但有嚴重瑕疵！）</span><br />
            <br />
            <br />
            （建議把日期移至 2011/10~ 2012/1月之間，才有記錄。）<br />
            （需搭配資料庫 <span class="style1">Calendar_Schedule資料表</span>、ADO.NET程式）<br />
            <br />
            <br />
            <asp:Calendar ID="Calendar1" runat="server" BackColor="White"
                BorderColor="#3366CC" BorderWidth="1px" Caption="MIS2000 Lab.個人行事曆"
                CellPadding="5" CellSpacing="5" DayNameFormat="Shortest" Font-Names="Verdana"
                Font-Size="8pt" ForeColor="#003399" Height="280px" Width="350px" OnDayRender="Calendar1_DayRender">
                <DayHeaderStyle BackColor="#C8E3FF" ForeColor="#336666" Height="1px" />
                <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <SelectedDayStyle BackColor="#66FF66" Font-Bold="True" ForeColor="#009900" />
                <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px"
                    Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                <TodayDayStyle BackColor="#FF99CC" ForeColor="White" />
                <WeekendDayStyle BackColor="#FFFF99" />
            </asp:Calendar>

        </div>
    <p class="style3">
        這個範例的缺點：
    </p>
    <p>
        1. Calendar的 <b>DayRender事件</b>，是每呈現一天就會運作一次。<br />
        本範例的寫法，每一天就要開啟一次資料庫的連結，太浪費資源。
    </p>
    <p>
        2. 應該針對<b>當天</b>、或是<b>當月份</b>，呈現行事曆的內容即可。
    </p>
    <p>
        3. 程式執行次數太多。如果DB有 10筆記錄，每個月 30天，<b> DayRender事件</b>共要執行 300次！
    </p>
        <asp:Label ID="Label1" runat="server" style="font-size: x-small"></asp:Label>
    </form>
    </body>
</html>
