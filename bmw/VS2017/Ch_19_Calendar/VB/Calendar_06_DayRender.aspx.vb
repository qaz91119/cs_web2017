﻿
Partial Class _Book_New_Samples_Calendar_Calendar_06_DayRender
    Inherits System.Web.UI.Page

    Protected Sub calendar1_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles calendar1.DayRender
        '-- Cell 屬性表示正在呈現的儲存格，
        '-- Day 屬性則表示要呈現在儲存格中的日期。 

        '--新增一個 HyperLink控制項到日曆裡面的 cell。
        If e.Day.Date = New DateTime(2012, 9, 7) Then
            '-- 請將日期移到 2011/9/7 以便觀看結果，謝謝。

            '-- 出現一個 HyperLink控制項。
            Dim hLink As New HyperLink()
            hLink.NavigateUrl = "http://www.dotblogs.com.tw/mis2000lab/"
            hLink.Text = "<br />購買MIS2000 Lab.新書"
            hLink.Target = "_blank"

            e.Cell.Controls.Add(hLink)
        End If

        '--新增一個 HyperLink控制項到日曆裡面的 cell。
        If e.Day.Date = New DateTime(2012, 9, 22) Then
            '-- 出現一個 HyperLink控制項。
            Dim hLink2 As New HyperLink()
            hLink2.NavigateUrl = "http://www.find.org.tw/"
            hLink2.Text = "<br />資策會FIND網站"
            hLink2.Target = "_blank"

            e.Cell.Controls.Add(hLink2)
        End If

        '--新增一個 HyperLink控制項到日曆裡面的 cell。
        If e.Day.Date = New DateTime(2012, 10, 20) Then
            '-- 出現一個 HyperLink控制項。
            Dim hLink3 As New HyperLink()
            hLink3.NavigateUrl = "http://www.iii.org.tw/"
            hLink3.Text = "<br />資策會網站"
            hLink3.Target = "_blank"

            e.Cell.Controls.Add(hLink3)
        End If

        '--新增一個 HyperLink控制項到日曆裡面的 cell。
        If e.Day.Date = New DateTime(2012, 11, 24) Then
            '-- 出現一個 HyperLink控制項。
            Dim hLink4 As New HyperLink()
            hLink4.NavigateUrl = "http://www.iiiedu.org.tw/south/"
            hLink4.Text = "<br />資策會數位教育所（南區）"
            hLink4.Target = "_blank"

            e.Cell.Controls.Add(hLink4)
        End If
    End Sub

End Class
