﻿
Partial Class _Book_New_Samples_Calendar_Calendar_Sample_3
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click

        '--參考網頁  http://msdn.microsoft.com/zh-tw/library/e057ff0d(VS.80).aspx
        '--呼叫日曆控制項中 SelectedDates 集合的 .Add()方法。
        '--您可以依任何順序加入日期，這是因為集合會為您排序所加入的日期。
        '--由於集合也強制唯一性，因此如果您加入的日期已存在，集合會予以忽略。 

        Calendar1.SelectedDates.Add(New DateTime(DropDownList1.SelectedValue, DropDownList2.SelectedValue, 10))
        Calendar1.SelectedDates.Add(New DateTime(DropDownList1.SelectedValue, DropDownList2.SelectedValue, 15))
        Calendar1.SelectedDates.Add(New DateTime(DropDownList1.SelectedValue, DropDownList2.SelectedValue, 20))

        '*******************************************************************
        '-- 設定「今天」的日期。日曆控制項就會跳到這個月份（當月的一日）了。
        Calendar1.TodaysDate = New DateTime(DropDownList1.SelectedValue, DropDownList2.SelectedValue, 1)

        Label1.Text = Calendar1.SelectedDate   '-- 當月的一日
    End Sub
End Class
