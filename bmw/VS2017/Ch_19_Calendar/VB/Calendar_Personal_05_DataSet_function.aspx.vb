﻿'----自己（宣告）寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己（宣告）寫的----


Partial Class _Book_New_Samples_Calendar_Calendar_Personal_05_DataSet
    Inherits System.Web.UI.Page


    Protected Sub Calendar1_DayRender(sender As Object, e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar1.DayRender
        Dim CStitle As String = DBInit(e.Day.Date)

        '--新增一個 Label控制項到日曆裡面的 cell。
        If CStitle <> "" Then
            '-- 出現一個 Label控制項。
            Dim LB As New Label
            LB.Text = "<br />" & CStitle
            e.Cell.Controls.Add(LB)
        End If
    End Sub


    '===========================
    Function DBInit(ByVal inputDate As Date) As String
        '----(連結資料庫 字串)----
        Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)
        Dim sqlstr As String = "Select CS_title From Calendar_Schedule Where CS_Time = '" & inputDate & "'"
        'Label1.Text &= sqlstr & "<hr />"   '--檢查SQL指令正確否？

        'Conn.Open()  '-- DataAdapter會自動開關資料庫的連結。
        Dim da As New SqlDataAdapter(sqlstr, Conn)  '-- 執行SQL指令，取出資料
        Dim ds As New DataSet
        da.Fill(ds, "CS")

        If ds.Tables("CS").DefaultView.Count > 0 Then
            Return ds.Tables("CS").Rows(0)(0).ToString
        Else
            Return ""    '--找不到，回傳一個空字串。
        End If
    End Function

End Class
