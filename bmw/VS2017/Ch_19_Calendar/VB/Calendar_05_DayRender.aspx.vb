﻿
Partial Class _Book_New_Samples_Calendar_Calendar_05
    Inherits System.Web.UI.Page

    Protected Sub calendar1_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles calendar1.DayRender
        '-- Cell 屬性表示正在呈現的儲存格，
        '-- Day 屬性則表示要呈現在儲存格中的日期。 

        If Not e.Day.IsOtherMonth And Not e.Day.IsWeekend Then
            '-- 把本月份、非週末假日的日期，通通改成黃色背景。
            e.Cell.BackColor = System.Drawing.Color.Yellow
        End If

        '--新增一個文字控制項（Literal）到日曆裡面的 cell。
        If e.Day.Date.Day = 18 Then
            '-- 本月的 18日，出現一個 Lietral控制項（註明為 "Holiday"）
            e.Cell.Controls.Add(New LiteralControl(ChrW(60) & "br" & ChrW(62) & ChrW(60) & "font color=red" & ChrW(62) & "Holiday" & ChrW(60) & "/font" & ChrW(62)))
        End If

    End Sub
End Class
