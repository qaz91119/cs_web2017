﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Calendar_Personal_03_DataReader.aspx.vb" Inherits="_Book_New_Samples_Calendar_Calendar_Personal_03_DataReader" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            background-color: #CCFFCC;
        }
        .style2
        {
            font-weight: bold;
            color: #FFFFFF;
            background-color: #009900;
        }
        .style3
        {
            color: #FF0000;
        }
        .style4
        {
            color: #FF0000;
            font-weight: bold;
        }
        .style5
        {
            color: #FF0000;
            background-color: #FFFF66;
        }
        .auto-style1 {
            text-decoration: line-through;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        個人行事曆（<span class="style2">修正版，DataReader</span>）<span class="style4">第二次修正！<b><span 
            class="style3">程式變短了</span></b></span><br />
        <br />
        <b><span class="style3">SQL指令，檢查每一天！</span><br />
        &quot;select * from Calendar_Schedule where cs_time = &#39;&quot; &amp; <span class="style5">
        e.Day.Date</span> &amp; &quot;&#39;&quot;</b><br />
        <br />
        （建議把日期移至 2011/10~ 2012/1月之間，才有記錄。）<br />
        （需搭配資料庫 <span class="style1">Calendar_Schedule資料表</span>、ADO.NET程式）<br />
        <br />
        <br />
        <asp:Calendar ID="Calendar1" runat="server" BackColor="White" 
            BorderColor="#3366CC" BorderWidth="1px" Caption="MIS2000 Lab.個人行事曆" 
            CellPadding="5" CellSpacing="5" DayNameFormat="Shortest" Font-Names="Verdana" 
            Font-Size="8pt" ForeColor="#003399" Height="280px" Width="350px">
            <DayHeaderStyle BackColor="#99FF99" ForeColor="#336666" Height="1px" />
            <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
            <OtherMonthDayStyle ForeColor="#999999" />
            <SelectedDayStyle BackColor="#66FF66" Font-Bold="True" ForeColor="#009900" />
            <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
            <TitleStyle BackColor="#009900" BorderColor="#3366CC" BorderWidth="1px" 
                Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
            <TodayDayStyle BackColor="#FF99CC" ForeColor="White" />
            <WeekendDayStyle BackColor="#FFFF99" />
        </asp:Calendar>


        <p>
            <strong>使用 Calendar的「Init」事件、「Dispose」事件、<span class="auto-style1">「VisibleMonthChanged」事件（當您變動月份時，才會重新整理DB資料）</span></strong>
        </p>
       
         <asp:Label ID="Label1" runat="server" style="font-size: x-small"></asp:Label>
    
    </div>
    </form>


</body>
</html>

