﻿
Partial Class Book_Sample_Ch03_Program__Book_WebControls_Calendar_Icon_Show
    Inherits System.Web.UI.Page


    Protected Sub ImageButton1_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton1.Click
        Calendar1.Visible = True
    End Sub


    Protected Sub Calendar1_SelectionChanged(sender As Object, e As EventArgs) Handles Calendar1.SelectionChanged
        TextBox1.Text = Calendar1.SelectedDate.ToShortDateString()

        Calendar1.Visible = False
    End Sub
End Class
