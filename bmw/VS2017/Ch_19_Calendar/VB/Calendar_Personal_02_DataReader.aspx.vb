﻿'----自己（宣告）寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己（宣告）寫的----


Partial Class _Book_New_Samples_Calendar_Calendar_Personal
    Inherits System.Web.UI.Page

    '----(連結資料庫 字串)----
    Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            '-- 第一次執行網頁。只列出目前這一個月的行程。
            Session("myMonth_start") = Now.Year & "/" & Now.Month & "/1"

            If (Now.Month + 1) > 12 Then
                Session("myMonth_end") = (Now.Year + 1) & "/1/1"
            Else
                Session("myMonth_end") = Now.Year & "/" & (Now.Month + 1) & "/1"
            End If
        End If
    End Sub


    '******************************************************** (start)
    Protected Sub Calendar1_Init(sender As Object, e As System.EventArgs) Handles Calendar1.Init
        '== 不放在 DayRender事件內，避免月曆出現每一天，就開關一次資料庫的連結。
        Conn.Open()   '---- 連結DB
    End Sub


    Protected Sub calendar1_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar1.DayRender
        Dim dr As SqlDataReader = Nothing
        Dim sqlstr As String = "select * from Calendar_Schedule where cs_time >= '" & Session("myMonth_start") & "' and cs_time < '" & Session("myMonth_end") & "'"
        'Label1.Text &= sqlstr & "<br />"    '--檢查SQL指令正確否？

        Dim cmd As SqlCommand = New SqlCommand(sqlstr, Conn)
        Try     '==== 以下程式，只放「執行期間」的指令！=====================
            dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料

            If dr.HasRows Then   '== 這個月有資料，才呈現。
                While dr.Read()
                    Dim csTime As Date = FormatDateTime(dr("cs_time"), DateFormat.ShortDate)
                    '-- 日期格式的轉換，請參閱 http://www.dotblogs.com.tw/mis2000lab/archive/2011/04/28/formatdatetime_vb_csharp_20110428.aspx
                    '-- 或是跟C#語法一樣，寫成 String.Format("{0:yyyy/MM/dd}", dr("cs_time"))

                    '*******************************************************(start)
                    '-- Cell 屬性表示正在呈現的儲存格，
                    '-- Day 屬性則表示要呈現在儲存格中的日期。 

                    '--新增一個 Label控制項到日曆裡面的 cell。
                    If e.Day.Date = csTime Then
                        '-- 出現一個 Label控制項。
                        Dim LB As New Label
                        LB.Text = "<br />" & dr("cs_title")

                        e.Cell.Controls.Add(LB)
                    End If
                    '*******************************************************(end)
                End While
            End If

        Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<hr />")
        Finally
            '---- Always call Close when done reading.
            If Not (dr Is Nothing) Then
                cmd.Cancel()
                '----關閉DataReader之前，一定要先「取消」SqlCommand
                '參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
                dr.Close()
            End If
        End Try
    End Sub


    Protected Sub Calendar1_Disposed(sender As Object, e As System.EventArgs) Handles Calendar1.Disposed
        '== 不放在 DayRender事件內，避免月曆出現每一天，就開關一次資料庫的連結。
        '---- Close the connection when done with it.
        If (Conn.State = ConnectionState.Open) Then
            Conn.Close()
            Conn.Dispose() '---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
        End If
    End Sub
    '******************************************************** (end)


    Protected Sub Calendar1_VisibleMonthChanged(sender As Object, e As System.Web.UI.WebControls.MonthChangedEventArgs) Handles Calendar1.VisibleMonthChanged
        '--參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.visiblemonthchanged.aspx
        Session("myMonth_start") = e.NewDate.Year & "/" & e.NewDate.Month & "/1"
        '--指向其他月份的話，就組成那個月的第一日。

        If (e.NewDate.Month + 1) > 12 Then
            Session("myMonth_end") = (e.NewDate.Year + 1) & "/1/1"
        Else
            Session("myMonth_end") = e.NewDate.Year & "/" & (e.NewDate.Month + 1) & "/1"
        End If
    End Sub

End Class
