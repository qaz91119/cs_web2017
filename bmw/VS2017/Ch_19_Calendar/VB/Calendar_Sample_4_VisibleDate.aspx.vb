﻿
Partial Class _Book_New_Samples_Calendar_Calendar_Sample_4
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        '--參考資料： http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.visibledate.aspx

        Calendar1.VisibleDate = New DateTime(DropDownList1.SelectedValue, DropDownList2.SelectedValue, 1)
        '-- 只會顯示「被選定的」月份！不包含日期喔！
        '-- 使用 VisibleDate屬性，以程式設計方式設定要顯示在 Calendar控制項中的「月份」。
    End Sub
End Class
