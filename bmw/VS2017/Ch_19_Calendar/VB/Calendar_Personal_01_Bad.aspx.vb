﻿'----自己（宣告）寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己（宣告）寫的----



Partial Class _Book_New_Samples_Calendar_Calendar_Personal
    Inherits System.Web.UI.Page


    Protected Sub calendar1_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar1.DayRender
        '=======微軟SDK文件的範本=======
        '----(連結資料庫 字串)----
        Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)
        Dim dr As SqlDataReader = Nothing
        Dim sqlstr As String = "select * from Calendar_Schedule"
        Dim cmd As SqlCommand = New SqlCommand(sqlstr, Conn)
        Try     '==== 以下程式，只放「執行期間」的指令！=====================
            Conn.Open()   '---- 這時候才連結DB
            dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料

            While dr.Read()
                Dim csTime As Date = FormatDateTime(dr("cs_time"), DateFormat.ShortDate)
                '-- 日期格式的轉換，請參閱 http://www.dotblogs.com.tw/mis2000lab/archive/2011/04/28/formatdatetime_vb_csharp_20110428.aspx
                                    
                '*******************************************************(start)
                '-- Cell 屬性表示正在呈現的儲存格，
                '-- Day 屬性則表示要呈現在儲存格中的日期。 

                '--新增一個 Label控制項到日曆裡面的 cell。
                If e.Day.Date = csTime Then
                    '-- 出現一個 Label控制項。
                    Dim LB As New Label
                    LB.Text = "<br />" & dr("cs_title")

                    e.Cell.Controls.Add(LB)
                End If
                '*******************************************************(end)
            End While

        Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<HR/>")

        Finally
            '---- Always call Close when done reading.
            If Not (dr Is Nothing) Then
                cmd.Cancel()
                '----關閉DataReader之前，一定要先「取消」SqlCommand
                '參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
                dr.Close()
            End If
            '---- Close the connection when done with it.
            If (Conn.State = ConnectionState.Open) Then
                Conn.Close()
                Conn.Dispose() '---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
            End If
        End Try

    End Sub


End Class
