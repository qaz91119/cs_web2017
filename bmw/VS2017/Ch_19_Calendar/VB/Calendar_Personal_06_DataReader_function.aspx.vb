﻿'----自己（宣告）寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己（宣告）寫的----


Partial Class _Book_New_Samples_Calendar_Calendar_Personal_06_DataReader
    Inherits System.Web.UI.Page


    Protected Sub Calendar1_DayRender(sender As Object, e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar1.DayRender
        Dim CStitle As String = DBInit(e.Day.Date)  '-- 比對DB。看看這個日期，是否當天有行程？

        '--新增一個 Label控制項到日曆裡面的 cell。
        If CStitle <> "" Then
            '-- 出現一個 Label控制項。
            'Dim LB As New Label
            'LB.Text = "<br />***" & CStitle
            'e.Cell.Controls.Add(LB)
            '-- 在這一個格子裡面，動態加入 Label控制項。

            '== 動態加入超連結的HyperLink控制項 ==
            Dim HL As New HyperLink
            HL.Text = "<br />***" & CStitle
            HL.NavigateUrl = "http://www.dotblogs.com.tw/mis2000lab/archive/2012/11/06/howto_calendar_personal_schedule.aspx"
            '-- 請輸入您想要的超連結網址（URL）。
            e.Cell.Controls.Add(HL)
        End If
    End Sub


    '===========================
    Function DBInit(ByVal inputDate As Date) As String
        '----(連結資料庫 字串)----
        Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)
        Conn.Open()

        Dim sqlstr As String = "Select CS_title From Calendar_Schedule Where CS_Time = '" & inputDate & "'"
        'Response.Write(sqlstr & "<br />")  '--檢查SQL指令正確否？
        Dim cmd As SqlCommand = New SqlCommand(sqlstr, Conn)
        Dim dr As SqlDataReader = cmd.ExecuteReader()
        Dim returnStr As String = Nothing
        If dr.Read() Then
            returnStr = dr(0)
        Else
            returnStr = ""  
        End If

        cmd.Cancel()
        dr.Close()
        Conn.Close()

        'If returnStr <> "" Then
            Return returnStr
        'Else
        '    Return ""  '--找不到，回傳一個空字串。
        'End If
    End Function

End Class
