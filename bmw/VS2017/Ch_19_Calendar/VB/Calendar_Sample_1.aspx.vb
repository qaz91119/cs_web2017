﻿
Partial Class _Book_WebControls_Calendar_Sample
    Inherits System.Web.UI.Page

    '--參考網頁： http://msdn.microsoft.com/zh-tw/library/8k0f6h1h.aspx


    Protected Sub Calendar1_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.Init
        Calendar1.SelectedDate = "2009/11/1"
        '--事先指定日期  (請調整為這個月的某一天，才看得到結果)
    End Sub


    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        Dim i As Integer

        '-- SelectedDates可以存放多天的選定日期
        '-- 參考網頁  http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.selecteddates.aspx
        For i = 0 To Calendar1.SelectedDates.Count - 1
            Label1.Text &= "<br />" & Calendar1.SelectedDates(i).ToShortDateString()
        Next

    End Sub
End Class
