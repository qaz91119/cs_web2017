﻿'----自己（宣告）寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己（宣告）寫的----


Partial Class _Book_New_Samples_Calendar_Calendar_Personal_03_DataSet
    Inherits System.Web.UI.Page

    '***********************************
    'Dim ds As DataSet
    Dim dt As DataTable
    '***********************************

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            '-- 第一次執行網頁。只列出目前這一個月的行程。
            Session("myMonth_start") = Now.Year & "/" & Now.Month & "/1"

            If (Now.Month + 1) > 12 Then
                Session("myMonth_end") = (Now.Year + 1) & "/1/1"
            Else
                Session("myMonth_end") = Now.Year & "/" & (Now.Month + 1) & "/1"
            End If

            DBInit()   '***自己寫的 DataSet程式***
        End If
    End Sub


    '******************************************************** (start)
    Protected Sub calendar1_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar1.DayRender

        For i As Integer = 0 To (dt.Rows.Count - 1)
            Dim csTime As Date = FormatDateTime(dt.Rows(i).Item("CS_time").ToString, DateFormat.ShortDate)
            '-- Cell 屬性表示正在呈現的儲存格，
            '-- Day 屬性則表示要呈現在儲存格中的日期。

            '--新增一個 Label控制項到日曆裡面的 cell。
            If e.Day.Date = csTime Then
                '-- 出現一個 Label控制項。
                Dim LB As New Label
                LB.Text = "<br />" & dt.Rows(i).Item("CS_title").ToString()
                e.Cell.Controls.Add(LB)
            End If
        Next

    End Sub
    '******************************************************** (end)


    Protected Sub Calendar1_VisibleMonthChanged(sender As Object, e As System.Web.UI.WebControls.MonthChangedEventArgs) Handles Calendar1.VisibleMonthChanged
        '--參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.calendar.visiblemonthchanged.aspx
        Session("myMonth_start") = e.NewDate.Year & "/" & e.NewDate.Month & "/1"
        '--指向其他月份的話，就組成那個月的第一日。

        If (e.NewDate.Month + 1) > 12 Then
            Session("myMonth_end") = (e.NewDate.Year + 1) & "/1/1"
        Else
            Session("myMonth_end") = e.NewDate.Year & "/" & (e.NewDate.Month + 1) & "/1"
        End If

        DBInit()   '***自己寫的 DataSet程式***
    End Sub


    '===================================
    Sub DBInit()
        '----(連結資料庫 字串)----
        Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)
        Dim sqlstr As String = "select * from Calendar_Schedule where cs_time >= '" & Session("myMonth_start") & "' and cs_time < '" & Session("myMonth_end") & "'"
        Label1.Text &= sqlstr & "<hr />"   '--檢查SQL指令正確否？

        'Conn.Open()   '-- DataAdapter會自動開關資料庫的連結。
        Dim da As New SqlDataAdapter(sqlstr, Conn)  '-- 執行SQL指令，取出資料

        Dim ds As New DataSet
        da.Fill(ds, "CS")

        dt = ds.Tables("CS")
    End Sub

End Class
