﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Datepicker.aspx.cs" Inherits="Book_Sample_jQuery_UI_Datepicker" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>日曆</title>
    <link rel="stylesheet" href="jQuery_JS_CSS/jquery-ui.css" />
    <script src="jQuery_JS_CSS/jquery-1.9.1.js"></script>
    <script src="jQuery_JS_CSS/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#TextBox1").datepicker();


            $("#TextBox2").datepicker({
                dateFormat: 'yy-mm-dd'
            });
        });
    </script>
    <script src="jQuery_JS_CSS/jquery.ui.datepicker-zh-TW.js"></script>

    <style type="text/css">
        .auto-style1 {
            color: #FF0000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        資料來源 <a href="http://jqueryui.com/datepicker/">http://jqueryui.com/datepicker/</a><br />
        <br />
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    

        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>===========================================</p>
        <p>
            修改日期呈現的格式，請寫成：</p>
        <p>
            $(function () { $(&quot;#TextBox1&quot;).datepicker(<span class="auto-style1"><strong>{ dateFormat: &#39;yy-mm-dd&#39;}); }</strong></span>);</p>
        <p>
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
    
        </p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>



    </div>
    </form>
</body>
</html>
