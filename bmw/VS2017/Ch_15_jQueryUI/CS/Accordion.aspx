﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Accordion.aspx.cs" Inherits="Book_Sample_jQuery_UI_Accordion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>手風琴折疊</title>
    <link rel="stylesheet" href="jQuery_JS_CSS/jquery-ui.css" />
    <script src="jQuery_JS_CSS/jquery-1.9.1.js"></script>
    <script src="jQuery_JS_CSS/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#accordion").accordion({
                collapsible: true   //按下標題，可以關閉內容。
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        資料來源 <a href="http://jqueryui.com/accordion/">http://jqueryui.com/accordion/</a><br />
        <br />
        <br />

        <div id="accordion">
            <h3>Section 1</h3>
            <div>
                <p>
                    === Section 1 ===<br />
                    === Section 1 ===<br />
                    === Section 1 ===<br />
                </p>
            </div>
            <h3>Section 2</h3>
            <div>
                <p>
                    === Section 2 ===<br />
                    === Section 2 ===<br />
                    === Section 2 ===<br />
                </p>
            </div>
            <h3>Section 3</h3>
            <div>
                <p>
                    === Section 3 ===<br />
                    === Section 3 ===<br />
                    === Section 3 ===<br />
                </p>
            </div>
            <h3>Section 4</h3>
            <div>
                <p>
                    === Section 4 ===<br />
                    === Section 4 ===<br />
                    === Section 4 ===<br />
                </p>
            </div>
        </div>


    
    </div>
    </form>
</body>
</html>
