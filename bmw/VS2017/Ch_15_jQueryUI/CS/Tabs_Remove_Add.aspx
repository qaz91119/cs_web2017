﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Tabs_Remove_Add.aspx.cs" Inherits="Book_Sample_jQuery_UI_Tabs_Remove_Add" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" href="jQuery_JS_CSS/jquery-ui.css" />
    <script src="jQuery_JS_CSS/jquery-1.9.1.js"></script>
    <script src="jQuery_JS_CSS/jquery-ui.js"></script>

    <style>
          #dialog label, #dialog input { display:block; }
          #dialog label { margin-top: 0.5em; }
          #dialog input, #dialog textarea { width: 95%; }
          #tabs { margin-top: 1em; }
          #tabs li .ui-icon-close { float: left; margin: 0.4em 0.2em 0 0; cursor: pointer; }
          #add_tab { cursor: pointer; }


        .auto-style1 { color: #FF0000; }  /* 畫面上的紅色字體 */ 
   </style>

    <script>
        $(function () {
            var tabTitle = $("#tab_title"),
              tabContent = $("#tab_content"),
              tabTemplate = "<li><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close' role='presentation'>Remove Tab</span></li>",

              tabCounter = 5;   //*** 重點！！全部的標籤數量！！ ***

            var tabs = $("#tabs").tabs();

            // modal dialog init: custom buttons and a "close" callback reseting the form inside
            var dialog = $("#dialog").dialog({
                autoOpen: false,
                modal: true,
                buttons: {
                    Add: function () {
                        addTab();
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {
                    form[0].reset();
                }
            });

            // addTab form: calls addTab function on submit and closes the dialog
            var form = dialog.find("form").submit(function (event) {
                addTab();
                dialog.dialog("close");
                event.preventDefault();
            });

            // actual addTab function: adds new tab using the input from the form above
            function addTab() {
                var label = tabTitle.val() || "Tab " + tabCounter,
                  id = "tabs-" + tabCounter,
                  li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)),
                  tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";

                tabs.find(".ui-tabs-nav").append(li);
                tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
                tabs.tabs("refresh");
                tabCounter++;
            }

            // addTab button: just opens the dialog 按下按鈕，出現一個對話視窗
            $("#add_tab")
              .button()
              .click(function () {
                  dialog.dialog("open");
              });

            // close icon: removing the tab on click 移除一個標籤！
            tabs.delegate("span.ui-icon-close", "click", function () {
                var panelId = $(this).closest("li").remove().attr("aria-controls");
                $("#" + panelId).remove();
                tabs.tabs("refresh");
            });

            tabs.bind("keyup", function (event) {
                if (event.altKey && event.keyCode === $.ui.keyCode.BACKSPACE) {
                    var panelId = tabs.find(".ui-tabs-active").remove().attr("aria-controls");
                    $("#" + panelId).remove();
                    tabs.tabs("refresh");
                }
            });
        });
    </script>

</head>
<body>

    <!-- *********************************************(Start)  -->
    <!-- *** 新增時，出現的對話視窗。 ***  -->
    <!-- *** 請放在 ASP.NET專屬的 <form></form> 外面。 ***  -->
    <div id="dialog" title="Tab data">
        <form>
            <fieldset class="ui-helper-reset">
                <label for="tab_title">Title</label>
                <input type="text" name="tab_title" id="tab_title" value="" class="ui-widget-content ui-corner-all" />
                <label for="tab_content">Content</label>
                <textarea name="tab_content" id="tab_content" class="ui-widget-content ui-corner-all"></textarea>
            </fieldset>
        </form>
    </div>
    <!-- *********************************************(End)  -->

   

            資料來源&nbsp; <a href="http://jqueryui.com/tabs/#manipulation">http://jqueryui.com/tabs/#manipulation</a><br />
            <br />
            可以移除、加入標籤（Tab）。<span class="auto-style1"><strong>不建議搭配ASP.NET的 Button按鈕，引發 PostBack會使功能失效。</strong></span><strong><br class="auto-style1" />
            </strong>
            <br />
            <br />


            <!-- 使用 ASP.NET的 Button會觸發 PostBack 而使本功能失效！！ -->
            <button id="add_tab">Add Tab_新增一個標籤（Tab）</button>
            <br />


            <div id="tabs">
                <ul>
                    <!-- 標籤後方，必須加上一段 <span> -->
                    <li><a href="#tabs-1">Nunc tincidunt（標籤1）</a><span class="ui-icon ui-icon-close" role="presentation">Remove Tab</span></li>
                    <li><a href="#tabs-2">Proin dolor（標籤2）</a><span class="ui-icon ui-icon-close" role="presentation">Remove Tab</span></li>
                    <li><a href="#tabs-3">Aenean lacinia（標籤3）</a><span class="ui-icon ui-icon-close" role="presentation">Remove Tab</span></li>
                </ul>
                <div id="tabs-1">
                    <p>（標籤1）Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
                </div>
                <div id="tabs-2">
                    <p>（標籤2）Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
                </div>
                <div id="tabs-3">
                    <p>（標籤3）Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.</p>
                    <p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.</p>
                </div>
            </div>


</body>
</html>
