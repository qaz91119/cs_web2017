﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Datepicker_MinMax.aspx.cs" Inherits="Book_Sample_jQuery_UI_Datepicker_MinMax" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>日曆</title>
    <link rel="stylesheet" href="jQuery_JS_CSS/jquery-ui.css" />
    <script src="jQuery_JS_CSS/jquery-1.9.1.js"></script>
    <script src="jQuery_JS_CSS/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#TextBox4").datepicker({ minDate: -20, maxDate: "+1M +10D" });
            //限制日期輸入的區間（只能往前20天，往後一個月又10天）


            $("#TextBox5").datepicker({
                changeMonth: true,
                changeYear: true
            });  //日曆可選擇「年」、「月」
        });
    </script>


    <style type="text/css">
        .auto-style1 {
            color: #FF0000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <p>===========================================</p>
            <p><a href="http://jqueryui.com/datepicker/#min-max">http://jqueryui.com/datepicker/#min-max</a></p>
            <p>
                限制日期輸入的區間（只能往前20天，往後一個月又10天），請寫成：
            </p>
            <p>
                $(function () { $(&quot;#TextBox4&quot;).datepicker(<span class="auto-style1"><strong>{ minDate: -20, maxDate: &quot;+1M +10D&quot; }</strong></span>);
            </p>
            <p>
                <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>

            </p>
            <p>&nbsp;</p>
            <p>==========================================</p>
            <p>日曆可選擇「年」、「月」 <a href="http://jqueryui.com/datepicker/#dropdown-month-year">http://jqueryui.com/datepicker/#dropdown-month-year</a></p>
            <p>$( &quot;#TextBox5&quot; ).datepicker({ <span class="auto-style1"><strong>changeMonth: true</strong></span>, <span class="auto-style1"><strong>changeYear: true</strong></span> });</p>
            <p>
                <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>

            </p>


        </div>
    </form>
</body>
</html>
