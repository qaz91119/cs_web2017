﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Autocomplete.aspx.cs" Inherits="Book_Sample_jQuery_UI_Autocomplete" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>自動完成（輸入關鍵字）</title>
    <link rel="stylesheet" href="jQuery_JS_CSS/jquery-ui.css" />
    <script src="jQuery_JS_CSS/jquery-1.9.1.js"></script>
    <script src="jQuery_JS_CSS/jquery-ui.js"></script>

    <style>
          /* 關鍵字如果太多，可以出現 ScrollBar */
          .ui-autocomplete {
            max-height: 150px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
          }
          /* IE 6 doesn't support max-height
           * we use height instead, but this forces the menu to always be this tall
           */
          * html .ui-autocomplete {
            height: 150px;
          }
  </style>

    <script>
        $(function () {
            //關鍵字列表，如下
            var availableTags = [
              "ActionScript",
              "AppleScript",
              "Asp",
              "ASP.NET 專題實務 (MIS2000 Lab.)",
              "BASIC",
              "C",
              "C++",
              "Clojure",
              "COBOL",
              "ColdFusion",
              "Erlang",
              "Fortran",
              "Groovy",
              "Haskell",
              "Java",
              "JavaScript",
              "Lisp",
              "MIS2000 Lab.",
              "mis2000lab",
              "Perl",
              "PHP",
              "Python",
              "Ruby",
              "Scala",
              "Scheme"
            ];
            $("#TextBox1").autocomplete({
                source: availableTags
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        資料來源&nbsp; <a href="http://jqueryui.com/autocomplete/">http://jqueryui.com/autocomplete/</a></div>
        <p>
            &nbsp;</p>
        <p>

            <div class="ui-widget">
                請輸入關鍵字（例如 a或 b）<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            </div>
            
        </p>
    </form>
</body>
</html>
