﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ASPnet_jQuery_Error_01.aspx.cs" Inherits="Book_Sample_jQuery_UI_ASPnet_jQuery_Error_01" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" href="jQuery_JS_CSS/jquery-ui.css" />
    <script src="jQuery_JS_CSS/jquery-1.9.1.js"></script>
    <script src="jQuery_JS_CSS/jquery-ui.js"></script>

    <style>
    .toggler { width: 500px; height: 200px; position: relative; }
    #button { padding: .5em 1em; text-decoration: none; }
    #effect { width: 240px;  padding: 1em;  font-size: 1.2em; border: 1px solid #000; background: #eee; color: #333; }
    .newClass { text-indent: 40px; letter-spacing: .4em; width: 410px; height: 100px; padding: 30px; margin: 10px; font-size: 1.6em; }
        .auto-style1 {
            color: #FF0000;
        }
  </style>

  <script>
  $(function() {
    $( "#Button1" ).click(function() {
      $( "#effect" ).addClass( "newClass", 1000, callback );
    });
 
    function callback() {
      setTimeout(function() {
        $( "#effect" ).removeClass( "newClass" );
      }, 1500 );
    }
  });
  </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        jQuery UI與 ASP.NET控制項的搭配<br />
        <br />
        範例來源：<a href="https://jqueryui.com/addClass/">https://jqueryui.com/addClass/</a><br />
        <br />
        本範例將 &lt;button&gt;<span class="auto-style1"><strong>改成 ASP.NET Button控制項</strong></span>而<strong>失敗！</strong><br />
        
        <!-- ************************** -->        
        <div class="toggler">
          <div id="effect" class="ui-corner-all">
              文字變化區。Etiam libero neque, luctus a, eleifend nec, semper at, lorem. Sed pede.
          </div>
        </div>
        <!-- ************************** -->        

        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="ASP.NET Button，按下去將會觸發 PostBack" class="ui-state-default ui-corner-all" />
    
    </div>
    </form>
</body>
</html>
