﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="jQuery_HtmlPage3_Error.aspx.cs" Inherits="Book_Sample_jQuery_UI_jQuery_HtmlPage3_Error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ASP.NET的Button控制項，本範例的效果出不來！</title>

    <script type="text/javascript" src="Scripts/jquery-1.8.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Button1").click(function () {
                $("h2").css("color", "red");
            });
        });

    </script>

    <style type="text/css">
        .auto-style1 {
            color: #FFFFFF;
            background-color: #FF3399;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            這是一個簡單的 jQuery語法練習。<strong><span class="auto-style1">錯誤版</span></strong><br />
            <br />
            <hr />

            <h2>MIS2000 Lab.</h2>
            <h2>簡單的 jQuery語法練習。</h2>
            <br />
            <br />
        </div>

        ASP.NET的Button控制項，本範例的效果出不來！<strong><span class="auto-style1">一閃即逝！</span></strong><br />

        <asp:Button ID="Button1" runat="server" Text="Button_本範例的效果出不來！" />


        <br />
        <br />
    </form>
</body>
</html>
