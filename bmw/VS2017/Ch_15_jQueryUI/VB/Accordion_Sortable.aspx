﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Accordion_Sortable.aspx.cs" Inherits="Book_Sample_jQuery_UI_Accordion_Sortable" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>手風琴折疊</title>
    <link rel="stylesheet" href="jQuery_JS_CSS/jquery-ui.css" />
    <script src="jQuery_JS_CSS/jquery-1.9.1.js"></script>
    <script src="jQuery_JS_CSS/jquery-ui.js"></script>

    <style>
          /* IE has layout issues when sorting (see #5413) */
          .group { zoom: 1 }
  </style>

    <script>
        $(function () {
            $("#accordion")
              .accordion({
                  header: "> div > h3"
              })
              .sortable({
                  axis: "y",
                  handle: "h3",
                  stop: function (event, ui) {
                      // IE doesn't register the blur when sorting
                      // so trigger focusout handlers to remove .ui-state-focus
                      ui.item.children("h3").triggerHandler("focusout");
                  }
              });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            資料來源 <a href="http://jqueryui.com/accordion/#sortable">http://jqueryui.com/accordion/#sortable</a><br />
            <br />
            可以「移動」標題，重新排序<br />
            <br />
            <br />

            <div id="accordion">

                <div class="group">  <!-- ** 新加入的標籤** -->
                    <h3>Section 1</h3>
                    <div>
                        <p>
                            === Section 1 ===<br />
                            === Section 1 ===<br />
                            === Section 1 ===<br />
                        </p>
                    </div>
                </div>

                <div class="group">  <!-- ** 新加入的標籤** -->
                    <h3>Section 2</h3>
                    <div>
                        <p>
                            === Section 2 ===<br />
                            === Section 2 ===<br />
                            === Section 2 ===<br />
                        </p>
                    </div>
                </div>

                <div class="group">  <!-- ** 新加入的標籤** -->
                    <h3>Section 3</h3>
                    <div>
                        <p>
                            === Section 3 ===<br />
                            === Section 3 ===<br />
                            === Section 3 ===<br />
                        </p>
                    </div>
                </div>

                <div class="group">  <!-- ** 新加入的標籤** -->
                    <h3>Section 4</h3>
                    <div>
                        <p>
                            === Section 4 ===<br />
                            === Section 4 ===<br />
                            === Section 4 ===<br />
                        </p>
                    </div>
                </div>

            </div>



        </div>
    </form>
</body>
</html>
