﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Datepicker_Animations.aspx.cs" Inherits="Book_Sample_jQuery_UI_Datepicker_Animations" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>日曆</title>
    <link rel="stylesheet" href="jQuery_JS_CSS/jquery-ui.css" />
    <script src="jQuery_JS_CSS/jquery-1.9.1.js"></script>
    <script src="jQuery_JS_CSS/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#TextBox1").datepicker();

            //動畫效果
            $("#DropDownList1").change(function () {
                $("#TextBox1").datepicker("option", "showAnim", $(this).val());
            });
        });
    </script>
    </head>
<body>
    <form id="form1" runat="server">
        <div>
            資料來源 <a href="http://jqueryui.com/datepicker/#animation">http://jqueryui.com/datepicker/#animation</a><br />
            <br />
            <p>
                &nbsp;<br />
                選擇動畫效果：<asp:DropDownList ID="DropDownList1" runat="server">
                    <asp:ListItem Value="show">Show (default)</asp:ListItem>
                    <asp:ListItem Value="slideDown">Slide down</asp:ListItem>
                    <asp:ListItem Value="fadeIn">Fade in</asp:ListItem>
                    <asp:ListItem Value="blind">Blind (UI Effect)</asp:ListItem>
                    <asp:ListItem Value="bounce">Bounce (UI Effect)</asp:ListItem>
                    <asp:ListItem Value="clip">Clip (UI Effect)</asp:ListItem>
                    <asp:ListItem Value="drop">Drop (UI Effect)</asp:ListItem>
                    <asp:ListItem Value="fold">Fold (UI Effect)</asp:ListItem>
                    <asp:ListItem Value="slide">Slide (UI Effect)</asp:ListItem>
                    <asp:ListItem Value="">None (無)</asp:ListItem>
                </asp:DropDownList>

            </p>
            <p>&nbsp;<br /><br /><br /></p>


            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>

        </div>

    </form>
</body>
</html>
