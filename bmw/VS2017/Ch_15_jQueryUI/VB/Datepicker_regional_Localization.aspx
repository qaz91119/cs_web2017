﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Datepicker_regional_Localization.aspx.cs" Inherits="Book_Sample_jQuery_UI_Datepicker_region" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>日曆</title>
    <link rel="stylesheet" href="jQuery_JS_CSS/jquery-ui.css" />
    <script src="jQuery_JS_CSS/jquery-1.9.1.js"></script>
    <script src="jQuery_JS_CSS/jquery-ui.js"></script>
    <script>
        $(function () {

            $("#TextBox3").datepicker($.datepicker.regional["zh-TW"]);
            //記得加上底下的繁體中文 CSS。 檔名 jquery.ui.datepicker-zh-TW.js


            $("#TextBox4").datepicker({
                showWeek: true,
                firstDay: 1
            }); //出現「週數」。一年52週。
        });
    </script>
    <script src="jQuery_JS_CSS/jquery.ui.datepicker-zh-TW.js"></script>

    <style type="text/css">
        .auto-style1 {
            color: #FF0000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p>===========================================</p>
            <p>
                <a href="http://jqueryui.com/datepicker/#localization">http://jqueryui.com/datepicker/#localization</a>&nbsp;
            </p>
            <p>
                正體中文的日曆，請寫成：
            </p>
            <p>
                $(&quot;#TextBox3&quot;).datepicker(<strong>$.datepicker<span class="auto-style1">.regional[&quot;zh-TW&quot;]</span></strong>);
            </p>
            <p>
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>

            </p>
            <p>&nbsp;</p>

            <p>===========================================</p>
            <p>週數（一年52週）&nbsp; <a href="http://jqueryui.com/datepicker/#show-week">http://jqueryui.com/datepicker/#show-week</a></p>
            <p>$(&quot;#TextBox4&quot;).datepicker(<strong>$.datepicker<span class="auto-style1">.regional[&quot;zh-TW&quot;]</span></strong>);
            </p>
            <p>
                <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>

            </p>


        </div>
    </form>
</body>
</html>
