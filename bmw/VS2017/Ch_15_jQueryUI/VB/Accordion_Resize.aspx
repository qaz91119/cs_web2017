﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Accordion_Resize.aspx.cs" Inherits="Book_Sample_jQuery_UI_Accordion_Resize" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>手風琴折疊</title>
    <link rel="stylesheet" href="jQuery_JS_CSS/jquery-ui.css" />
    <script src="jQuery_JS_CSS/jquery-1.9.1.js"></script>
    <script src="jQuery_JS_CSS/jquery-ui.js"></script>
    <style>
          #accordion-resizer {
            padding: 10px;
            width: 450px;
            height: 250px;
          }
  </style>
    <script>
        $(function () {
            $("#accordion").accordion({
                heightStyle: "fill"
            });
        });
        $(function () {
            $("#accordion-resizer").resizable({
                minHeight: 140,
                minWidth: 200,
                resize: function () {
                    $("#accordion").accordion("refresh");
                }
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            資料來源 <a href="http://jqueryui.com/accordion/#fillspace">http://jqueryui.com/accordion/#fillspace</a><br />
            <br />
            用滑鼠指向框框的四個邊角，可以放大、縮小（Re-size）<br />
            <br />

            <div id="accordion-resizer" class="ui-widget-content">

                    <div id="accordion">
                        <h3>Section 1</h3>
                        <div>
                            <p>
                                === Section 1 ===<br />
                                === Section 1 ===<br />
                                === Section 1 ===<br />
                            </p>
                        </div>
                        <h3>Section 2</h3>
                        <div>
                            <p>
                                === Section 2 ===<br />
                                === Section 2 ===<br />
                                === Section 2 ===<br />
                            </p>
                        </div>
                        <h3>Section 3</h3>
                        <div>
                            <p>
                                === Section 3 ===<br />
                                === Section 3 ===<br />
                                === Section 3 ===<br />
                            </p>
                        </div>
                        <h3>Section 4</h3>
                        <div>
                            <p>
                                === Section 4 ===<br />
                                === Section 4 ===<br />
                                === Section 4 ===<br />
                            </p>
                        </div>
                    </div>

            </div>
                


        </div>
    </form>
</body>
</html>

