﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Datepicker_FormatDate.aspx.cs" Inherits="Book_Sample_jQuery_UI_Datepicker_FormatDate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>日曆</title>
    <link rel="stylesheet" href="jQuery_JS_CSS/jquery-ui.css" />
    <script src="jQuery_JS_CSS/jquery-1.9.1.js"></script>
    <script src="jQuery_JS_CSS/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#TextBox1").datepicker();

            //日期格式
            $("#DropDownList1").change(function () {
                $("#TextBox1").datepicker("option", "dateFormat", $(this).val());
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            資料來源 <a href="http://jqueryui.com/datepicker/#date-formats">http://jqueryui.com/datepicker/#date-formats</a><br />
            <br />
            <p>
                &nbsp;<br />
                選擇日期格式：<asp:DropDownList ID="DropDownList1" runat="server">
                    <asp:ListItem Value="mm/dd/yy">Default - mm/dd/yy</asp:ListItem>
                    <asp:ListItem Value="yy-mm-dd">ISO 8601 - yy-mm-dd</asp:ListItem>
                    <asp:ListItem Value="d M, y">Short - d M, y</asp:ListItem>
                    <asp:ListItem Value="d MM, y">Medium - d MM, y</asp:ListItem>
                    <asp:ListItem Value="DD, d MM, yy">Full - DD, d MM, yy</asp:ListItem>
                    <asp:ListItem Value="day' d 'of' MM 'in the year' yy">With text - 'day' d 'of' MM 'in the year' yy</asp:ListItem>
                </asp:DropDownList>

            </p>
            <p>&nbsp;<br /><br /><br /></p>


            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>

        </div>

    </form>
</body>
</html>
