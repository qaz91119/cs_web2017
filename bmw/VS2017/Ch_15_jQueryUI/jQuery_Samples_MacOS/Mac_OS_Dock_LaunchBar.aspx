﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Mac_OS_Dock_LaunchBar.aspx.cs" Inherits="Book_Sample_jQuery_Samples_Mac_OS_Dock_LaunchBar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>

		<style>
			body {
				background-color: #444;
			}
		    .auto-style1 {
                color: #FFFFFF;
            }

			
			/* Dock */
			#dock {
				display: block;
				margin: 30px auto;
				top: 0px;
				width: 100%;
				text-align: center;
				overflow: hidden;
			}

			#dock ul {
				margin: 0px -60px;
				padding: 0px;
				list-style: none;
				height: 198px;
				line-height: 198px;
				text-align: center;
				white-space: nowrap;
				display: inline-block;
				background: url("img/dock-middle.png") bottom left repeat-x;
			}

			#dock ul:before,
			#dock ul:after {
				content: "";
				color: transparent;
				display: inline-block;
				width: 0px;
				padding-top: 60px;
				margin-top: -60px;
				vertical-align: bottom;
			}

			#dock ul:before {
				padding-left: 60px;
				margin-left: -60px;
				background: url("img/dock-left.png") bottom left repeat-x;
			} 

			#dock ul:after {
				padding-right: 60px;
				margin-right: -60px;
				background: url("img/dock-right.png") bottom right repeat-x;
			}

			/* Dock Icons */
			#dock li {
				display: inline-block;
				vertical-align: bottom;
				margin: 0px;
				padding: 0px;
				position: relative;
				overflow: visible;
			}

			#dock li.seperator {
				background: url("img/dock-seperator.png") bottom left no-repeat;
				width: 20px;
				height: 158px;
				position: relative;
				vertical-align: bottom;
			}

			#dock li a {
				height: 153px;
				width: 48px;
				display: inline-block;
				text-align: center;
				position: relative;
				vertical-align: bottom;
				text-decoration: none;
				color: black;
				white-space: normal;
				letter-spacing: normal;	
				line-height: 1.3em;
				text-align: center;
				font-family: Arial;
				padding-top: 40px;
				margin-bottom: 5px;
				overflow: visible;
			}
			
			body.no_js #dock li a:hover {
				width: 128px !important;
			}

			#dock li a span {
				position: relative;
				line-height: 1.3em;
				display: none;
				text-align: center;
				font-weight: bold;
				font-size: 80%;
				padding: 4px 12px;
				background-color: #EEE;
				
				opacity: 0.4;
				-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=40)";
				
				margin: 0px auto;
				
				top: -40px;
				
				border-radius: 10px;
				-moz-border-radius: 10px;
				-webkit-border-radius: 10px;
			}

			#dock li a:hover span {
				display: inline-block;
			}

			#dock li a:hover span:after {
				display: block;
				height: 9px;
				color: transparent;
				background: transparent url("img/span.png") center top no-repeat;
				position: absolute;
				content: "";
				text-align: center;
				margin: 0px;
				padding: 0px;
				width: 100%;
				margin-left: -16px;
			}

			#dock li a img {
				height: auto;
				width: 100%;
				position: absolute;
				left: 0px;
				bottom: 10px;
				text-align: center;
				border: 0px none;
				margin-bottom: 15px;
				
				box-reflect: below 1px gradient(linear, left top, left bottom, from(transparent), color-stop(0.5, transparent), to(rgba(255,255,255,0.1)));
				-webkit-box-reflect: below 1px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(0.5, transparent), to(rgba(255,255,255,0.1)));
				-moz-box-reflect: below 1px -moz-gradient(linear, left top, left bottom, from(transparent), color-stop(0.5, transparent), to(rgba(255,255,255,0.1)));
			}
		</style>

		<!--[if lte IE 7]>
		<style>
			/* Inline block fix */
			#dock ul {
				display: inline;
				zoom: 1;
			}

			#dock li, #dock li a {
				display: inline;
				zoom: 1;
			}

			/* Image quality fix */
			img {
				-ms-interpolation-mode: bicubic;
			}

			#dock li a span {
				filter: alpha(opacity=40);
			}
		</style>
		<![endif]-->
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
		<script>
		    //library
		    function distance(x0, y0, x1, y1) {
		        var xDiff = x1-x0;
		        var yDiff = y1-y0;
				
		        return Math.sqrt(xDiff*xDiff + yDiff*yDiff);
		    }
			
		    $(document).ready(function() {
		        var proximity = 180;
		        var iconSmall = 48, iconLarge = 128; //css also needs changing to compensate with size
		        var iconDiff = (iconLarge - iconSmall);
		        var mouseX, mouseY;
		        var dock = $("#dock");
		        var animating = false, redrawReady = false;
				
		        $(document.body).removeClass("no_js");
				
		        //below are methods for maintaining a constant 60fps redraw for the dock without flushing
		        $(document).bind("mousemove", function(e) {
		            if (dock.is(":visible")) {
		                mouseX = e.pageX;
		                mouseY = e.pageY;
					
		                redrawReady = true;
		                registerConstantCheck();
		            }
		        });
				
		        function registerConstantCheck() {
		            if (!animating) {
		                animating = true;
						
		                window.setTimeout(callCheck, 15);
		            }
		        }
				
		        function callCheck() {
		            sizeDockIcons();
					
		            animating = false;
				
		            if (redrawReady) {
		                redrawReady = false;
		                registerConstantCheck();
		            }
		        }
				
		        //do the maths and resize each icon
		        function sizeDockIcons() {
		            dock.find("li").each(function() {
		                //find the distance from the center of each icon
		                var centerX = $(this).offset().left + ($(this).outerWidth()/2.0);
		                var centerY = $(this).offset().top + ($(this).outerHeight()/2.0);
						
		                var dist = distance(centerX, centerY, mouseX, mouseY);
						
		                //determine the new sizes of the icons from the mouse distance from their centres
		                var newSize =  (1 - Math.min(1, Math.max(0, dist/proximity))) * iconDiff + iconSmall;
		                $(this).find("a").css({width: newSize});
		            });
		        }
		    });
		    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <strong><span class="auto-style1">網站首頁，搭配 MacOS-Like的DOCK選單（Launch Bar）<br />
        </span>

        <span class="auto-style1">jQuery的套件來源：</span></strong><a href="http://www.aplweb.co.uk/blog/js/mac-like-icon-dock-v3/">http://www.aplweb.co.uk/blog/js/mac-like-icon-dock-v3/</a> <br /><br /><br />
    
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="id" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="Vertical" PageSize="5">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                <asp:BoundField DataField="test_time" HeaderText="test_time" SortExpression="test_time" />
                <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <RowStyle BackColor="#F7F7DE" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#FBFBF2" />
            <SortedAscendingHeaderStyle BackColor="#848384" />
            <SortedDescendingCellStyle BackColor="#EAEAD3" />
            <SortedDescendingHeaderStyle BackColor="#575357" />
        </asp:GridView>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testConnectionString %>" 
            SelectCommand="SELECT [id], [test_time], [title] FROM [test]"></asp:SqlDataSource>

        <strong><span class="auto-style1">請先完成 ASP.NET畫面之後，再來加入 jQuery特效。</span></strong><br />
    </div>
    </form>


    <!-- ******************************************************************************* -->

    <!-- Note: comments are used between icons to remove white-space when display inline-block is used -->
    <div id="dock">
        <ul>
            <li><a href="#address"><span>Address</span><img src="js-mac-like-icon-dock-v3/img/icon-address.png" alt="[address]" /></a></li>
            <!--
				-->
            <li><a href="#band"><span>Band</span><img src="js-mac-like-icon-dock-v3/img/icon-band.png" alt="[band]" /></a></li>
            <!--
				-->
            <li><a href="../Ch03_WebControls/09_Calendar.aspx"><span>Calendar（日曆控制項，Ch.3）</span><img src="js-mac-like-icon-dock-v3/img/icon-calendar.png" alt="[calendar]" /></a></li>
            <!--
				-->
            <li class="active"><a href="../Ch16/Case_1_Login.aspx"><span>Chat（聊天室）</span><img src="js-mac-like-icon-dock-v3/img/icon-chat.png" alt="[chat]" /></a></li>
            <!--
				-->
            <li class="active"><a href="#music"><span>Music</span><img src="js-mac-like-icon-dock-v3/img/icon-music.png" alt="[music]" /></a></li>
            <!--
				-->
            <li><a href="../Ch18_FileUpload/FileUpload_DB_03_List_jQuery.aspx"><span>Photo（圖片展示。ListView+FileUpload）</span><img src="js-mac-like-icon-dock-v3/img/icon-photo.png" alt="[photo]" /></a></li>
            <!--
				-->
            <li><a href="#text"><span>Text</span><img src="js-mac-like-icon-dock-v3/img/icon-text.png" alt="[text]" /></a></li>
            <!--
				-->
            <li class="seperator"></li>
            <!--
				-->
            <li><a href="../Ch16/Case_1_Login.aspx"><span>Applications（聊天室）</span><img src="js-mac-like-icon-dock-v3/img/icon-applications.png" alt="[apps]" /></a></li>
            <!--
				-->
            <li><a href="#folder?src=/pictures/"><span>Pictures</span><img src="js-mac-like-icon-dock-v3/img/icon-pictures.png" alt="[pictures]" /></a></li>
            <!--
				-->
            <li><a href="#folder?src=/documents/"><span>Documents</span><img src="js-mac-like-icon-dock-v3/img/icon-documents.png" alt="[documents]" /></a></li>
            <!--
				-->
            <li><a href="#bin"><span>Bin</span><img src="js-mac-like-icon-dock-v3/img/icon-bin.png" alt="[bin]" /></a></li>
        </ul>
    </div>

</body>
</html>
