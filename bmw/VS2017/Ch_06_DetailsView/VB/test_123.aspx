﻿<%@ Page Language="VB" %>

<%@ Import NameSpace = "System" %>
<%@ Import NameSpace = "System.Data" %>
<%@ Import NameSpace = "System.Data.SqlClient" %>

<%@ Import NameSpace = "System.Web.Configuration" %>
<!-- 作者註解：
      寫 Inline Code的時候， NameSpace的英文大小寫，千萬不能寫錯。 -->

<%
'--註解：第一，連結SQL資料庫
    'Dim Conn As SqlConnection = New SqlConnection("server=localhost;uid=test; pwd=test; database=test")
    
    '-- 下列寫法，必須搭配 System.Web.Configuration命名空間。
    '-- 連結字串存在 Web.Config檔案裡。
    Dim Conn As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)
    Conn.Open()

'--註解：第二，執行SQL指令，使用DataReader
Dim sqlstr As String = "Select * From test"
Dim cmd As SQLCommand = New SQLCommand(sqlstr ,Conn)
Dim dr As SQLDataReader = cmd.ExecuteReader()

'--註解：第三，自由發揮底下可以簡寫成 dr("欄位名稱")
While dr.Read()
      Response.Write("文章編號：" & dr.Item("id") & “<br>”)
      Response.Write("日    期：" & dr.Item("test_time")  & “<br>”)
      Response.Write("文章標題：" & dr.Item("title"))
      Response.Write("<hr>")
End While

'--註解：第四，關閉資源
cmd.Cancel()
dr.Close

Conn.Close
Conn.Dispose()
%>