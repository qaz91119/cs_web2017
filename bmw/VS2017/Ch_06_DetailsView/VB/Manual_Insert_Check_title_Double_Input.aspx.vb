﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----


Partial Class _Book_Insert_Update_Check_UserName_Double_Input
    Inherits System.Web.UI.Page


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim Conn As SqlConnection = New SqlConnection
        '----上面已經事先寫好 Imports System.Web.Configuration ----
        Conn.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString
        Conn.Open()   '---- 連結DB

        Dim dr As SqlDataReader = Nothing

        Dim cmd As SqlCommand
        cmd = New SqlCommand("select * from test where title = '" & Trim(TextBox2.Text) & "'", Conn)

            dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料

        '////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        '避免資料重複輸入。如果文章 [標題]有重複，就停止。
            If dr.HasRows() Then
                '--找到相同的文章標題，所以程式停止。
                Response.Write("Error~ 找到相同的文章標題，所以程式停止！！")
                Response.End()
            Else
                '-- 沒有找到相同標題的文章，所以可以新增這筆資料
            SqlDataSource1.Insert()
                '--呼叫 SqlDataSource1執行新增資料的命令，相關指令已經寫在HTML裡面了。
                Response.Write("新增資料成功！！")
            End If
        '////////////////////////////////////////////////////////////////////////////////////////////////////////////////


            If Not (dr Is Nothing) Then
                cmd.Cancel()
                '----關閉DataReader之前，一定要先「取消」SqlCommand
                dr.Close()
            End If

            '---- Close the connection when done with it.
            If (Conn.State = ConnectionState.Open) Then
                Conn.Close()
                Conn.Dispose()
            End If


    End Sub
End Class
