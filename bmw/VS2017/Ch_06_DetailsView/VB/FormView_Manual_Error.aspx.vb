﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----


Partial Class _Book_Ch6_Insert_FormView_Manual_Error
    Inherits System.Web.UI.Page

    Sub myDBInit()
        '====從資料庫連結開始，100%都用手寫程式====
        Dim SqlDataSource2 As SqlDataSource = New SqlDataSource
        '== 連結資料庫的連接字串 ConnectionString  ==
        SqlDataSource2.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString
        '== 撰寫SQL指令 ==
        If ViewState("t_id") = Nothing Then
            ViewState("t_id") = 5
        End If
        '-- ViewState一旦沒有值，就會出錯！
        SqlDataSource2.SelectCommand = "SELECT * FROM [test] where id = " & ViewState("t_id")
        '== 執行SQL指令 .select() ==
        SqlDataSource2.DataSourceMode = SqlDataSourceMode.DataSet
        Dim dv As DataView = SqlDataSource2.Select(New DataSourceSelectArguments)

        '============================================
        FormView1.DataSource = dv
        FormView1.DataBind()
        '============================================

        SqlDataSource1.Dispose()
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Not Page.IsPostBack Then

        '== 不要小看這一段 IF判別式喔！他會讓你的資料更新產生很大的變化！！
        '**********************************************
        '== 註解了這個 IF判別式，導致更新時出現異常狀況。
        '**********************************************
        myDBInit()
        'End If
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        ViewState("t_id") = GridView1.SelectedValue   '-- 被選取的那一列資料「主索引鍵」
        myDBInit()
    End Sub

    '====================================================================================
    '==  改變 FormView的畫面模式 ==
    '====================================================================================
    Protected Sub ButtonEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonEdit.Click
        FormView1.ChangeMode(FormViewMode.Edit)  '-- 改變成「編輯」模式
        ButtonEdit.Visible = False
        ButtonCancle.Visible = True
        ButtonUpdate.Visible = True
        myDBInit()
    End Sub


    Protected Sub ButtonUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonUpdate.Click
        '-- 開始更新（Update）資料，寫入資料庫。


        Dim myTextBox_Test_time As TextBox = FormView1.FindControl("TextBox_E_Test_time")
        Response.Write(myTextBox_Test_time.Text)

        
    End Sub

    Protected Sub ButtonCancle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancle.Click
        FormView1.ChangeMode(FormViewMode.ReadOnly)  '-- 取消編輯模式，回到「瀏覽（唯讀）」模式

        ButtonEdit.Visible = True
        ButtonCancle.Visible = False
        ButtonUpdate.Visible = False
        myDBInit()
    End Sub
    '====================================================================================
End Class
