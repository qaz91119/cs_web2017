﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DetailsView_Article_BR_3_ContainerDataItem.aspx.aspx.vb" Inherits="Book_Sample_Ch06_Program__Book_Ch6_Insert_DetailsView_Article_BR_3_ContainerDataItem_aspx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>文章的分段、段落</title>
    <style type="text/css">
        .auto-style1 {
            color: #FF3300;
        }
        .auto-style2 {
            color: #FF0000;
        }
        .auto-style3 {
            background-color: #FFFF00;
        }
    </style>
</head>
<body>
    <p>
        透過 DetailsView來觀看一篇文章，如何讓「<strong>文章的分段、段落</strong>」呈現出來？
    </p>
    <p>
        類似上一個程式的作法---使用 DataBinding Expreesion的作法&nbsp; <span class="auto-style1"><strong>&lt;%#</strong></span> <strong>myArticle()</strong> <span class="auto-style2"><strong>%&gt; </strong></span>
    </p>
    <form id="form1" runat="server">
        <p>
            但 <span class="auto-style2"><strong>HTML畫面之中，改成 &lt;%# myArticle(<span class="auto-style3">Container.DataItem</span>) %&gt;</strong></span>
        </p>
        <p>
            <br />
            先把文章內文（article欄位）轉成 Template樣板
            
            <asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" AutoGenerateRows="False" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="4" DataKeyNames="id" DataSourceID="SqlDataSource1" Height="50px" Width="640px" BorderStyle="None">
                <EditRowStyle BackColor="#009999" ForeColor="#CCFF99" Font-Bold="True" />
                <Fields>
                    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                    <asp:BoundField DataField="test_time" HeaderText="test_time" SortExpression="test_time" />
                    <asp:BoundField DataField="class" HeaderText="class" SortExpression="class" />
                    <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
                    <asp:BoundField DataField="summary" HeaderText="summary" SortExpression="summary" />


                    <asp:TemplateField HeaderText="article" SortExpression="article">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Style="font-weight: 700; color: #0000FF"
                                Text='<%# myArticle(Container.DataItem) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:BoundField DataField="author" HeaderText="author" SortExpression="author" />
                </Fields>
                <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                <RowStyle BackColor="White" ForeColor="#003399" />
            </asp:DetailsView>


            <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                ConnectionString="<%$ ConnectionStrings:testConnectionString %>"
                SelectCommand="SELECT [id], [test_time], [class], [title], [summary], [article], [author] FROM [test]"></asp:SqlDataSource>
        </p>
        <p>
            &nbsp;
        </p>
        <p>
            &nbsp;
        </p>
        <div>
        </div>
    </form>
</body>
</html>
