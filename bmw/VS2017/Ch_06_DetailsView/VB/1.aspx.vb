﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----


Partial Class _Book_Ch6_1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Conn As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)
        Conn.Open()   '==第一、連結資料庫

        Dim cmd As SqlCommand = New SqlCommand("select Top 10 id,test_time,title from test", Conn)
        Dim dr As SqlDataReader = cmd.ExecuteReader()   '==第二、執行SQL指令，取出資料

        '==第三，自由發揮
        While dr.Read()
            Response.Write("文章編號：" & dr("id").ToString() + "<br>")
            Response.Write("日    期：" & dr("test_time").ToString() + "<br>")
            Response.Write("文章標題：" & dr("title").ToString())
            Response.Write("<hr>")
        End While
 
        cmd.Cancel()
        dr.Close()
        Conn.Close()  '==第四、關閉資料庫的連接與相關資源
    End Sub

End Class
