﻿
Partial Class test_GridView_test_1_Manual
    Inherits System.Web.UI.Page

    Sub myDBInit()
        '====從資料庫連結開始，100%都用手寫程式====
        Dim SqlDataSource1 As SqlDataSource = New SqlDataSource
        '== 連結資料庫的連接字串 ConnectionString  ==
        SqlDataSource1.ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString
        '== 撰寫SQL指令 ==
        SqlDataSource1.SelectCommand = "SELECT * FROM [test]"
        '== 執行SQL指令 .select() ==
        SqlDataSource1.DataSourceMode = SqlDataSourceMode.DataSet
        Dim dv As Data.DataView = SqlDataSource1.Select(New DataSourceSelectArguments)

        '============================================
        GridView1.DataSource = dv
        GridView1.DataBind()
        '============================================

        SqlDataSource1.Dispose()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        myDBInit()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '== 資料新增 ==
        Dim SqlDataSource2 As SqlDataSource = New SqlDataSource

        '== 連結資料庫的連接字串 ConnectionString  ==
        SqlDataSource2.ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString

        '== 撰寫SQL指令(Insert Into) ==
        SqlDataSource2.InsertCommand = "Insert into test(title,test_time,class,summary,article,author) values('" & TextBox1.Text & "','" & FormatDateTime(Now(), DateFormat.ShortDate) & "','" & ListBox1.SelectedItem.Value & "','" & TextBox2.Text & "','" & TextBox3.Text & "','" & TextBox4.Text & "')"

        '== 執行SQL指令 / 新增 .Insert() ==
        Dim aff_row As Integer = SqlDataSource2.Insert()

        If aff_row = 0 Then
            Response.Write("資料新增失敗！")
            'MsgBox("資料新增失敗！")
        Else
            Response.Write("資料新增成功！")
            'MsgBox("資料新增成功！")
        End If

        myDBInit() '== GridView的資料重整
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        myDBInit() '== GridView的資料重整
    End Sub

End Class
