﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----


Partial Class Default_book_4_Add_Manual_Parameter
    Inherits System.Web.UI.Page


    Sub myDBInit()
        '====從資料庫連結開始，100%都用手寫程式====
        Dim SqlDataSource1 As SqlDataSource = New SqlDataSource
        '== 連結資料庫的連接字串 ConnectionString  ==
        SqlDataSource1.ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString
        '== 撰寫SQL指令 ==
        SqlDataSource1.SelectCommand = "SELECT * FROM [test]"
        '== 執行SQL指令 .select() ==
        SqlDataSource1.DataSourceMode = SqlDataSourceMode.DataSet
        Dim dv As Data.DataView = SqlDataSource1.Select(New DataSourceSelectArguments)

        '============================================
        GridView1.DataSource = dv.Table
        GridView1.DataBind()
        '============================================

        SqlDataSource1.Dispose()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        myDBInit()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '== 資料新增 ==
        Dim SqlDataSource2 As SqlDataSource = New SqlDataSource

        '== 連結資料庫的連接字串 ConnectionString  ==
        SqlDataSource2.ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString

        '*******************************************************************************
        '== 撰寫SQL指令(Insert Into) == 
        '== (使用參數來做，避免 SQL Injection攻擊)
        SqlDataSource2.InsertParameters.Add("myTitle", TextBox1.Text)
        SqlDataSource2.InsertParameters.Add("myTest_time", FormatDateTime(Now(), DateFormat.ShortDate))
        SqlDataSource2.InsertParameters.Add("myClass", ListBox1.SelectedItem.Value.ToString())
        SqlDataSource2.InsertParameters.Add("mySummary", TextBox2.Text)
        SqlDataSource2.InsertParameters.Add("myArticle", TextBox3.Text)
        SqlDataSource2.InsertParameters.Add("myAuthor", TextBox4.Text)

        SqlDataSource2.InsertCommand = "Insert into test(title,test_time,class,summary,article,author) values(@myTitle,@myTest_time,@myClass,@mysummary,@myArticle,@myAuthor)"
        '==使用 @參數的時候，前後沒有加上單引號（’）。
        '*******************************************************************************

        '== 執行SQL指令 / 新增 .Insert() ==
        Dim aff_row As Integer = SqlDataSource2.Insert()

        If aff_row = 0 Then
            Response.Write("資料新增失敗！")
        Else
            Response.Write("資料新增成功！")
        End If
 
        
        SqlDataSource2.Dispose()

        myDBInit() '== GridView的資料重整
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        myDBInit() '== GridView的資料重整
    End Sub

End Class
