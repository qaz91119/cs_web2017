﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----


Partial Class VS2008_Book_Sample__Book_Ch6_Insert_DetailsView_Manual
    Inherits System.Web.UI.Page


    Sub DBInit()
        '====自己手寫的程式碼， DataAdapter / DataSet ====(Start)
        '----上面已經事先寫好 Imports System.Web.Configuration ----
        '----連結資料庫
        Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString.ToString)

        Dim myAdapter As SqlDataAdapter
        myAdapter = New SqlDataAdapter("select id, test_time, class, title, summary, article, author from test", Conn)

        Dim ds As New DataSet()

        Try  '==== 以下程式，只放「執行期間」的指令！=================
            'Conn.Open()   '---- 不用寫，DataAdapter會自動開啟
            myAdapter.Fill(ds, "test")    '---- 這時候執行SQL指令。取出資料，放進 DataSet。

            DetailsView1.DataSource = ds     '----標準寫法 GridView1.DataSource = ds.Tables("test").DefaultView ----
            DetailsView1.DataBind()

        Catch ex As Exception
            Response.Write("<HR/> Exception Error Message----  " + ex.ToString())
        Finally
            '---- 不用寫，DataAdapter會自動關閉
            'If (Conn.State = ConnectionState.Open) Then
            '  Conn.Close()
            '  Conn.Dispose()
            'End If
        End Try
        '====自己手寫的程式碼， DataAdapter / DataSet ====(End)
    End Sub
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            DBInit()   '---只有第一次執行本程式，才會進入 if判別式內部。
        End If
    End Sub


    Protected Sub DetailsView1_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles DetailsView1.ItemCommand
        If e.CommandName = "Edit" Then
            Label_Msg.Text = "<font color=red>編輯（Edit）模式</font>"
            DetailsView1.ChangeMode(DetailsViewMode.Edit)  '==超級重點！！
            '-- ChangeMode()方法， 參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.detailsview.changemode.aspx
            'DetailsView1.DefaultMode = DetailsViewMode.Edit
            '-- DefaultMode 參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.detailsview.defaultmode.aspx

            DetailsView1.AllowPaging = False
            '-- 處於「編輯模式」，不可以有分頁。
            DBInit()
        End If

        If e.CommandName = "New" Then
            Label_Msg.Text = "<font color=green>新增（Insert）模式</font>"
            DetailsView1.ChangeMode(DetailsViewMode.Insert)  '==超級重點！！
            'DetailsView1.DefaultMode = DetailsViewMode.Insert

            DetailsView1.AllowPaging = False
            '-- 處於「新增模式」，不可以有分頁。
            DBInit()
        End If

        If e.CommandName = "Cancel" Then
            Label_Msg.Text = "<font color=green>你剛剛按下取消（Cancel）按鈕</font>"
            DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)  '==超級重點！！
            'DetailsView1.DefaultMode = DetailsViewMode.ReadOnly

            DetailsView1.AllowPaging = True
            DBInit()
        End If

    End Sub


    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs) Handles DetailsView1.ModeChanging
        'If e.NewMode = DetailsViewMode.Edit Or e.NewMode = DetailsViewMode.Insert Then
        'DetailsView1.AllowPaging = False
        ''-- 處於「編輯模式」與「新增模式」的時候，不可以有分頁。
        'End If

        'If e.NewMode = DetailsViewMode.ReadOnly Then
        '    DetailsView1.AllowPaging = True
        'End If
    End Sub


    Protected Sub DetailsView1_ModeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DetailsView1.ModeChanged
        ''-- 參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.detailsview.modechanged.aspx
        '' Display the current mode in the header row.
        'Select Case DetailsView1.CurrentMode
        'Case DetailsViewMode.Edit
        'DetailsView1.HeaderText = "Edit Mode(編輯模式)"
        'DetailsView1.HeaderStyle.ForeColor = System.Drawing.Color.Red
        'DetailsView1.HeaderStyle.BackColor = System.Drawing.Color.LightGray

        'Case DetailsViewMode.Insert
        'DetailsView1.HeaderText = "Insert Mode(新增模式)"
        'DetailsView1.HeaderStyle.ForeColor = System.Drawing.Color.Green
        'DetailsView1.HeaderStyle.BackColor = System.Drawing.Color.Yellow

        'Case DetailsViewMode.ReadOnly
        'DetailsView1.HeaderText = "Read-Only Mode(唯讀模式，展現資料)"
        'DetailsView1.HeaderStyle.ForeColor = System.Drawing.Color.Blue
        'DetailsView1.HeaderStyle.BackColor = System.Drawing.Color.White

        'Case Else
        'DetailsView1.HeaderText = "Error!"
        'End Select
    End Sub

    
    Protected Sub DetailsView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewPageEventArgs) Handles DetailsView1.PageIndexChanging
        '----分頁----
        If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
            '-- CurrentMode 參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.detailsview.currentmode.aspx
            '--分頁的時候，不可以使用「編輯模式」
            e.Cancel = True
        End If

        DetailsView1.PageIndex = e.NewPageIndex
        Label_Msg.Text = "分頁（PageIndexChanging()事件）.....目前是第" & e.NewPageIndex & "頁（頁數從[零]算起）"
        DBInit()
    End Sub


    'Protected Sub DetailsView1_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DetailsView1.PageIndexChanged
    '-- 寫在這裡也可正常運作。
    '    DBInit()
    'End Sub


    Protected Sub DetailsView1_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdateEventArgs) Handles DetailsView1.ItemUpdating
        ''====自己手寫的程式碼， DataAdapter / DataSet ====(Start)
        '-- 資料更新的 ADO.NET程式碼，請您自己寫。
        ''====自己手寫的程式碼， DataAdapter / DataSet ====(End)


        Label_Msg.Text = "<font color=green>你剛剛按下更新（Update）按鈕</font>"
        Response.Write("更新完畢-----Update")
        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)  '==超級重點！！
        'DetailsView1.DefaultMode = DetailsViewMode.ReadOnly

        DetailsView1.AllowPaging = True
        DBInit()
    End Sub
End Class
