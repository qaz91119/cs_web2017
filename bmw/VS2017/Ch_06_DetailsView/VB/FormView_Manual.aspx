﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FormView_Manual.aspx.vb" Inherits="_Book_Ch6_Insert_FormView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <br />
    100% 自己動手寫程式
    <hr />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" 
            DataSourceID="SqlDataSource1" PageSize="5">
            <Columns>
                <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" 
                    ReadOnly="True" SortExpression="id" />
                <asp:BoundField DataField="test_time" HeaderText="test_time" 
                    SortExpression="test_time" />
                <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testConnectionString %>" 
            SelectCommand="SELECT [id], [test_time], [title] FROM [test]">
        </asp:SqlDataSource>
    <br />
    <br />

    <hr />
    <br />
        <asp:FormView ID="FormView1" runat="server" >
            <EditItemTemplate>
                文章編號：<asp:Label ID="Label_E_id" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                <br />
                日期：<asp:TextBox ID="TextBox_E_Test_time" runat="server" Text='<%# Bind("test_time") %>'></asp:TextBox>
                <br />
                分類：<asp:TextBox ID="TextBox_E_class" runat="server" Text='<%# Bind("class") %>'></asp:TextBox>
                <br />
                <br />
                標題：<asp:TextBox ID="TextBox_E_title" runat="server" Width="400px" Text='<%# Bind("title") %>'></asp:TextBox>
                <br />
                摘要：<asp:TextBox ID="TextBox_E_summary" runat="server" Height="50px" TextMode="MultiLine" 
                    Width="400px" Text='<%# Bind("summary") %>'></asp:TextBox>
                <br />
                內容：<asp:TextBox ID="TextBox_E_article" runat="server" Height="200px" 
                    TextMode="MultiLine" Width="400px" Text='<%# Bind("article") %>'></asp:TextBox>
                <br />
                作者：<asp:TextBox ID="TextBox_E_author" runat="server" Text='<%# Bind("author") %>'></asp:TextBox>
            </EditItemTemplate>
            <InsertItemTemplate>
                日期：<asp:TextBox ID="TextBox_I_Test_time" runat="server"></asp:TextBox>
                <br />
                分類：<asp:TextBox ID="TextBox_I_class" runat="server"></asp:TextBox>
                <br />
                <br />
                標題：<asp:TextBox ID="TextBox_I_title" runat="server" Width="400px"></asp:TextBox>
                <br />
                摘要：<asp:TextBox ID="TextBox_I_summary" runat="server" Height="50px" TextMode="MultiLine" 
                    Width="400px"></asp:TextBox>
                <br />
                內容：<asp:TextBox ID="TextBox_I_article" runat="server" Height="200px" 
                    TextMode="MultiLine" Width="400px"></asp:TextBox>
                <br />
                作者：<asp:TextBox ID="TextBox_I_author" runat="server"></asp:TextBox>
            </InsertItemTemplate>
            <ItemTemplate>
                文章編號：<asp:Label ID="Label_id" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                <br />
                日期：<asp:Label ID="Label_test_time" runat="server" Text='<%# Eval("test_time") %>'></asp:Label>
                <br />
                分類：<asp:Label ID="Label_class" runat="server" Text='<%# Eval("class") %>'></asp:Label>
                <br />
                <br />
                標題：<asp:Label ID="Label_title" runat="server" Text='<%# Eval("title") %>'></asp:Label>
                <br />
                摘要：<asp:Label ID="Label_summary" runat="server" Text='<%# Eval("summary") %>'></asp:Label>
                <br />
                內容：<asp:Label ID="Label_article" runat="server" Text='<%# Eval("article") %>'></asp:Label>
                <br />
                作者：<asp:Label ID="Label_author" runat="server" Text='<%# Eval("author") %>'></asp:Label>
               
               <br />
            </ItemTemplate>
        </asp:FormView>
    <hr />
        <br />
    
    <!-- '*** 自己設計 Button按鈕，修改 FormView的模式 *****************  -->
    <asp:Button ID="ButtonEdit" runat="server" Text="Edit（編輯）" />
    <asp:Button ID="ButtonUpdate" runat="server" Text="Update（更新）" visible=false/>
    <asp:Button ID="ButtonCancle" runat="server" Text="Cancle（取消）" visible=false/>
    </div>
    </form>
</body>
</html>
