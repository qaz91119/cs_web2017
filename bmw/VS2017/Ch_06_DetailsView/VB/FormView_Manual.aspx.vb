﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----

Partial Class _Book_Ch6_Insert_FormView
    Inherits System.Web.UI.Page


    Sub myDBInit()
        '====從資料庫連結開始，100%都用手寫程式====
        Dim SqlDataSource2 As SqlDataSource = New SqlDataSource
        '== 連結資料庫的連接字串 ConnectionString  ==
        SqlDataSource2.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString
        '== 撰寫SQL指令 ==
        If ViewState("t_id") = Nothing Then
            ViewState("t_id") = 5
        End If
        '-- ViewState一旦沒有值，就會出錯！
        SqlDataSource2.SelectCommand = "SELECT * FROM [test] where id = " & ViewState("t_id")
        '== 執行SQL指令 .select() ==
        SqlDataSource2.DataSourceMode = SqlDataSourceMode.DataSet
        Dim dv As DataView = SqlDataSource2.Select(New DataSourceSelectArguments)

        '============================================
        FormView1.DataSource = dv
        FormView1.DataBind()
        '============================================

        SqlDataSource1.Dispose()
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            myDBInit()
        End If
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        ViewState("t_id") = GridView1.SelectedValue   '-- 被選取的那一列資料「主索引鍵」
        myDBInit()
    End Sub

    '====================================================================================
    '==  改變 FormView的畫面模式 ==
    '====================================================================================
    Protected Sub ButtonEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonEdit.Click
        FormView1.ChangeMode(FormViewMode.Edit)  '-- 改變成「編輯」模式
        ButtonEdit.Visible = False
        ButtonCancle.Visible = True
        ButtonUpdate.Visible = True
        myDBInit()
    End Sub


    Protected Sub ButtonUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonUpdate.Click
        '-- 開始更新（Update）資料，寫入資料庫。
        Dim SqlDataSource3 As SqlDataSource = New SqlDataSource

        '== 連結資料庫的連接字串 ConnectionString  ==
        SqlDataSource3.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString

        '*******************************************************************************
        '== 撰寫SQL指令(Insert Into) == 
        '== (使用參數來做，避免 SQL Injection攻擊)
        Dim myLabel_id As Label = FormView1.FindControl("Label_E_id")
        Dim myTextBox_Test_time As TextBox = FormView1.FindControl("TextBox_E_Test_time")
        Dim myTextBox_class As TextBox = FormView1.FindControl("TextBox_E_class")
        Dim myTextBox_title As TextBox = FormView1.FindControl("TextBox_E_title")
        Dim myTextBox_summary As TextBox = FormView1.FindControl("TextBox_E_summary")
        Dim myTextBox_article As TextBox = FormView1.FindControl("TextBox_E_article")
        Dim myTextBox_author As TextBox = FormView1.FindControl("TextBox_E_author")

        SqlDataSource3.UpdateParameters.Add("myID", myLabel_id.Text)
        SqlDataSource3.UpdateParameters.Add("myTest_time", myTextBox_Test_time.Text)
        'SqlDataSource3.UpdateParameters.Add("myTest_time", FormatDateTime(Now(), DateFormat.ShortDate))  '--以目前的時間為日期
        SqlDataSource3.UpdateParameters.Add("myTitle", myTextBox_title.Text)
        SqlDataSource3.UpdateParameters.Add("myClass", myTextBox_class.Text)
        SqlDataSource3.UpdateParameters.Add("mySummary", myTextBox_summary.Text)
        SqlDataSource3.UpdateParameters.Add("myArticle", myTextBox_article.Text)
        SqlDataSource3.UpdateParameters.Add("myAuthor", myTextBox_author.Text)

        SqlDataSource3.UpdateCommand = "Update test set test_time = @myTest_time, class=@myClass, title=@mytitle, summary=@mysummary, article=@myArticle, author=@myAuthor where id = @myID"
        '==使用 @參數的時候，前後沒有加上單引號（’）。
        '*******************************************************************************

        '== 執行SQL指令 / 新增 .Insert() ==
        Dim aff_row As Integer = SqlDataSource3.Update()

        If aff_row = 0 Then
            Response.Write("資料新增失敗！")
        Else
            Response.Write("資料新增成功！")
        End If

        SqlDataSource3.Dispose()

        '========================================
        '== 更新資料完成！
        FormView1.ChangeMode(FormViewMode.ReadOnly)  '-- 改變成「瀏覽（唯讀）」模式

        ButtonEdit.Visible = True
        ButtonCancle.Visible = False
        ButtonUpdate.Visible = False
        myDBInit()

        GridView1.DataBind()
    End Sub

    Protected Sub ButtonCancle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancle.Click
        FormView1.ChangeMode(FormViewMode.ReadOnly)  '-- 取消編輯模式，回到「瀏覽（唯讀）」模式

        ButtonEdit.Visible = True
        ButtonCancle.Visible = False
        ButtonUpdate.Visible = False
        myDBInit()
    End Sub
    '====================================================================================
End Class
