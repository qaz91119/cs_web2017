﻿
Partial Class _Book_Ch6_Insert_Get_InsertData_Id
    Inherits System.Web.UI.Page

    Protected Sub SqlDataSource1_Inserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSource1.Inserted
        '== 新增一筆資料之後，立刻獲得這筆資料的 id值(Primary Key) 

        Label1.Text = e.Command.Parameters("@get_test_id").Value.ToString()
    End Sub
End Class
