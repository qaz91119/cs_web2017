﻿
Partial Class Book_Sample_Ch06_Program__Book_Ch6_Insert_DetailsView_Article_BR_3_ContainerDataItem_aspx
    Inherits System.Web.UI.Page


    '***************************************************
    '*** 底下的 function需要加入引數
    '***************************************************
    Public Function myArticle(record As Object) As String

        Dim result As Object = DataBinder.Eval(record, "article")   '--後面的參數是「欄位名稱」。

        If Not result Is DBNull.Value Then
            Return result.ToString().Replace(vbCrLf, "<br />")
        Else
            Return ""
        End If

    End Function

End Class
