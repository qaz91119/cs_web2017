﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Book_Sample_Ch06_DetailsView_Article_BR : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ////*****相同程式，DetailsView放在 Page_Load事件*** 有效！！！（FormView無效）***********
        Label LB = (Label)DetailsView1.FindControl("Label1");

        LB.Text = LB.Text.Replace("\r\n", "<br />");
    }


    protected void DetailsView1_DataBound(object sender, EventArgs e)
    {
        ////*** 成功！！***（寫在這裡，DetailsView / FormView兩者都可行！）

        //Label LB = (Label)DetailsView1.FindControl("Label1");

        //LB.Text = LB.Text.Replace("\r\n", "<br />");
    }
}