﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DetailsView_Article_BR_2_DataBindingExpression.aspx.cs" Inherits="Book_Sample_B10_CaseStudy_DIY_DetailsView_Article_BR_1_DataBindingExpression" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>文章的分段、段落</title>
    <style type="text/css">
        .auto-style1 {
            color: #FF3300;
        }
        .auto-style2 {
            color: #FF0000;
        }
    </style>
</head>
<body>
    <p>
        透過 DetailsView來觀看一篇文章，如何讓「<strong>文章的分段、段落</strong>」呈現出來？
    </p>
    <p>
        使用 DataBinding Expreesion的作法&nbsp; <span class="auto-style1"><strong>&lt;%#</strong></span> <strong>myArticle()</strong> <span class="auto-style2"><strong>%&gt; </strong></span>
    </p>
    <form id="form1" runat="server">
        <p>
            <br />
            先把文章內文（article欄位）轉成 Template樣板
            
            <asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" AutoGenerateRows="False" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" DataKeyNames="id" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="None" Height="50px" Width="640px" OnDataBound="DetailsView1_DataBound">
                <AlternatingRowStyle BackColor="PaleGoldenrod" />
                <EditRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                <Fields>
                    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                    <asp:BoundField DataField="test_time" HeaderText="test_time" SortExpression="test_time" />
                    <asp:BoundField DataField="class" HeaderText="class" SortExpression="class" />
                    <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
                    <asp:BoundField DataField="summary" HeaderText="summary" SortExpression="summary" />


                    <asp:TemplateField HeaderText="article" SortExpression="article">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Style="font-weight: 700; color: #0000FF" 
                                Text='<%# myArticle() %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:BoundField DataField="author" HeaderText="author" SortExpression="author" />
                </Fields>
                <FooterStyle BackColor="Tan" />
                <HeaderStyle BackColor="Tan" Font-Bold="True" />
                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
            </asp:DetailsView>


            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:testConnectionString %>" 
                SelectCommand="SELECT [id], [test_time], [class], [title], [summary], [article], [author] FROM [test]">
            </asp:SqlDataSource>
        </p>
        <p>
            &nbsp;
        </p>
        <p>
            &nbsp;
        </p>
        <div>
        </div>
    </form>
</body>
</html>
