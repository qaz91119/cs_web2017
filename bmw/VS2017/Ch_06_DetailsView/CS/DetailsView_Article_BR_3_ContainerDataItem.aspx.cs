﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//----自己寫的（宣告）----
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
//----自己寫的----


public partial class Book_Sample_B10_CaseStudy_DIY_DetailsView_Article_BR_3_ContainerDataItem : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //***************************************************
    //*** 底下的 function需要加入引數
    //***************************************************
    public String myArticle(object record)
    {
        object result = DataBinder.Eval(record, "article");  //--後面的參數是「欄位名稱」。

        if (result != DBNull.Value)
        {
            return result.ToString().Replace("\r\n", "<br />");
        }
        else
        {
            return "";
        }       

    }
}