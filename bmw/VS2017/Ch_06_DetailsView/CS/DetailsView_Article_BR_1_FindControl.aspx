﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DetailsView_Article_BR_1_FindControl.aspx.cs" Inherits="Book_Sample_Ch06_DetailsView_Article_BR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>文章的分段、段落</title>
    <style type="text/css">
        .auto-style1 {
            color: #FF0000;
        }
    </style>
</head>
<body>
    <p>
        透過 DetailsView來觀看一篇文章，如何讓「<strong>文章的分段、段落</strong>」呈現出來？</p>
    <p>
        1.&nbsp; 寫在 Page_Load裡面<strong>有效！！</strong>但是 <span class="auto-style1"><strong>FormView無效</strong></span><br />
        2.&nbsp; 寫在 <span class="auto-style1"><strong>DataBound事件</strong></span>內。</p>
    <form id="form1" runat="server">
        <p>
            <br />
            先把文章內文（article欄位）轉成 Template樣板<asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" AutoGenerateRows="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="id" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="Horizontal" Height="50px" OnDataBound="DetailsView1_DataBound" Width="640px">
                <EditRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <Fields>
                    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                    <asp:BoundField DataField="test_time" HeaderText="test_time" SortExpression="test_time" />
                    <asp:BoundField DataField="class" HeaderText="class" SortExpression="class" />
                    <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
                    <asp:BoundField DataField="summary" HeaderText="summary" SortExpression="summary" />

                    <asp:TemplateField HeaderText="article" SortExpression="article">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" style="font-weight: 700; color: #0000FF" 
                                           Text='<%# Bind("article") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="author" HeaderText="author" SortExpression="author" />
                </Fields>
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
            </asp:DetailsView>

            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:testConnectionString %>" 
                SelectCommand="SELECT [id], [test_time], [class], [title], [summary], [article], [author] FROM [test]">
            </asp:SqlDataSource>
        </p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
    <div>
    
    </div>
    </form>
</body>
</html>
