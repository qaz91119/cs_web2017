﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="2_Index_Master.aspx.vb" Inherits="CaseStudy_DIY_2_Index_Master" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> 網站首頁#2 </title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
        .style2
        {
            background-color: #FFCCCC;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <b>Master-Detail的首頁 #2</b></form>
    透過 <b><span class="style1">HTML控制項</span>的 Table<span class="style1"><span 
        class="style2">&lt;table </span>runat=&quot;server&quot;&gt;</span></b>來呈現首頁內容
    <hr />
    <div>
         <br />


        <!-- 放入一個 HTML的Table控制項。注意，他是 runat="server" -->
        <!-- 並非 ASP.NET控制項，如： <asp:Table> -->

         <table id="myTable"  width="90%" CellPadding="5" CellSpacing="0" Border="1" BorderColor="black" runat="server" />

    </div>
    </form>
</body>
</html>
