﻿'----自己寫的（宣告）----
Imports System
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的（宣告）----

Partial Class CaseStudy_DIY_Disp_2_SqlDataSource
    Inherits System.Web.UI.Page


    '== 重點 ==================================
    '== 必須設定為 Public。否則就會視為 Private而發生錯誤。
    Public myTitle, mySummary, myTestTime, myArticle, myAuthor As String
    '=========================================

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim dv As DataView
        dv = CType(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)
        '-- 執行 Select的動作，從資料庫裡面獲得資料。

        myTitle = dv.Table.Rows(0)("title").ToString()
        mySummary = dv.Table.Rows(0)("summary").ToString()
        myTestTime = dv.Table.Rows(0)("test_time").ToString()
        myArticle = dv.Table.Rows(0)("article").ToString()
        myAuthor = dv.Table.Rows(0)("author").ToString()

        '== 重 點！！==
        Page.DataBind()
        '--與網頁（Page）進行 DataBinding，把每一個欄位的資料，透過 DataBinding Expression呈現在畫面上。

    End Sub

End Class
