﻿
Partial Class VS2010_Book_Sample_Ch03_Program__Book_WebControls_String_Format_1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim myDateTime As String = Date.Now.ToLongDateString()
        Dim myInt As Integer = 100000

        Response.Write(String.Format("日期：{0}。金額：{1}", myDateTime, myInt))
        Response.Write("<hr />")
        Response.Write(String.Format("日期：{0:yyyy/MM/dd}。金額：{1:C}", myDateTime, myInt))

    End Sub
End Class
