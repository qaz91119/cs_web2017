﻿'----自己寫的（宣告）----
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----


Partial Class Book_Sample_Ch06_Program__Book_Ch6_Insert_DetailsView_Article_BR_1_DataBindingExpression
    Inherits System.Web.UI.Page


    Public Function myArticle() As String
        '=======微軟SDK文件的範本=======

        '----上面已經事先寫好NameSpace --  Imports System.Web.Configuration ----     
        '----或是寫成下面這一行 (連結資料庫)----
        Dim Conn As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)

        Dim dr As SqlDataReader = Nothing
        Dim cmd As SqlCommand = New SqlCommand("select article from test where id =" & DetailsView1.DataKey.Value, Conn)

        Dim ArticleString As String = ""

        Try     '==== 以下程式，只放「執行期間」的指令！=====================
            '== 第一，連結資料庫。
            Conn.Open()  '---- 這時候才連結DB

            '=============================================
            '== 方法 A ======================================
            ''== 第二，執行SQL指令。
            'dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料

            ''==第三，自由發揮，把執行後的結果呈現到畫面上。
            'dr.Read()
            'ArticleString = dr("article").ToString().Replace(vbCrLf, "<br />")

            '=============================================
            '== 方法 B ======================================
            '== 第二，執行SQL指令。
            Dim result As String = cmd.ExecuteScalar()
            '---- 這時候執行SQL指令，取出第一列、第一個欄位的資料
            '---- 不需要用到 dr.Read()
            '==第三，自由發揮，把執行後的結果呈現到畫面上。
            ArticleString = result.ToString().Replace(vbCrLf, "<br />")

        Catch ex As Exception
            '---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" & ex.ToString() & "<hr />")
        Finally
            ' == 第四，釋放資源、關閉資料庫的連結。
            '---- Always call Close when done reading.
            If Not dr Is Nothing Then
                cmd.Cancel()
                '----關閉DataReader之前，一定要先「取消」SqlCommand
                '參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
                dr.Close()
            End If
            '---- Close the connection when done with it.
            If Conn.State = ConnectionState.Open Then
                Conn.Close()
                Conn.Dispose()    '--- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
            End If

        End Try

        Return ArticleString

    End Function

End Class
