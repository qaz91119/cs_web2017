﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Disp_2_Pre_Html.aspx.vb" Inherits="VS2010_Book_Sample_CaseStudy_DIY_Disp_2_Pre_Html" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title> 查看某一篇文章細部內容 (文章可分段落) </title>
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            background-color: #FF9999;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    查看某一篇文章細部內容 (<span class="style1">文章可分段落，透過 HTML的&lt;pre&gt;標籤</span>) 
    <br /><br />
    <div align="center">
            <asp:Label ID="Label1_title" runat="server" 
                style="font-weight: 700; font-size: xx-large; color: #0033CC"></asp:Label>
    </div>
    <hr />
    <asp:Label ID="Label2_summary" runat="server" 
        style="color: #666666; font-style: italic;"></asp:Label>
    <br />
    <div align="right">
        發表日期：<asp:Label ID="Label3_test_time" runat="server" 
            style="font-style: italic; color: #993333"></asp:Label>
    </div>   
    <hr />
    <br />

    <!-- 重點在此！！！！-->
    <pre>
           <asp:Label ID="Label4_article" runat="server" Width="90%"></asp:Label>
    </pre>
    
    
    <hr />
    <div align="right">
        作者：<asp:Label ID="Label5_author" runat="server"></asp:Label>
    </div>        
    <hr />
    <br />
    </form>
</body>
</html>
