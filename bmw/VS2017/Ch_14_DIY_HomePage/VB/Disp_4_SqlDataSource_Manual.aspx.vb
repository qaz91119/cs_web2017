﻿'----自己寫的----
Imports System
Imports System.Web.Configuration

Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----

Partial Class CaseStudy_DIY_Disp_3_SqlDataSource_Manual
    Inherits System.Web.UI.Page


    '== 重點 ==================================
    '== 必須設定為 Public。否則就會視為 Private而發生錯誤。
    Public myTitle, mySummary, myTestTime, myArticle, myAuthor, my_hit_no As String
    '=========================================

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim SqlDataSource1 As New SqlDataSource
        SqlDataSource1.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString

        If Request("id") = "" Then
            SqlDataSource1.SelectParameters.Add("id", Request("id").ToString())
            'SqlDataSource1.SelectParameters("id").DefaultValue = 5
        Else
            SqlDataSource1.SelectParameters.Add("id", "5")
            '-- 沒有提供文章編號的話，就自己設定一個預設值。
        End If

        SqlDataSource1.SelectCommand = "SELECT [id], [test_time], [title], [summary], [article], [author], [hit_no] FROM [test] WHERE ([id] = @id)"

        SqlDataSource1.DataSourceMode = SqlDataSourceMode.DataSet
        '== 如果 DataSourceMode 屬性設為 DataSet 值，則 .Select()方法會傳回 DataView 物件。

        Dim dv As DataView
        dv = CType(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)
        '-- 執行 .Select()的動作，從資料庫裡面獲得資料。

        myTitle = dv.Table(0).Item("title").ToString()
        mySummary = dv.Table.Rows(0)("summary").ToString()
        myTestTime = dv.Table.Rows(0)("test_time").ToString()
        myArticle = dv.Table.Rows(0)("article").ToString()
        myAuthor = dv.Table.Rows(0)("author").ToString()
 
        my_hit_no  =  dv.Table.Rows(0)("hit_no").ToString()
       '-- 點閱率 ！


        '== 重 點！！==
        Page.DataBind()
        '-- 與網頁（Page）進行 DataBinding，把每一個欄位的資料，透過 DataBinding Expression呈現在畫面上。

    End Sub
End Class
