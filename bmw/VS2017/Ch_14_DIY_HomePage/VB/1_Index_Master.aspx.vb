﻿'----自己寫的（宣告）----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的（宣告）----


Partial Class CaseStudy_DIY_Default_Master
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Conn As SqlConnection = New SqlConnection
        '----上面已經事先寫好 Imports System.Web.Configuration ----
        Conn.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString
        '----或是寫成下面這一行 (連結資料庫)----
        'Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString.ToString())

        Dim dr As SqlDataReader = Nothing

        Dim cmd As SqlCommand
        cmd = New SqlCommand("select top 20 id,test_time,title,summary from test", Conn)
        '---- 或是寫成這一行 Dim cmd As New SqlCommand("select id,test_time,summary,author from test", Conn)

        Try     '==== 以下程式，只放「執行期間」的指令！=====================
            Conn.Open()   '---- 這時候才連結DB

            dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料

            '****************************************(start)
            Dim myTitle, mySummary As String

            While (dr.Read())   '---- 一次讀取一筆記錄，呈現到畫面上。
                myTitle = "<Strong><B><A href=Disp.aspx?id=" & dr.Item("id") & ">" & dr.Item("title") & "</A></B></Strong>"
                mySummary = "<small><font color=#969696>" & dr.Item("summary") & "</font></small>......<A href=Disp.aspx?id=" & dr.Item("id") & ">詳見內文</A>"
                '-- 重點在此。我們以組合字串的方式，串出HTML碼的超連結（也就是<a>…</a>標籤）

                Response.Write("<table class='style1'>")
                Response.Write("  <tr>")
                Response.Write("     <td width='20%'>" & dr.Item("test_time") & "</td>")
                Response.Write("     <td width='80%'>" & myTitle & "</td>")
                Response.Write("  </tr>")
                Response.Write("  <tr>")
                Response.Write("     <td width='20%'>&nbsp;</td>")
                Response.Write("     <td width='80%'>" & mySummary & "</td>")
                Response.Write("  </tr>")
                Response.Write("</table>")
            End While
            '****************************************(end)


        Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" & ex.ToString())

        Finally
            '---- Always call Close when done reading.
            If Not (dr Is Nothing) Then
                cmd.Cancel()
                '----關閉DataReader之前，一定要先「取消」SqlCommand
                '參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
                dr.Close()
            End If

            '---- Close the connection when done with it.
            If (Conn.State = ConnectionState.Open) Then
                Conn.Close()
                Conn.Dispose() '---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
            End If

        End Try
    End Sub
End Class
