﻿
Partial Class _Book_WebControls_String_Format
    Inherits System.Web.UI.Page


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '-- 資料來源：http://msdn.microsoft.com/en-us/library/fht0f5be.aspx

        Dim s As String = String.Format("(C) Currency: . . . . . . . . {0:C}" & "<br>" & _
                "(D) Decimal:. . . . . . . . . {0:D}" & "<br>" & _
                "(E) Scientific: . . . . . . . {1:E}" & "<br>" & _
                "(F) Fixed point:. . . . . . . {1:F}" & "<br>" & _
                "(G) General:. . . . . . . . . {0:G}" & "<br>" & _
                "    (default/預設值):. . . {0} (default = 'G')" & "<br>" & _
                "(N) Number: . . . . . . . . . {0:N}" & "<br>" & _
                "(P) Percent:. . . . . . . . . {1:P}" & "<br>" & _
                "(R) Round-trip: . . . . . . . {1:R}" & "<br>" & _
                "(X) Hexadecimal:. . . . . . . {0:X}" & "<br>", _
                -123, -123.45F)

        Label1.Text = s


        Dim d As String = String.Format("(d) Short date: . . . . . . . {0:d}" & "<br>" & _
                  "(D) Long date:. . . . . . . . {0:D}" & "<br>" & _
                  "(t) Short time: . . . . . . . {0:t}" & "<br>" & _
                  "(T) Long time:. . . . . . . . {0:T}" & "<br>" & _
                  "(f) Full date/short time: . . {0:f}" & "<br>" & _
                  "(F) Full date/long time:. . . {0:F}" & "<br>" & _
                  "(g) General date/short time:. {0:g}" & "<br>" & _
                  "(G) General date/long time: . {0:G}" & "<br>" & _
                  "    (default/預設值):. . . . . . {0} (default = 'G')" & "<br>" & _
                  "(M) Month:. . . . . . . . . . {0:M}" & "<br>" & _
                  "(R) RFC1123:. . . . . . . . . {0:R}" & "<br>" & _
                  "(s) Sortable: . . . . . . . . {0:s}" & "<br>" & _
                  "(u) Universal sortable: . . . {0:u} (invariant)" & "<br>" & _
                  "(U) Universal full date/time: {0:U}" & "<br>" & _
                  "(Y) Year: . . . . . . . . . . {0:Y}" & "<br>", _
                   DateTime.Now())

        Label2.Text = d
    End Sub
End Class
