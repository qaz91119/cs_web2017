﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----

Partial Class CaseStudy_DIY_Disp_5_Message
    Inherits System.Web.UI.Page

    '=====================
    Public Conn As SqlConnection
    '=====================

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNumeric(Request("id")) And Request("id") <> "" Then
            'Dim Conn As SqlConnection
            Conn = New SqlConnection
            '----上面已經事先寫好 Imports System.Web.Configuration ----
            Conn.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString

            Dim dr As SqlDataReader = Nothing
            '**** 重 點！*************************************************
            Dim cmd As New SqlCommand("select * from test where id = " & Request("id"), Conn)
            '************************************************************

            Try
                Conn.Open()   '---- 這時候才連結DB
                dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料

                Dim myArticle As String = Nothing
                '****************************************(start)
                dr.Read()

                Label1_title.Text = dr.Item("title").ToString()
                Label2_summary.Text = dr.Item("summary").ToString()
                Label3_test_time.Text = dr.Item("test_time").ToString()

                '////////////////////////////////////////////////////////////////
                myArticle = dr.Item("article").ToString()
                Label4_article.Text = Replace(myArticle, vbCrLf, "<br>")
                '////////////////////////////////////////////////////////////////

                Label5_author.Text = dr.Item("author").ToString()
                '****************************************(end)

            Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
                Response.Write("<b>Error Message----  </b>" & ex.ToString())
            Finally
                If Not (dr Is Nothing) Then
                    cmd.Cancel()
                    '----關閉DataReader之前，一定要先「取消」SqlCommand
                    dr.Close()
                End If

                Article_Message()

                '-- 展現這一篇文章的讀者留言

                If (Conn.State = ConnectionState.Open) Then
                    Conn.Close()
                    Conn.Dispose() '---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
                End If
            End Try

        Else
            Response.Write("<h2>Error...沒有這篇文章！</h2>")
            Response.End()
            '--程式中斷，不再執行。您也可以改寫成 Return。看看畫面有何不同？
        End If

    End Sub

    '== 列出這篇文章的讀者留言(來自 test_talk資料表)
    Sub Article_Message()

        'Using Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)
        '    Conn.Open()
        Dim dr As SqlDataReader = Nothing

        '**** 重 點！*************************************************
        Using cmd As New SqlCommand("select * from test_talk where test_id = " & Request("id"), Conn)
            dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料
            If dr.HasRows Then
                Label1.Visible = True
                Label2.Visible = True

                While dr.Read()
                    Label2.Text = Label2.Text & "<table>"
                    Label2.Text = Label2.Text & "<tr>"
                    Label2.Text = Label2.Text & "    <td width='20%'>" & dr.Item("test_time") & "</td>"
                    Label2.Text = Label2.Text & "    <td width='80%' bgcolor='#C0C0C0'>訪客名稱：&nbsp;&nbsp;" & dr.Item("author") & "</td>"
                    Label2.Text = Label2.Text & "</tr>"
                    Label2.Text = Label2.Text & "<tr>"
                    Label2.Text = Label2.Text & "    <td width='20%'>&nbsp;</td>"
                    Label2.Text = Label2.Text & "    <td width='80%' bgcolor='#C0C0C0'>" & dr.Item("article") & "</td>"
                    Label2.Text = Label2.Text & "</tr>"
                    Label2.Text = Label2.Text & "</table><br>"
                End While
            End If

        End Using
        '************************************************************
        'End Using  '-- Conn，資料庫連結

    End Sub
End Class
