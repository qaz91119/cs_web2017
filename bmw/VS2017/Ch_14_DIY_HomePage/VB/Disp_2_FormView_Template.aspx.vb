﻿'----自己寫的（宣告）----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的（宣告）----


Partial Class VS2010_Book_Sample_CaseStudy_DIY_HomePage_Disp_2_FormView_Template
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If IsNumeric(Request("id")) And Request("id") <> "" Then
            Dim Conn As SqlConnection = New SqlConnection
            '----上面已經事先寫好 Imports System.Web.Configuration ----
            Conn.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString

            Dim dr As SqlDataReader = Nothing
            '**** 重 點！*************************************************
            Dim cmd As New SqlCommand("select * from test where id = " & Request("id"), Conn)
            '************************************************************

            Try
                Conn.Open()   '---- 這時候才連結DB
                dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料

                '****************************************(start)
                FormView1.DataSource = dr
                FormView1.DataBind()
                '****************************************(end)

            Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
                Response.Write("<b>Error Message----  </b>" & ex.ToString())
            Finally
                If Not (dr Is Nothing) Then
                    cmd.Cancel()
                    '----關閉DataReader之前，一定要先「取消」SqlCommand
                    dr.Close()
                End If
                If (Conn.State = ConnectionState.Open) Then
                    Conn.Close()
                    Conn.Dispose() '---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
                End If
            End Try

        Else
            Response.Write("<h2>Error...沒有這篇文章！</h2>")
            Response.End()
            '--程式中斷，不再執行。您也可以改寫成 Return。看看畫面有何不同？
        End If
    End Sub


    '***  文章分段  ****************************************************
    Protected Sub FormView1_DataBound(sender As Object, e As System.EventArgs) Handles FormView1.DataBound
        Dim lb As Label = FormView1.FindControl("articleLabel")
        Dim lb_mis2000lab As Label = FormView1.FindControl("Label_mis2000lab")

        '////////////////////////////////////////////////////////////////
        lb_mis2000lab.Text = lb.Text.Replace(vbCrLf, "<br />")
        '-- VB語法中的「vbCrLf」，C#請用「 \r\n」來取代。
        '///////////////////////////////////////////////////////////////
    End Sub
End Class
