﻿'----自己寫的（宣告）----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient

Imports System.Web.UI.HtmlControls    '-- HtmlTableRow 會用到這個命名空間！
'----自己寫的----


Partial Class CaseStudy_DIY_2_Index_Master
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim Conn As SqlConnection = New SqlConnection
        '----上面已經事先寫好 Imports System.Web.Configuration ----
        Conn.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString
        '----或是寫成下面這一行 (連結資料庫)----
        'Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString.ToString())

        Dim dr As SqlDataReader = Nothing

        Dim cmd As SqlCommand
        cmd = New SqlCommand("select top 20 id,test_time,title,summary from test", Conn)
        '---- 或是寫成這一行 Dim cmd As New SqlCommand("select id,test_time,summary,author from test", Conn)

        Try     '==== 以下程式，只放「執行期間」的指令！=====================
            Conn.Open()   '---- 這時候才連結DB

            dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料

            '****************************************(start)
            Dim myTitle, mySummary As String

            While (dr.Read())
                myTitle = "<Strong><B><A href=Disp.aspx?id=" & dr.Item("id") & ">" & dr.Item("title") & "</A></B></Strong>"
                mySummary = "<small><font color=#969696>" & dr.Item("summary") & "</font></small>......<A href=Disp.aspx?id=" & dr.Item("id") & ">詳見內文</A>"

               '-- 底下的 BuildNewRow(參數A, 參數B)是自己寫的 Function
                myTable.Rows.Add(BuildNewRow(dr.Item("test_time"), myTitle))
                myTable.Rows.Add(BuildNewRow("", mySummary))
                '-- 一列、一列地加入表格裡面。
            End While
            '****************************************(end)


        Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<HR/>")

        Finally
            '---- Always call Close when done reading.
            If Not (dr Is Nothing) Then
                cmd.Cancel()
                '----關閉DataReader之前，一定要先「取消」SqlCommand
                '參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
                dr.Close()
            End If

            '---- Close the connection when done with it.
            If (Conn.State = ConnectionState.Open) Then
                Conn.Close()
                Conn.Dispose() '---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
            End If

        End Try
    End Sub



    '=================================================
    '-- 只需輸入兩個參數（內容均為文字），就會個別放進兩個 Cells（儲存格）裡面。
    Function BuildNewRow(ByVal Cell1Text As String, ByVal Cell2Text As String) As HtmlTableRow
        '-- 必須用到 Imports System.Web.UI.HtmlControls
        Dim row As New HtmlTableRow()  '--新的一列

        Dim cell1 As New HtmlTableCell()  '--儲存格、格子
        Dim cell2 As New HtmlTableCell()

        'Set the contents of the two cells.
        '--第一個格子，日期（左邊）
        cell1.Controls.Add(New LiteralControl(Cell1Text))
        cell1.Width = "15%"

        'Add the cells to the row.
        row.Cells.Add(cell1)

        '--第二個格子，標題與摘要的詳細內容（右邊）
        cell2.Controls.Add(New LiteralControl(Cell2Text))
        cell2.Width = "85%"
        'Add the cells to the row.
        row.Cells.Add(cell2)

        Return row  '--回傳一個 HtmlTableRow
    End Function

End Class
