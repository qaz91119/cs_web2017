﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="3_Index_Master.aspx.vb" Inherits="CaseStudy_DIY_3_Index_Master" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> 網站首頁#3 </title>
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            background-color: #FFFF00;
        }
        .style2
        {
            color: #009900;
        }
        .style3
        {
            color: #FF0000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <b>Master-Detail的首頁 #3<br />
    </b>
    <br />
&nbsp;&nbsp;&nbsp;
    透過<span class="style1">Table控制項</span> <b>&lt;<span class="style3">asp:table</span> runat=&quot;server&quot;&gt;，<span 
        class="style2">後置程式碼</span></b>來呈現首頁內容
    <hr />
    <div>
         <br />

<!-- 放入一個 ASP.NET的 Table控制項。注意，他是 runat="server" -->
         
         <asp:table id="myTable"  width="90%" CellPadding="5" CellSpacing="0" Border="1" 
             BorderColor="Blue" runat="server" BackColor="#CCFFFF" BorderStyle="Dotted" 
             BorderWidth="3px" />


    </div>
    </form>
</body>
</html>
