﻿
Partial Class Book_Sample_Ch06_Program__Book_Ch6_Insert_FormView_Article_BR
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ''*****相同程式，放在 Page_Load事件無效！！！ DetailsView就成功！***********
        'Dim LB As Label = FormView1.FindControl("articleLabel")
        'LB.Text = LB.Text.Replace(vbCrLf, "<br />")
    End Sub


    Protected Sub FormView1_DataBound(sender As Object, e As EventArgs) Handles FormView1.DataBound
        '*** 成功！！***
        Dim LB As Label = FormView1.FindControl("articleLabel")
        '寫成這樣更好  Dim LB As Label = CType(FormView1.FindControl("articleLabel"), Label)

        LB.Text = LB.Text.Replace(vbCrLf, "<br />")
    End Sub
End Class
