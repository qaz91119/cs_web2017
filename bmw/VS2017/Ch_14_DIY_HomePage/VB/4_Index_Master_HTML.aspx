﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="4_Index_Master_HTML.aspx.vb" Inherits="VS2010_Book_Sample_CaseStudy_DIY_HomePage_4_Index_Master_HTML" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            font-size: x-large;
            font-weight: bold;
        }
        .style2
        {
            font-size: large;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        與美工人員設計好的HTML一起搭配。<br />
        <br />
        <table border="0" width="626px" id="table1" cellspacing="0" cellpadding="0">
            <tr>
                <td background="images/bgOpenTopL.jpg" height="156px" width="76px">
                </td>
                <td height="156px" background="images/bgOpenTopR.jpg" width="550px">
                    <span class="style1">漂亮的網站首頁</span><br />
                    (HTML 網頁與 ASP.NET檔案結合)
                </td>
            </tr>
            <tr>
                <td width="73px" background="images/bgOpenMidTileL.gif">
                </td>
                <td background="images/bgOpenMidTileR.gif">
                </td>
            </tr>
            <tr>
                <td width="73px" background="images/bgOpenMidTileL.gif">
                </td>
                <td background="images/bgOpenMidTileR.gif">
                    <asp:Label ID="Label1" runat="server" Width="553px" EnableViewState="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="73px" background="images/bgOpenMidTileL.gif">
                </td>
                <td background="images/bgOpenMidTileR.gif">
                </td>
            </tr>
            <tr>
                <td width="73px" background="images/bgOpenMidTileL.gif">
                </td>
                <td background="images/bgOpenMidTileR.gif">
                    ===================================================<br />
                    <span class="style2">XYZ公司</span><br />
                    台北市中華路一段七號八樓<br />
                    TEL : (02) 1234-5678<br />
                </td>
            </tr>
            <tr>
                <td width="76px" background="images/bgOpenBotL.jpg" height="173px">
                </td>
                <td background="images/bgOpenMidTileR.gif" width="550px" valign="bottom">
                    <img border="0" src="images/bgOpenBotR.jpg" width="550px" height="28px">
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
