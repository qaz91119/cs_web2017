﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Disp_3_SqlDataSource.aspx.vb" Inherits="CaseStudy_DIY_Disp_2_SqlDataSource" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title> 查看某一篇文章細部內容 </title>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center"> 
            <!-- '標題 -->
            <asp:Label ID="Label1_title" runat="server" Text="<%#myTitle %>" 
                
                style="font-weight: 700; font-size: xx-large; color: #0033CC; background-color: #FFFF66;"></asp:Label>
    </div>
    <hr />
    <asp:Label ID="Label2_summary" runat="server" Text="<%#mySummary %>" 
        style="color: #666666; font-style: italic;"></asp:Label>
    <br />
    <div align="right">
        發表日期：
        <asp:Label ID="Label3_test_time" runat="server" Text="<%#myTestTime %>"
            style="font-style: italic; color: #993333"></asp:Label>
    </div>   
    <hr />
    <br /> 
    <!-- '文章內文 -->
    <asp:Label ID="Label4_article" runat="server" Text="<%#myArticle %>"></asp:Label>
    <hr />
    <div align="right">
        作者：
        <asp:Label ID="Label5_author" runat="server" Text="<%#myAuthor %>"></asp:Label>
    </div>        
    <hr />
    <br />
    
    
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:testConnectionString %>" 
        
        SelectCommand="SELECT [id], [test_time], [title], [summary], [article], [author] FROM [test] WHERE ([id] = @id)">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="3" Name="id" QueryStringField="id" 
                Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    </form>
</body>
</html>
