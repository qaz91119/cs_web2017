﻿'----自己寫的（宣告）----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的（宣告）----


Partial Class VS2010_Book_Sample_CaseStudy_DIY_Disp_2_HitNo_SqlCommand_Manual
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim my_hit_no As Integer = 0  '--點閱數

        If IsNumeric(Request("id")) And Request("id") <> "" Then
            Dim Conn As SqlConnection = New SqlConnection
            '----上面已經事先寫好 Imports System.Web.Configuration ----
            Conn.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString

            Dim dr As SqlDataReader = Nothing

            '**** 重 點！*************************************************
            '**** 改用 SqlCommand的 ＠參數來作
            Dim cmd As New SqlCommand("select * from test where id = @id", Conn)

            cmd.Parameters.Add("@id", SqlDbType.Int)
            cmd.Parameters("@id").Value = CType(Request("id"), Integer)

            '== 參考網址  http://msdn.microsoft.com/zh-tw/library/system.data.sqlclient.sqlcommand.parameters.aspx
            '************************************************************

            Try
                Conn.Open()   '---- 這時候才連結DB
                dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料

                Dim myArticle As String = Nothing
                '****************************************(start)
                dr.Read()

                Label1_title.Text = dr.Item("title").ToString()
                Label2_summary.Text = dr.Item("summary").ToString()
                Label3_test_time.Text = dr.Item("test_time").ToString()

                '//////////// 文章分段 /////////////////////////////////////
                myArticle = dr.Item("article").ToString()
                Label4_article.Text = Replace(myArticle, vbCrLf, "<br>")
                '////////////////////////////////////////////////////////////////

                Label5_author.Text = dr.Item("author").ToString()

                Label6_hit_no.Text = dr.Item("hit_no")  '--點閱數
                my_hit_no = CType(dr.Item("hit_no"), Integer)  '--點閱數
                '****************************************(end)

            Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
                Response.Write("<b>Error Message----  </b>" & ex.ToString())
            Finally
                If Not (dr Is Nothing) Then
                    cmd.Cancel()
                    '----關閉DataReader之前，一定要先「取消」SqlCommand
                    dr.Close()
                End If

                '===============================================
                '== 點閱 / 點擊次數加一
                '==
                '== 改用 SqlCommand的 ＠參數來作
                '===============================================
                Dim cmd1 As New SqlCommand("update test set hit_no = " & (my_hit_no + 1) & " where id = " & Request("id"), Conn)
                cmd1.Parameters.Add("@id", SqlDbType.Int)
                cmd1.Parameters("@id").Value = CType(Request("id"), Integer)

                cmd1.ExecuteNonQuery()
                cmd1.Cancel()


                If (Conn.State = ConnectionState.Open) Then
                    Conn.Close()
                    Conn.Dispose() '---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
                End If
            End Try

        Else
            Response.Write("<h2>Error...沒有這篇文章！</h2>")
            Response.End()
            '--程式中斷，不再執行。您也可以改寫成 Return。看看畫面有何不同？
        End If

    End Sub
End Class
