﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="1_Index_Master_StringFormat.aspx.vb" Inherits="VS2010_Book_Sample_CaseStudy_DIY_HomePage_1_Index_Master_StringFormat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
        .style2
        {
            color: #0000FF;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" enableviewstate="True">
    <div>
    
        1. 改用 <span class="style2"><strong>String.Format()</strong></span>來作，可以避免字串相連的紊亂<br />
        2. 因為自己撰寫 ADO.NET，所以可以把 <span class="style1"><strong>Label控制項</strong></span>的<strong>「EnableViewState」屬性都關閉</strong>。減少 
        ViewState字串的傳輸。<br />
        <br />
    
    </div>
    <asp:Label ID="Label1" runat="server" EnableViewState="False"></asp:Label>
    </form>
</body>
</html>
