﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="2_Index_Master.aspx.cs" Inherits="Book_Sample_CaseStudy_DIY_2_Index_Master" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title> 網站首頁#2 </title>
    <style type="text/css">
        .style1
        {
            background-color: #FF9999;
        }
        .style2
        {
            color: #FF0000;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <b>Master-Detail的首頁 #2</b></form>
    透過 <span class="style2">HTML控制項</span>的<b> <span class="style1">&lt;table runat=&quot;server&quot;&gt;</span></b>來呈現首頁內容
    <hr />
    <div>
         <br />

         <table runat="server" id="myTable"  width="90%" 
         cellpadding="5" cellspacing="0" border="1" bordercolor="black"  />


    </div>
    </form>
</body>
</html>

