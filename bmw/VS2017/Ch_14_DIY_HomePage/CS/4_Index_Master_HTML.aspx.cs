﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//----自己寫的（宣告）----
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
//----自己寫的（宣告）----

public partial class Book_Sample_B10_CaseStudy_DIY_4_Index_Master_HTML : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //----上面已經事先寫好 using System.Web.Configuration ----
        //----(連結資料庫)----
        SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString.ToString());

        SqlDataReader dr = null;

        SqlCommand cmd = new SqlCommand("select top 5 id,test_time,title,summary from test", Conn);

        try     //==== 以下程式，只放「執行期間」的指令！=====================
        {
            Conn.Open();   //---- 這時候才連結DB

            dr = cmd.ExecuteReader();   //---- 這時候執行SQL指令，取出資料

            //****************************************(start)
            string myTitle, mySummary;

            while (dr.Read())
            {
                myTitle = "<Strong><B><A href=Disp.aspx?id=" + dr["id"] + ">" + dr["title"] + "</A></B></Strong>";
                mySummary = "<small><font color=#969696>" + dr["summary"] + "</font></small>......<A href=Disp.aspx?id=" + dr["id"] + ">詳見內文</A>";

                Label1.Text += "<table width=\"520px\">";
                Label1.Text += "  <tr>";

                String s = String.Format("{0:yyyy/MM/dd}", (System.DateTime)dr["test_time"]);
                // C#比較麻煩，必須把日期，強制轉成日期時間型態，才能搭配 String.Format。
                // 另外一篇很棒的參考資料：  http://blog.darkthread.net/post-2009-04-01-date-formate-slash.aspx
                Label1.Text += "     <td width='20%'>" + s + "</td>";
                Label1.Text += "     <td width='80%'>" + myTitle + "</td>";
                Label1.Text += "  </tr>";
                Label1.Text += "  <tr>";
                Label1.Text += "     <td width='20%'>&nbsp;</td>";
                Label1.Text += "     <td width='80%'>" + mySummary + "</td>";
                Label1.Text += "  </tr>";
                Label1.Text += "</table>";
            }
            //****************************************(end)

        }
        catch (Exception ex)   //---- 如果程式有錯誤或是例外狀況，將執行這一段
        {
            Response.Write("<b>Error Message----  </b>" + ex.ToString());
        }
        finally
        {
            //---- Always call Close when done reading.
            if (dr != null)
            {
                cmd.Cancel();
                //----關閉DataReader之前，一定要先「取消」SqlCommand
                //參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
                dr.Close();
            }

            //---- Close the connection when done with it.
            if (Conn.State == ConnectionState.Open)
            {
                Conn.Close();
                Conn.Dispose();  //---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
            }
        }
    }
}