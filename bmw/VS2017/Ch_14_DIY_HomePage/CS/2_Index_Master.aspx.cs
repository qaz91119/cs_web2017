﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//----自己寫的（宣告）----
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;   //-- HtmlTableRow 會用到這個命名空間！
//----自己寫的（宣告）----


public partial class Book_Sample_CaseStudy_DIY_2_Index_Master : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlConnection Conn = new SqlConnection();
        //----上面已經事先寫好 Imports System.Web.Configuration ----
        Conn.ConnectionString = WebConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString;
        //----或是寫成下面這一行 (連結資料庫)----
        //SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString.ToString());

        SqlDataReader dr = null;

        SqlCommand cmd = new SqlCommand("select top 20 id,test_time,title,summary from test", Conn);

        try     //==== 以下程式，只放「執行期間」的指令！=====================
        {
            Conn.Open();   //---- 這時候才連結DB

            dr = cmd.ExecuteReader();   //---- 這時候執行SQL指令，取出資料

            //****************************************(start)
            string myTitle, mySummary;

            while (dr.Read())
            {
                myTitle = "<Strong><B><A href=Disp.aspx?id=" + dr["id"] + ">" + dr["title"] + "</A></B></Strong>";
                mySummary = "<small><font color=#969696>" + dr["summary"] + "</font></small>......<A href=Disp.aspx?id=" + dr["id"] + ">詳見內文</A>";

                //== 底下的 BuildNewRow(參數A, 參數B)是自己寫的 Function ==
                myTable.Rows.Add(BuildNewRow(dr["test_time"].ToString(), myTitle));
                myTable.Rows.Add(BuildNewRow("", mySummary));
            }
            //****************************************(end)

        }
        catch (Exception ex)   //---- 如果程式有錯誤或是例外狀況，將執行這一段
        {
            Response.Write("<b>Error Message----  </b>" + ex.ToString());
        }            
        finally
        {
            //---- Always call Close when done reading.
            if (dr != null)
            {
                cmd.Cancel();
                //----關閉DataReader之前，一定要先「取消」SqlCommand
                //參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
                dr.Close();
            }

            //---- Close the connection when done with it.
            if (Conn.State == ConnectionState.Open) 
            {
                Conn.Close();
                Conn.Dispose();  //---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
            }
        }
    }



//=================================================
//—只需輸入兩個參數（內容均為文字），就會個別放進兩個 Cells（儲存格）裡面。

    HtmlTableRow BuildNewRow(string Cell1Text, string Cell2Text)
    {   //必須用到 using System.Web.UI.HtmlControls;。
        HtmlTableRow row = new HtmlTableRow();  //-- 新的一列

        HtmlTableCell cell1 = new HtmlTableCell();  //-- 新的儲存格
        HtmlTableCell cell2 = new HtmlTableCell();

        // Set the contents of the two cells.
        //--第一個格子，日期（左邊）
        cell1.Controls.Add(new LiteralControl(Cell1Text));
        cell1.Width = "15%";
        // Add the cells to the row.
        row.Cells.Add(cell1);

        //--第二個格子，標題與摘要的詳細內容（右邊）
        cell2.Controls.Add(new LiteralControl(Cell2Text));
        cell2.Width = "85%";
       // Add the cells to the row.
        row.Cells.Add(cell2);

        return row;  //--回傳一個 HtmlTableRow
    }

}