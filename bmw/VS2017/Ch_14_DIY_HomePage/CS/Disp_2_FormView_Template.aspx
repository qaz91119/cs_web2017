﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Disp_2_FormView_Template.aspx.cs" Inherits="Book_Sample_CaseStudy_DIY_Disp_2_Template" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        以大型控制項（如 FormView）的「樣版」為例，作<span class="style1"><strong>文章的分段</strong></span>。<br />
        <br />
        本範例使用到<strong> .FindControl()方法</strong>。<br />
        <br />
        <br />
        <br />
        <asp:FormView ID="FormView1" runat="server" BackColor="LightGoldenrodYellow" 
            BorderColor="Tan" BorderWidth="1px" CellPadding="2" DataKeyNames="id" 
            ForeColor="Black" Width="688px" ondatabound="FormView1_DataBound">

            <EditRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
            <FooterStyle BackColor="Tan" />
            <HeaderStyle BackColor="Tan" Font-Bold="True" />
            <ItemTemplate>
                id:
                <asp:Label ID="idLabel" runat="server" Text='<%#Bind("id") %>' 
                    style="font-weight: 700; color: #FF0000" />
                <br />
                test_time:<br />
                <asp:Label ID="test_timeLabel" runat="server" Text='<%#Bind("test_time", "{0:yyyy/MM/dd}") %>' style="color: #0000FF" />
                <br />
                title:<br />
                <asp:Label ID="titleLabel" runat="server" Text='<%#Bind("title") %>' style="font-weight: 700; color: #0000FF; font-size: large" />
                <br />
                summary:<br />
                <asp:Label ID="summaryLabel" runat="server" Text='<%#Bind("summary") %>' style="font-style: italic; color: #0000FF" />
                <br />
                <br />
                ===============================================<br />
                article:（透過 DataBinding Expression來作）<br />
                <asp:Label ID="articleLabel" runat="server" Text='<%#Bind("article") %>' Style="font-weight: 700;
                    color: #006600" />
                <br />
                ===============================================<br />
                *** 以下是透過後置程式碼，改寫過的（功能：文章分段）***<br /><hr />
                <asp:Label ID="Label_mis2000lab" runat="server" />
                <br />
                ===============================================<br />
                <br />
                author:<br />
                <asp:Label ID="authorLabel" runat="server" Text='<%#Bind("author") %>' style="font-weight: 700; background-color: #FFFF00" />
                <br />
            </ItemTemplate>
            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" 
                HorizontalAlign="Center" />
        </asp:FormView>


    
    </div>
    </form>
</body>
</html>
