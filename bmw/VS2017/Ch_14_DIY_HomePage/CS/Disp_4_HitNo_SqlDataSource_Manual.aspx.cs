﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//----自己寫的----
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
//----自己寫的----


public partial class Book_Sample_CaseStudy_DIY_Disp_4_HitNo_SqlDataSource_Manual : System.Web.UI.Page
{

    //== 重點 ==================================
    //== 必須設定為 public。否則就會視為 private而發生錯誤。
    public String myTitle, mySummary, myTestTime, myArticle, myAuthor, my_hit_no;
    //=========================================


    protected void Page_Load(object sender, EventArgs e)
    {
       SqlDataSource SqlDataSource1 = new SqlDataSource();
        SqlDataSource1.ConnectionString = WebConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString;


        if (Request["id"] != null)
        {
            SqlDataSource1.SelectParameters.Add("id", Request["id"].ToString());
            //SqlDataSource1.SelectParameters["id"].DefaultValue = "5";
        }
        else
        {
            SqlDataSource1.SelectParameters.Add("id", "5");
        }
        

        SqlDataSource1.SelectCommand = "SELECT [id], [test_time], [title], [summary], [article], [author], [hit_no] FROM [test] WHERE ([id] = @id)";

        SqlDataSource1.DataSourceMode = SqlDataSourceMode.DataSet;
        //== 如果 DataSourceMode 屬性設為 DataSet 值，則 Select 方法會傳回 DataView 物件。

        DataView dv = (DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty);
        //-- 執行 Select的動作，從資料庫裡面獲得資料。

        myTitle = dv.Table.Rows[0]["title"].ToString();
        mySummary = dv.Table.Rows[0]["summary"].ToString();
        myTestTime = dv.Table.Rows[0]["test_time"].ToString();
        myArticle = dv.Table.Rows[0]["article"].ToString();
        myAuthor = dv.Table.Rows[0]["author"].ToString();
        my_hit_no = dv.Table.Rows[0]["hit_no"].ToString();

        //== 重 點！！==
        Page.DataBind();
        //--與網頁（Page）進行 DataBinding，把每一個欄位的資料，透過 DataBinding Expression呈現在畫面上。
    }
}