﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Disp_4_HitNo_SqlDataSource_Manual.aspx.cs" Inherits="Book_Sample_CaseStudy_DIY_Disp_4_HitNo_SqlDataSource_Manual" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title> 查看某一篇文章細部內容（自己手寫 SqlDataSouce程式） </title>
    <style type="text/css">
        .style1
        {
            color: #CC3300;
            font-weight: bold;
        }
        .style2
        {
            color: #0000FF;
            font-weight: bold;
            background-color: #FFFF00;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    查看某一篇文章細部內容（<span class="style1">自己手寫 SqlDataSouce程式 + </span>
    <span class="style2">點閱次數</span>） 
    <br />
    <br />
    <div align="center">
            <asp:Label ID="Label1_title" runat="server" Text="<%#myTitle %>" 
                style="font-weight: 700; font-size: xx-large; color: #CC3300"></asp:Label>
    </div>
    <hr />
    <asp:Label ID="Label2_summary" runat="server" Text="<%#mySummary %>" 
        style="color: #666666; font-style: italic;"></asp:Label>
    <br />
    <div align="right">
        發表日期：<asp:Label ID="Label3_test_time" runat="server" Text="<%#myTestTime %>"
            style="font-style: italic; color: #993333"></asp:Label>
            &nbsp;&nbsp;
            點閱數：<asp:Label ID="Label1" runat="server" Text="<%#my_hit_no %>"
            style="font-style: italic; color: #0000FF; background-color: #FFFF00;"></asp:Label>
    </div>   
    <hr />
    <br />
    <asp:Label ID="Label4_article" runat="server" Text="<%#myArticle %>"></asp:Label>
    <hr />
    <div align="right">
        作者：<asp:Label ID="Label5_author" runat="server" Text="<%#myAuthor %>"></asp:Label>
    </div>        
    <hr />
    <br />

    </form>
</body>
</html>

