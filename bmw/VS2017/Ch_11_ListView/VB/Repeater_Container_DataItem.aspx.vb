﻿
Partial Class VS2010_Book_Sample_Ch12_Program_Repeater_NamingContainer
    Inherits System.Web.UI.Page

    '===============
    Dim list As ArrayList
    '===============

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        list = New ArrayList

        list.Add("One")
        list.Add("Two")
        list.Add("Three")

        Repeater1.DataSource = list
        Repeater1.DataBind()
    End Sub


    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        '-- 使用 .FindControl()方法，在 Repeater控制項的第一個項目中搜尋名為 Message 的控制項，
        '-- 然後判斷該控制項的 NamingContainer 物件。程式碼接著判斷首次呼叫 NamingContainer 屬性
        '-- 所傳回控制項的命名容器，並且繼續在控制項樹狀結構中執行這個動作，直到找到沒有命名容器的控制項為止 
        '-- (WalkContainers 方法也會在最底層加入控制項型別，其本身不是命名容器)。

        Dim x As Control = Repeater1.Items(0).FindControl("Message")

        If Not x Is Nothing Then
            list = WalkContainers(x)
        End If

        Repeater1.DataSource = list
        Repeater1.DataBind()
    End Sub


    Private Function WalkContainers(ByVal ctl As Control) As ArrayList
        Dim ret As New ArrayList
        Dim parent As Control = ctl.NamingContainer

        If Not parent Is Nothing Then
            Dim sublist As ArrayList = WalkContainers(parent)

            For j As Integer = 0 To sublist.Count - 1
                ret.Add(sublist(j))
            Next
        End If
        ret.Add(ctl.GetType.Name)
        Return ret
    End Function


End Class
