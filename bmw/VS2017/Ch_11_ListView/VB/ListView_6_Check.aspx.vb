﻿'===================================
Imports System.Collections   '--DictionaryEntry需要用到！
'===================================

'--資料來源：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.listview.itemupdating.aspx


Partial Class test_DataList_ListView_6_Check
    Inherits System.Web.UI.Page

    Protected Sub ListView1_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewInsertEventArgs) Handles ListView1.ItemInserting

        For Each de As DictionaryEntry In e.Values   '--注意這裡的差異！
            ' Check if the value is null or empty
            If de.Value Is Nothing OrElse de.Value.ToString().Trim().Length = 0 Then
                Label1.Text = "（欄位不可以留空白！必填！）Cannot set a field to an empty value."
                e.Cancel = True  '--取消本次的新增。
            End If
        Next

    End Sub



    Protected Sub ListView1_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewUpdateEventArgs) Handles ListView1.ItemUpdating

        For Each de As DictionaryEntry In e.NewValues   '--注意這裡的差異！
            ' Check if the value is null or empty
            If de.Value Is Nothing OrElse de.Value.ToString().Trim().Length = 0 Then
                Label1.Text = "（欄位不可以留空白！必填！）Cannot set a field to an empty value."
                e.Cancel = True  '--取消本次的更新。
            End If
        Next

    End Sub
End Class
