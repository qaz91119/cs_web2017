﻿
Partial Class VS2010_Book_Sample_Ch12_Program_test_DataList_Repeater_Items
    Inherits System.Web.UI.Page


    Protected Sub Repeater1_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles Repeater1.ItemCommand
        Response.Write("<h3>您剛剛按下的按鈕是：（從零算起。由下而下）</h3>")

        Response.Write("Repeater1.Items(e.Item.ItemIndex).ItemIndex --" & Repeater1.Items(e.Item.ItemIndex).ItemIndex.ToString())
        Response.Write("<br />e.Item.ItemIndex --" & e.Item.ItemIndex)

        Response.Write("<br />Repeater1.Items.Count --" & Repeater1.Items.Count & "<hr / >")
        '== Repeater的「Items」屬性，指的是 ItemTemplate有五筆記錄！

        For Each row As RepeaterItem In Repeater1.Items
            Response.Write("<br />Repeater1.Items的 ItemType --" & row.ItemType)
            '=========================================
            '== Itemtype = 2 代表 Item（資料呈現的那一列）。
            '== ItemType =3 代表 AlternatingItem（隔列換色）。

            Response.Write("<ul>")
            For i As Integer = 0 To 8
                Response.Write("<li>Repeater1.Items的 控制項 UniqueID --" & row.Controls(i).UniqueID & "</li>")
            Next
            Response.Write("</ul>")
        Next

    End Sub


End Class
