﻿
Partial Class Book_Sample_Ch12_Program_test_DataList_ListView_DataPager_Manual
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim DP As DataPager = ListView1.FindControl("DataPager1")
        '--正式的寫法 Dim DP As DataPager = CType(ListView1.FindControl("DataPager1"), DataPager)

        DP.PageSize = Convert.ToInt32(RadioButtonList1.SelectedValue)
    End Sub


    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        'Dim DP As DataPager = ListView1.FindControl("DataPager1")
        ''--正式的寫法 Dim DP As DataPager = CType(ListView1.FindControl("DataPager1"), DataPager)

        'DP.PageSize = Convert.ToInt32(RadioButtonList1.SelectedValue)
    End Sub
End Class
