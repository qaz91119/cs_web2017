<%@ Page Language="vb" %>
<%@ Import NameSpace = "System.Data" %>
<%@ Import NameSpace = "System.Data.SqlClient" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Inline Code</title>
    <style type="text/css">
        .style1
        {
            color: #FFFF99;
            font-weight: bold;
            background-color: #CC0000;
        }
    </style>
</head>
<body>

    <div align="center">
        <span class="style1">Inline Code</span>

    <br /><hr /><br />

<%  
'--註解：第一，連結SQL資料庫
    Dim Conn As SqlConnection = New SqlConnection("server=localhost; uid=test; pwd=test; database=test")
Conn.Open()

'--註解：第二，執行SQL指令，使用DataReader
Dim sqlstr As String = "select id,test_time,title,summary from test "
    Dim cmd As SqlCommand = New SqlCommand(sqlstr, Conn)
    Dim dr As SqlDataReader = cmd.ExecuteReader()

'--註解：第三，自由發揮
    '--（Repeater控制項的<ItemTemplate>樣版，自動幫我們完成這一段迴圈的程式）
      Response.Write("<table width='90%' border=1>")

While dr.Read()
      Response.Write("</tr>")
      Response.Write("  <td>" & dr.Item("id") & "</td>")
      Response.Write("  <td>" & dr.Item("test_time") & "</td>")
      Response.Write("  <td>" & dr.Item("title") & "</td>")
      Response.Write("  <td>" & dr.Item("summary") & "</td>")
      Response.Write("</tr>")
End While

      Response.Write("</table>")

'--註解：第四，關閉資料庫的連接
cmd.Cancel()
dr.Close()
Conn.Close()
%>
    
    </div>
  
</body>
</html>
