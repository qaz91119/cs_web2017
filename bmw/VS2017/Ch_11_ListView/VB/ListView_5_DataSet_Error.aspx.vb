﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----


Partial Class VS2010_Book_Sample_test_DataList_ListView_5_DataSet_Error
    Inherits System.Web.UI.Page

    Sub DBInit()   '--註解：這一段程式是自己手寫的。Dataset版。
        '----連結資料庫---
        Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString.ToString)
        Dim myAdapter As SqlDataAdapter = New SqlDataAdapter("select top 10 * from test order by id DESC", Conn)
        Dim ds As New DataSet

        Try
            myAdapter.Fill(ds, "test")    '---- 這時候執行SQL指令。取出資料，放進 DataSet。

            ListView1.DataSource = ds
            '----標準寫法 ListView1.DataSource = ds.Tables("test").DefaultView ----
            ListView1.DataBind()
        Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<HR/>")
        End Try
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            '--註解：只有第一次執行本程式，才會執行這一段。
            DBInit()
        End If
    End Sub

    Protected Sub ListView1_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles ListView1.ItemCommand
        If e.CommandName = "my_Detail" Then
            ListView1.SelectedIndex = -1
            '--註解：「-1」代表離開「選取(Select)」按鈕的狀態！

            'DBInit()  '--把註解給去除掉，就會是正確範例了。
        End If
    End Sub



    Protected Sub ListView1_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewSelectEventArgs) Handles ListView1.SelectedIndexChanging
        ListView1.SelectedIndex = e.NewSelectedIndex
        '--註解：點選某一篇文章後可以看見這一篇文章的細部內容。

        'DBInit()  '--把註解給去除掉，就會是正確範例了。
    End Sub

End Class
