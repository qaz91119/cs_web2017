﻿
Partial Class Book_Sample_Ch10_Program_ListView_Update_FindControl
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            '**** 以下兩種寫法都會報錯！！**************************
            '**** 需要寫在 ListView的 DataBound事件裡面才對。

            'Dim lb As Label = ListView1.Items(0).FindControl("titleLabel")
            ''-- 程式第一次執行（Page_Load事件），抓取第一列紀錄（ListView程式寫成Items(0)） title欄位的值。
            'Response.Write(lb.Text)

            'Response.Write(ListView1.Items(0).Controls.Count)
        End If
    End Sub


    Protected Sub ListView1_DataBound(sender As Object, e As EventArgs) Handles ListView1.DataBound

        Dim lb As Label = ListView1.Items(0).FindControl("titleLabel")
        '-- 抓取第一列紀錄（ListView程式寫成Items(0)） title欄位的值。
        '或是寫成Dim lb As Label = CType(ListView1.Items(0).FindControl("titleLabel"), Label)

        Response.Write("<font color=red>")
        Response.Write("   抓取第一列紀錄（ListView程式寫成Items(0)） title欄位的值 -- " & lb.Text)

        Response.Write("   <br />一個Item裡面有幾個控制項？ListView1.Items(0).Controls.Count -- " & ListView1.Items(0).Controls.Count)
        Response.Write("</font>")
    End Sub

    
End Class
