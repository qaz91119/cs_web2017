﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Repeater_Container_DataItem.aspx.vb" Inherits="VS2010_Book_Sample_Ch12_Program_Repeater_NamingContainer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <b>HOW TO：存取控制項命名容器的成員</b><br />
        參考資料：<a href="http://msdn.microsoft.com/zh-tw/library/858twd77(v=VS.100).aspx">http://msdn.microsoft.com/zh-tw/library/858twd77(v=VS.100).aspx</a> <br />
        <br />
        <br />
        <table>
              <asp:repeater id="Repeater1" runat="server" >
                      <ItemTemplate>
                                <tr>
                                  <td align="center" style="width:100%;">

                                       <span id="message" runat="server">
                                             <%# Container.DataItem%>
                                       </span>

                                  </td>
                                </tr>
                      </ItemTemplate>
              </asp:repeater>
        </table>
        <br />
        <br />

        <asp:Button ID="Button1" runat="server" Text="Button" />



        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testConnectionString %>" 
            SelectCommand="SELECT * FROM [test]" >
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
