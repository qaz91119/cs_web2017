﻿
Partial Class Book_Sample_Ch10_Program_ListView_Update_FindControl_2
    Inherits System.Web.UI.Page

    Protected Sub ListView1_ItemUpdating(sender As Object, e As ListViewUpdateEventArgs) Handles ListView1.ItemUpdating
        '==抓取「編輯」模式裡面，使用者修改後的欄位值。

        Response.Write("對應 test資料表的主索引鍵 --" & ListView1.DataKeys(e.ItemIndex).Value)


        '*********************************
        '  重點來了!! 以下兩行程式碼跟上一個範例不同!!
        '*********************************
        '--以下兩種寫法，請您任選其一
        Dim tb As TextBox = ListView1.Items(e.ItemIndex).FindControl("titleTextBox")
        '--或是寫成Dim tb As TextBox = CType(ListView1.Items(e.ItemIndex).FindControl("titleTextBox"), TextBox)

        Response.Write("<br /> title -- " & tb.Text)

        Response.End()   '==因為本範例的 ListViewView搭配 SqlDataSource，不寫這一列程式會出現錯誤。
    End Sub

End Class
