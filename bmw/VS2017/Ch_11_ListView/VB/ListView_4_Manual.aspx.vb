﻿
Partial Class VS2010_Book_Sample_test_DataList_ListView_4_Manual
    Inherits System.Web.UI.Page

    Protected Sub Button1_Delete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1_Delete.Click
        If ListView1.SelectedIndex >= 0 Then
            ListView1.DeleteItem(ListView1.SelectedIndex)
            '== 使用 DeleteItem 方法，以程式設計的方式刪除資料來源中指定索引的資料錄。
            '== 這個方法通常用於從 ListView 控制項之外刪除資料錄，例如從頁面上的另一個控制項。
            '== 這個方法會引發 ItemDeleted 和 ItemDeleting 事件。 
            '--參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.listview.deleteitem.aspx
            Label1.Text = "******刪除成功！！******"
        Else
            Label1.Text = "您尚未選定任何一筆資料"
        End If
    End Sub


    '== 刪除之後（必須搭配 SqlDataSource才行），有無出現例外狀況？==
    Protected Sub ListView1_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewDeletedEventArgs) Handles ListView1.ItemDeleted
        ' Check if an exception occurred to display an error message.
        If Not (e.Exception Is Nothing) Then
            Label1.Text = "發生例外狀況...."
            e.ExceptionHandled = True
        Else
            ListView1.SelectedIndex = -1  '-- 「未」選定任何一筆資料
        End If
    End Sub


    Protected Sub ListView1_PagePropertiesChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.PagePropertiesChangingEventArgs) Handles ListView1.PagePropertiesChanging
        '--當頁面屬性變更時，但是在 ListView 控制項使用 SetPageProperties 方法設定新的值之前會引發 PagePropertiesChanging 事件。
        '--這可讓您在每次發生這個事件時執行自訂常式，例如清除 SelectedIndex 或 EditIndex 屬性。 
        '--參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.listview.onpagepropertieschanging.aspx

        ListView1.SelectedIndex = -1  '-- 「未」選定任何一筆資料
        ListView1.EditIndex = -1

        Button1_Delete.Visible = True
        Button3_Update.Visible = False
    End Sub


    '***********************************************************************************
    '*** 進入「編輯」模式 ***
    '***********************************************************************************
    Protected Sub Button2_Edit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2_Edit.Click
        If ListView1.SelectedIndex >= 0 Then
            '== 進入「編輯」模式！==
            ListView1.EditIndex = ListView1.SelectedIndex
            Button1_Delete.Visible = False   '--進入編輯模式，把「刪除」按鈕給隱藏起來
            Button3_Update.Visible = True
        Else
            Label1.Text = "您尚未選定任何一筆資料"
        End If
    End Sub


    '***********************************************************************************
    '*** 更 新 ***
    '***********************************************************************************
    Protected Sub Button3_Update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3_Update.Click

        If ListView1.SelectedIndex >= 0 Then
            '== 更新這一筆資料！==
            ListView1.UpdateItem(ListView1.SelectedIndex, False)
            '--參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.listview.updateitem.aspx
        Else
            Label1.Text = "您尚未選定任何一筆資料"
        End If

        Button1_Delete.Visible = True
    End Sub


    Protected Sub ListView1_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewUpdateEventArgs) Handles ListView1.ItemUpdating
        '--參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.listview.itemupdating.aspx

        For Each de As DictionaryEntry In e.NewValues
            '-- 透過For迴圈逐一檢查，如果發現有任何一個欄位的內容是空白，這次的資料更新就會自動取消！
            If de.Value Is Nothing OrElse de.Value.ToString().Trim().Length = 0 Then
                Label1.Text = "有欄位是空白的，所以暫停這次的資料更新！"
                e.Cancel = True
            End If
        Next

    End Sub


    '***********************************************************************************
    '*** 新增一筆資料 ***
    '***********************************************************************************
    Protected Sub Button4_Insert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button4_Insert.Click
        '--參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.listview.insertnewitem.aspx
        ListView1.InsertNewItem(True)

        Button1_Delete.Visible = True
    End Sub


    Protected Sub ListView1_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewInsertedEventArgs) Handles ListView1.ItemInserted

        If Not (e.Exception Is Nothing) Then
            If e.AffectedRows = 0 Then  '-- AffectedRows = 0表示新增失敗！
                e.KeepInInsertMode = True
                Label1.Text = "新增資料失敗，出現例外狀況"
            Else
                Label1.Text = "新增資料失敗，出現例外狀況"
            End If

            e.ExceptionHandled = True
        End If
    End Sub
End Class

