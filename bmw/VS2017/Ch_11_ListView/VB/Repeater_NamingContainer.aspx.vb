﻿
Partial Class VS2010_Book_Sample_Ch12_Program_test_DataList_Repeater_NaminContainer
    Inherits System.Web.UI.Page


    Protected Sub Repeater1_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles Repeater1.ItemCommand
        Response.Write("<h3>您剛剛按下的按鈕是：（從零算起。由下而下）</h3>")
        Response.Write(Repeater1.Items(e.Item.ItemIndex).ItemIndex.ToString())
    End Sub



    Protected Sub Button2_Click(sender As Object, e As System.EventArgs) Handles Button2.Click
        Dim myCurrentItem As RepeaterItem

        '== Repeater的「Items」屬性，指的是 ItemTemplate有五筆記錄！
        For Each myCurrentItem In Repeater1.Items
            ' Display the UniqueID.
            myLabel1.Text &= "<br />The UniqueID is : " & CType(myCurrentItem.Controls(7), Control).UniqueID

            '==============================================
            Dim myNamingContainer As Control = myCurrentItem.Controls(7).NamingContainer

            ' Display the NamingContainer.
            myLabel2.Text &= "<br />The NamingContainer is : " & myNamingContainer.UniqueID
        Next

        '===================================
        '== 上面的 .Controls()方法，裡面的 Index索引值。
        '== 輸入 1，可以抓到 Button1。
        '== 輸入 3，可以抓到 Label1。id欄位。
        '== 輸入 5，可以抓到 Label2。test_time欄位。
        '== 輸入 7，可以抓到 Label3。Title欄位。
        '==請您參閱範例 Repeater_Items.aspx，會比較容易瞭解。
    End Sub

End Class
