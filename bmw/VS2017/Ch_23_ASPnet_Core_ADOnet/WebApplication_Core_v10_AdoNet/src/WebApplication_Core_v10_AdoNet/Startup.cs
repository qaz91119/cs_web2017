﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
//************************************
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Text.Encodings.Web;
//************************************

namespace WebApplication_Core_v10_AdoNet
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }         

            app.Run(async (context) =>
            {
                ////中文亂碼的解決  http://www.cnblogs.com/artech/p/encoding-registeration-4-net-core.html
                //Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                //Encoding.GetEncoding(65001);

                await context.Response.WriteAsync("Hello World!");

                //=======================================(start)
                //== 方法一 ==
                //string connectionString = @"server=.\sqlexpress;integrated security=SSPI;database=test";

                //== 方法二 == 改成appsettings.json
                ////http://stackoverflow.com/questions/38282652/asp-net-core-1-0-configurationbuilder-addjsonfileappsettings-json-not-fin
                var configurationBuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
                //  讀取設定檔的內容

                IConfiguration config = configurationBuilder.Build();
                string connectionString = config["ConnectionStrings:DefaultConnection"];



                var Conn = new SqlConnection(connectionString);
                Conn.Open();
               // await context.Response.WriteAsync("<br /> connection opened");

                var Com = new SqlCommand("Select title from test Where id = @ID", Conn);
                Com.Parameters.AddWithValue("ID", 3);
                                
                using (SqlDataReader dr = Com.ExecuteReader())
                {
                    while (await dr.ReadAsync())
                    {
                        // (1). 中文亂碼的解決 -- 瀏覽器的編碼，請改成 UTF-8
                        var title = dr["title"];
                        await context.Response.WriteAsync("<br />" + title + "<hr />");

                        // (2). 中文亂碼的解決 -- 不用修改瀏覽器的編碼。請加入 System.Text.Encodings.Web 命名空間
                        var title2 = HtmlEncoder.Default.Encode(dr["title"].ToString());
                        await context.Response.WriteAsync("<br />" + title2 + "<hr />");
                    }
                }

                Conn.Close();
                //=======================================(end)
            });            

        }
    }
}
