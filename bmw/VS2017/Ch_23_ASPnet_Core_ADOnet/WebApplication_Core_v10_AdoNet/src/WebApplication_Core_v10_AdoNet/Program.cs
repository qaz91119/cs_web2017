﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
//*************************************************
//需要額外新增的 NameSpace
using System.Data.SqlClient;

using Microsoft.AspNetCore.Http;
//*************************************************

namespace WebApplication_Core_v10_AdoNet
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

        host.Run();            
        }

    }


}
