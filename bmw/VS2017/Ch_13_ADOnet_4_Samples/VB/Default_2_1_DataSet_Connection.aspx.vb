﻿'----自己寫的（宣告）----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
'----自己寫的（宣告）----


Partial Class test_DataSet_DataReader_1_DataSet
    Inherits System.Web.UI.Page

    '====自己手寫的程式碼， DataAdapter / DataSet ====(Start)
    Sub DBInit()
        '=======微軟SDK文件的範本=======
        '----上面已經事先寫好 Imports System.Web.Configuration ----
        '----(1). 連結資料庫----
        '----連結資料庫的另一種寫法----
        'Dim Conn As SqlConnection = New SqlConnection
        'Conn.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString
        Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString.ToString)

        '----(2). 執行SQL指令（Select陳述句）----
        Dim myAdapter As SqlDataAdapter
        myAdapter = New SqlDataAdapter("select id,test_time,title,author from test", Conn)

        Dim ds As New DataSet()

        Try  '==== 以下程式，只放「執行期間」的指令！=================
            ' ---- 不用寫Conn.Open() ，DataAdapter會自動開啟

            myAdapter.Fill(ds, "test")    '---- 這時候執行SQL指令。取出資料，放進 DataSet。


            '***********************************
            '*** .Fill()方法之後，資料庫連線就中斷囉！
            Response.Write("<hr />資料庫連線 Conn.State ---- " & Conn.State.ToString() & "<hr />")
            '***********************************


            '----(3). 自由發揮。由 GridView來呈現資料。----
            GridView1.DataSource = ds
            '----標準寫法 GridView1.DataSource = ds.Tables("test").DefaultView ----
            GridView1.DataBind()


        Catch ex As Exception
            '---- http://www.dotblogs.com.tw/billchung/archive/2009/03/31/7779.aspx
            Response.Write("<hr /> Exception Error Message----  " + ex.ToString())
        Finally
            '----(4). 釋放資源、關閉連結資料庫----
            '---- 不用寫，DataAdapter會自動關閉
            'If (Conn.State = ConnectionState.Open) Then
            '  Conn.Close()
            '  Conn.Dispose()
            'End If
        End Try

    End Sub
    '====自己手寫的程式碼， DataAdapter / DataSet ====(End)




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            DBInit()    '--網頁第一次執行的時候，才會運作到這一行！
        End If
    End Sub


    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        '== 分 頁 ==
        GridView1.PageIndex = e.NewPageIndex

        DBInit()
    End Sub
End Class
