﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----

Partial Class test_ADO_NET_Default_1_2_DataReader_Manual_Using
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim SQLstr As String = "SELECT * From test"

        Using Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)
            Dim cmd As New SqlCommand(SQLstr, Conn)
            Conn.Open()  '==第一，連結資料庫==
            '—註解：開啟資料庫連線！但後面「不」需要寫關閉的動作（Conn.Close()），因為Using....End Using 會自己處理資源的後續動作。

            '==第二，設定SQL指令****（巢狀 Using）****==
            Using dr As SqlDataReader = cmd.ExecuteReader()

                '==第三，自由發揮，將資料呈現在畫面上==
                '---- 把DataReader獲得的資料，跟GridView控制項繫結（.DataBind()）在一起。
                '---- GridView控制項就會自動把資料展現在畫面上了。
                GridView1.DataSource = dr
                GridView1.DataBind()

                cmd.Cancel()
                dr.Close()
            End Using  '--  處置DataReader****（巢狀 Using）****

        End Using
        '==第四，關閉資源＆資料庫的連線==
        '—註解：資料庫連線（Conn）會「自動關閉(.Close)&處置(.Dispose)」。因為它是由 Using來開啟，End Using會自動關閉與處置之。
    End Sub

End Class
