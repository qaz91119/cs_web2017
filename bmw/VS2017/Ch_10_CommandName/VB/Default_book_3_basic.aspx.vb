﻿
Partial Class Default_book_3_basic
    Inherits System.Web.UI.Page

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        Response.Write("<br>GridView1.SelectedIndex--" & GridView1.SelectedIndex)
        Response.Write("<br>GridView1.SelectedRow.RowIndex--" & GridView1.SelectedRow.RowIndex)
        Response.Write("<br>")
        Response.Write("<br>GridView1.SelectedValue（資料庫主索引鍵）--" & GridView1.SelectedValue)
        Response.Write("<br>GridView1.SelectedDataKey.Value（資料庫主索引鍵）--" & GridView1.SelectedDataKey.Value)

        '--測試用的  GridView1.EditIndex = GridView1.SelectedIndex

        '--註解：這一行程式碼，可以這樣解釋--
        '--    使用者選取的這一行（GridView1.SelectedIndex），進入GridView的「編輯」模式（GridView1.EditIndex）。
    End Sub


    Protected Sub GridView1_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GridView1.SelectedIndexChanging
        '== 進入「選取」模式（光棒效果）==
        GridView1.SelectedIndex = e.NewSelectedIndex

        '--測試用的  GridView1.SelectedIndex = GridView1.SelectedIndex
        '--註解：這一行程式碼，可以這樣解釋--
        '--    使用者選取的這一行（GridView1.SelectedIndex），進入GridView的「選取」模式（GridView1.SelectedIndex）。
        '--    也就是「光棒效果」。
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        '== 離開「選取」模式（取消光棒效果）==
        If e.CommandName = "Cancel_Select" Then
            GridView1.SelectedIndex = -1
        End If
        '=================================


        '******************************************************
        Dim pk_index As Integer = CInt(e.CommandArgument)
        '******************************************************
            '-- 下面這一列成是在某些頁數會出錯，例如第五頁。因為您點選第五頁，e.CommandArgument會是 5。
            '-- 而這個 GridView每一頁只有五列（RowIndex為 0~4）所以會出錯。
            '-- 進一步的說明，請看我的文章 -- http://www.dotblogs.com.tw/mis2000lab/archive/2013/06/20/rowcommand_commandargument_page_20130620.aspx
            'Response.Write("<br><font color=red>被點選的這一列，對應資料表的主索引鍵-- </font>" & GridView1.DataKeys(pk_index).Value)

            Response.Write("<br /><font color=red>被點選的這一列，e.CommandArgument-- </font>" + pk_index.ToString());

    End Sub



    Protected Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridView1.RowEditing
        '== 進入「編輯」模式 ==
        GridView1.EditIndex = e.NewEditIndex
        '--註解：這裡還要重新執行 DataBinding。例如：GridView1.DataBind()。
        '--          但因為我們事先設定好 GridView的「SqlDataSourceID」屬性=SqlDataSource1。
        '--          SqlDataSource會自動幫我們完成，所以這裡省略了。


        '--測試用的，這行可正確執行！  GridView1.EditIndex = GridView1.SelectedIndex
    End Sub

    Protected Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit
        '== 離開「編輯」模式（取消「編輯」模式）==
        GridView1.EditIndex = -1
        '--註解：這裡還要重新執行 DataBinding。例如：GridView1.DataBind()。
        '--          但因為我們事先設定好 GridView的「SqlDataSourceID」屬性=SqlDataSource1。
        '--          SqlDataSource會自動幫我們完成，所以這裡省略了
    End Sub


    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        '== GridView的分頁 ==
        GridView1.PageIndex = e.NewPageIndex

        '--註解：這裡還要重新執行 DataBinding。例如：GridView1.DataBind()。
        '--          但因為我們事先設定好 GridView的「SqlDataSourceID」屬性=SqlDataSource1。
        '--          SqlDataSource會自動幫我們完成，所以這裡省略了。
    End Sub


End Class
