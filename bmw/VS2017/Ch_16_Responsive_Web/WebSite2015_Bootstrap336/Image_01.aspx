﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Image_01.aspx.cs" Inherits="Book_Samples_BootStrap_Image_01" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>縮圖 #1</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/Content/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="/Content/bootstrap-theme.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="/Scripts/jquery-1.11.1.min.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>
    </head>
<body>
    <form id="form1" runat="server">
        <br />
        <br />
        <br />
        圖片的 <strong>class=&quot;img-circle&quot;</strong>表示「<strong>圓形</strong>」<br />
        <br />


      <div class="row">
        <div class="col-lg-3">
          <img class="img-circle" src="Images_Book/02.jpg" alt="Generic placeholder image" width="140" height="140">
          <h2>Heading</h2>
          <p>ASP.NET 專題實務 I--VB入門實戰(VS 2015版)</p>
          <p><a class="btn btn-default" href="http://www.tenlong.com.tw/items/957224471X?item_id=1008701" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-3 -->
        <div class="col-lg-3">
          <img class="img-circle" src="Images_Book/XP15177.jpg" alt="Generic placeholder image" width="140" height="140">
          <h2>Heading</h2>
          <p>ASP.NET 專題實務 I--C#入門實戰(VS 2015版)</p>
          <p><a class="btn btn-default" href="http://www.tenlong.com.tw/items/9572244582?item_id=1007642" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-3 -->
        <div class="col-lg-3">
          <img class="img-circle" src="Images_Book/XP16067.jpg" alt="Generic placeholder image" width="140" height="140">
          <h2>Heading</h2>
          <p>ASP.NET 專題實務 II--範例應用與進階功能</p>
          <p><a class="btn btn-default" href="http://www.tenlong.com.tw/items/9572245090?item_id=1011591" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-3 -->
      </div><!-- /.row -->
 



    <p><br /><br />
        ========================================================================</p>
    <br> 以下是 ASP.NET Image控制項
    <p>&nbsp;</p>

      <div class="row">
        <div class="col-lg-3">
                    <asp:Image ID="Image1" runat="server" ImageUrl="Images_Book/02.jpg"
                        data-src="holder.js/185x250" class="img-circle" />
        </div><!-- /.col-lg-3 -->
        <div class="col-lg-3">
                    <asp:Image ID="Image4" runat="server" ImageUrl="Images_Book/XP15177.jpg"
                        data-src="holder.js/185x250" class="img-circle" />
        </div><!-- /.col-lg-3 -->
        <div class="col-lg-3">
                    <asp:Image ID="Image5" runat="server" ImageUrl="Images_Book/XP16067.jpg"
                        data-src="holder.js/185x250" class="img-circle" />
        </div><!-- /.col-lg-3 -->
      </div><!-- /.row -->
        

    <p>
        &nbsp;</p>
    <p>
        
        </p>
    </form>
    </body>
</html>
