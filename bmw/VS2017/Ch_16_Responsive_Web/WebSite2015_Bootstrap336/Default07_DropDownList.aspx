﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default07_DropDownList.aspx.cs" Inherits="Default07_DropDownList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/Content/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="/Content/bootstrap-theme.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="/Scripts/jquery-1.11.1.min.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>

    <!-- **** 本範例必須加入以下的檔案，才能生效。********************************************** -->
    <!-- Custom styles for this template -->
    <link href="/Content/theme336.css" rel="stylesheet">
    <!-- **** 本範例必須加入以下的檔案，才能生效。********************************************** -->
    

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/Scripts/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    <form id="form1" runat="server">
    <div>
    
      <div class="page-header">
        <h1>Dropdown menus（下拉式選單）</h1>
      </div>

      <div class="dropdown theme-dropdown clearfix">
        <a id="dropdownMenu1" href="#" class="sr-only dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
          <li class="active"><a href="#">Action（預設，被選定）</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>

          <li role="separator" class="divider"></li>   <!-- 分隔線 -->

          <li><a href="#">Separated link</a></li>
          <li><a href="#">         <img src="Images_Book\bmw.jpg" /> BMW </a></li>
        </ul>
      </div>

        <br /><br />
        務必加入這個CSS檔，不然下拉式選單會失效<br />
        /Content/theme336.css

    </div>
    </form>
</body>
</html>
