﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="Default06_Table_ADOnet.aspx.cs" Inherits="Default01_CDN" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>表格，Table</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/Content/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="/Content/bootstrap-theme.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="/Scripts/jquery-1.11.1.min.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>

    <link href="/Content/theme336.css" rel="stylesheet">
    
</head>
<body role="document">
    <form id="form1" runat="server">
    <div> 



      <div class="page-header">
        <h1>*** Tables ***</h1>
      </div>


      <div class="row">
        <div class="col-md-6">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Mark（第一種）</td>
                <td>Otto</td>
                <td>@mdo</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Larry</td>
                <td>the Bird</td>
                <td>@twitter</td>
              </tr>
            </tbody>
          </table>
        </div>



        <div class="col-md-6">
          <table class="table table-striped">  <!-- striped （條紋，隔列換底色）-->
            <thead>
              <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Mark（第二種）</td>
                <td>Otto</td>
                <td>@mdo</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Larry</td>
                <td>the Bird</td>
                <td>@twitter</td>
              </tr>
            </tbody>
          </table>
        </div>

      </div>  <!-- END *** <div class="row"> -->



      <div class="page-header">
        <h1>*** GridView的CssClass屬性 ***</h1>
      </div>
        <br />

             <asp:GridView ID="GridView1" runat="server" CssClass="table table-hover" width="1024px"
                 AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" 
                 DataKeyNames="id" DataSourceID="SqlDataSource1">
                 <Columns>
                     <asp:CommandField ShowEditButton="True" ShowSelectButton="True" />
                     <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                     <asp:BoundField DataField="test_time" HeaderText="test_time" SortExpression="test_time" />
                     <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
                     <asp:BoundField DataField="author" HeaderText="author" SortExpression="author" />
                 </Columns>
             </asp:GridView>
             <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testConnectionString %>" DeleteCommand="DELETE FROM [test] WHERE [id] = @id" InsertCommand="INSERT INTO [test] ([test_time], [title], [author]) VALUES (@test_time, @title, @author)" SelectCommand="SELECT [id], [test_time], [title], [author] FROM [test]" UpdateCommand="UPDATE [test] SET [test_time] = @test_time, [title] = @title, [author] = @author WHERE [id] = @id">
                 <DeleteParameters>
                     <asp:Parameter Name="id" Type="Int32" />
                 </DeleteParameters>
                 <InsertParameters>
                     <asp:Parameter Name="test_time" Type="DateTime" />
                     <asp:Parameter Name="title" Type="String" />
                     <asp:Parameter Name="author" Type="String" />
                 </InsertParameters>
                 <UpdateParameters>
                     <asp:Parameter Name="test_time" Type="DateTime" />
                     <asp:Parameter Name="title" Type="String" />
                     <asp:Parameter Name="author" Type="String" />
                     <asp:Parameter Name="id" Type="Int32" />
                 </UpdateParameters>
             </asp:SqlDataSource>



        <br /><h1>ASP.NET的 GridView控制項，搭配Bootstrap的表格</h1>


             <asp:GridView ID="GridView2" runat="server" CssClass="table table-striped" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1">
                 <Columns>
                     <asp:CommandField ShowEditButton="True" ShowSelectButton="True" />
                     <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                     <asp:BoundField DataField="test_time" HeaderText="test_time" SortExpression="test_time" />
                     <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
                     <asp:BoundField DataField="author" HeaderText="author" SortExpression="author" />
                 </Columns>
             </asp:GridView>

    </div>
    </form>
</body>
</html>
