﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default03_First_Sample.aspx.cs" Inherits="Default01_CDN" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>第一個範例 -- http://getbootstrap.com/examples/starter-template/ </title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/Content/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="/Content/bootstrap-theme.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="/Scripts/jquery-1.11.1.min.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>

    <link rel="icon" href="http://getbootstrap.com/favicon.ico">
    <link rel="stylesheet" href="navbar.css">   <!-- 本範例專屬的CSS -->

</head>
<body>
    <form id="form1" runat="server">
    


      <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">Project name 主題</a>
          </div>

          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#">Home</a></li>  <!-- class="active" 代表按鈕被按下、預設就被選定 -->
              <li><a href="#">About</a></li>
              <li><a href="#">Contact</a></li>

              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    下拉式選單Dropdown </a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>   <!-- 分隔線 -->
                  <li class="dropdown-header"> ** Nav header **</li>
                  <li><a href="#">Separated link</a></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="active">  <!-- class="active" 代表按鈕被按下、預設就被選定 -->
                  <a href="./">Default <span class="sr-only">(current)</span></a>
              </li>
              <li><a href="../navbar-static-top/">Static top</a></li>
              <li><a href="../navbar-fixed-top/">Fixed top</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>


      <!-- 下方的文字說明、大標題 -->
      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>Navbar example（導覽列）</h1>
        <p>This example is a quick exercise to illustrate how the default, static navbar and fixed to top navbar work. It includes the responsive CSS and HTML, so it also adapts to your viewport and device.</p>
        <p>
          <a class="btn btn-lg btn-primary" href="#" role="button">
              （藍色按鈕）View navbar docs &raquo;</a>
        </p>
      </div>

    </div> <!-- /container -->


    </form>
</body>
</html>
