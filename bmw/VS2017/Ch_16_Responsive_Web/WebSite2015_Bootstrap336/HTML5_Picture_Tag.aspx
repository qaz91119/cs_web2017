﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HTML5_Picture_Tag.aspx.cs" Inherits="HTML5_Picture_Tag" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>IE 11瀏覽器可能有問題，建議改用 Chrome瀏覽器</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <h3>HTML5的新 &lt;picture&gt;標籤</h3>
        IE 11瀏覽器可能有問題，建議改用 Chrome瀏覽器 or Edge瀏覽器（Win10）
        <hr />

        <picture>         
            <source srcset="\Images_Book\45_Book_big.jpg" media="(min-width: 1024px)">
            <source srcset="\Images_Book\45_Book_middle.jpg" media="(min-width: 750px)">
            <source srcset="\Images_Book\45_Book_small.jpg" media="(min-width: 300px)">

            <img src="\Images_Book\45_Book_big.jpg" alt="預設的圖片">
        </picture>

    </div>
    </form>
    <p>
        &nbsp;</p>
    <p>
        影片：<a href="http://channel9.msdn.com/Events/Visual-Studio/Connect-event-2014/814">http://channel9.msdn.com/Events/Visual-Studio/Connect-event-2014/814</a></p>
    <p>
        參考文章：<a href="http://webdesign.tutsplus.com/tutorials/quick-tip-how-to-use-html5-picture-for-responsive-images--cms-21015">http://webdesign.tutsplus.com/tutorials/quick-tip-how-to-use-html5-picture-for-responsive-images--cms-21015</a>
    </p>
</body>
</html>
