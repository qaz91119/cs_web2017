﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default08_Image_Circle.aspx.cs" Inherits="Default08_Image_Circle" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>使用本地（自己網站內）的檔案</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/Content/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="/Content/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="/Scripts/jquery-1.11.1.min.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
        <p>
            圓形的圖片外觀 -- class="img-circle"</p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
        <asp:Image ID="Image1" runat="server" class="img-circle" ImageUrl="~/Images_Book/45_Book_small.jpg" />
        <br />
        <br />
        <asp:Image ID="Image2" runat="server" class="img-circle" ImageUrl="~/Images_Book/45_Book_middle.jpg" />
        <br />
        <br />

    </form>
</body>
</html>
