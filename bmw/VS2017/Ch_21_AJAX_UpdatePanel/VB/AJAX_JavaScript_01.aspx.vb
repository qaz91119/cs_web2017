﻿
Partial Class VS2010_Book_Sample_Ch19_Program_test_ADO_NET_AJAX_JavaScript_01
    Inherits System.Web.UI.Page


    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        ScriptManager.RegisterStartupScript(Button1, GetType(Button), "JavaScript_Sample1", "window.alert(""Hello!! Code Behind!!"")", True)

        '*** 參數 ***
        '  control型別：System.Web.UI.Control
        '     正在註冊用戶端指令碼（JavaScript）區塊的控制項。

        '  type型別：System.Type
        '     用戶端指令碼區塊的型別。這個參數通常是使用 typeof 運算子 (C#) 或 GetType 運算子 (Visual Basic) 所指定，用以擷取註冊指令碼的控制項型別。
        '  key型別：System.String
        '     指令碼區塊的唯一識別項。請自己命名！
        '  script型別：System.String
        '     JavaScript指令碼。
        '  addScriptTags型別：System.Boolean
        '     若要使用 <script> 和 </script> 標記來匡住 JavaScript指令碼區塊，則為 true，否則為 false。
    End Sub



    Protected Sub Button2_Click(sender As Object, e As System.EventArgs) Handles Button2.Click
        Dim JavaScript_Str As String = "window.alert(""Hello!! --" & DateTime.Now.ToLongTimeString() & "--"")"
        '失敗！！  ----  Dim JavaScript_Str As String = "return confirm(""再一次確認，您要刪除這筆資料嗎？"")"
        '失敗！！  ----  Dim JavaScript_Str As String= "if (confirm(""再一次確認，您確定要刪除嗎？"")){return true;} else {return false;}"

        ScriptManager.RegisterStartupScript(Button2, GetType(Button), "JavaScript_Sample1", JavaScript_Str, True)
    End Sub
End Class
