﻿
Partial Class VS2010_Book_Sample_Ch19_Program_BackHistory_02
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click

        If (ScriptManager1.IsInAsyncPostBack) And (Not ScriptManager1.IsNavigating) Then
            ScriptManager1.AddHistoryPoint("BackHistory", TextBox1.Text)
        End If

        Label1.Text = TextBox1.Text
    End Sub

    '==== 重點！！ ================================
    Protected Sub ScriptManager1_Navigate(sender As Object, e As System.Web.UI.HistoryEventArgs) Handles ScriptManager1.Navigate
        Label1.Text = e.State("BackHistory")
    End Sub
End Class
