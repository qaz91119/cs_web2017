﻿
Partial Class Book_Sample_Ch19_Program_test_ADO_NET_Warning_Color_3_Auto
    Inherits System.Web.UI.Page


    Protected Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

        Dim i As Integer = System.DateTime.Now.Second

        Dim i_str As String = i Mod 5   '--計算後，傳回餘數。

        Select Case i_str
            Case "1"
                Image1.ImageUrl = "Chart_Images/1.jpg"
            Case "2"
                Image1.ImageUrl = "Chart_Images/2.jpg"
            Case "3"
                Image1.ImageUrl = "Chart_Images/3.jpg"
            Case "4"
                Image1.ImageUrl = "Chart_Images/4.jpg"
            Case Else
                '-- 以上皆非。餘數為零。
                Image1.ImageUrl = "Chart_Images/5.jpg"
        End Select

    End Sub
End Class
