﻿'----自己（宣告）寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己（宣告）寫的----


Partial Class Book_Sample_Ch19_Program_test_ADO_NET_AJAX_Case_Timer_ADOnet
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Label3.Text = Now.ToLongTimeString()
    End Sub


    Protected Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        '=======微軟SDK文件的範本=======
        '----連結資料庫----
        Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)

        Dim dr As SqlDataReader = Nothing

        Dim cmd As SqlCommand = New SqlCommand("Select count(id) From test", Conn)

        Try     '==== 以下程式，只放「執行期間」的指令！=====================
            '== 第一，連結資料庫。
            Conn.Open()   '---- 這時候才連結DB

            '== 第二，執行SQL指令。
            'dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料

            '==第三，自由發揮，把執行後的結果呈現到畫面上。
            Label2.Text = cmd.ExecuteScalar()

            Label1.Text = Now.ToLongTimeString()

        Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
            '參考資料： http://www.dotblogs.com.tw/billchung/archive/2009/03/31/7779.aspx
            Response.Write("<b>Error Message----  </b>" & ex.ToString() & "<hr />")
        Finally
            '== 第四，釋放資源、關閉資料庫的連結。
            '---- Always call Close when done reading.
            If Not (dr Is Nothing) Then
                cmd.Cancel()
                '----關閉DataReader之前，一定要先「取消」SqlCommand
                '參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
                dr.Close()
            End If

            '---- Close the connection when done with it.
            If (Conn.State = ConnectionState.Open) Then
                Conn.Close()
                Conn.Dispose() '---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
            End If
        End Try
    End Sub


    '***********************************************************************************
    Protected Sub GridView1_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GridView1.SelectedIndexChanging
        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub


    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        DetailsView1.ChangeMode(DetailsViewMode.Insert)
        'DetailsView1.DefaultMode = DetailsViewMode.Insert
    End Sub

    Protected Sub SqlDataSource2_Inserted(sender As Object, e As SqlDataSourceStatusEventArgs) Handles SqlDataSource2.Inserted
        '-- 新增成功後，DetailsView變換畫面，給使用者一個提醒（表示新增已經成功）
        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
        'DetailsView1.DefaultMode = DetailsViewMode.ReadOnly
    End Sub


End Class
