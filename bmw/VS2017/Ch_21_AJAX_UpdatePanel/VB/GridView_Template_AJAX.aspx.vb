﻿'=======================
Imports System.Text    '--StringBuilder會用到。


Partial Class VS2010_Book_Sample_Ch19_Program_test_ADO_NET_GridView_Template_AJAX
    Inherits System.Web.UI.Page


    Protected Sub ChangeQuantity(ByVal Sender As Button, ByVal delta As Integer)
        Dim quantityLabel As Label = CType(Sender.FindControl("QuantityLabel"), Label)
        Dim currentQuantity As Integer = Int32.Parse(quantityLabel.Text)
        currentQuantity = Math.Max(0, currentQuantity + delta)

        quantityLabel.Text = currentQuantity.ToString(System.Globalization.CultureInfo.InvariantCulture)
    End Sub

    Protected Sub OnDecreaseQuantity(ByVal Sender As Object, ByVal E As EventArgs)
        ChangeQuantity(Sender, -1)
    End Sub

    Protected Sub OnIncreaseQuantity(ByVal Sender As Object, ByVal E As EventArgs)
        ChangeQuantity(Sender, 1)
    End Sub


    '***************************************************************************************
    Protected Sub Button1_Click1(sender As Object, e As System.EventArgs) Handles Button1.Click

        Dim sb As New StringBuilder()
        sb.Append("<hr /><font color=red>Beverage order : </font><hr />")

        For Each row As GridViewRow In GridView1.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim quantityLabel As Label = CType(row.FindControl("QuantityLabel"), Label)
                Dim currentQuantity As Int32 = Int32.Parse(quantityLabel.Text)
                sb.Append(row.Cells(0).Text & "  :  <font color=blue>" & currentQuantity & "</font><br/>")
            End If
        Next
        SummaryLabel.Text = sb.ToString()

    End Sub

End Class
