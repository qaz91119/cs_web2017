﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AJAX_003_Multiple.aspx.vb" Inherits="CaseStudy_Easy_AJAX_AJAX_003_Multiple" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style2 {
            color: #0000FF;
        }
        .auto-style3 {
            color: #990099;
        }
    </style>
</head>
<body>
    <p>
        <br />
    </p>
    <form id="form1" runat="server">
        外部時間（PostBack）--<%=System.DateTime.Now.ToLongTimeString()%>
        <p>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
        </p>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
            <ContentTemplate>
                =======================<br />
                內部時間（AJAX<span class="auto-style3"><strong>，每三秒更新</strong></span>）--<%=System.DateTime.Now.ToLongTimeString()%><br /><asp:Timer ID="Timer1" runat="server" Interval="3000">
                </asp:Timer>
                <br />
                =======================
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
            </Triggers>
        </asp:UpdatePanel>
        <p>
            &nbsp;</p>
        <p>
            </p>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
            <ContentTemplate>
                =======================<br />
                內部時間（AJAX，<span class="auto-style2"><strong>每一秒更新</strong></span>）--<%=System.DateTime.Now.ToLongTimeString()%><br /><asp:Timer ID="Timer2" runat="server" Interval="1000">
                </asp:Timer>
                <br />
                =======================
            </ContentTemplate>

            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer2" EventName="Tick" />
            </Triggers>
        </asp:UpdatePanel>
        <p>            </p>

        <p>
            重點：</p>
        <p>
            (1). 畫面上「<span class="auto-style2"><strong>多個</strong></span>」UpdatePanel。</p>
        <p>
            (2). 因此，只需進行「切割手術」，並且設定 Trigger屬性。</p>
        <p>
            (3). 使用<strong> Timer控制項</strong>。</p>
    <div>
    
    </div>
    </form>
</body>
</html>
