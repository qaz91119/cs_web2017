﻿
Partial Class AJAX_CallBack_Search_Eng_2_Manual
    Inherits System.Web.UI.Page


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        '-- 自己寫程式，動態加入Trigger。       
        '-- 參考資料：http://bibby.be/2008/06/updatepanelcontrol.html

        Dim myTrigger As AsyncPostBackTrigger = New AsyncPostBackTrigger()

        UpdatePanel1.Triggers.Add(myTrigger)
        myTrigger.ControlID = Button1.UniqueID  '-- UniqueID是重點。
    End Sub




    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '-- 記得要修改原本的 SqlDataSource的「參數」。
        '-- 原本的參數：<asp:ControlParameter ControlID="TextBox1" PropertyName="Text" 
        '--                                                            Name="title" Type="String" />
        '-- 修改後：<asp:Parameter Name="title" Type="String" />

        SqlDataSource1.SelectParameters("title").DefaultValue = Server.HtmlEncode(TextBox1.Text)

    End Sub


End Class
