<%@ Page Language="VB" AutoEventWireup="false" CodeFile="11.aspx.vb" Inherits="_Book_WebControls_11" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PlaceHolder控制項</title>
</head>
<body>
    <form id="form1" runat="server">
    PlaceHolder控制項<br />
    
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        外部時間：<%=System.DateTime.Now.ToLongTimeString() %><br />

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                ============================================<br />   
                <br />
                *****************************************************<br />
                <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                <br />
                *****************************************************<br />
                <br />
                <asp:Button ID="Button1" runat="server" Text="按下去，看看結果有何改變" />
                <br />
                <br />
                內部時間：<%=System.DateTime.Now.ToLongTimeString() %>
                <br />
                <br />
                ============================================
            </ContentTemplate>
        </asp:UpdatePanel>    
    </div>
    </form>
 
</body>
</html>
