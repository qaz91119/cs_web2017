﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default_9_AJAX.aspx.vb" Inherits="test_ADO_NET_Default_9_AJAX" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript">

      function pageLoad() {
      }

    </script>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
            background-color: #FFFF00;
            font-weight: bold;
        }
        .style2
        {
            color: #0000FF;
        }
        .style3
        {
            color: #FFFF99;
            background-color: #006600;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        UpdatePanel「外面」的時間：<%= System.DateTime.Now.ToLongTimeString()%><br />
        <br />
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <br />UpdatePanel「<span class="style1">內部</span>」的時間：<%= System.DateTime.Now.ToLongTimeString()%><br />
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="style3">
            (特別注意，會持續跳動！)</span><br />
            <br />
            <br />
            <asp:Timer ID="Timer1" runat="server" Interval="1000">
            </asp:Timer>
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
