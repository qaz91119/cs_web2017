﻿
Partial Class AJAX_CallBack_GridView_Template_Delete_PopUp_CodeBehind
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        '-- 請參考微軟MSDN網站的說明：
        '   參考網址  http://msdn2.microsoft.com/zh-tw/library/system.web.ui.webcontrols.datacontrolrowtype(VS.80).aspx
        
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim d_button As Button
            d_button = CType(e.Row.Cells(0).FindControl("Button1"), Button)

            '正確執行 ---- d_button.OnClientClick = "javascript:return confirm('再一次確認，您確定要對 id編號-- " & e.Row.Cells(1).Text & " 的資料，進行處置動作嗎？')"
            d_button.Attributes.Add("onclick", "javascript:if (confirm('再一次確認，您確定要刪除嗎？')){return true;} else {return false;}")
            
            '====================================================
            '  Error錯誤寫法 ----- d_button.Attributes.Add("onclick", "if(!window.confirm('確定要刪除嗎？')) return;")
            '====================================================
        End If

    End Sub


End Class
