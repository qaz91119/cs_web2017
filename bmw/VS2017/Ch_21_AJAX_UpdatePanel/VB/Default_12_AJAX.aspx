﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default_12_AJAX.aspx.vb" Inherits="test_ADO_NET_Default_12_AJAX" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript">
    
      function pageLoad() {
      }
    
    </script>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
            background-color: #FFFF00;
        }
        .style2
        {
            color: #0000FF;
            background-color: #99CCFF;
            font-weight: bold;
        }
        .style3
        {
            background-color: #99CCFF;
        }
        .style4
        {
            background-color: #FFFF00;
        }
        .style5
        {
            font-weight: bold;
            text-decoration: underline;
            color: #FF0000;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        範例：巢狀的 UpdatePanel （<span class="style5">成功！！</span>）<br />
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        UpdatePanel「外面」的時間：<%= System.DateTime.Now.ToLongTimeString()%><br />
        <br />
        <br />
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="False" 
        UpdateMode="Conditional">
        <ContentTemplate>
            <br />
            <br />&nbsp; 1. <span class="style4">第一個 UpdatePanel，每3,000毫秒</span>就會自動進行「局部更新」。<br />
            &nbsp; UpdatePanel #1「<span class="style1">內部</span>」的時間：
            <b><font color="red"><%= System.DateTime.Now.ToLongTimeString()%></font></b><br />
            <asp:Timer ID="Timer1" runat="server" Interval="3000">
            </asp:Timer>
            <br />
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <hr />
                       <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. <span class="style3">第二個 UpdatePanel，每1,000毫秒</span>就會自動進行「局部更新」。<br />
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; UpdatePanel #2「<span class="style2">內部</span>」的時間：
                       <b><font color="blue"><%= System.DateTime.Now.ToLongTimeString()%></font></b>
                    <hr />
                    <asp:Timer ID="Timer2" runat="server" Interval="1000">
                    </asp:Timer>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
        </Triggers>
     </asp:UpdatePanel>
    </form>
</body>
</html>
