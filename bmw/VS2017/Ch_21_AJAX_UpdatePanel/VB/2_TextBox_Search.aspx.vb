﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----

Partial Class _Book_Case_Study_1
    Inherits System.Web.UI.Page

    Protected Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        Dim Conn As SqlConnection = New SqlConnection
        '----上面已經事先寫好 Imports System.Web.Configuration ----
        Conn.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString

        'Dim dr As SqlDataReader = Nothing
        Dim cmd As New SqlCommand("select title from test where id = " & TextBox1.Text, Conn)

        Try     '==== 以下程式，只放「執行期間」的指令！=====================
            Conn.Open()   '---- 這時候才連結DB

            '---- 這時候執行SQL指令，取出資料。只有撈出單一個欄位，用這方法最快
            If cmd.ExecuteScalar() = Nothing Then
                TextBox2.Text = "查無資料！"
            Else
                TextBox2.Text = cmd.ExecuteScalar()
            End If

        Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<HR/>")

        Finally
            ''---- Always call Close when done reading.
            'If Not (dr Is Nothing) Then
            cmd.Cancel()
            ''----關閉DataReader之前，一定要先「取消」SqlCommand
            ''參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
            'dr.Close()
            'End If

            '---- Close the connection when done with it.
            If (Conn.State = ConnectionState.Open) Then
                Conn.Close()
                Conn.Dispose() '---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
            End If
        End Try
    End Sub
End Class
