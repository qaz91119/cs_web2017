﻿
Partial Class VS2010_Book_Sample_Ch19_Program_test_ADO_NET_Customizing_Error_Handling_01
    Inherits System.Web.UI.Page


    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        Try
            Dim a As Int32 = Int32.Parse(TextBox1.Text)
            Dim b As Int32 = Int32.Parse(TextBox2.Text)

            Dim res As Int32 = a / b
            Label1.Text = res.ToString()

        Catch ex As Exception
            If (TextBox1.Text.Length > 0 AndAlso TextBox2.Text.Length > 0) Then
                '=================================================
                ex.Data("ExtraInfo") = " You can't divide " & TextBox1.Text & " by " & TextBox2.Text & "."
                '==重 點！！會把這個錯誤，傳給下面的 ScriptManager1_AsyncPostBackError事件！！
            End If
            Throw ex
        End Try

    End Sub


    '***************************************************************************************
    '***  重 點！！！
    Protected Sub ScriptManager1_AsyncPostBackError(sender As Object, e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError

        If (e.Exception.Data("ExtraInfo") <> Nothing) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.Message &
                                                                                                                              e.Exception.Data("ExtraInfo").ToString()
            '== 上面的 Button_click事件傳來的「ExtraInfo」變數
        Else
            ScriptManager1.AsyncPostBackErrorMessage = "An unspecified error occurred."
        End If

    End Sub

End Class
