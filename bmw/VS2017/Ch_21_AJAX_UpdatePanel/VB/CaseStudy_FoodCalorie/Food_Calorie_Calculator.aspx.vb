﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----

Partial Class test_ADO_NET_Food_Calorie_Calculator
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '----上面已經事先寫好 Imports System.Web.Configuration ----
        '----連結資料庫----
        Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString.ToString)
        Dim dr As SqlDataReader = Nothing
        Dim sqlstr As String = "select food_calorie from food_calorie where id = " & DropDownList1.SelectedValue
        Dim cmd As SqlCommand = New SqlCommand(sqlstr, Conn)
        Try
            Conn.Open()   '---- 這時候才連結DB
            Dim food_calorie As Integer = cmd.ExecuteScalar()   '---- 這時候執行SQL指令，取出資料

            '-- 計算卡路里
            Label1.Text = CInt(TextBox1.Text) * food_calorie

        Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<HR/>")
        Finally
            '---- Always call Close when done reading.
            If Not (dr Is Nothing) Then
                cmd.Cancel()                '----關閉DataReader之前，一定要先「取消」SqlCommand
                dr.Close()
            End If
            '---- Close the connection when done with it.
            If (Conn.State = ConnectionState.Open) Then
                Conn.Close()
                Conn.Dispose()
            End If
        End Try
    End Sub
End Class
