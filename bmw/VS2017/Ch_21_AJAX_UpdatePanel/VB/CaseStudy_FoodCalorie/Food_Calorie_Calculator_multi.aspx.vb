﻿Partial Class test_ADO_NET_Food_Calorie_Calculator_multi
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If ViewState("total") = Nothing Then
            ViewState("total") = 0
        End If
    End Sub


    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
        '-- 點選任何一個子選項，底下的 ListBox就會出現，並立刻計算卡路里
        ListBox1.Items.Add(DropDownList1.SelectedItem.Text & "@" & DropDownList1.SelectedValue)

        Label1.Text = "<font color=blue>" & DropDownList1.SelectedValue & "</font>"  '--被選取的這項食物的卡路里

        ViewState("total") = ViewState("total") + CInt(DropDownList1.SelectedValue)
        Label2.Text = ViewState("total")
    End Sub


    Protected Sub ListBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        '-- 點選 Listbox 的子選項，可以刪除之。
        Dim word_length As Integer = Len(ListBox1.SelectedItem.Text) - InStr(1, ListBox1.SelectedItem.Text, "@")
        Label1.Text = "<font color=red> -" & Right(ListBox1.SelectedItem.Text, word_length) & "</font>"  '--被選取的這項食物的卡路里

        ViewState("total") = ViewState("total") - CInt(Right(ListBox1.SelectedItem.Text, word_length))
        Label2.Text = ViewState("total")

        ListBox1.Items.Remove(ListBox1.SelectedItem.Text)  '--移除 ListBox1「被選到的」子選項
    End Sub


End Class
