﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AJAX_JavaScript_00.aspx.vb" Inherits="VS2010_Book_Sample_Ch19_Program_test_ADO_NET_AJAX_ResponseWrite_01" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        這是一個錯誤的功能，請看錯誤畫面與訊息。<br />
        <a href="http://msdn.microsoft.com/zh-tw/library/bb359558.aspx">
        http://msdn.microsoft.com/zh-tw/library/bb359558.aspx</a>
        <br />
        <br />
        <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
        <br />
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <hr />
                <% Response.Write("Hello !! <br />")%>
                <% Response.Write(DateTime.Now())%>
                <% Response.Write("<script language=""javascript"">window.alert(""--" & DateTime.Now() & "--"")</script>")%>
                <hr />
            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
