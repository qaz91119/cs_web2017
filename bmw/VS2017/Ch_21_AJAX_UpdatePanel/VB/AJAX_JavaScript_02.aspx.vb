﻿
Partial Class VS2010_Book_Sample_Ch19_Program_test_ADO_NET_AJAX_JavaScript_02
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        'ScriptManager.RegisterStartupScript(Me, GetType(Button), "JavaScript_Sample", "window.alert(""Hello!! Code Behind!!"")", true)

        ' 如果您要註冊與部分網頁更新 [無關]的啟動指令碼，而且只要在初始網頁呈現期間註冊一次指令碼，
        ' 請使用 ClientScriptManager 類別的 RegisterStartupScript 方法。
    End Sub



    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        ScriptManager.RegisterStartupScript(Me, GetType(Button), "JavaScript_Sample2", "window.alert(""Hello!! Code Behind!!"")", True)

        '*** 參數 ***

        '  page型別：System.Web.UI.Page
        '    正在註冊用戶端指令碼區塊的頁面物件。

        '  type型別：System.Type
        '     用戶端指令碼區塊的型別。這個參數通常是使用 typeof 運算子 (C#) 或 GetType 運算子 (Visual Basic) 所指定，用以擷取註冊指令碼的控制項型別。
        '  key型別：System.String
        '     指令碼區塊的唯一識別項。請自己命名！
        '  script型別：System.String
        '     JavaScript指令碼。
        '  addScriptTags型別：System.Boolean
        '     若要使用 <script> 和 </script> 標記來匡住 JavaScript指令碼區塊，則為 true，否則為 false。
    End Sub



    Protected Sub Button2_Click(sender As Object, e As System.EventArgs) Handles Button2.Click
        'Dim JavaScript_Str As String = "window.alert(""Hello!! --" & DateTime.Now.ToLongTimeString() & "--"")"
        Dim JavaScript_Str As String = "if (confirm(""再一次確認，您確定要刪除嗎？"")){return true;} else {return false;}"

        ScriptManager.RegisterStartupScript(Me, GetType(Button), "JavaScript_Sample2", JavaScript_Str, True)
    End Sub
End Class
