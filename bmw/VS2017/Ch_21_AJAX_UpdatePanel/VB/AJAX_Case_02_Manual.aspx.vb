﻿
Partial Class AJAX_CallBack_AJAX_Case_02_Manual
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '-- 利用 RegisterAsyncPostBackControl 方法，將一個控制項註冊成觸發程序。
        '-- 接著它用 Update 方法，以程式設計方式來重新整理 UpdatePanel 控制項。

        '-- 資料來源：http://msdn.microsoft.com/zh-tw/library/bb386452.aspx

        ScriptManager1.RegisterAsyncPostBackControl(Button1)
        ScriptManager1.RegisterAsyncPostBackControl(Button2)

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        UpdatePanel2.Update()
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        UpdatePanel1.Update()
    End Sub
End Class
