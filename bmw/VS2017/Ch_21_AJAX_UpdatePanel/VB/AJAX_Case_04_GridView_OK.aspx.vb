﻿
Partial Class AJAX_CallBack_AJAX_Case_02_GridView
    Inherits System.Web.UI.Page


    '--本範例源自於微軟MSDN網站：http://msdn.microsoft.com/zh-tw/library/bb386452.aspx

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        '-- GridView.RowDataBound 事件
        '-- 資料列繫結至 GridView 控制項中的資料時發生。
        '-- 使用 RowDataBound 事件，先修改資料來源中某個欄位的值，再顯示到 GridView 控制項。

        '-- 參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.gridview.rowdatabound.aspx

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim SQLds As SqlDataSource = CType(e.Row.FindControl("SqlDataSource2"), SqlDataSource)
            '-- 先抓到 GridView1每一列 DataRow裡面的 SqlDataSource2控制項

            Dim DRV As System.Data.DataRowView = CType(e.Row.DataItem, System.Data.DataRowView)

            SQLds.SelectParameters("t_id").DefaultValue = DRV("id").ToString()
            '-- DRV("id")的意思： 
            '--           外部的（父）UpdatePanel的 GridView1，目前這一列資料的「主索引鍵（id欄位）」是幾號？
            '-- 把上一步驟抓到的「主索引鍵」，交給 SqlDataSource2去執行。

        End If

    End Sub

End Class
