﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Warning_Color_2.aspx.cs" Inherits="Book_Sample_Ch19_AJAX_Warning_Color_2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        用了AJAX（UpdatePanel），所以當我們點選DropDownList的時候，圖片變化不會導致PostBack。<br />
        <br />
        <%=System.DateTime.Now.ToLongTimeString() %>
    
        <br />
        出貨率：<asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
            <asp:ListItem Value="1">10%</asp:ListItem>
            <asp:ListItem Value="2">20%</asp:ListItem>
            <asp:ListItem Value="3">30%</asp:ListItem>
            <asp:ListItem Value="4">40%</asp:ListItem>
            <asp:ListItem Value="5">50%</asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                ===========================================<br />
                <asp:Image ID="Image1" runat="server" />
                <br />
                <br />UpdatePanel時間--<font color="red"><%=System.DateTime.Now.ToLongTimeString() %></font><br />===========================================
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="DropDownList1" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <br />
        <%=System.DateTime.Now.ToLongTimeString() %>
    </div>
    </form>
</body>
</html>
