﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Book_Sample_Ch19_AJAX_Warning_Color_2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (DropDownList1.SelectedValue)
        {
            case "1":
                Image1.ImageUrl = "Chart_Images/1.jpg";
                break;
            case "2":
                Image1.ImageUrl = "Chart_Images/2.jpg";
                break;
            case "3":
                Image1.ImageUrl = "Chart_Images/3.jpg";
                break;
            case "4":
                Image1.ImageUrl = "Chart_Images/4.jpg";
                break;
            case "5":
                Image1.ImageUrl = "Chart_Images/5.jpg";
                break;

            default:
                Image1.ImageUrl = "Chart_Images/1.jpg";
                break;
        }
        
    }
}