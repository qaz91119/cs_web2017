﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Book_Sample_Ch19_AJAX_Warning_Color_3_Auto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        int i = System.DateTime.Now.Second;

        String i_str = (i % 5).ToString();   //--計算後，傳回餘數。
        switch (i_str)
        {
            case "1":
                Image1.ImageUrl = "Chart_Images/1.jpg";
                break;
            case "2":
                Image1.ImageUrl = "Chart_Images/2.jpg";
                break;
            case "3":
                Image1.ImageUrl = "Chart_Images/3.jpg";
                break;
            case "4":
                Image1.ImageUrl = "Chart_Images/4.jpg";
                break;

            default:
                Image1.ImageUrl = "Chart_Images/5.jpg";    //--以上皆非。餘數為零。
                break;
        }
    }
}