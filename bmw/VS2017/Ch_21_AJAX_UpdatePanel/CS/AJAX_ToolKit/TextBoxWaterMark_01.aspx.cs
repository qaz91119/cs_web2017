﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Book_Sample_Ch19_AJAX_AJAX_ToolKit_TextBoxWaterMark : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Label1.Text = "<font color=red>TextBox1.Text -- " + TextBox1.Text + "</font><br />";
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Label2.Text = "<font color=blue>TextBox2.Text -- " + TextBox2.Text + "</font>";
    }
}