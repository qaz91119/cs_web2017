﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TextBoxWaterMark_01.aspx.cs" Inherits="Book_Sample_Ch19_AJAX_AJAX_ToolKit_TextBoxWaterMark" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <strong>浮水印 &lt;ajaxToolkit:TextBoxWatermarkExtender</strong><br />

        <br />
        
        <br />
        第一種狀況：正常的網頁。<br />
        <br />
        外部時間：<%=System.DateTime.Now.ToLongTimeString() %>
        <br />
        <br />

                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>

                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                    TargetControlID="TextBox1"
                    WatermarkCssClass="TextboxWatermark" WatermarkText="TextBox1浮水印">
                </ajaxToolkit:TextBoxWatermarkExtender>

               <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button1" />
        <br />

        <asp:Label ID="Label1" runat="server" style="font-weight: 700; color: #FF00FF"></asp:Label>

        <br />
       
        <br />
        <br />
        第二種狀況，放在 UpdatePanel裡面。<br />
        <br />
        以下有 UpdatePanel ----
        <br />
        <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                =========================================<br />
                內部時間：<%=System.DateTime.Now.ToLongTimeString() %><br /><asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                <br />
                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                    TargetControlID="TextBox2"
                    WatermarkCssClass="TextboxWatermark" WatermarkText="TextBox2浮水印">
                </ajaxToolkit:TextBoxWatermarkExtender>

                <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Button2" />
                <br />
                <asp:Label ID="Label2" runat="server" style="color: #0066FF; font-weight: 700"></asp:Label>
                <br />
                =========================================<br />

            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Button2" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    
    </div>
    </form>
    <p>
        &nbsp;</p>
</body>
</html>
