﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TextBoxWaterMark_02.aspx.cs" Inherits="Book_Sample_Ch19_AJAX_AJAX_ToolKit_TextBoxWaterMark_02" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <strong>浮水印 &lt;ajaxToolkit:TextBoxWatermarkExtender</strong><br />

            <br />
            外部時間：<%=System.DateTime.Now.ToLongTimeString() %>
            <br />

            <br />
            <br />

            第三種狀況，放在 UpdatePanel裡面，而且放在 GridView「樣版」內部。<br />
            <br />
            以下有 UpdatePanel ----
            <br />
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <br />
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    =========================================<br />
                    內部時間：<%=System.DateTime.Now.ToLongTimeString() %><br /><br /><br /><asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1">
                        <Columns>
                            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                            <asp:TemplateField HeaderText="title" SortExpression="title">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("title") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("title") %>'></asp:Label>
                                    <hr />
                                    <br />

                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                    <br />
                                    <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                        TargetControlID="TextBox2"
                                        WatermarkCssClass="TextboxWatermark" WatermarkText="TextBox2浮水印">
                                    </ajaxToolkit:TextBoxWatermarkExtender>
                                    
                                </ItemTemplate>
                                <ItemStyle BackColor="#FFCCFF" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                         ConnectionString="<%$ ConnectionStrings:testConnectionString %>" 
                        SelectCommand="SELECT Top 3 [id], [title] FROM [test]"></asp:SqlDataSource>

                    <br />

                    <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Button2_抓GridView第一列的TextBox" />
                    <br />
                    <asp:Label ID="Label2" runat="server" Style="color: #0066FF; font-weight: 700"></asp:Label>
                    <br />
                    =========================================<br />

                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Button2" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>

        </div>
    </form>

</body>
</html>
