﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Book_Sample_Ch19_AJAX_CaseStudy_Easy_AJAX_AJAX_002_Page_Load : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //== 本範例與上一個範例的差別！ ==
        Label1.Text = System.DateTime.Now.ToLongTimeString();
    }
}