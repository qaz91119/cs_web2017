﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AJAX_004_Multiple_Manual.aspx.cs" Inherits="Book_Sample_Ch19_AJAX_CaseStudy_Easy_AJAX_AJAX_004_Multiple_Manual" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .auto-style3 {
            color: #FF0000;
        }
        .auto-style4 {
            color: #006600;
        }
    </style>
</head>
<body>
    <p>
        <br />
    </p>
    <form id="form1" runat="server">
        外部時間（PostBack）--<%=System.DateTime.Now.ToLongTimeString()%>
        <p>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
        </p>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                =======================<br />
                內部時間（AJAX<span class="auto-style3"><strong>，每三秒更新</strong></span>）--<%=System.DateTime.Now.ToLongTimeString()%><br /><asp:Timer ID="Timer1" runat="server" Interval="3000" OnTick="Timer1_Tick">
                </asp:Timer>

                <asp:Label ID="Label1" runat="server" Style="font-weight: 700; color: #FF0000" Text="Label"></asp:Label>
                <br />
                =======================
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
            </Triggers>
        </asp:UpdatePanel>
        <p>
            &nbsp;
        </p>
        <p>
        </p>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                =======================<br />
                內部時間（AJAX<span class="auto-style4">，<strong>每一秒更新</strong></span>）--<%=System.DateTime.Now.ToLongTimeString()%><br /><asp:Timer ID="Timer2" runat="server" Interval="1000" OnTick="Timer2_Tick">
                </asp:Timer>

                <asp:Label ID="Label2" runat="server" Style="font-weight: 700; color: #006600" Text="Label"></asp:Label>
                <br />
                =======================
            </ContentTemplate>

            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer2" EventName="Tick" />
            </Triggers>
        </asp:UpdatePanel>

        <p></p>
        <p>重點：</p>
        <p>
            (1). 畫面上「<span class="auto-style4"><strong>多個</strong></span>」UpdatePanel。
        </p>
        <p>
            (2). 因此，只需進行「切割手術」，並且設定 Trigger屬性。
        </p>
        <p>
            (3). 使用 Timer控制項。
        </p>
        <p>
            (4). 後置程式碼，寫在<strong>Timer的 <span class="auto-style3">Tick事件</span></strong>。
        </p>

        </div>
    </form>
</body>
</html>
