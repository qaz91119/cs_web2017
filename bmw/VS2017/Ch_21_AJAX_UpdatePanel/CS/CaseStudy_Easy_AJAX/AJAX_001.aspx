﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AJAX_001.aspx.cs" Inherits="Book_Sample_Ch19_AJAX_CaseStudy_Easy_AJAX_AJAX_001" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <p>
        <br />
    </p>
    <form id="form1" runat="server">
        外部時間（PostBack）--<%=System.DateTime.Now.ToLongTimeString()%>
        <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
        <p>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
        </p>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                =======================<br />
                內部時間（AJAX）--<%=System.DateTime.Now.ToLongTimeString()%><br /><asp:Label ID="Label1" runat="server" Style="font-weight: 700; color: #FF00FF" Text="Label"></asp:Label>
                <br />
                =======================
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Button1" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <p>
            &nbsp;
        </p>
        <p>
            &nbsp;
        </p>
        <p>
            重點：
        </p>
        <p>
            (1). 畫面上只有「一個」UpdatePanel。
        </p>
        <p>
            (2). 因此，只需設定 Trigger屬性。
        </p>
        <p>
            (3). 後置程式碼，寫在 Button1_Click事件裡面。
        </p>
        <div>
        </div>
    </form>
</body>
</html>
