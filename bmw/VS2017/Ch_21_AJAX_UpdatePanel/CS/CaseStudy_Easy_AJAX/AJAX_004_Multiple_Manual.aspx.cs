﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Book_Sample_Ch19_AJAX_CaseStudy_Easy_AJAX_AJAX_004_Multiple_Manual : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        Label1.Text = "Code Behind --" + System.DateTime.Now.ToLongTimeString();
    }
    protected void Timer2_Tick(object sender, EventArgs e)
    {
        Label2.Text = "Code Behind --" + System.DateTime.Now.ToLongTimeString();
    }
}