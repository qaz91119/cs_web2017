﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Warning_Color_3_Auto.aspx.cs" Inherits="Book_Sample_Ch19_AJAX_Warning_Color_3_Auto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .auto-style1 {
            color: #FFFF00;
        }
        .auto-style2 {
            background-color: #FF0000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            用了AJAX（UpdatePanel），圖片變化不會導致PostBack。<br />
            <br />
            使用<span class="auto-style1"><strong><span class="auto-style2"> Timer的 Tick事件</span></strong></span>，隨著時間變化，圖片也會變化。<br />
            如果這事件裡面的程式，是從資料庫撈出的成果，就是即時的「線上戰情室」。<br />
            <%=System.DateTime.Now.ToLongTimeString() %>

            <br />
            <br />
            <br />
            <br />
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    ===========================================<br />
                    <asp:Image ID="Image1" runat="server" />
                    <br />
                    <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick">
                    </asp:Timer>
                    <br />
                    UpdatePanel時間--<font color="red"><%=System.DateTime.Now.ToLongTimeString() %></font><br />===========================================
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <%=System.DateTime.Now.ToLongTimeString() %>
        </div>
    </form>
</body>
</html>
