<%@ Page Language="C#" AutoEventWireup="true" CodeFile="11.aspx.cs" Inherits="Ch03_WebControls_11" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PlaceHolder控制項</title>
</head>
<body>
    <form id="form1" runat="server">
    PlaceHolder控制項<br />
    <div>
        <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        外部時間：<%=System.DateTime.Now.ToLongTimeString() %><br />

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                ============================================<br />
                <br />
                <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                <br />
                <br />
                <asp:Button ID="Button1" runat="server" Text="按下去，看看結果有何改變" OnClick="Button1_Click" />
                <br />
                <br />
                內部時間：<%=System.DateTime.Now.ToLongTimeString() %>
                <br />
                <br />
                ============================================
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
        <br />
        <br />

    </div>
    </form>
</body>
</html>
