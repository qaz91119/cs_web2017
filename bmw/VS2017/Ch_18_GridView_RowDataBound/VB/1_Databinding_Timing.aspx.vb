﻿
Partial Class Book_Sample_Ch11_Program_GridView_RowDataBound_7_Samples_1_Databinding_Timing
    Inherits System.Web.UI.Page


    Protected Sub GridView1_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowCreated
        Response.Write("<font color=red>*** RowCreated （優先執行）***</font><br />")
    End Sub

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        Response.Write("<h3>*** RowDataBound ***</h3>")
    End Sub


    Protected Sub GridView1_DataBinding(sender As Object, e As EventArgs) Handles GridView1.DataBinding
        Response.Write("<h3><font color=Green>*** GridView1_DataBinding***</font></h3><br />")
    End Sub

End Class
