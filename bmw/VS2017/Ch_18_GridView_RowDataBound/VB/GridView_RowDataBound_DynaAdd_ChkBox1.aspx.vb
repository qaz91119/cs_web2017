﻿
Partial Class Book_Sample_Ch11_Program_GridView_RowDataBound_7_Samples_GridView_RowDataBound_DynaAdd_ChkBox1
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        '重點！！改用 RowCreated事件就成功了

        If e.Row.RowType = DataControlRowType.DataRow Then

            '*** 下列程式，請參閱上集Ch.11 範例GridView_RowDataBound_6_CaseStudy.aspx。***(start)
            '*** 重點！既是「偶數列(Alternate)」、又進入編輯模式。
            If e.Row.RowState = DataControlRowState.Edit Or
                                 (e.Row.RowState = (DataControlRowState.Edit Or DataControlRowState.Alternate)) Then
                Dim CB As CheckBox = New CheckBox()
                CB.ID = "DynaAdd_CB1"
                CB.Text = "請選我"

                e.Row.Cells(2).Controls.Add(CB)
                ' Response.Write("<h3>" & CB.UniqueID & "</h3>")      ' 測試用
            End If
        End If
        '*********************************************************************(end)

    End Sub


    Protected Sub GridView1_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles GridView1.RowUpdating
        '*** 按下「更新」按鈕。 ***

        Dim CB As CheckBox = GridView1.Rows(e.RowIndex).FindControl("DynaAdd_CB1")
        Response.Write("<h3>" & CB.Checked.ToString() & "</h3>")
        Response.End()   ' 抓不到，因為PostBack的緣故。

        ''**************************************************************
        ''*** 另一種作法（可抓到值）**************************************
        ' ''測試用
        'Response.Write("<h3>" & (e.RowIndex + 2).ToString() & "</h3>")
        'Response.End();

        'If ((e.RowIndex + 2) < 10) Then
        '    Response.Write("<h3>" & Request("GridView1$ctl0" & (e.RowIndex + 2).ToString() & "$DynaAdd_CB1") & "</h3>")
        '    '有勾選的話，就會出現「on」字樣
        'Else
        '    Response.Write("<h3>" & Request("GridView1$ctl" & (e.RowIndex + 2).ToString() & "$DynaAdd_CB1") & "</h3>")
        '    '有勾選的話，就會出現「on」字樣        '       
        'End If
        'Response.End()

    End Sub
End Class
