﻿
Partial Class test_GridView_Manual_Set_Width_2
    Inherits System.Web.UI.Page

    '***********************
    '-- 參考資料：本範例改寫自 http://msdn.microsoft.com/zh-tw/library/ms178296.aspx
    Public colWidth As Integer = 1
    '***********************

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        colWidth = CInt(Server.HtmlEncode(TextBox1.Text))

        '*** 必須重新 DataBinding，才會讓 GridView的控制項重新整理一次，觸發相關事件。 ***
        GridView1.DataBind()
    End Sub


    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        '-- GridView.RowDataBound 事件
        '-- 資料列繫結至 GridView 控制項中的資料時發生。
        '-- 使用 RowDataBound 事件，先修改資料來源中某個欄位的值，再顯示到 GridView 控制項。

        '-- 參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.gridview.rowdatabound.aspx

        If e.Row.RowType = DataControlRowType.DataRow Then

            If colWidth > 0 Then
                GridView1.Columns(0).ItemStyle.Width = colWidth  '--第一個欄位
                GridView1.Columns(0).ItemStyle.Wrap = True

                GridView1.Columns(1).ItemStyle.Width = colWidth * 5  '--第二個欄位
                GridView1.Columns(1).ItemStyle.Wrap = True  '--欄位裡面的文字，可以自動換行。

                GridView1.Columns(2).ItemStyle.Width = colWidth * 15  '--第三個欄位
                GridView1.Columns(2).ItemStyle.Wrap = True
            Else
                Label1.Text = "輸入的寬度，不可小於、等於[零]"
            End If

        End If

    End Sub


End Class
