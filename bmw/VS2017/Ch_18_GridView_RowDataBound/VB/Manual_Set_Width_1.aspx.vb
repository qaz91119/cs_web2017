﻿
Partial Class test_GridView_Manual_Set_Width_1
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '--資料來源：http://msdn.microsoft.com/zh-tw/library/ms178296.aspx

        Dim colWidth As Integer = CInt(Server.HtmlEncode(TextBox1.Text))

        If colWidth > 0 Then
            For i As Integer = 0 To GridView1.Columns.Count - 1
                GridView1.Columns(i).ItemStyle.Width = colWidth
                '-- GridView每一欄（行）Columns(i)的寬度（ItemStyle.Width）屬性
            Next
        Else
            Label1.Text = "輸入的寬度，不可小於、等於[零]"
        End If

    End Sub
End Class
