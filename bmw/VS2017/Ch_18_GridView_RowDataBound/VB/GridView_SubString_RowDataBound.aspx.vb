﻿
Partial Class Book_Sample_Ch11_Program_GridView_SubString_RowDataBound
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim summary As String = e.Row.Cells(2).Text

            If summary.Length > 60 Then
                e.Row.Cells(2).Text = summary.Substring(0, 60) & "......"
                '-- .Substring()方法，從零算起的第幾個字「長度」。
            End If

        End If

    End Sub
End Class
