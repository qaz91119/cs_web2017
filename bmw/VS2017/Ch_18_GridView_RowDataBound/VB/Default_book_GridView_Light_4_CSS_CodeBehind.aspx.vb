﻿
Partial Class Book_Sample_Ch11_Program_Default_book_GridView_Light_4_CSS_CodeBehind
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        '-- 請參考微軟MSDN網站的說明：
        '   參考網址  http://msdn2.microsoft.com/zh-tw/library/system.web.ui.webcontrols.datacontrolrowtype(VS.80).aspx

        If e.Row.RowType = DataControlRowType.DataRow Then
            '== 要加入 CSS的光棒效果，每一列（Row）都必須加入才行。<tr class="altrow">
            '== e.Row代表每一列，轉成HTML之後就是表格的 <tr>標籤。

            If (e.Row.RowIndex Mod 2) = 0 Then
                '隔列換色，所以只有偶數列要換底色
                e.Row.Attributes.Add("class", "altrow")
            End If

        End If
    End Sub

End Class
