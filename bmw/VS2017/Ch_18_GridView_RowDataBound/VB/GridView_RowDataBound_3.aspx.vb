﻿
Partial Class Book_Sample_Ch11_Program_GridView_RowDataBound_7_Samples_GridView_RowDataBound_3
    Inherits System.Web.UI.Page

    Private Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound

        ' 按下「編輯」按鈕之後，以前的版本會報錯。新版VS 2013 / 2015不會。
        If e.Row.RowType = DataControlRowType.DataRow Then
            Response.Write(e.Row.Cells(3).Text & "<br>")
        End If

        '' 錯誤寫法  只會出現單數列（1 / 3 / 5 / 7 / 9），雙數列的標題消失了！!
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    If (e.Row.RowState = DataControlRowState.Normal) Then
        '        Response.Write(e.Row.Cells(3).Text & "<br>")
        '    End If
        'End If

        '' ***** 正確版本！! **************************************
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    If e.Row.RowState <> DataControlRowState.Edit Then
        '        Response.Write(e.Row.Cells(3).Text & "<br>")
        '    End If
        'End If

        ''  ***** 正確版本！! **************************************
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    If e.Row.RowState = DataControlRowState.Normal Or e.Row.RowState = DataControlRowState.Alternate Then
        '        Response.Write(e.Row.Cells(3).Text & "<br>")
        '    End If
        'End If

    End Sub
End Class
