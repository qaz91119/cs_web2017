﻿
Partial Class VS2010_Book_Sample_Ch07_Program_Default_book_GridView_Light_3_JavaScript
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        '-- 請參考微軟MSDN網站的說明：
        '   參考網址  http://msdn2.microsoft.com/zh-tw/library/system.web.ui.webcontrols.datacontrolrowtype(VS.80).aspx

        If e.Row.RowType = DataControlRowType.DataRow Then
            '*** 方法一  ************************************************************************
            '==要加入 JavaScript的光棒效果，每一列（Row）都必須加入才行。
            '== e.Row代表每一列，轉成HTML之後就是表格的 <tr>標籤。
            e.Row.Attributes.Add("OnMouseover", "this.style.backgroundColor='#E3EAEB'")
            e.Row.Attributes.Add("OnMouseout", "this.style.backgroundColor='#FFFFFF'")


            '*** 方法二  ************************************************************************
            '== JavaScript的光棒效果，加到每一個格子（Cells）裡面。
            'For i As Integer = 0 To (e.Row.Cells.Count - 1)    '==計算每一列有幾個格子？
            '    e.Row.Cells(i).Attributes.Add("OnMouseover", "this.style.backgroundColor='#E3EAEB'")
            '    e.Row.Cells(i).Attributes.Add("OnMouseout", "this.style.backgroundColor='#FFFFFF'")
            'Next

        End If
    End Sub

End Class
