﻿
Partial Class Book_Sample_Ch11_Program_GridView_TextWrap_02
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        GridView1.Attributes.Add("Style", "word-break:break-all; word-break:break-word")
        ' 在GridView轉換成傳統HTML表格時，加入這段CSS。

        ' 執行後，網頁的HTML原始檔：  
        ' <table cellspacing="0" rules="all" border="1" id="GridView1" style="border-collapse:collapse;word-break:break-all; word-break:break-word">

    End Sub
End Class
