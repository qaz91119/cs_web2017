﻿'----自己寫的（宣告)----
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的（宣告)----

Partial Class GridView_RowDataBound_2_CaseStudy_ADOnet
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim Conn As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)
        Dim dr As SqlDataReader = Nothing
        Dim cmd As SqlCommand = New SqlCommand("select [name], [math] from [student_test]", Conn)

        Try     '==== 以下程式，只放「執行期間」的指令！=====================
            '== 第一，連結資料庫。
            Conn.Open()   '---- 這時候才連結DB

            '== 第二，執行SQL指令。
            dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料

            '==第三，自由發揮，把執行後的結果呈現到畫面上。
            '==自己寫迴圈==
            Label1.Text = "<table border=1>"
            While (dr.Read())
                Label1.Text &= "<tr>"
                Label1.Text &= "<td>" & dr("name") & "</td>"

                If dr("math") < 60 Then
                    ' 不及格，出現紅字！
                    Label1.Text &= "<td><font color=red>" & dr("math") & "</font></td>"
                Else
                    Label1.Text &= "<td>" & dr("math") & "</td>"
                End If

                Label1.Text &= "</tr>"
            End While
            Label1.Text &= "</table>"   '若資料量大，請用StringBuilder來做（命名空間 System.Text）
            
        Catch ex As Exception
            '---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" & ex.ToString() & "<hr />")

        Finally
            ' == 第四，釋放資源、關閉資料庫的連結。
            '---- Always call Close when done reading.
            If (dr IsNot Nothing) Then
                cmd.Cancel()
                '----關閉DataReader之前，一定要先「取消」SqlCommand
                '參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
                dr.Close()
            End If

            '---- Close the connection when done with it.
            If (Conn.State = ConnectionState.Open) Then
                Conn.Close()
                Conn.Dispose()   '---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
            End If

        End Try

    End Sub

End Class
