﻿
Partial Class GridView_RowDataBound_6_RowState
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowCreated(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated

        If e.Row.RowType = DataControlRowType.DataRow Then

            '== 修正上一支程式的缺點！
            '== 但發生其他錯誤，第二、第四列的 Edit按鈕不會動作。 ==
            If e.Row.RowState = DataControlRowState.Normal Then
                Dim btn As Button = CType(e.Row.FindControl("Button1"), Button)
                btn.CommandName = "Edit"
            End If

            '    '== [正確版] =============================================
            '    If e.Row.RowState = DataControlRowState.Normal Or e.Row.RowState = DataControlRowState.Alternate Then
            '        Dim btn As Button = CType(e.Row.FindControl("Button1"), Button)
            '        btn.CommandName = "Edit"
            '    End If
        End If
    End Sub



    'Protected Sub GridView1_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
    '    '== 程式放在這裡也會運作喔！==
    '    If e.Row.RowType = DataControlRowType.DataRow Then

    '        '== [正確版] =============================================
    '        If e.Row.RowState = DataControlRowState.Normal Or e.Row.RowState = DataControlRowState.Alternate Then
    '            Dim btn As Button = CType(e.Row.FindControl("Button1"), Button)
    '            btn.CommandName = "Edit"
    '        End If
    '    End If
    'End Sub
End Class
