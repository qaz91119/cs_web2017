﻿
Partial Class Book_Sample_Ch11_Program_GridView_TextWrap_01
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Style.Add("word-break", "break-all")
            ' 動態加入CSS的「Style」

            '執行成果，畫面的HTML原始檔：
            '    <td style="color:#CC0066;width:150px;word-break:break-all;">
            '    每一列的第三格（title欄位）都有這段CSS。
        End If

    End Sub
End Class
