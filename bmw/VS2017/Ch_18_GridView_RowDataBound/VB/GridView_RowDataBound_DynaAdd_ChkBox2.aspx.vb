﻿
Partial Class Book_Sample_Ch11_Program_GridView_RowDataBound_7_Samples_GridView_RowDataBound_DynaAdd_ChkBox2
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowCreated
        '重點！！改用 RowCreated事件就成功了

        If e.Row.RowType = DataControlRowType.DataRow Then

            '*** 下列程式，請參閱上集Ch.11 範例GridView_RowDataBound_6_CaseStudy.aspx。***(start)
            '*** 重點！既是「偶數列(Alternate)」、又進入編輯模式。
            If e.Row.RowState = DataControlRowState.Edit Or
                                 (e.Row.RowState = (DataControlRowState.Edit Or DataControlRowState.Alternate)) Then
                Dim CB As CheckBox = New CheckBox()
                CB.ID = "DynaAdd_CB1"
                CB.Text = "請選我"

                e.Row.Cells(2).Controls.Add(CB)
                ' Response.Write("<h3>" & CB.UniqueID & "</h3>")      ' 測試用
            End If
        End If
        '*********************************************************************(end)
    End Sub


    Protected Sub GridView1_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles GridView1.RowUpdating
        '*** 按下「更新」按鈕。 ***

        Dim CB As CheckBox = GridView1.Rows(e.RowIndex).FindControl("DynaAdd_CB1")
        Response.Write("<h3>" & CB.Checked.ToString() & "</h3>")
        Response.End()   ' 抓不到，因為PostBack的緣故。
    End Sub
End Class
