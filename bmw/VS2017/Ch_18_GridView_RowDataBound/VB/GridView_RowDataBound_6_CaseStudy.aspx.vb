﻿
Partial Class Book_Sample_Ch11_Program_GridView_Edit_DefaultValue
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            If e.Row.RowState = DataControlRowState.Edit Or _
                e.Row.RowState = (DataControlRowState.Edit Or DataControlRowState.Alternate) Then

                Dim tb As TextBox = e.Row.FindControl("TextBox1")
                '正確寫法：Dim tb As TextBox = Ctype(e.Row.FindControl("TextBox1"), TextBox)

                tb.Text = "進入編輯模式，立刻給予「預設值」。"
            End If

        End If
        '參考資料：http://www.blueshop.com.tw/board/show.asp?subcde=BRD20090210113050XB9

    End Sub
End Class
