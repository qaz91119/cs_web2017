﻿
Partial Class GridView_RowDataBound_2_CaseStudy_PreRender
    Inherits System.Web.UI.Page


    Protected Sub GridView1_PreRender(sender As Object, e As EventArgs) Handles GridView1.PreRender
        ' 在控制項呈現到畫面「之前」，做最後的處理

        For Each GV_Row As GridViewRow In GridView1.Rows
            ' 參考範例  http://www.dotblogs.com.tw/mis2000lab/archive/2012/01/13/gridview_multi_row_updating_20120113.aspx

            If Convert.ToInt32(GV_Row.Cells(5).Text) < 60 Then
                GV_Row.Cells(5).ForeColor = System.Drawing.Color.Red
            End If

        Next
    End Sub


End Class
