﻿
Partial Class GridView_PreRender
    Inherits System.Web.UI.Page

    Protected Sub GridView1_PreRender(sender As Object, e As EventArgs) Handles GridView1.PreRender
        ' 在控制項呈現到畫面「之前」，做最後的處理

        Dim sum As Integer

        For Each GV_Row As GridViewRow In GridView1.Rows
            ' 參考範例  http://www.dotblogs.com.tw/mis2000lab/archive/2012/01/13/gridview_multi_row_updating_20120113.aspx

            sum += Convert.ToInt32(GV_Row.Cells(5).Text)
        Next

        Label1.Text = "數學成績的加總 = " & sum.ToString()
    End Sub

End Class
