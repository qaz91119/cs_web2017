﻿
Partial Class VS2010_Book_Sample_Ch11_Program_GridView_RowDataBound_7
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowCreated(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        '*** 重點！！動態加入 PlaceHolder裡面的
        '***  Button按鈕的「CommandName」，必須放在 RowCreated事件才生效！ ***

        If e.Row.RowType = DataControlRowType.DataRow Then

            '== 修正上一支程式的缺點！
            '== 但發生其他錯誤，第二、第四列的 Edit按鈕不會動作。 ==
            'If e.Row.RowState = DataControlRowState.Normal Then
            '    Dim btn As Button = CType(e.Row.FindControl("Button1"), Button)
            '    btn.CommandName = "Edit"
            'End If

            '== [正確版] =============================================
            If e.Row.RowState = DataControlRowState.Normal Or e.Row.RowState = DataControlRowState.Alternate Then
                Dim btn As New Button  '== 動態加入的按鈕，必須使用 New

                '*********************************************************************
                btn.ID = "Button1"   '== 很特別的解法！！不要加上ID屬性，就會正常運作！！==
                '*********************************************************************

                btn.Text = "自訂的、動態加入PlaceHolder的編輯按鈕"
                btn.CommandName = "Edit"

                Dim ph As PlaceHolder = CType(e.Row.FindControl("PlaceHolder1"), PlaceHolder)
                ph.Controls.Add(btn)
            End If
        End If
    End Sub


    'Protected Sub GridView1_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
    '    '*** 重點！！有 Bug！ ***

    '    If e.Row.RowType = DataControlRowType.DataRow Then

    '        '== [正確版] =============================================
    '        If e.Row.RowState = DataControlRowState.Normal Or e.Row.RowState = DataControlRowState.Alternate Then
    '            Dim btn As New Button  '== 動態加入的按鈕，必須使用 New
    '            btn.ID = "Button1"
    '            btn.Text = "自訂的、動態加入PlaceHolder的編輯按鈕"
    '            btn.CommandName = "Edit"

    '            Dim ph As PlaceHolder = CType(e.Row.FindControl("PlaceHolder1"), PlaceHolder)
    '            ph.Controls.Add(btn)
    '        End If
    '    End If
    'End Sub
End Class
