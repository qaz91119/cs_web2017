﻿
Partial Class Book_Sample_Ch12_Program_test_DataList_ListView_ItemDataBound
	Inherits System.Web.UI.Page

	Private Sub ListView1_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles ListView1.ItemDataBound

		' 參考資料 https://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.listviewitem.itemtype(v=vs.110).aspx

		If e.Item.ItemType = ListViewItemType.DataItem Then
			' 發現 e.Item.DataItem 原來是 System.Data.DataRowView
			'Response.Write(e.Item.DataItem.ToString() & "<br>")

			Dim DRV As System.Data.DataRowView = e.Item.DataItem

			' (1). 列出第一個欄位的值
			Response.Write("id--" & DRV.Row.ItemArray(0) & "<br />")

			' (2). 列出某一個欄位的值，透過.FindControl()方法
			Dim LB As Label = e.Item.FindControl("nameLabel")
			Response.Write(LB.Text & "<br />")

			' (3). 資料來源 http//www.dotblogs.com.tw/dyco/archive/2009/06/15/8830.aspx
			Response.Write(DataBinder.Eval(e.Item.DataItem, "student_id").ToString() & "<hr />")
		End If

	End Sub
End Class
