﻿
Partial Class Book_Sample_Ch11_Program_GridView_RowDataBound_7_Samples_GridView_RowDataBound_6_2_RowState
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            '== [正確版] =============================================
            If e.Row.RowState = DataControlRowState.Normal Then
                Response.Write("Normal <br />")
            End If
            If e.Row.RowState = DataControlRowState.Alternate Then
                Response.Write("Alternate <br />")
            End If
            If e.Row.RowState = DataControlRowState.Edit Then
                Response.Write("edit <br />")
            End If

            '************************************************
            '*** 重點！既是「偶數列(Alternate)」、又進入編輯模式。
            If e.Row.RowState = (DataControlRowState.Edit Or DataControlRowState.Alternate) Then
                Response.Write("Alternate -- edit <br />")
            End If

        End If
    End Sub
End Class
