﻿
Partial Class CaseStudy_Search_Engine_Search_Engine_CheckBoxList_2
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim Search_String As String = ""
        Dim u_select As Boolean = False

        For i = 0 To (CheckBoxList1.Items.Count - 1)
            If CheckBoxList1.Items(i).Selected Then
                '***  與上一支程式的差異所在 ***************************************
                Search_String = Search_String & " [class] LIKE '%" & CheckBoxList1.Items(i).Text & "%' or "
                '******************************************************************

                u_select = True   '//使用者有點選任何一個 CheckBoxList子選項
            End If
        Next

        '//**********************************
        '// 消除最後一個多餘的「 or」三個字
        Dim leng As Integer = Len(Search_String)
        Search_String = Left(Search_String, (leng - 3))
        '//**********************************

        If u_select Then
            Label1.Text = Search_String
        Else
            Label1.Text = "您尚未點選任何一個 CheckBoxList子選項"
            Response.End()
        End If

        '//=======================================
        '//== SqlDataSource1 資料庫的連接字串 ConnectionString，
        '//== 已事先寫在「HTML畫面的設定」裡面 ==
        '//=======================================
        SqlDataSource1.SelectCommand = "SELECT [test_time], [id], [class], [title] FROM [test] WHERE " & Search_String
        '//這次不使用 SqlDataSource提供的@參數
    End Sub
End Class
