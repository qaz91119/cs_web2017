﻿'----自己寫的（宣告）----
Imports System.Web.Configuration

Imports System.Data
Imports System.Data.SqlClient
'----自己寫的（宣告）----


Partial Class CaseStudy_Search_Engine_Search_Engine_2_Manual
    Inherits System.Web.UI.Page


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        DBInit()
    End Sub


    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        '---- 分 頁 ----
        GridView1.PageIndex = e.NewPageIndex
        DBInit()
    End Sub


    '====自己手寫的程式碼， DataAdapter / DataSet ====(start)
    Sub DBInit()
        '----上面已經事先寫好 using System.Web.Configuration ----
        '----連結資料庫的另一種寫法----
        Dim Conn As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)
        Dim myAdapter As SqlDataAdapter = Nothing

        Try
            'Conn.Open()  '---- 這一行註解掉，可以不用寫，DataAdapter會自動開啟

            '-- 作者註解：SqlDataAdapter的 .Fill()方法使用 SQL指令的SELECT，從資料來源擷取資料。
            '   此時，DbConnection物件（如Conn）必須是有效的，但不需要是開啟的
            '  （因為DataAdapter會自動開啟或關閉連結）。
            '   如果在呼叫 .Fill ()方法之前關閉 IDbConnection，它會先開啟連接以擷取資料，
            '   然後再關閉連接。如果在呼叫 .Fill ()方法之前開啟連接，它會保持開啟狀態。
            '   因此，我們使用SqlDataAdapter的時候，不需要寫程式去控制Conn.Open()與 Conn.Close()。

            '=====重 點===== 方法一 ===========(start)
            'Dim mySearchString As String = "Select id,test_time,title,summary,author From test Where 1=1 "

            'If TextBox1.Text <> "" Then
            '    mySearchString = mySearchString & " and title like '%" & TextBox1.Text & "%'"
            'End If

            'If TextBox2.Text <> "" Then
            '    mySearchString = mySearchString & " and summary like '%" & TextBox2.Text & "%'"
            'End If
            '============= 方法一 ===========(end)


            '*** 方法二 ********************************************(start)
            '-- 避免出現 Where 1=1的條件，增加SQL指令執行上的負擔。
            Dim mySearchString As String = "Select id,test_time,title,summary,author From test "

            Dim myWhereString As String = ""
            Dim hasAND As Integer = 0

            If Len(TextBox1.Text) > 0 Then
                '== 也可以寫成這樣，更好！  TextBox1.Text.Trim.Length，去除字串「前後」的空白
                myWhereString = " title like '%" & TextBox1.Text & "%'"
                hasAND = hasAND + 1
            End If

            If Len(TextBox2.Text) > 0 Then
                '== 也可以寫成這樣，更好！  TextBox2.Text.Trim.Length，去除字串「前後」的空白
                If hasAND > 0 Then    '-- 要不要加上 AND字樣？
                    myWhereString = myWhereString & " And summary like '%" & TextBox2.Text & "%'"
                Else
                    myWhereString = " summary like '%" & TextBox2.Text & "%'"
                End If
                hasAND = hasAND + 1
            End If

            '--方法二 的 重點！--
            '這段程式碼 If Len(myWhereString) > 0 Then可改寫如下，跟C#一樣
             If myWhereString.Length > 0 Then
                '== 也可以寫成這樣，更好！  myWhereString.Trim.Length，去除字串「前後」的空白
                '-- 有條件子句的話，才加上「Where」字樣。
                mySearchString &= " Where " & myWhereString
            End If
            '*** 方法二 ********************************************(end)


            Response.Write(mySearchString.ToString() & "<hr />")
            '=====重 點=====(end)


            myAdapter = New SqlDataAdapter(mySearchString, Conn)
            Dim ds As New DataSet()
            myAdapter.Fill(ds, "test")     '把資料庫撈出來的資料，填入DataSet裡面。
            ' DataSet是由許多 DataTable組成的，我們目前只放進一個名為 test的 DataTable而已。

            GridView1.DataSource = ds.Tables("test")
            GridView1.DataBind()
        Catch ex As Exception
            Response.Write("<hr />" & ex.ToString() & "<hr />")
        End Try
        '使用SqlDataAdapter的時候，不需要寫程式去控制Conn.Open()與 Conn.Close()。

    End Sub
    '====自己手寫的程式碼， DataAdapter / DataSet ====(end)

End Class
