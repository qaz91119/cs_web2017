﻿'----自己寫的（宣告）----
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的（宣告）----


Partial Class VS2010_Book_Sample_Search_Engine_test_2
    Inherits System.Web.UI.Page


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        DBInit()
    End Sub


    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        '//---- 分 頁 ----
        GridView1.PageIndex = e.NewPageIndex
        DBInit()
    End Sub


    '//====自己手寫的程式碼， DataAdapter / DataSet ====(start)
    Sub DBInit()
        '//----上面已經事先寫好 using System.Web.Configuration ----
        '//----連結資料庫的另一種寫法----
        Dim Conn As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)

        Try
            '//Conn.Open()  '//---- 這一行註解掉，可以不用寫，DataAdapter會自動開啟

            '//作者註解：SqlDataAdapter的 .Fill()方法使用 SQL指令的SELECT，從資料來源擷取資料。
            '//   此時，DbConnection物件（如Conn）必須是有效的，但不需要是開啟的
            '//  （因為DataAdapter會自動開啟或關閉連結）。
            '//   如果在呼叫 .Fill ()方法之前關閉 IDbConnection，它會先開啟連接以擷取資料，
            '//   然後再關閉連接。如果在呼叫 .Fill ()方法之前開啟連接，它會保持開啟狀態。
            '//   因此，我們使用SqlDataAdapter的時候，不需要寫程式去控制Conn.Open()與 Conn.Close()。


            '=======================================================
            '== 參考資料：http://msdn.microsoft.com/zh-tw/library/bbw6zyha(v=vs.80).aspx
            '== 方法一 ==
            Dim myAdapter As New SqlDataAdapter()  '--務必先有 DataAdapter才行！

            Dim selectCMD As New SqlCommand("Select id,test_time,title,summary,author From test Where 1=1 and ([title] LIKE '%' + @title + '%') and ([summary] LIKE '%' + @summary + '%')", Conn)
            myAdapter.SelectCommand = selectCMD

            selectCMD.Parameters.Add("@title", SqlDbType.VarChar, 120).Value = TextBox1.Text
            selectCMD.Parameters.Add("@summary", SqlDbType.VarChar, 200).Value = TextBox2.Text



            Dim ds As New DataSet()
            myAdapter.Fill(ds, "test")     '//執行SQL指令。把資料庫撈出來的資料，填入DataSet裡面。
            '// DataSet是由許多 DataTable組成的，我們目前只放進一個名為 test的 DataTable而已。



            '=======================================================
            GridView1.DataSource = ds.Tables("test")
            GridView1.DataBind()
        Catch ex As Exception
            Response.Write("<HR/>" & ex.ToString() & "<HR/>")
        End Try
        '//使用SqlDataAdapter的時候，不需要寫程式去控制Conn.Open()與 Conn.Close()。

    End Sub
    '//====自己手寫的程式碼， DataAdapter / DataSet ====(end)

End Class
