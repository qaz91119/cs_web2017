﻿'----自己寫的（宣告）----
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的（宣告）----


Partial Class VS2010_Book_Sample_Search_Engine_2_TextBox_Search
    Inherits System.Web.UI.Page


    Protected Sub TextBox1_TextChanged(sender As Object, e As System.EventArgs) Handles TextBox1.TextChanged
        '=======微軟SDK文件的範本=======
        '---- (連結資料庫)----
        Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)

        Dim cmd As New SqlCommand("select title from test where id = " & TextBox1.Text, Conn)

        Try     '==== 以下程式，只放「執行期間」的指令！=====================
            Conn.Open()   '---- 這時候才連結DB

            '---- 這時候執行SQL指令，取出資料。只有撈出單一個欄位，用這方法最快
            If cmd.ExecuteScalar() = Nothing Then
                TextBox2.Text = "查無資料！"
            Else
                TextBox2.Text = cmd.ExecuteScalar()
            End If

        Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<HR/>")

        Finally
            '---- Close the connection when done with it.
            If (Conn.State = ConnectionState.Open) Then
                Conn.Close()
                Conn.Dispose() '---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
            End If

        End Try
    End Sub

End Class
