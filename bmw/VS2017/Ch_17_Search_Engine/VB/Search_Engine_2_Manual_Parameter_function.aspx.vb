﻿'----自己寫的（宣告）----
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的（宣告）----


Partial Class VS2010_Book_Sample_Search_Engine_Search_Engine_2_Manual_Parameter_function
    Inherits System.Web.UI.Page

    '***********************************************
    Private Conn As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)
    Private myAdapter As New SqlDataAdapter()  '-- 務必先有 DataAdapter才行！
    Private cmd As New SqlCommand()

    Private mySearchString As String = "Select id,test_time,title,summary,author From test Where 1=1 "
    '***********************************************


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        DBInit()
    End Sub


    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        '---- 分 頁 ----
        GridView1.PageIndex = e.NewPageIndex
        DBInit()
    End Sub


    ' ====自己手寫的程式碼， DataAdapter / DataSet ====(start)
    Sub DBInit()
        Try
            '=======================================================
            '== 參考資料：http://msdn.microsoft.com/zh-tw/library/bbw6zyha(v=vs.80).aspx
            '== 方法一 ==
            '=========

            If TextBox1.Text <> "" Then
                mySearchString = mySearchString & Generate_SQL_Where("title", SqlDbType.VarChar, 120, TextBox1.Text)
                '-- (1).自己寫的 function，用來組合 SQL指令的「Where子句」、「參數」。
            End If

            If TextBox2.Text <> "" Then
                mySearchString = mySearchString & Generate_SQL_Where("summary", SqlDbType.VarChar, 200, TextBox2.Text)
                '-- (1).自己寫的 function，用來組合 SQL指令的「Where子句」、「參數」。
            End If

            '=======================================================
            GridView1.DataSource = Generate_DataSet(mySearchString)  '-- (2).自己寫的 function，用來執行SQL指令(DataAdapter)，傳回DataSet。
            GridView1.DataBind()

        Catch ex As Exception
            Response.Write("<HR/>" & ex.ToString() & "<HR/>")
        End Try
        ' 使用SqlDataAdapter的時候，不需要寫程式去控制Conn.Open()與 Conn.Close()。

    End Sub
    ' ====自己手寫的程式碼， DataAdapter / DataSet ====(end)



    '****************************************************************
    '*** (1). 自己寫的 function ***
    Function Generate_SQL_Where(ByVal u_CloumnName As String, ByVal u_dbType As SqlDbType, ByVal u_Int As Integer, ByVal u_Input As String) As String

        Dim WhereStr As String = " and ([" & u_CloumnName & "] LIKE '%' + @" & u_CloumnName & " + '%')"
        '== SQL指令的寫法很特別！！==
        '-- 輸入欄位名稱，拼湊成這樣的 Where字串。例如： " and ([summary] LIKE '%' + @summary + '%')"

        cmd.Parameters.Add("@" & u_CloumnName, u_dbType, u_Int)
        cmd.Parameters("@" & u_CloumnName).Value = u_Input

        '== 希望組合成下面的參數。
        ' cmd.Parameters.Add("@title", SqlDbType.VarChar, 120)
        ' cmd.Parameters("@title").Value = TextBox1.Text

        Return WhereStr
    End Function


    '*** (2). 自己寫的 function，用來執行SQL指令(DataAdapter)，傳回DataSet。
    Function Generate_DataSet(ByVal SQLstr As String) As DataSet
        cmd.CommandText = mySearchString
        cmd.Connection = Conn

        myAdapter.SelectCommand = cmd

        Dim ds As New DataSet()
        myAdapter.Fill(ds, "test")     ' 執行SQL指令。把資料庫撈出來的資料，填入DataSet裡面。
        ' DataSet是由許多 DataTable組成的，我們目前只放進一個名為 test的 DataTable而已。

        Return ds
    End Function

End Class
