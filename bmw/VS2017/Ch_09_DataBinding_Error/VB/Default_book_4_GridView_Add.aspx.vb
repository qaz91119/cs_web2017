﻿
Partial Class Default_book_4_GridView_Add
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        If e.CommandName = "Insert" Then
            GridView1.DataSourceID = Nothing
            '==註解： DataSourceID屬性如果是「空」的，就會引發錯誤，
            '==            找不到資料！便會展開 GridView的「EmptyData」樣版了！
        End If
    End Sub


    Protected Sub SqlDataSource1_Inserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSource1.Inserted

        GridView1.DataSourceID = "SqlDataSource1"
        '== 完成新增一筆資料後，重新讓 GridView作 DataBinding，展示資料庫裡面的全部最新資料。
    End Sub
End Class
