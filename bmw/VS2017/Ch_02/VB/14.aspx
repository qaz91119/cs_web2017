<%@ Page Language="VB" %>

<%  '--註解：在畫面上，連續印出a1，a2，a3……，a100

Dim I As Integer = 1

For I = 1 to 100
     Response.Write("a")      '--註解：在畫面上，印出a
     Response.Write(I)   '--註解：在畫面上，印出「數字」之後，跟前面的a字，拼湊成「a1」，「a2」…..等等。

     Response.Write("，<br />")  '--註解：<br />是HTML的換行符號
Next

%>