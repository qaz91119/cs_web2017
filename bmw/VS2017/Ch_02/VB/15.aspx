<%@ Page Language="VB" %>

<%  '--註解：在畫面上，連續印出a1，a2，a3……，a100

Dim I As Integer = 1

While I <= 100
     Response.Write("a" & I )   '--註解：”a”是字串，而後面的I是變數。
     Response.Write( "，<br />")   '--註解：HTML的換行標籤 <br />

     I = I +1
End While
%>
