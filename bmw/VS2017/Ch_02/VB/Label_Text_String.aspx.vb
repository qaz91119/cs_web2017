﻿
Partial Class Book_Sample_Ch02_program_Label_Text_String
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'Label1.Text = Label1.Text & 1
        'Label1.Text = Label1.Text + 1   '***正常***

        'Label1.Text = Label1.Text & "1"
        'Label1.Text = Label1.Text + "1"

        Label1.Text += 1

    End Sub
End Class
