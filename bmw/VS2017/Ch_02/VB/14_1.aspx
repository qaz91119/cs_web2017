<%@ Page Language="VB" %>

<%  '--註解：在畫面上，連續印出a1，a2，a3……，a100

Dim I As Integer = 1

For I = 1 to 100
     Response.Write("a")      '--註解：在畫面上，印出a
     Response.Write(I)   '--註解：在畫面上，印出「數字」之後，跟前面的a字，拼湊成「a1」，「a2」…..等等。

            '*************************************************
            ' 範例14.aspx還有一個小缺點，就是到了最後一個 a100。程式就該結束，
            ' 但畫面上卻多了一個「，」號，讓輸出畫面不美觀。
            ' 我加上一個 If判別式來解決這個問題
            '*************************************************
            If (i < 100) Then
                Response.Write(" ，<br />")  '--註解：<br />是HTML的換行符號
            End If

Next

%>
