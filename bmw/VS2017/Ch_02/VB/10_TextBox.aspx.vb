﻿
Partial Class VS2010_Book_Sample_Ch02_program_10_TextBox
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click

        Dim a As Integer = CType(TextBox1.Text, Integer)

        If a > 10 Then
            Response.Write("恭喜！您輸入的值，大於10喔！")
        Else

            If a = 10 Then
                Response.Write("您輸入的值，剛好等於10。")
            Else
                Response.Write("抱歉！您輸入的值，小於10。")
            End If

        End If
    End Sub
End Class
