<%@ Page Language="VB" %>

<% '註解：判別使用者的輸入值（限定1~3）？連結到相關網站。

    If Request("u_number") = "" or Request("u_number") > 3 or Request("u_number") < 1 Then
        '註解：如果使用者不輸入數字的話，就會出現警告訊息。
        Response.Write("使用者務必輸入一個數字！限定1~3")
        Response.Write(“<br />輸入訊息有誤！”)
    Else
        If Request("u_number") = 1 Then
            Response.Redirect("http://www.find.org.tw/")
        End If

        If Request("u_number") = 2 Then
            Response.Redirect("http://www.iii.org.tw/")
        End If

        If Request("u_number") = 3 Then
            Response.Redirect("http://www.yahoo.com.tw/")
        End If
    End If
%>
