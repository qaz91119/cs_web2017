<%@ Page Language="VB" %>

<% '註解：判別使用者的輸入值（限定1~3）？連結到相關網站。

Select Case Request("u_number")

         Case 1
                 Response.Redirect("http://www.find.org.tw/")

         Case 2
                 Response.Redirect("http://www.iii.org.tw/")

         Case 3
                 Response.Redirect("http://www.yahoo.com.tw/")

         Case Else
                 '註解：如果使用者不輸入數字的話，就會出現警告訊息。
                 Response.Write("使用者務必輸入一個數字！限定1~3")

End Select
%>
