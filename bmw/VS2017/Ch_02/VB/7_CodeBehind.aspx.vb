﻿
Partial Class VS2010_Book_Sample_Ch02_program_7_CodeBehind
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Response.BufferOutput = True    '先打開緩衝區

        Response.Write("把資料「第一次」放進緩衝區！")
        Response.Clear()  '直接「清除」緩衝區，所以，使用者在網頁上看不見上面這段話！

        Response.Write("把資料「第二次」放進緩衝區！")
        Response.Flush()    '這次，使用者應該看的到這段話囉！
    End Sub
End Class
