﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Book_Sample_Ch02_Label_Text_String : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //Label1.Text = Label1.Text + 1;   //字串相連
        //Label1.Text = Label1.Text + "1";  //字串相連

        //Label1.Text = (Convert.ToInt32(Label1.Text) + 1).ToString();  //正常


        //Label1.Text = 1 + 1 + "a";   //答案： 2a
        Label1.Text = "" + 1 + 1 + "a";   //答案是 11a

        //Label1.Text++;   //錯誤 
        //編譯器錯誤訊息: CS0023: 無法將運算子 '++' 套用至類型 'string' 的運算元
    }
}