﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//==自己寫的（宣告）==
using System.Security.Cryptography;   //-- MD5專用
using System.Text;                            //-- StringBuilder專用
//==自己寫的（宣告）==



public partial class Book_Sample_B12_Member_Login_Session_MD5_Case2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        String source = TextBox1.Text;

        String hash = getMd5Hash(source);  //--加密的 function寫在下面。

        Label1.Text = "原本的字串「<font color=blue>" + source + "</font>」，經過加密後變成：&nbsp;&nbsp; " + hash;
    }


    //===================================================================
    // Hash an input string and return the hash as a 32 character hexadecimal string.
    // 資料來源：http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.md5.aspx 
    //
    //== 底下這部分的程式，就算讀者不懂也可以使用它。照著做就對了。
    //== 只要知道我們輸入一段字串，它就會用MD5進行加密後，傳回「加密後的字串」。

    protected String getMd5Hash(String uInput)
    {
        // Create a new instance of the MD5 object.
        // MD5必須搭配 System.Security.Cryptography命名空間
        MD5 md5Hasher = MD5.Create();

        // Convert the input string to a byte array(陣列) and compute the hash.
        Byte[] MD5data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(uInput));

        // Create a new Stringbuilder to collect the bytes
        // and create a string.
        // StringBuilder必須搭配 System.Text命名空間
        StringBuilder sBuilder = new StringBuilder();

        // Loop through each byte of the hashed data 
        // and format each one as a hexadecimal string.
        for (int i = 0; i < MD5data.Length; i++)
        {
            sBuilder.Append(MD5data[i].ToString("x2"));  //--變成十六進位
        }

        // Return the hexadecimal string.
        return sBuilder.ToString();
    }


    //*****************************************************************************
    //***  MD5加密後的資料進行驗證
    protected void Button2_Click(object sender, EventArgs e)
    {
        //-- 輸入兩個字串，進行驗證比對。
        //        第一個字串，是原始的輸入字串（未加密）。
        //        第二個字串，是經過 MD5加密之後的字串。
        if (verifyMd5Hash(TextBox1.Text, TextBox2.Text))  {
            Label2.Text = "驗證成功！";
        }
        else  {
            Label2.Text = "驗證失敗！！您輸入的 MD5碼，跟一開始輸入的「原始字串」不符合！The hashes are not same.";
        }
    }

    // Verify a hash against a string.
    //        第一個字串，是原始的輸入字串（未加密）。
    //        第二個字串，是經過 MD5加密之後的字串。
    protected Boolean verifyMd5Hash(String Input_Source, String Input_MD5) 
    {
        // Hash the input.
        String hashOfInput = getMd5Hash(Input_Source);

        // Create a StringComparer an comare the hashes.
        StringComparer comparer = StringComparer.OrdinalIgnoreCase;

        if (comparer.Compare(hashOfInput, Input_MD5) == 0)   {
            return true;
        }
        else  {
            return false;
        }
            
    }


}