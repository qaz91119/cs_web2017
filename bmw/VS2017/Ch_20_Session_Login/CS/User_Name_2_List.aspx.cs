﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Book_Sample_B12_Member_Login_Session_User_Name_2_List : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    //*********************************************************
        //    if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
        //    //**  重點！！！進入「編輯」模式以後，不執行這一段，就不會報錯！(start)
        //    //*********************************************************
        //    {
        //        Label lb = (Label)e.Row.FindControl("Label1");

        //        PlaceHolder ph = (PlaceHolder)e.Row.FindControl("PlaceHolder1");
        //        //== 將會在 PlaceHolder裡面，動態產生 Button「編輯」按鈕

        //        // C#沒有 InStr函數，可以參閱這篇文章： http://www.diybl.com/course/4_webprogram/asp.net/asp_netshl/2008828/138240.html
        //        // 或是 http://www.dotblogs.com.tw/mis2000lab/archive/2009/01/14/instr_function_090114.aspx

        //        if (lb.Text.IndexOf(Session["u_realname"].ToString(), 0) != -1)
        //        // .IndexOf()方法。找不到的話， 會傳回「-1」。
        //        // 找到的話，回傳一個 Integer數字（從零算起）。表示在字串裡面第幾個字，符合條件。 
        //        {
        //            //==================================================
        //            //== 作者名稱相同，才可以修改這篇文章。如果名字不同，就不會動態產生「編輯」按鈕。

        //            Button ButonIn1 = (Button)e.Row.FindControl("Button_mis2000lab_1");
        //            ButonIn1.Visible = true;
        //            Button ButonIn2 = (Button)e.Row.FindControl("Button_mis2000lab_2");
        //            ButonIn2.Visible = true;

        //            //==================================================
        //            //== （以下失敗！）
        //            //==  動態加入 Button到 PlaceHolder裡面，無法觸發 GridView編輯模式 ==
        //            //==================================================
        //            Button b1 = new Button();
        //            b1.ID = "Button_Edit_1";
        //            b1.CommandName = "Edit";   //-- 重點！！這樣才能觸發 GridView編輯模式
        //            b1.Text = "動態加入PlaceHolder裡面「編輯1」按鈕";
        //            b1.Font.Italic = true;
        //            b1.ForeColor = System.Drawing.Color.Gray;
        //            //--   動態加入 GridView的「編輯」按鈕！ *** 重點！！*** 跟上一個範例不同。

        //            ph.Controls.Add(b1);    //-- 在 PlaceHolder裡面，動態產生 Button「編輯」按鈕
        //            //-- .Controls.Add()  動態加入控制項。
        //            //---------------------------------------------------------------------------------
        //            Button b2 = new Button();
        //            b2.ID = "Button_Delete_2";
        //            b2.CommandName = "Delete";   //-- 重點！！這樣才能觸發 GridView刪除事件
        //            b2.Text = "動態加入PlaceHolder裡「刪除1」按鈕";
        //            b2.Font.Italic = true;
        //            b2.ForeColor = System.Drawing.Color.Gray;
        //            ph.Controls.Add(b2);
        //            //==================================================
        //        }

        //    }   //**  重點！！！進入「編輯」模式以後，不執行這一段，就不會報錯！(end)
        //}
    }


    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //*********************************************************
            if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
            //**  重點！！！進入「編輯」模式以後，不執行這一段，就不會報錯！(start)
            //*********************************************************
            {
                Label lb = (Label)e.Row.FindControl("Label1");

                PlaceHolder ph = (PlaceHolder)e.Row.FindControl("PlaceHolder1");
                //== 將會在 PlaceHolder裡面，動態產生 Button「編輯」按鈕

                // C#沒有 InStr函數，可以參閱這篇文章： http://www.diybl.com/course/4_webprogram/asp.net/asp_netshl/2008828/138240.html
                // 或是 http://www.dotblogs.com.tw/mis2000lab/archive/2009/01/14/instr_function_090114.aspx

                if (lb.Text.IndexOf(Session["u_realname"].ToString(), 0) != -1)
                // .IndexOf()方法。找不到的話， 會傳回「-1」。
                // 找到的話，回傳一個 Integer數字（從零算起）。表示在字串裡面第幾個字，符合條件。 
                {
                    //==================================================
                    //== 作者名稱相同，才可以修改這篇文章。如果名字不同，就不會動態產生「編輯」按鈕。

                    Button ButonIn1 = (Button)e.Row.FindControl("Button_mis2000lab_1");
                    ButonIn1.Visible = true;
                    Button ButonIn2 = (Button)e.Row.FindControl("Button_mis2000lab_2");
                    ButonIn2.Visible = true;

                    //==================================================
                    //== （以下失敗！）
                    //==  動態加入 Button到 PlaceHolder裡面，無法觸發 GridView編輯模式 ==
                    //==================================================
                    Button b1 = new Button();
                    b1.ID = "Button_Edit_1";
                    b1.CommandName = "Edit";   //-- 重點！！這樣才能觸發 GridView編輯模式
                    b1.Text = "動態加入PlaceHolder裡面「編輯1」按鈕";
                    b1.Font.Italic = true;
                    b1.ForeColor = System.Drawing.Color.Gray;
                    //--   動態加入 GridView的「編輯」按鈕！ *** 重點！！*** 跟上一個範例不同。

                    ph.Controls.Add(b1);    //-- 在 PlaceHolder裡面，動態產生 Button「編輯」按鈕
                    //-- .Controls.Add()  動態加入控制項。
                    //---------------------------------------------------------------------------------
                    Button b2 = new Button();
                    b2.ID = "Button_Delete_2";
                    b2.CommandName = "Delete";   //-- 重點！！這樣才能觸發 GridView刪除事件
                    b2.Text = "動態加入PlaceHolder裡「刪除1」按鈕";
                    b2.Font.Italic = true;
                    b2.ForeColor = System.Drawing.Color.Gray;
                    ph.Controls.Add(b2);
                    //==================================================
                }

            }   //**  重點！！！進入「編輯」模式以後，不執行這一段，就不會報錯！(end)
        }
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        //-- 登出，把自己的 Session清空。
        Response.Redirect("User_Name_2_Login.aspx");
    }




}