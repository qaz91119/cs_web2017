﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserRight_1_Login.aspx.cs" Inherits="Book_Sample_B12_Member_Login_Session_UserRight_1_Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>搭配使用者權限，可刪除？可編輯/更新？</title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
        .style2
        {
            font-weight: bold;
            color: #FFFF66;
            background-color: #CC0000;
        }
        .style4
        {
            color: #FFFF99;
            font-weight: bold;
            background-color: #CC0000;
        }
        .style7
    {
        font-size: x-large;
    }
    </style>
</head>
<body bgcolor="#CCFF99">
    <form id="form1" runat="server">
    <div>
        以 Session來作「權限控管」<span class="style2">搭配使用者權限，可刪除？可編輯/更新？</span><br />
        <br />
        &nbsp;&nbsp;&nbsp; 當使用者登入之後，會有不同的權限。<br />
        <b>&nbsp;&nbsp;&nbsp; 權限夠大</b>或是<b>文章發表人</b>，才能修改資料。</div>
    <p>
        <span class="style1">*</span>Name :
        <asp:TextBox ID="TextBox1" runat="server" Style="margin-bottom: 0px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="必填！不可以留空白！"
            Style="font-weight: 700" ControlToValidate="TextBox1"></asp:RequiredFieldValidator>
    </p>
    <p>
        Passwd :
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        &nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button1_login" runat="server" Text="Login（登入 / 使用者權限）" OnClick="Button1_login_Click" />
    </p>
    <p>
        <b><span class="style4">GridView「編輯」模式裡面，<span class="style7">&quot;部分&quot; </span>欄位可更新、<span 
            class="style7">&quot;部分&quot;</span>欄位不可更新！！</span></b></p>
    <hr />
    <p align="center">
        <asp:Button ID="Button2_email_passwd" runat="server" Text="忘記密碼，請E-Mail給我～" OnClick="Button2_email_passwd_Click" />
    </p>
    </form>
</body>
</html>
