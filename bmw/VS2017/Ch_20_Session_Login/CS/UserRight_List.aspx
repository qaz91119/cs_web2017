﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserRight_List.aspx.cs" Inherits="Book_Sample_B12_Member_Login_Session_UserRight_List" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--#INCLUDE FILE="defense.aspx"-->
<!-- 這段程式將會防範，直接以網址URL進入的「非法闖入者」-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>搭配使用者權限，可刪除？可編輯/更新？</title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
        .style4
        {
            color: #FFFF99;
            font-weight: bold;
            background-color: #CC0000;
        }
        .style5
        {
            background-color: #FFFF00;
        }
    </style>
</head>
<body bgcolor="#CCFF99">
    <form id="form1" runat="server">
    <div>
        <b><span class="style4">搭配使用者權限，可刪除？可編輯/更新？</span></b><br />
        <br />
        db_User資料表裡面，新增兩個欄位：<b><span class="style5">Update</span>Right</b>與 <b><span class="style5">
            Delete</span>Right</b>。<asp:Button ID="Button1" runat="server" 
            Font-Bold="True" Font-Size="Large"
                ForeColor="Red" Text="Button_登出" onclick="Button1_Click" />
        <br />
        <b><span class="style4">動態產生 GridView「編輯」、「刪除」的功能！！</span></b><br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="3" DataKeyNames="id" DataSourceID="SqlDataSource1" GridLines="None"
            PageSize="5" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px"
            CellSpacing="1">
            <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
            <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
            <Columns>
                <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" InsertVisible="False"
                    ReadOnly="True" />
                <asp:BoundField DataField="test_time" HeaderText="test_time" SortExpression="test_time"
                    DataFormatString="{0:yyyy/MM/dd}" />
                <asp:BoundField DataField="title" HeaderText="title" SortExpression="title"></asp:BoundField>
                <asp:BoundField DataField="author" HeaderText="author" SortExpression="author" />
            </Columns>
            <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#594B9C" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#33276A" />
        </asp:GridView>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testConnectionString %>"
            SelectCommand="SELECT [id], [title], [test_time], [author] FROM [test]" DeleteCommand="DELETE FROM [test] WHERE [id] = @id"
            UpdateCommand="UPDATE [test] SET [title] = @title, [test_time] = @test_time, [author] = @author WHERE [id] = @id">
            <DeleteParameters>
                <asp:Parameter Name="id" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="title" Type="String" />
                <asp:Parameter Name="test_time" Type="DateTime" />
                <asp:Parameter Name="author" Type="String" />
                <asp:Parameter Name="id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <br />
        HTML畫面上的設定多加了 <span class="style1">&lt;!--#INCLUDE FILE=&quot;defense.aspx&quot;--&gt;</span>，在本檔案的<span
            class="style1">最上方</span>！</div>
    </form>
</body>
</html>
