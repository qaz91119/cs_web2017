﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Book_Sample_B12_Member_Login_Session_test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //*************************************************
        //**  失敗！！程式寫在這裡，「編輯」按鈕按下去沒有反應！

        //if (e.Row.RowState == DataControlRowState.Normal | e.Row.RowState ==DataControlRowState.Alternate)
        //{
        //    //*********************************************************
        //    if (e.Row.RowState != DataControlRowState.Edit)
        //    {
        //        PlaceHolder ph = (PlaceHolder)e.Row.FindControl("PlaceHolder1");

        //        Button b1 = new Button();
        //        b1.ID = "Button_Edit_1";
        //        b1.CommandName = "Edit";   //-- 重點！！這樣才能觸發 GridView編輯模式
        //        b1.Text = "MIS2000 Lab.「編輯」按鈕";
        //        //--   動態加入 GridView的「編輯」按鈕！ *** 重點！！*** 跟上一個範例不同。

        //        ph.Controls.Add(b1);   //-- 在 PlaceHolder裡面，動態產生 Button「編輯」按鈕
        //        //-- .Controls.Add()  動態加入控制項。
        //    }
        //}

    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //*********************************************************
            if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
            {
                PlaceHolder ph = (PlaceHolder)e.Row.FindControl("PlaceHolder1");

                Button b1 = new Button();

                //*********************************************************
                //b1.ID = "Button_Edit_1";   //== 很特別的解法！！不要加上ID屬性，就會正常運作！！==
                //*********************************************************


                b1.CommandName = "Edit";   //-- 重點！！這樣才能觸發 GridView編輯模式
                b1.Text = "MIS2000 Lab.「編輯」按鈕";
                //--   動態加入 GridView的「編輯」按鈕！ *** 重點！！*** 跟上一個範例不同。

                ph.Controls.Add(b1);   //-- 在 PlaceHolder裡面，動態產生 Button「編輯」按鈕
                //-- .Controls.Add()  動態加入控制項。
            }
        }
    }
}