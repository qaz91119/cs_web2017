﻿<%@ Page Language="C#" Debug="true" AutoEventWireup="true" CodeFile="String_Prime.aspx.cs" Inherits="Book_Sample_B12_Member_Login_Session_String_Prime" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        把字串轉換成「質數」<br />
        <br />
        質數表，請看 <a href="http://fm.u41.tw/2009/11/blog-post_7646.html">http://fm.u41.tw/2009/11/blog-post_7646.html</a> <br />
        <br />
        2，3，5，7，11，13，17，19，23，29，31，37， 41，43，47，53，59，61，67，71，73，79，83，89， 97，101，103，107，109，113，127，131，137，139，149，151
        <br />
        <br />
        <br />
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
        <br />
        <br />
    
    </div>
        轉換成數字（除以質數151的 餘數）---
        <asp:Label ID="Label1" runat="server" style="color: #990033; font-weight: 700"></asp:Label>
        <br />
        <br />
        拿上面的結果，作SHA512加密 --- <asp:Label ID="Label2" runat="server" style="font-weight: 700; color: #0033CC"></asp:Label>
    </form>
</body>
</html>
