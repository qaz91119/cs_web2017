﻿<%@ Page Language="C#" Debug="true" AutoEventWireup="true" CodeFile="AES_Case1.aspx.cs" Inherits="Book_Sample_B12_Member_Login_Session_AES_Case1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .style1
        {
            font-size: small;
        }
    </style>
</head>
<body>
    <p>
        <span class="style1">資料來源：<a href="http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.aes.aspx">http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.aes.aspx</a> </span>
        <br class="style1" />
    </p>
    <p class="style1">
        命名空間： System.Security.Cryptography</p>
    <form id="form2" runat="server">
    <div>
    
        請輸入一段字，讓 <strong>AES演算法</strong>為您加密：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
&nbsp;&nbsp;
        <asp:Button ID="Button1" runat="server" Text="Submit" onclick="Button1_Click" />
        <hr />
        <br />
       
        <br />
        AES加密後的成果：<asp:Label ID="Label1" runat="server" 
            style="font-weight: 700; color: #009933; background-color: #CCFFCC;" ></asp:Label>

        
    
    </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        加密的成果：<asp:Label ID="Label2" runat="server"></asp:Label>
        <br />
        <br />
        請輸入加密後的結果：<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Button" />
        <br />
        <asp:Label ID="Label3" runat="server"></asp:Label>
    </form>

</body>
</html>
