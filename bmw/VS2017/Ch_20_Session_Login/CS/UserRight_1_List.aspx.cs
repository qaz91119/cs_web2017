﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Book_Sample_B12_Member_Login_Session_UserRight_1_List : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //***** 重點在這個IF判別式 *******************************************************************
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //***** 底下這段IF判別式，很玄妙！！！ 不這樣寫的話，會出現「格列換色」的編輯模式，內容空白！！*****
            //-- 參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.datacontrolrowstate.aspx
            //--                http://www.blueshop.com.tw/board/show.asp?subcde=BRD20090210113050XB9

            if ((e.Row.RowState == DataControlRowState.Edit) | 
                     (e.Row.RowState == (DataControlRowState.Edit | DataControlRowState.Alternate)))
            {  //****************************************************************************************
                if (Session["UpdateRight"].ToString() == "Y")
                {
                    //== 有「編輯」的權限
                    TextBox tb1 = (TextBox)e.Row.FindControl("TextBox3_test_titme");
                    tb1.Visible = true;

                    TextBox tb2= (TextBox)e.Row.FindControl("TextBox1_title");
                    tb2.Visible = true;

                    TextBox tb3 = (TextBox)e.Row.FindControl("TextBox2_author");
                    tb3.Visible = true;
                }
                else
                {
                    //== 無「編輯」的權限
                    Label lb1= (Label)e.Row.FindControl("Label4_test_time");
                    lb1.Visible = true;

                     Label lb2= (Label)e.Row.FindControl("Label5_title");
                    lb2.Visible = true;

                    Label lb3= (Label)e.Row.FindControl("Label6_author");
                    lb3.Visible = true;

                    //**** 沒有編輯的權限，就把「更新」按鈕給隱藏起來！ ******
                    Button UpdateButton= (Button)e.Row.FindControl("Button1_Update");
                    UpdateButton.Visible = false;
                }
            }
        }

    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        //-- 登出，把自己的 Session清空。

        Response.Redirect("UserRight_1_Login.aspx");
    }
}