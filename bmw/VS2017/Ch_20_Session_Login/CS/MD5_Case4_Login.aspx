﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MD5_Case4_Login.aspx.cs" Inherits="Book_Sample_B12_Member_Login_Session_MD5_Case4_Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>用MD5 雜湊演算法，在會員登入的密碼檢查上面</title>
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            background-color: #00FFFF;
        }
        .style2
        {
            font-weight: bold;
            background-color: #66FFFF;
        }
    </style>
</head>
<body>
    <p>
        用<span class="style1">MD5 雜湊演算法</span>，在會員登入的密碼檢查上面</p>
    <br />
    <br />
    <form id="form1" runat="server">
    <p>
        帳號：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    </p>
    <p>
        密碼：<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
&nbsp;（上一支程式新增的使用者密碼，已經被用<span class="style2">MD5 雜湊演算法</span>加密保護）</p>
    <p>
        &nbsp;<asp:Button ID="Button1" runat="server" Text="Login..." 
            onclick="Button1_Click" />
    </p>
    </form>
 
</body>
</html>
