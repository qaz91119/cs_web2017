﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Book_Sample_B12_Member_Login_Session_User_Edit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["u_rank"].ToString() != "5")
        {
            //-- (1). RANK=5表示最高等級。我們的使用者權限是依照數字(1~5)來區分，數字越大，權限越大。

            TextBox u_author = (TextBox)FormView1.FindControl("authorTextBox");

            if (Right(u_author.Text, 3) != Session["u_name"].ToString()) 
            {
                //-- (2). 權限不夠的話。再次比對作者姓名，必須相同才能修改。
                Response.Write("<h2><font colore=red>抱歉，您權限不夠，也不是本文作者，因此不得修改！</font></h2>");
                Response.End();
            }
        }
    }


    protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
    {
        Response.Write("<h2><font colore=blue>謝謝，修改已經成功！</font></h2>");
    }


    //************************************************
    //資料來源：http://www.csharphelp.com/archives4/archive616.html
    public static string Right(string param, int length)
    {
        //start at the index based on the lenght of the sting minus
        //the specified lenght and assign it a variable
        string result = param.Substring(param.Length - length, length);
        //return the result of the operation
        return result;
    }

}