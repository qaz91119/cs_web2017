﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//==== 自己宣告 ==================
using System.IO;
using System.Security.Cryptography;
//資料來源： http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.aes.aspx



public partial class Book_Sample_B12_Member_Login_Session_AES_Case1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
            try
            {
                string original = TextBox1.Text;

                // Create a new instance of the Aes class.  
                // This generates a new key and initialization vector (IV).
                using (Aes myAes = Aes.Create())
                {   // Encrypt the string to an array of bytes.
                    byte[] encrypted = EncryptStringToBytes_Aes(original, myAes.Key, myAes.IV);

                    // Byte[]轉成字串、互轉。
                    // 資料來源：http://stackoverflow.com/questions/1134671/c-how-can-i-safely-convert-a-byte-array-into-a-string-and-back
                    // Label2.Text = new string(encrypted.Select(Convert.ToChar).ToArray());


                    // Decrypt the bytes to a string.
                    string roundtrip = DecryptStringFromBytes_Aes(encrypted, myAes.Key, myAes.IV);

                    //Display the original data and the decrypted data.
                    Label1.Text = String.Format("您輸入的字串（準備加密）：   {0} <hr />", original);
                    Label1.Text += String.Format("解密之後（還原）： {0}", roundtrip);
                }
            }
            catch (Exception ex)
            {
                Response.Write("Error: " + ex.Message);
            }
    }


        //==== 加密 ================================
        byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText--您沒有輸入任何字串");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV Key");

            byte[] encrypted;

            // Create an Aes object with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {   //資料來源：http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.cryptostream.aspx
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {   //Write all data to the stream (CryptoStream).
                            swEncrypt.Write(plainText);
                        }                        
                    }
                    encrypted = msEncrypt.ToArray();
                }
            }
            // Return the encrypted bytes[] from the memory stream.
            return encrypted;
        }


        //==== 解密 ================================
        string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText--沒有輸入「需要解密」的字串");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV Key");

            // Declare the string used to hold the decrypted text.
            string plaintext = null;

            // Create an Aes object with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {   //資料來源：http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.cryptostream.aspx
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {   // Read the decrypted bytes[] from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }





    protected void Button2_Click(object sender, EventArgs e)
    {
    //    using (Aes myAes = Aes.Create())
    //    {   // 字串轉成 Byte[]、互轉。
    //        // 資料來源：http://stackoverflow.com/questions/1134671/c-how-can-i-safely-convert-a-byte-array-into-a-string-and-back
    //        byte[] encrypted = TextBox2.Text.Select(Convert.ToByte).ToArray();
    //        Label3.Text = DecryptStringFromBytes_Aes(encrypted, myAes.Key, myAes.IV);
    //    }            
    }
}

