﻿<%@ Page Language="C#" Debug="true" AutoEventWireup="true" CodeFile="User_List.aspx.cs" Inherits="Book_Sample_B12_Member_Login_Session_User_List" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--#INCLUDE FILE="defense.aspx"-->
<!-- 這段程式將會防範，直接以網址URL進入的「非法闖入者」-->

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>未命名頁面</title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
        .style2
        {
            color: #FF0000;
            font-weight: bold;
        }
        .style3
        {
            font-size: small;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <b>點選文章編號，便可以修改這篇文章。</b><br />
        <span class="style3">&nbsp;&nbsp;&nbsp; 權限最高的管理員，可以修改全部的文章。</span><br 
            class="style3" />
        <span class="style3">&nbsp;&nbsp;&nbsp; 自己撰寫的文章，自己也可以修改。</span><br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id" 
            DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" PageSize="5">
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="id" 
                    DataNavigateUrlFormatString="User_edit.aspx?id={0}" DataTextField="id">
                    <ItemStyle Font-Bold="True" Font-Size="Medium" />
                </asp:HyperLinkField>
                <asp:BoundField DataField="test_time" DataFormatString="{0:yyyy/MM/dd}" 
                    HeaderText="test_time" SortExpression="test_time" />
                <asp:BoundField DataField="title" HeaderText="title" SortExpression="title">
                    <ItemStyle Font-Bold="True" Font-Size="Medium" />
                </asp:BoundField>
                <asp:BoundField DataField="author" HeaderText="author" 
                    SortExpression="author" />
            </Columns>
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        
        
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testConnectionString %>" 
            SelectCommand="SELECT [id], [title], [test_time], [author] FROM [test]">
        </asp:SqlDataSource>
        <br />
        本程式<span class="style2">沒有</span>後置程式碼，只靠HTML畫面上的設定。<br />
        並且多加了一行
        <span class="style1">&lt;!--#INCLUDE FILE=&quot;defense.aspx&quot;--&gt;</span>，在本檔案的<span 
            class="style1">最上方</span>！</div>
    </form>
</body>
</html>

