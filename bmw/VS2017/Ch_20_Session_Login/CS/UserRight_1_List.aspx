﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserRight_1_List.aspx.cs" Inherits="Book_Sample_B12_Member_Login_Session_UserRight_1_List" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--#INCLUDE FILE="defense.aspx"-->
<!-- 這段程式將會防範，直接以網址URL進入的「非法闖入者」-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>搭配使用者權限，可刪除？可編輯/更新？</title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
        .style4
        {
            color: #FFFF99;
            font-weight: bold;
            background-color: #CC0000;
        }
        .style5
        {
            background-color: #FFFF00;
        }
        .style6
        {
            color: #0000FF;
        }
        .style7
        {
            font-size: x-large;
        }
    </style>
</head>
<body bgcolor="#CCFF99">
    <form id="form1" runat="server">
    <div>
        <b><span class="style4">搭配使用者權限，可刪除？可編輯/更新？</span></b><br />
        <br />
        db_User資料表裡面，新增兩個欄位：<b><span class="style5">Update</span>Right</b>與 <b><span class="style5">
            Delete</span>Right</b>。<asp:Button ID="Button1" runat="server" 
            Font-Bold="True" Font-Size="Large"
                ForeColor="Red" Text="Button_登出" onclick="Button1_Click" />
        <br />
        <b><span class="style4">GridView「編輯」模式裡面，<span class="style7">&quot;部分&quot; </span>
            欄位可更新、<span class="style7">&quot;部分&quot;</span>欄位不可更新！！</span></b><br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="3" DataKeyNames="id" DataSourceID="SqlDataSource1" GridLines="None"
            PageSize="5" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px"
            CellSpacing="1" onrowdatabound="GridView1_RowDataBound">
            <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
            <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <EditItemTemplate>
                        <asp:Button ID="Button1_Update" runat="server" CausesValidation="True" CommandName="Update"
                            Text="Update" />
                        &nbsp;<asp:Button ID="Button2_Cancel" runat="server" CausesValidation="False" CommandName="Cancel"
                            Text="Cancel" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Button ID="Button1" runat="server" CausesValidation="False" CommandName="Edit"
                            Text="Edit" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" InsertVisible="False"
                    ReadOnly="True" />
                <asp:TemplateField HeaderText="test_time" SortExpression="test_time">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox3_test_titme" runat="server" Visible="false" Text='<%# Bind("test_time", "{0:yyyy/MM/dd}") %>'></asp:TextBox>
                        <br />
                        <asp:Label ID="Label4_test_time" runat="server" Visible="false" Text='<%# Bind("test_time", "{0:yyyy/MM/dd}") %>'
                            Style="background-color: #FFFF00"></asp:Label>
                        <br />
                        <span class="style1">這裡都預先設定為<strong>隱形</strong></span><br />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("test_time", "{0:yyyy/MM/dd}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="title" SortExpression="title">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1_title" runat="server" Visible="false" Text='<%# Bind("title") %>'></asp:TextBox>
                        <br />
                        <asp:Label ID="Label5_title" runat="server" Visible="false" Text='<%# Bind("title") %>'
                            Style="background-color: #FFFF00"></asp:Label>
                        <br />
                        <span class="style1">這裡都預先設定為<strong>隱形</strong></span><br />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("title") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="author" SortExpression="author">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2_author" runat="server" Visible="false" Text='<%# Bind("author") %>'></asp:TextBox>
                        <br />
                        <asp:Label ID="Label6_author" runat="server" Visible="false" Text='<%# Bind("author") %>'
                            Style="background-color: #FFFF00"></asp:Label>
                        <br />
                        <span class="style1">這裡都預先設定為<strong>隱形</strong></span><br />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("author") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#594B9C" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#33276A" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testConnectionString %>"
            SelectCommand="SELECT [id], [title], [test_time], [author] FROM [test]" DeleteCommand="DELETE FROM [test] WHERE [id] = @id"
            InsertCommand="INSERT INTO [test] ([title], [test_time], [author]) VALUES (@title, @test_time, @author)"
            UpdateCommand="UPDATE [test] SET [title] = @title, [test_time] = @test_time, [author] = @author WHERE [id] = @id">
            <DeleteParameters>
                <asp:Parameter Name="id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="title" Type="String" />
                <asp:Parameter Name="test_time" Type="DateTime" />
                <asp:Parameter Name="author" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="title" Type="String" />
                <asp:Parameter Name="test_time" Type="DateTime" />
                <asp:Parameter Name="author" Type="String" />
                <asp:Parameter Name="id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <br />
        <span class="style6"><strong>1. 在 GridView的「編輯模式」裡面，先把各欄位轉成「樣版」。<br />
            2. 並且動手加入 Label控制項（沒有權限可修改的人，只能看見 Label控制項）。<br />
        </strong></span>
        <br />
        <br />
        HTML畫面上的設定多加了 <span class="style1">&lt;!--#INCLUDE FILE=&quot;defense.aspx&quot;--&gt;</span>，在本檔案的<span
            class="style1">最上方</span>！</div>
    </form>
</body>
</html>
