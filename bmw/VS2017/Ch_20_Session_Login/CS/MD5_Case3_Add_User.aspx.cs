﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//==自己寫的（宣告）==
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;

using System.Security.Cryptography;   //-- MD5專用
using System.Text;                            //-- StringBuilder專用
//==自己寫的（宣告）==


public partial class Book_Sample_B12_Member_Login_Session_MD5_Case3_Add_User : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (Check_SQLInjection(TextBox1.Text, TextBox2.Text, TextBox3.Text, TextBox4.Text) != 0)
        {
            //-- 自訂函數。為了避免SQL Injection攻擊，發現可疑字將會立刻阻擋！
            Response.End();    //-- 程式終止。或是寫成 Exit Sub
        }

        //======================================================
        //==  以參數的方式，進行資料存取，比較不會受到 SQL Injection攻擊。
        //==  自己寫程式透過 SqlDataSource來做會員資料「新增」的功能。
        //======================================================
        SqlDataSource SqlDataSource2 = new SqlDataSource();

        //== 連結資料庫的連接字串 ConnectionString  ==
        SqlDataSource2.ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString;

        //*******************************************************************************
        //== 撰寫SQL指令(Insert Into) == 
        //== (使用參數來做，避免 SQL Injection攻擊)
        SqlDataSource2.InsertParameters.Add("myRealName", TextBox1.Text);
        SqlDataSource2.InsertParameters.Add("myName", TextBox2.Text);

        //--會員的「密碼」，會透過 MD5做加密的動作。
        //-- MD5的 function寫在下面。
        String hash = getMd5Hash(TextBox3.Text);
        SqlDataSource2.InsertParameters.Add("myPasswd", hash);

        SqlDataSource2.InsertParameters.Add("mySex", RadioButtonList1.SelectedValue.ToString());
        SqlDataSource2.InsertParameters.Add("myEmail", TextBox4.Text);

        SqlDataSource2.InsertCommand = "Insert into db_user(real_name, name, password, sex, email, rank) values(@myRealName, @myName, @myPasswd, @mySex, @myEmail, '1')";
        //*******************************************************************************

        //== 執行SQL指令 / 新增 .Insert() ==
        int aff_row = SqlDataSource2.Insert();

        if (aff_row == 0)
        {
            Response.Write("資料新增失敗！");
        }
        else
        {
            Response.Write("資料新增成功！");
        }
    }

    
    //******************************************************************
    //***  避免SQL Injection攻擊
    //******************************************************************
    protected int Check_SQLInjection(String str_1, String str_2, String str_3, String str_4)
    {
        //-- 自訂函數。為了避免SQL Injection攻擊，發現可疑字將會立刻阻擋！
        int i = 0;

        if ((str_1.IndexOf("--", 0) != -1) || (str_1.IndexOf("1=1", 0) != -1))
        {
            Response.Write("<h2>「真實姓名」的欄位發現可疑字  立刻阻擋！</h2>");
            i = 1;
        }
        else
        {
            i = 0;  //--通過檢查

            if ((str_2.IndexOf("--", 0) != -1) || (str_2.IndexOf("1=1", 0) != -1))
            {
                Response.Write("<h2>「帳號」的欄位發現可疑字  立刻阻擋！</h2>");
                i = 1;
            }
            else
            {
                i = 0;  //--通過檢查

                if ((str_3.IndexOf("--", 0) != -1) || (str_3.IndexOf("1=1", 0) != -1))
                {
                    Response.Write("<h2>「密碼」的欄位發現可疑字  立刻阻擋！</h2>");
                    i = 1;
                }
                else
                {
                    i = 0;  //--通過檢查

                    if ((str_4.IndexOf("--", 0) != -1) || (str_4.IndexOf("1=1", 0) != -1))
                    {
                        Response.Write("<h2>「E-Mail」的欄位發現可疑字  立刻阻擋！</h2>");
                        i = 1;
                    }
                    else
                    {
                        i = 0;  //--通過檢查
                    }
                    //*** 其他相關的攻擊字串，可以仿造上面寫法，繼續增加。 ***

                }
            }
        }

        return i;   //--返回值。檢查結果（1表示失敗，沒通過。）
    }





    //===================================================================
    // Hash an input string and return the hash as a 32 character hexadecimal string.
    // 資料來源：http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.md5.aspx 
    //
    //== 底下這部分的程式，就算讀者不懂也可以使用它。照著做就對了。
    //== 只要知道我們輸入一段字串，它就會用MD5進行加密後，傳回「加密後的字串」。

    protected String getMd5Hash(String uInput)
    {
        // Create a new instance of the MD5 object.
        // MD5必須搭配 System.Security.Cryptography命名空間
        MD5 md5Hasher = MD5.Create();

        // Convert the input string to a byte array(陣列) and compute the hash.
        Byte[] MD5data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(uInput));

        // Create a new Stringbuilder to collect the bytes
        // and create a string.
        // StringBuilder必須搭配 System.Text命名空間
        StringBuilder sBuilder = new StringBuilder();

        // Loop through each byte of the hashed data 
        // and format each one as a hexadecimal string.
        for (int i = 0; i < MD5data.Length; i++)
        {
            sBuilder.Append(MD5data[i].ToString("x2"));  //--變成十六進位
        }

        // Return the hexadecimal string.
        return sBuilder.ToString();
    }




}