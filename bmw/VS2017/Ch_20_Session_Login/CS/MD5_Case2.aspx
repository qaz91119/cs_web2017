﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MD5_Case2.aspx.cs" Inherits="Book_Sample_B12_Member_Login_Session_MD5_Case2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            background-color: #FFCC66;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        1. 請輸入一段字，讓 <span class="style1">MD5 雜湊演算法</span>為您加密：
           <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
&nbsp;&nbsp;
        <asp:Button ID="Button1" runat="server" Text="button1_Submit" 
            onclick="Button1_Click" />
        <br />       
        <br />
        MD5加密後的成果：<asp:Label ID="Label1" runat="server" 
            style="font-weight: 700; color: #FF0066" ></asp:Label>
        <br />
        <br />
        <hr />
        <br />
        <br />
        2. 請將加密後的字串輸入，我來幫您驗證：<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
&nbsp;&nbsp;
        <asp:Button ID="Button2" runat="server" Text="button2_Submit" 
            onclick="Button2_Click" />
        <br />       
        <br />
        驗證成果：<asp:Label ID="Label2" runat="server" 
            style="font-weight: 700; color: #009900" ></asp:Label>
    
    </div>
    </form>
</body>
</html>

