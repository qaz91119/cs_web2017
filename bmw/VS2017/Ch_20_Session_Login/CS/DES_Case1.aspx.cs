﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//== 自己加入宣告 ===================
using System.Security.Cryptography;
using System.Text;
using System.IO;
//==============================


public partial class Book_Sample_B12_Member_Login_Session_DES_Case1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            // Create a new DES object to generate a key and initialization vector (IV).  Specify one 
            // of the recognized simple names for this algorithm.
            DES DESalg = DES.Create("DES_XXXYYYZZZ");

            // Create a string to encrypt.
            string sData = TextBox1.Text;

            // Encrypt text to a file using the file name, key, and IV.
            byte[] encrypted = EncryptTextToFile(sData, DESalg.Key, DESalg.IV);

            // Decrypt the text from a file using the file name, key, and IV.
            Label1.Text = DecryptTextFromFile(encrypted, DESalg.Key, DESalg.IV);
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }


    //== 加密 =============================================
    byte[] EncryptTextToFile(String Data, byte[] Key, byte[] IV)
    {
        try
        {
            // Create a new DES object.
            DES DESalg = DES.Create();
            byte[] encrypted;

            using (MemoryStream msEncrypt = new MemoryStream())
            {
                // Create a CryptoStream using the FileStream 
                // and the passed key and initialization vector (IV).
                CryptoStream cStream = new CryptoStream(msEncrypt,
                    DESalg.CreateEncryptor(Key, IV),
                    CryptoStreamMode.Write);

                // Create a StreamWriter using the CryptoStream.
                StreamWriter sWriter = new StreamWriter(cStream);

                // Write the data to the stream to encrypt it.
                sWriter.WriteLine(Data);

                // Close the streams and
                // close the file.
                sWriter.Close();
                cStream.Close();

                encrypted = msEncrypt.ToArray();
            }
            return encrypted;
        }
        catch (CryptographicException e)
        {
            Response.Write("A Cryptographic error occurred: " + e.Message + "<hr />");
            return null;
        }
    }



    //== 解密 =============================================
    string DecryptTextFromFile(byte[] cipherText, byte[] Key, byte[] IV)
    {
        try
        {
             // Create a new DES object.
            DES DESalg = DES.Create();

            using (MemoryStream msEncrypt = new MemoryStream(cipherText))
            {
                // Create a CryptoStream using the FileStream 
                // and the passed key and initialization vector (IV).
                CryptoStream cStream = new CryptoStream(msEncrypt,
                    DESalg.CreateDecryptor(Key, IV),
                    CryptoStreamMode.Read);

                // Create a StreamReader using the CryptoStream.
                StreamReader sReader = new StreamReader(cStream);

                // Read the data from the stream 
                // to decrypt it.
                string val = sReader.ReadLine();

                // Close the streams and
                // close the file.
                sReader.Close();
                cStream.Close();
                
                // Return the string. 
                return val;
            }
        }
        catch (CryptographicException e)
        {
            Response.Write("A Cryptographic error occurred: " + e.Message + "<hr />");
            return null;
        }
    }

}