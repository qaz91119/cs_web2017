﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="User_Name_1_Error_List.aspx.cs" Inherits="Book_Sample_B12_Member_Login_Session_User_Name_Error_List" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--#INCLUDE FILE="defense.aspx"-->
<!-- 這段程式將會防範，直接以網址URL進入的「非法闖入者」-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>搭配使用者姓名，才可 編輯/更新？（有錯誤）</title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
        .style4
        {
            color: #FFFF99;
            font-weight: bold;
            background-color: #006600;
        }
        .style5
        {
            color: #FFFFFF;
            font-weight: bold;
            background-color: #FF0000;
        }
        .style6
        {
            color: #FF0000;
            font-weight: bold;
        }
        .style7
        {
            background-color: #FFCCCC;
        }
    </style>
</head>
<body bgcolor="#ccff99">
    <form id="form1" runat="server">
    <div>
        <span class="style4">搭配使用者姓名，才可 編輯/更新？&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>（有錯誤）
        <asp:Button ID="Button1" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="#009900"
            Text="Button_登出" onclick="Button1_Click" />
        <br />
        <br />
        <span class="style4">比對作者姓名，原作者才能改文章。</span>動態產生 GridView「編輯」的功能！！
        <br />
        <br />
        <span class="style7">粉紅底色</span>的欄位，請設定為<b>樣版</b>！！<br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" DataKeyNames="id" DataSourceID="SqlDataSource1" GridLines="None"
            PageSize="5" ForeColor="#333333" onrowdatabound="GridView1_RowDataBound">
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <EditItemTemplate>
                        <asp:Button ID="Button1" runat="server" CausesValidation="True" CommandName="Update"
                            Text="更新"></asp:Button>
                        &nbsp;<asp:Button ID="Button2" runat="server" CausesValidation="False" CommandName="Cancel"
                            Text="取消"></asp:Button>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Button ID="Button1" runat="server" CausesValidation="False" CommandName="Edit"
                            Text="編輯"></asp:Button>
                    </ItemTemplate>
                    <ItemStyle BackColor="#FFCCFF" />
                </asp:TemplateField>
                <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" InsertVisible="False"
                    ReadOnly="True" />
                <asp:BoundField DataField="test_time" HeaderText="test_time" SortExpression="test_time"
                    DataFormatString="{0:yyyy/MM/dd}" />
                <asp:BoundField DataField="title" HeaderText="title" SortExpression="title"></asp:BoundField>
                <asp:TemplateField HeaderText="author" SortExpression="author">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("author") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("author") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle BackColor="#FFCCFF" />
                </asp:TemplateField>
            </Columns>
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testConnectionString %>"
            SelectCommand="SELECT [id], [title], [test_time], [author] FROM [test]" DeleteCommand="DELETE FROM [test] WHERE [id] = @id"
            UpdateCommand="UPDATE [test] SET [title] = @title, [test_time] = @test_time, [author] = @author WHERE [id] = @id">
            <DeleteParameters>
                <asp:Parameter Name="id" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="title" Type="String" />
                <asp:Parameter Name="test_time" Type="DateTime" />
                <asp:Parameter Name="author" Type="String" />
                <asp:Parameter Name="id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <br />
        <span class="style5">缺點：</span><span class="style1">按下</span><span class="style6">「編輯」按鈕</span><span
            class="style1">就會報錯！！</span><br />
        <br />
        HTML畫面上的設定多加了 <span class="style1">&lt;!--#INCLUDE FILE=&quot;defense.aspx&quot;--&gt;</span>，在本檔案的<span
            class="style1">最上方</span>！</div>
    </form>
</body>
</html>
