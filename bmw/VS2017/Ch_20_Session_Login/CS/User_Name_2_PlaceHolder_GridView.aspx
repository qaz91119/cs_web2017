﻿<%@ Page Language="C#" Debug="true" AutoEventWireup="true" CodeFile="User_Name_2_PlaceHolder_GridView.aspx.cs"
    Inherits="Book_Sample_B12_Member_Login_Session_test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            background-color: #FFFF00;
        }
        .style2
        {
            color: #FF0000;
            font-weight: bold;
        }
        .auto-style1 {
            font-size: large;
        }
        .style6
        {
            color: #FF0000;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <b>課前練習</b> ---- 動態加入GridView「編輯」按鈕 in <span class="style1">PlaceHolder控制項</span>裡面。<br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    務必寫在 <span class="style2">RowCreated事件</span>才能運作。
    <br /><br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AutoGenerateColumns="False" DataKeyNames="id" 
            DataSourceID="SqlDataSource1" onrowdatabound="GridView1_RowDataBound" onrowcreated="GridView1_RowCreated">
            <Columns>

                <asp:CommandField ShowEditButton="True" />

                <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" 
                    ReadOnly="True" SortExpression="id" />
                <asp:TemplateField HeaderText="test_time" SortExpression="test_time">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("test_time") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("test_time") %>'></asp:Label>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server">

                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="PlaceHolder2" runat="server">
                            <asp:Button ID="Button2" runat="server" CommandName="Edit" Text="Button_Inside_Edit" />
                        </asp:PlaceHolder>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
            </Columns>
        </asp:GridView>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testConnectionString %>" 
            DeleteCommand="DELETE FROM [test] WHERE [id] = @id" 
            SelectCommand="SELECT [id], [test_time], [title] FROM [test]" 
            UpdateCommand="UPDATE [test] SET [test_time] = @test_time, [title] = @title WHERE [id] = @id">
            <DeleteParameters>
                <asp:Parameter Name="id" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="test_time" Type="DateTime" />
                <asp:Parameter Name="title" Type="String" />
                <asp:Parameter Name="id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    
        <br />
        <br />
        <strong><span class="auto-style1">有一個嚴重的Bug --</span></strong><br />
        如果把<strong>畫面左邊</strong>（GridView內建的「編輯」按鈕取消），自己動態加入 PlaceHolder的「編輯」按鈕也會失效！<br />
        <br />
        解決方法：<br />
        方法一，把自己動態加入的「編輯」按鈕，<span class="style6"><strong>不要</strong></span>動態加上 ID屬性即可。。<br />
        方法二，請看 RowDataBound七個入門範例的 <strong>第八號範例</strong>。</div>
    </form>
</body>
</html>
