﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//----自己寫的（宣告）----
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;

using System.Net;
using System.Net.Mail;    //--發信必備的NameSpace，用來取代 舊版的System.Web.Mail
//----自己寫的（宣告）----


public partial class Book_Sample_B12_Member_Login_Session_User_Name_Error_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void Button1_login_Click(object sender, EventArgs e)
    {
        if (Check_SQLInjection(TextBox1.Text, TextBox2.Text) != 0)
        {
            //-- 自訂函數。為了避免SQL Injection攻擊，發現可疑字將會立刻阻擋！
            Response.End();  //--程式終止。 
        }

        SqlConnection Conn = new SqlConnection();
        //----上面已經事先寫好 System.Web.Configuration命名空間 ----
        Conn.ConnectionString = WebConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString;
        //----(連結資料庫)----

        SqlDataReader dr = null;
        SqlCommand cmd = new SqlCommand("Select * From db_user Where name = '" + TextBox1.Text + "' And password = '" + TextBox2.Text + "'", Conn);
        //----透過字串組合的方式，組成SQL指令，容易被人攻擊！！
        // 建議改成 -- 
        // SqlCommand cmd = new SqlCommand("Select * From db_user Where name = @NM And password = @PW", Conn);
        // cmd.Parameters.AddWithValue("@NM", TextBox1.Text);
        // cmd.Parameters.AddWithValue("@PW", TextBox2.Text);

        try
        {
            Conn.Open();   //---- 這時候才連結DB
            dr = cmd.ExecuteReader();   //---- 這時候執行SQL指令，取出資料
            if (dr.HasRows)
            {
                dr.Read();
                Session["u_name"] = dr["name"].ToString();
                Session["u_realname"] = dr["real_name"].ToString();

                Session["u_rank"] = dr["rank"].ToString();

                Session["Login"] = "OK";   //--通過帳號與密碼的認證，就獲得 Session。

                cmd.Cancel();
                dr.Close();
                Conn.Close();
                Conn.Dispose();   //--先關閉資源後，下面再來轉換網頁
                Response.Redirect("User_Name_Error_List.aspx");
                //--通過帳號與密碼的認證，就可以進入後端的管理區。
            }
            else
            {
                Response.Write("<h2>帳號/密碼有誤！</h2>");
                cmd.Cancel();
                dr.Close();
                Conn.Close();
                Conn.Dispose();
                return;  //--程式終止、離開
            }
        }
        catch (Exception ex)   //---- 如果程式有錯誤或是例外狀況，將執行這一段
        {
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<HR/>");
        }
    }


    //**************************************************************
    //***  忘記密碼，發 E-Mail通知  
    //               (自己寫的小功能，此為虛擬碼。請依照自己需求修改)
    //**************************************************************
    protected void Button2_email_passwd_Click(object sender, EventArgs e)
    {
        if (Check_SQLInjection(TextBox1.Text, "") != 0)
        {    //-- 自訂函數。為了避免SQL Injection攻擊，發現可疑字將會立刻阻擋！
            Response.Write("<h2>「真實姓名」的欄位發現可疑字  立刻阻擋！</h2>");
            Response.End();  //--程式終止。或是寫成 Exit Sub(VB語法）、return（C#語法）
        }

        SqlConnection Conn = new SqlConnection();
        //----上面已經事先寫好 System.Web.Configuration命名空間 ----
        Conn.ConnectionString = WebConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString;
        //----(連結資料庫)----

        SqlDataReader dr = null;
        SqlCommand cmd = new SqlCommand("select [email], [password] from db_user where name = '" + TextBox1.Text + "'", Conn);
        //----透過字串組合的方式，組成SQL指令，容易被人攻擊！！

        try
        {
            Conn.Open();   //---- 這時候才連結DB
            dr = cmd.ExecuteReader();   //---- 這時候執行SQL指令，取出資料放到 DataReader
            if (dr.HasRows)
            {
                dr.Read();  //--讀取SQL指令，Select撈出來。每次一筆記錄

                //*********************************************************(Start)***
                //-- 通過帳號的認證，就可以收到E-Mail。
                //-- 參考網址：http://msdn.microsoft.com/zh-tw/library/system.net.mail.mailmessage(VS.80).aspx

                MailMessage u_Mail = new MailMessage("admin@xxx.com.tw", dr["email"].ToString(), "信件標題", "您好，您的密碼是：" + dr["password"].ToString());
                //-- 四個參數分別是：發信人。收信人。標題。信件內容。

                SmtpClient mail_client = new SmtpClient("127.0.0.1");
                mail_client.Credentials = CredentialCache.DefaultNetworkCredentials;
                //-- 用於驗證 (Authentication) 的認證 (如果 SMTP 伺服器需要的話)。
                //-- NameSpace為「System.Net」
                mail_client.Send(u_Mail);
                //*********************************************************(End)***
            }
            else
            {
                Response.Write("<h2>帳號有誤！無此人</h2>");
            }
            cmd.Cancel();
            dr.Close();
            Conn.Close();
            Conn.Dispose();
        }
        catch (Exception ex)   //---- 如果程式有錯誤或是例外狀況，將執行這一段
        {
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<HR/>");
        }
    }


    //**************************************************************
    //***  避免SQL Injection攻擊
    //**************************************************************
    protected int Check_SQLInjection(String str_1, String str_2)
    {
        //-- 自訂函數。為了避免SQL Injection攻擊，發現可疑字將會立刻阻擋！
        int i = 0;

        //=================================================
        //== 方法一 ==
        //if ((str_1.IndexOf("--", 0) != -1) || (str_1.IndexOf("1=1", 0) != -1))
        //{
        //    //Response.Write("<h2>「真實姓名」的欄位發現可疑字  立刻阻擋！</h2>");
        //    i = 1;
        //}
        //else
        //{
        //    if ((str_2.IndexOf("--", 0) != -1) || (str_2.IndexOf("1=1", 0) != -1))
        //    {
        //        //Response.Write("<h2>「帳號」的欄位發現可疑字  立刻阻擋！</h2>");
        //        i = 1;
        //    }
        //    else
        //    {
        //            // if (...) {
        //            // } else {
        //            // }
        //        i = 0;  //--通過檢查
        //    }
        //    i = 0;  //--通過檢查
        //    //*** 其他相關的攻擊字串，可以仿造上面寫法，繼續增加。 ***
        //}
        //return i;   //--返回值。檢查結果（1表示失敗，沒通過。）

        //=================================================
        //== 方法二 ==

        //if ((str_1.IndexOf("--", 0) != -1) || (str_1.IndexOf("1=1", 0) != -1))
        //{
        //    //Response.Write("<h2>「真實姓名」的欄位發現可疑字  立刻阻擋！</h2>");
        //    return 1;  //--返回值。檢查結果（1表示失敗，沒通過。）
        //}
        //if ((str_2.IndexOf("--", 0) != -1) || (str_2.IndexOf("1=1", 0) != -1))
        //{
        //    //Response.Write("<h2>「真實姓名」的欄位發現可疑字  立刻阻擋！</h2>");
        //    return 1;  //--返回值。檢查結果（1表示失敗，沒通過。）
        //}
        ////*** 其他相關的攻擊字串，可以仿造上面寫法，繼續增加。 ***

        //return 0;  //--返回值。通過。

        //=================================================
        //== 方法三 ==
        //== 請您參閱「重構（Refactoring）」一書的  Ch.9-5節

        if ((str_1.IndexOf("--", 0) != -1)) return 1;  //--返回值。檢查結果（1表示失敗，沒通過。）
        if ((str_1.IndexOf("1=1", 0) != -1)) return 1;  //--返回值。檢查結果（1表示失敗，沒通過。）

        if ((str_2.IndexOf("--", 0) != -1)) return 1;  //--返回值。檢查結果（1表示失敗，沒通過。）
        if ((str_2.IndexOf("1=1", 0) != -1)) return 1;  //--返回值。檢查結果（1表示失敗，沒通過。）

        //*** 其他相關的攻擊字串，可以仿造上面寫法，繼續增加。 ***

        return 0;  //--返回值。通過。
    }
}