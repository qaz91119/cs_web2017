﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//==自己寫的（宣告）==
using System.Security.Cryptography;   //-- RSA專用
using System.Text;                            //-- StringBuilder專用


//-- 資料來源：http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.rsacryptoserviceprovider.aspx 

public partial class Book_Sample_B12_Member_Login_Session_RSA_Case1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //Create a UnicodeEncoder to convert between byte array and string.
        UnicodeEncoding ByteConverter = new UnicodeEncoding();

        //== 用 Byte陣列來存放 RSA加/解密的文字
        Byte[] dataToEncrypt  = ByteConverter.GetBytes(TextBox1.Text);  //== 輸入字串
        Byte[] encryptedData;
        Byte[] decryptedData;

        //== 透過 RSACryptoServiceProvider 產生公開金鑰（public key）與私密金鑰（private key）
        RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();

        //---------------------------------------------------------------------------------------
        //Pass the data to ENCRYPT, the public key information 
        //(using RSACryptoServiceProvider.ExportParameters(false),
        //and a boolean flag specifying no OAEP padding.
        encryptedData = RSAEncrypt(dataToEncrypt, RSA.ExportParameters(false), false);

        // StringBuilder必須搭配 System.Text命名空間
        StringBuilder sBuilder = new StringBuilder();
        //== 把 Byte陣列轉成文字
        for(int i =0; i<encryptedData.Length; i++)
        {
            sBuilder.Append(encryptedData[i].ToString("x2"));  //== 變成十六進位
        }
  
        //== 加密後的文字
        Label1.Text = sBuilder.ToString();

        //---------------------------------------------------------------------------------------
        //Pass the data to DECRYPT, the private key information 
        //(using RSACryptoServiceProvider.ExportParameters(true),
        //and a boolean flag specifying no OAEP padding.
        decryptedData = RSADecrypt(encryptedData, RSA.ExportParameters(true), false);

        //Display the decrypted plaintext to the console. 
        //== 解密後的文字
        Label2.Text = ByteConverter.GetString(decryptedData);

    }



    //************************
    //***  RSA 加密  ************
    protected byte[] RSAEncrypt(byte[] DataToEncrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
    {
        try
        {    
            //Create a new instance of RSACryptoServiceProvider.
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();

            //Import the RSA Key information. This only needs
            //toinclude the public key information.
            RSA.ImportParameters(RSAKeyInfo);

            //Encrypt the passed byte array and specify OAEP padding.  
            //OAEP padding is only available on Microsoft Windows XP or later.  
            //OAEP padding 只能用在Microsoft Windows XP或後續的作業系統版本  
            return RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
            //== 傳回值都是 Byte陣列
        }
        //Catch and display a CryptographicException to the console.
        catch(CryptographicException e)
        {
            Label3.Text = e.Message.ToString();
            //Response.Write(e.Message.ToString());
            return null;
        }

    }






    //************************
    //***  RSA 解密  ************
        protected byte[] RSADecrypt(byte[] DataToDecrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            try
            {
                //Create a new instance of RSACryptoServiceProvider.
                RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();

                //Import the RSA Key information. This needs
                //to include the private key information.
                RSA.ImportParameters(RSAKeyInfo);

                //Decrypt the passed byte array and specify OAEP padding.  
                //OAEP padding is only available on Microsoft Windows XP or later.  
                //OAEP padding 只能用在Microsoft Windows XP或後續的作業系統版本  
                return RSA.Decrypt(DataToDecrypt, DoOAEPPadding);
                //== 傳回值都是 Byte陣列
            }
            //Catch and display a CryptographicException to the console.
            catch (CryptographicException e)
            {
                Label3.Text = e.ToString();
                //Response.Write(e.ToString());
                return null;
            }

        }



}