﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Book_Sample_B12_Member_Login_Session_User_Name_Error_List : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
                Label lb = (Label)e.Row.FindControl("Label1");

                Button b = (Button)e.Row.FindControl("Button1");   //抓到 GridView的「編輯」按鈕！！ ***重點！！

                // C#沒有 InStr函數，可以參閱這篇文章： http://www.diybl.com/course/4_webprogram/asp.net/asp_netshl/2008828/138240.html
                // 或是 http://www.dotblogs.com.tw/mis2000lab/archive/2009/01/14/instr_function_090114.aspx

                if (lb.Text.IndexOf(Session["u_realname"].ToString(), 0) == -1)
                // .IndexOf()方法。找不到的話， 會傳回「-1」。
                // 找到的話，回傳一個 Integer數字（從零算起）。表示在字串裡面第幾個字，符合條件。 
                {
                    //============================
                    //== 作者名稱相同，才可以修改這篇文章。如果名字不同，就會把「編輯」按鈕隱藏起來！！
                    b.Visible = false;
                    //-- 這段不會動！ GridView1.AutoGenerateEditButton = True
                    //============================
                }
        }
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        //-- 登出，把自己的 Session清空。

        Response.Redirect("User_Name_Error_Login.aspx");
    }
}