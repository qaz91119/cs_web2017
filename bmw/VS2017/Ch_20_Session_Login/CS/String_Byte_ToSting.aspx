﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="String_Byte_ToSting.aspx.cs" Inherits="Book_Sample_B12_Member_Login_Session_String_Byte_ToSting" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        Byte[] 與 字串互相轉換<br />
        <br />
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
        <br />
        <br />
        轉成 Byte[] --- <asp:Label ID="Label1" runat="server" style="font-weight: 700; color: #CC00CC"></asp:Label>
        <br />
    
    </div>
        再從 Byte[]轉回字串 ---
        <asp:Label ID="Label2" runat="server" style="font-weight: 700; color: #009900"></asp:Label>
    </form>
</body>
</html>
