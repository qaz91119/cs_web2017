﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//==自己寫的（宣告）==
using System.Security.Cryptography;   //-- SHA1專用
using System.Text;                            //-- StringBuilder專用


public partial class Book_Sample_B12_Member_Login_Session_String_Prime : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        // 字串轉成 Byte[]、互轉。
        // 資料來源：http://stackoverflow.com/questions/1134671/c-how-can-i-safely-convert-a-byte-array-into-a-string-and-back
        //方法一（字串轉成Byte[]）：
        //byte[] encrypted = TextBox1.Text.Select(Convert.ToByte).ToArray();

        //方法二（字串轉成Byte[]）：
        byte[] encrypted = System.Text.Encoding.Default.GetBytes(TextBox1.Text);

        //陣列  http://msdn.microsoft.com/zh-tw/library/9b9dty7d%28v=vs.110%29.aspx
        int[] prime = {47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127};

        int myCount = 0;
        for (int i = 0; i < encrypted.Length; i++)
        {
            int firstResult = Convert.ToInt32(encrypted[i].ToString()) % 17;
            myCount += prime[firstResult];
        }

        Label1.Text = Convert.ToString(myCount % 151);


        //Label2.Text = getSHA1Hash(Convert.ToString(myCount % 151));  //--加密的 function寫在下面。
    }


    ////===================================================================
    //// 資料來源：http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.sha1.aspx 
    ////
    ////== 底下這部分的程式，就算讀者不懂也可以使用它。照著做就對了。
    ////== 只要知道我們輸入一段字串，它就會用 SHA1進行加密後，傳回「加密後的字串」。
    //protected String getSHA1Hash(String uInput)
    //{
    //    //** 方法一 ****************************************************************
    //    SHA512 SHA1Hasher = SHA512.Create();
    //    //-- SHA1必須搭配 System.Security.Cryptography命名空間

    //    Byte[] data = SHA1Hasher.ComputeHash(Encoding.Default.GetBytes(uInput));
    //    //-- SHA1的 .ComputeHash(Byte[]) 方法，計算指定位元組陣列的雜湊值。
    //    //  （字串轉成Byte[]）  System.Text.Encoding.Default.GetBytes(uInput)

    //    //** 方法二 ****************************************************************
    //    //SHA512CryptoServiceProvider sha1SP = new SHA512CryptoServiceProvider();
    //    ////-- 使用密碼編譯服務提供者 (CSP) 所提供之實作，計算輸入資料的 SHA1 雜湊值。
    //    ////-- http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.sha1cryptoserviceprovider.aspx
    //    //Byte[] data = sha1SP.ComputeHash(Encoding.Default.GetBytes(uInput));

    //    StringBuilder sBuilder = new StringBuilder();   //-- StringBuilder必須搭配 System.Text命名空間

    //    // Loop through each byte of the hashed data and format each one as a hexadecimal string.
    //    for (int i = 0; i < data.Length; i++)
    //    {
    //        sBuilder.Append(data[i].ToString("x2"));  //--變成十六進位
    //    }

    //    return sBuilder.ToString();
    //}
}