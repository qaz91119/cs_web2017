﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Book_Sample_B12_Member_Login_Session_String_Byte_ToSting : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        // 字串轉成 Byte[]、互轉。
        // 資料來源：http://stackoverflow.com/questions/1134671/c-how-can-i-safely-convert-a-byte-array-into-a-string-and-back
        //方法一（字串轉成Byte[]）：
        //byte[] encrypted = TextBox1.Text.Select(Convert.ToByte).ToArray();

        //方法二（字串轉成Byte[]）：
        byte[] encrypted = System.Text.Encoding.Default.GetBytes(TextBox1.Text);


        for (int i = 0; i < encrypted.Length; i++)
        {
            Label1.Text += encrypted[i].ToString();   //--變成十六進位
        }


        //================================================
        // Byte[]轉成字串、互轉。
        // 資料來源：http://stackoverflow.com/questions/1134671/c-how-can-i-safely-convert-a-byte-array-into-a-string-and-back
        Label2.Text = new string(encrypted.Select(Convert.ToChar).ToArray());

        
    }
}