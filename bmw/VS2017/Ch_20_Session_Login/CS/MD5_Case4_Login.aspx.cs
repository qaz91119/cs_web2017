﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//==自己寫的（宣告）==
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;

using System.Security.Cryptography;   //-- MD5專用
using System.Text;                            //-- StringBuilder專用
//==自己寫的（宣告）==


public partial class Book_Sample_B12_Member_Login_Session_MD5_Case4_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (Check_SQLInjection(TextBox1.Text, TextBox2.Text) != 0)
        {
            //-- 自訂函數。為了避免SQL Injection攻擊，發現可疑字將會立刻阻擋！
            Response.End();    //-- 程式終止。
        }

        String hash = getMd5Hash(TextBox2.Text);  //-- MD5加密的 function寫在下面。


        //----上面已經事先寫好 System.Web.Configuration命名空間 ----
        //---- (連結資料庫)----
        SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString.ToString());
        Conn.Open();

        SqlDataReader dr = null;
        SqlCommand cmd = new SqlCommand("select name, rank from db_user where name = '" + TextBox1.Text + "' and password = '" + hash + "'", Conn);

        dr = cmd.ExecuteReader();   //---- 這時候執行SQL指令，取出資料

        if (!dr.Read())
        {
            Response.Write("<h2><font color=red>帳號或是密碼有錯！</font></h2>");

            cmd.Cancel();      //----關閉DataReader之前，一定要先「取消」SqlCommand
            dr.Close();
            Conn.Close();
            Conn.Dispose();

            Response.End();   //--程式終止。或是寫成 Exit Sub，脫離這個事件。
        }
        else
        {
            Session["Login"] = "OK";
            //--帳號密碼驗證成功，才能獲得這個 Session("Login") = "OK" 鑰匙

            Session["u_name"] = dr["name"].ToString();
            Session["u_rank"] = dr["rank"].ToString();

            cmd.Cancel();
            dr.Close();
            Conn.Close();
            Conn.Dispose();

            Response.Write("<h2><font color=blue>帳號 / 密碼驗證成功</font></h2>");
            // Response.Redirect("Session_Login_end.aspx")
            //--帳號密碼驗證成功，導向另一個網頁。
        }
    }


    //******************************************************************
    //***  避免SQL Injection攻擊
    //******************************************************************
    protected int Check_SQLInjection(String str_1, String str_2)
    {
        //-- 自訂函數。為了避免SQL Injection攻擊，發現可疑字將會立刻阻擋！
        int i = 0;

        if ((str_1.IndexOf("--", 0) != -1) || (str_1.IndexOf("1=1", 0) != -1))
        {
            Response.Write("<h2>「真實姓名」的欄位發現可疑字  立刻阻擋！</h2>");
            i = 1;
        }
        else
        {
            i = 0;  //--通過檢查

            if ((str_2.IndexOf("--", 0) != -1) || (str_2.IndexOf("1=1", 0) != -1))
            {
                Response.Write("<h2>「帳號」的欄位發現可疑字  立刻阻擋！</h2>");
                i = 1;
            }
            else
            {
                i = 0;  //--通過檢查
            }
            //*** 其他相關的攻擊字串，可以仿造上面寫法，繼續增加。 ***
        }
        
        return i;   //--返回值。檢查結果（1表示失敗，沒通過。）
    }

    
    //===================================================================
    // Hash an input string and return the hash as a 32 character hexadecimal string.
    // 資料來源：http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.md5.aspx 
    //
    //== 底下這部分的程式，就算讀者不懂也可以使用它。照著做就對了。
    //== 只要知道我們輸入一段字串，它就會用MD5進行加密後，傳回「加密後的字串」。

    protected String getMd5Hash(String uInput)
    {
        // Create a new instance of the MD5 object.
        // MD5必須搭配 System.Security.Cryptography命名空間
        MD5 md5Hasher = MD5.Create();

        // Convert the input string to a byte array(陣列) and compute the hash.
        Byte[] MD5data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(uInput));

        // Create a new Stringbuilder to collect the bytes
        // and create a string.
        // StringBuilder必須搭配 System.Text命名空間
        StringBuilder sBuilder = new StringBuilder();

        // Loop through each byte of the hashed data 
        // and format each one as a hexadecimal string.
        for (int i = 0; i < (MD5data.Length - 1); i++)
        {
            sBuilder.Append(MD5data[i].ToString("x2"));  //--變成十六進位
        }

        // Return the hexadecimal string.
        return sBuilder.ToString();
    }

}