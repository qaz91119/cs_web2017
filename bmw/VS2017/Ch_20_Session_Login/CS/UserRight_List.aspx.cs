﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Book_Sample_B12_Member_Login_Session_UserRight_List : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UpdateRight"].ToString() == "Y")
        {
            GridView1.AutoGenerateEditButton = true;
        }

        if (Session["DeleteRight"].ToString() == "Y")
        {
            GridView1.AutoGenerateDeleteButton = true;
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        //-- 登出，把自己的 Session清空。

        Response.Redirect("UserRight_Login.aspx");
    }
}