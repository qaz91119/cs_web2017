﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DES_Case1.aspx.cs" Inherits="Book_Sample_B12_Member_Login_Session_DES_Case1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        DES加密與解密（需要金鑰）<br />
        <a href="http://msdn.microsoft.com/zh-tw/library/xyb7hay8.aspx">http://msdn.microsoft.com/zh-tw/library/xyb7hay8.aspx</a>
        <br />
        <br />
        請輸入字串：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
        <br />
    
    </div>
        DES加密：<asp:Label ID="Label1" runat="server" style="font-weight: 700; color: #FF0066"></asp:Label>
        <br />
        <br />
        DES解密：<asp:Label ID="Label2" runat="server" style="font-weight: 700; color: #009900"></asp:Label>
    </form>
</body>
</html>
