﻿'==自己寫的==
Imports System.Security.Cryptography   '-- RSA專用
Imports System.Text                            '-- StringBuilder專用


Partial Class _Book_App_Session_RSA_Case1
    Inherits System.Web.UI.Page

    '資料來源：http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.rsacryptoserviceprovider.aspx 

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Create a UnicodeEncoder to convert between byte array and string.
        Dim ByteConverter As New UnicodeEncoding()

        '== 用 Byte陣列來存放 RSA加/解密的文字
        Dim dataToEncrypt() As Byte = ByteConverter.GetBytes(TextBox1.Text)  '== 輸入字串
        Dim encryptedData() As Byte
        Dim decryptedData() As Byte

        '== 透過 RSACryptoServiceProvider 產生公開金鑰（public key）與私密金鑰（private key）
        Dim RSA As New RSACryptoServiceProvider()

        '---------------------------------------------------------------------------------------
        'Pass the data to ENCRYPT, the public key information 
        '(using RSACryptoServiceProvider.ExportParameters(false),
        'and a boolean flag specifying no OAEP padding.
        encryptedData = RSAEncrypt(dataToEncrypt, RSA.ExportParameters(False), False)

        ' StringBuilder必須搭配 System.Text命名空間
        Dim sBuilder As New StringBuilder()
        '== 把 Byte陣列轉成文字
        For i As Integer = 0 To encryptedData.Length - 1
            sBuilder.Append(encryptedData(i).ToString("x2"))  '== 變成十六進位
        Next i
        '== 加密後的文字
        Label1.Text = sBuilder.ToString()

        '---------------------------------------------------------------------------------------
        'Pass the data to DECRYPT, the private key information 
        '(using RSACryptoServiceProvider.ExportParameters(true),
        'and a boolean flag specifying no OAEP padding.
        decryptedData = RSADecrypt(encryptedData, RSA.ExportParameters(True), False)

        'Display the decrypted plaintext to the console. 
        '== 解密後的文字
        Label2.Text = ByteConverter.GetString(decryptedData)

    End Sub

    '************************
    '***  RSA 加密  **********
    Function RSAEncrypt(ByVal DataToEncrypt() As Byte, ByVal RSAKeyInfo As RSAParameters, ByVal DoOAEPPadding As Boolean) As Byte()
        Try
            'Create a new instance of RSACryptoServiceProvider.
            Dim RSA As New RSACryptoServiceProvider()

            'Import the RSA Key information. This only needs
            'toinclude the public key information.
            RSA.ImportParameters(RSAKeyInfo)

            'Encrypt the passed byte array and specify OAEP padding.  
            'OAEP padding 只能用在Microsoft Windows XP或後續的作業系統版本  
            Return RSA.Encrypt(DataToEncrypt, DoOAEPPadding)
            '== 傳回值都是 Byte陣列

        Catch e As CryptographicException
            Response.Write(e.Message)
            Return Nothing
        End Try
    End Function


    '************************
    '***  RSA 解密  **********
    Function RSADecrypt(ByVal DataToDecrypt() As Byte, ByVal RSAKeyInfo As RSAParameters, ByVal DoOAEPPadding As Boolean) As Byte()
        Try
            'Create a new instance of RSACryptoServiceProvider.
            Dim RSA As New RSACryptoServiceProvider()

            'Import the RSA Key information. This needs
            'to include the private key information.
            RSA.ImportParameters(RSAKeyInfo)

            'Decrypt the passed byte array and specify OAEP padding.  
            'OAEP padding 只能用在Microsoft Windows XP或後續的作業系統版本 
            Return RSA.Decrypt(DataToDecrypt, DoOAEPPadding)
            '== 傳回值都是 Byte陣列
        Catch e As CryptographicException
            Response.Write(e.ToString())
            Return Nothing
        End Try
    End Function

End Class
