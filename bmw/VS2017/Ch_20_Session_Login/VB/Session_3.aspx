﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Session_3.aspx.vb" Inherits="Book_Sample_Ch16_Program__Book_App_Session_Session_3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Session_3</title>
</head>
<body>
    <%  '--註解：這段程式是 Inline Code，檔名為 Session_3.aspx。

        Session("my_test1") = "***只有你一人與這一個瀏覽器***看的到這訊息！"
        Session("my_test2") = 1000
    
        Dim a As ArrayList = New ArrayList()
        For i As Integer = 1 To 3
            '寫入ArrayList
            a.Add(i)
        Next
        
        Session("my_test3") = a  '--ArrayList A的內容是1 2 3。
    
        Response.Write("****執行完畢！****<hr><br>")
        '-------------------------------------------------------------------------------------------------------
    
    
        If Not Session("my_test1") Is Nothing Then
            Response.Write("<br /> my_test1的值----" & Session("my_test1"))
        End If

        If Not Session("my_test2") Is Nothing Then
            Response.Write("<br /> my_test2的值----" & Session("my_test2"))
        End If
       
    
        If Not Session("my_test3") Is Nothing Then
            Dim B As ArrayList = Session("my_test3")
        
            Response.Write("<br> my_test3的值----")   '讀取ArrayList
            
            For Each item As Integer In B
                Response.Write(item)
            Next
        End If
    %>
</body>
</html>
