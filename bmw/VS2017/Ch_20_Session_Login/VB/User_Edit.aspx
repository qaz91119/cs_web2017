<%@ Page Language="VB" AutoEventWireup="false" CodeFile="User_Edit.aspx.vb" Inherits="_Book_App_Session_User_Edit" %>

<!--#INCLUDE FILE="defense.aspx"-->
<!-- 這段程式將會防範，直接以網址URL進入的「非法闖入者」-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>未命名頁面</title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
            font-weight: bold;
        }
        .style2
        {
            background-color: #99CCFF;
        }
        .style3
        {
            color: #FF0000;
            font-weight: bold;
            font-size: small;
        }
        .style4
        {
            font-size: small;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <b>點選文章編號，便可以透過 <span class="style2">FormView</span>來修改這篇文章。</b><br />
&nbsp;<span class="style4">&nbsp;&nbsp; @ 權限最高的管理員，可以修改全部的文章。</span><br 
            class="style4" />
        <span class="style4">&nbsp;&nbsp;&nbsp; @ 自己撰寫的文章，自己也可以修改。</span><br 
            class="style4" />
        <span class="style4">&nbsp;&nbsp;&nbsp; @ 多加了一行 </span> <span class="style3">&lt;!--#INCLUDE FILE=&quot;defense.aspx&quot;--&gt;</span><span 
            class="style4">，在本檔案的</span><span 
            class="style3">最上方</span>！<br />
        <br />
        <span class="style1">上述的重點，將會寫在「後置程式碼」裡面。</span><br />
        <br />
        <asp:FormView ID="FormView1" runat="server" BackColor="#DEBA84" 
            BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
            CellSpacing="2" DataSourceID="SqlDataSource1" DefaultMode="Edit" 
            GridLines="Both" Width="550px" DataKeyNames="id">
            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
            <EditItemTemplate>
                id:
                <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                <br />
                test_time（日期）:
                <asp:TextBox ID="test_timeTextBox" runat="server" 
                    Text='<%# Bind("test_time", "{0:d}") %>'></asp:TextBox>
                <br />
                class（分類）: <asp:TextBox ID="classTextBox" runat="server" Text='<%# Bind("class") %>'></asp:TextBox>
                <br />
                <br />
                title（標題）: <asp:TextBox ID="titleTextBox" runat="server" Text='<%# Bind("title") %>' 
                    Width="380px"></asp:TextBox>
                <br />
                <br />
                summary（摘要）: <asp:TextBox ID="summaryTextBox" runat="server" Height="50px" 
                    Text='<%# Bind("summary") %>' TextMode="MultiLine" Width="350px"></asp:TextBox>
                <br />
                <br />
                article（全文）: <asp:TextBox ID="articleTextBox" runat="server" Width="370px" 
                    Height="150px" Text='<%# Bind("article") %>' TextMode="MultiLine"></asp:TextBox>
                <br />
                <br />
                author（作者）: <asp:TextBox ID="authorTextBox" runat="server" 
                    Text='<%# Bind("author") %>'></asp:TextBox>
                <br />
                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                    CommandName="Update" Text="更新" />
                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                    CausesValidation="False" CommandName="Cancel" Text="取消" />
            </EditItemTemplate>
            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
            <EmptyDataTemplate>
                抱歉！沒有這篇文章喔∼∼
            </EmptyDataTemplate>
            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
        </asp:FormView>
        
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testConnectionString %>" 
            DeleteCommand="DELETE FROM [test] WHERE [id] = @id" 
            SelectCommand="SELECT [id], [test_time], [class], [title], [summary], [article], [author] FROM [test] WHERE ([id] = @id)" 
            UpdateCommand="UPDATE [test] SET [test_time] = @test_time, [class] = @class, [title] = @title, [summary] = @summary, [article] = @article, [author] = @author WHERE [id] = @id">
            <SelectParameters>
                <asp:QueryStringParameter Name="id" QueryStringField="id" Type="Int32" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="id" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter DbType="DateTime" Name="test_time" />
                <asp:Parameter Name="class" Type="String" />
                <asp:Parameter Name="title" Type="String" />
                <asp:Parameter Name="summary" Type="String" />
                <asp:Parameter Name="article" Type="String" />
                <asp:Parameter Name="author" Type="String" />
                <asp:Parameter Name="id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <br />
        
        
    </div>
    </form>
 
</body>
</html>
