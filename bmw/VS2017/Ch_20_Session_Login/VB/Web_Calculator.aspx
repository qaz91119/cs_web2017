﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Web_Calculator.aspx.vb" Inherits="_Book_App_Session_Web_Calculator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>小小計算機（Web版）</title>
    </head>
<body>
    <form id="form1" runat="server">
    <div>
    
        小小計算機（Web版），只有加法能運作 
        #1&nbsp; (2012/5/7修改）<br />
        <br />
        <asp:TextBox ID="TextBox1" runat="server" Font-Bold="True" Font-Size="Large"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="Button7" runat="server" Font-Bold="True" Font-Size="Medium" 
            Text="7" />
&nbsp;<asp:Button ID="Button8" runat="server" Font-Bold="True" Font-Size="Medium" 
            Text="8" />
&nbsp;<asp:Button ID="Button9" runat="server" Font-Bold="True" Font-Size="Medium" 
            Text="9" />
        &nbsp;
        <asp:Button ID="ButtonCE" runat="server" Font-Bold="True" Font-Size="Medium" 
            ForeColor="#009900" Text="CE 清除" Width="105px" />
        <br />
        <asp:Button ID="Button4" runat="server" Font-Bold="True" Font-Size="Medium" 
            Text="4" />
&nbsp;<asp:Button ID="Button5" runat="server" Font-Bold="True" Font-Size="Medium" 
            Text="5" />
&nbsp;<asp:Button ID="Button6" runat="server" Font-Bold="True" Font-Size="Medium" 
            Text="6" />
        &nbsp;
        <asp:Button ID="Button_x3" runat="server" Font-Bold="True" Font-Size="Medium" 
            Text="X 乘" Width="50px" ForeColor="#999999" />
        <asp:Button ID="Button_x4" runat="server" Font-Bold="True" Font-Size="Medium" 
            Text="/ 除" Width="50px" ForeColor="#999999" />
        <br />
        <asp:Button ID="Button1" runat="server" Font-Bold="True" Font-Size="Medium" 
            Text="1" />
&nbsp;<asp:Button ID="Button2" runat="server" Font-Bold="True" Font-Size="Medium" 
            Text="2" />
&nbsp;<asp:Button ID="Button3" runat="server" Font-Bold="True" Font-Size="Medium" 
            Text="3" />
        &nbsp;
        <asp:Button ID="Button_x1" runat="server" Font-Bold="True" Font-Size="Medium" 
            ForeColor="Red" Text="+ 加" Width="50px" />
        <asp:Button ID="Button_x2" runat="server" Font-Bold="True" Font-Size="Medium" 
            Text="- 減" Width="50px" ForeColor="#999999" />
        <br />
        <asp:Button ID="Button0" runat="server" Font-Bold="True" Font-Size="Medium" 
            Text="0" Width="80px" />
    
    &nbsp;
        <asp:Button ID="ButtonEnd" runat="server" Font-Bold="True" Font-Size="Large" 
            Text="=" Width="100px" ForeColor="#FF0066" />
    
        <br />
        <br />
        您剛剛輸入的：<asp:Label ID="Label1" runat="server" style="color: #009900"></asp:Label>
    
    </div>
    </form>
</body>
</html>
