﻿
Partial Class VS2010_Book_Sample_Ch16_Program__Book_App_Session_Session_Lock_Test_01
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        If TextBox1.Text = "123" And TextBox2.Text = "123" Then
            Session("Login_OK") = "OK"

            System.Threading.Thread.Sleep(30000)
            '--系統會沉睡30秒鐘
        End If

    End Sub
End Class
