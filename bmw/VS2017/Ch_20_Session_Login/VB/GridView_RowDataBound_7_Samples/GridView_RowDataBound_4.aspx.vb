﻿
Partial Class VS2010_Book_Sample_Ch11_Program_GridView_RowDataBound_4
    Inherits System.Web.UI.Page


    Protected Sub GridView1_RowCreated(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        Response.Write("<br /><br /><b>RowCreated事件</b><hr />")

        '== 通常都會搭配這幾段 if 判別式
        If e.Row.RowType = DataControlRowType.Header Then
            '-- 只有 GridView呈現「表頭」列的時候，才會執行這裡！
            Response.Write("「表頭」列<font color=blue>DataControlRowType.Header </font><hr /><br />")
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            '-- 當 GridView呈現「每一列」資料列（記錄）的時候，才會執行這裡！
            '-- 所以這裡就像迴圈一樣，會反覆執行喔！！
            Response.Write("「每一列」資料列（記錄）DataControlRowType.DataRow<br />")
        End If

        '*********************************************************
        '*** 重點！！請您瞭解這個事件的參數 e代表什麼意思？ ***
        '*********************************************************
        '-- 這個事件的「e」，代表 GridView產生畫面上的「每一列」。
    End Sub



    Protected Sub GridView1_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Response.Write("<hr /><font color=red>RowDataBound事件</font><hr />")

        '== 通常都會搭配這幾段 if 判別式
        If e.Row.RowType = DataControlRowType.Header Then
            '-- 只有 GridView呈現「表頭」列的時候，才會執行這裡！
            Response.Write("「表頭」列<font color=blue>DataControlRowType.Header </font><hr /><br />")
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            '-- 當 GridView呈現「每一列」資料列（記錄）的時候，才會執行這裡！
            '-- 所以這裡就像迴圈一樣，會反覆執行喔！！
            Response.Write("「每一列」資料列（記錄）<font color=red>DataControlRowType.DataRow</font><br />")
        End If

        '*********************************************************
        '*** 重點！！請您瞭解這個事件的參數 e代表什麼意思？ ***
        '*********************************************************
        '-- 這個事件的「e」，代表 GridView產生畫面上的「每一列」。
    End Sub
End Class
