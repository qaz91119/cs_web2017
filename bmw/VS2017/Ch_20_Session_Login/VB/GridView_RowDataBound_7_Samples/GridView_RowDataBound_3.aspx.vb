﻿
Partial Class VS2010_Book_Sample_Ch11_Program_GridView_RowDataBound_3
    Inherits System.Web.UI.Page

    '============================
    Protected widestData As Integer
    '== 資料來源： http://msdn.microsoft.com/zh-tw/library/ms178296(v=VS.100).aspx


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        widestData = 0
    End Sub


    Protected Sub GridView1_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim drv As System.Data.DataRowView = CType(e.Row.DataItem, System.Data.DataRowView)

        If e.Row.RowType = DataControlRowType.DataRow Then   '== 通常會在事件內，使用這個 if判別式
            If drv IsNot Nothing Then
                Dim catName As String = drv(1).ToString()
                'Response.Write(catName & "<br />")    '-- catName是第二格（title欄位）的文字
                Dim catNameLen As Integer = catName.Length

                If catNameLen > widestData Then
                    widestData = catNameLen
                End If
                GridView1.Columns(2).ItemStyle.Width = widestData * 30
                GridView1.Columns(2).ItemStyle.Wrap = False   '--  是否自動換列？

            End If
        End If

    End Sub

End Class
