﻿
Partial Class Book_Sample_Ch11_Program_GridView_RowDataBound_7_Samples_GridView_RowDataBound_2_CaseStudy
    Inherits System.Web.UI.Page



    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            '-- 當 GridView呈現「每一列」資料列（記錄）的時候，才會執行這裡！
            '-- 所以這裡就像迴圈一樣，會反覆執行喔！！


            '*************************************************************
            If CInt(e.Row.Cells(4).Text) < 60 Then
                e.Row.Cells(4).ForeColor = Drawing.Color.Red   '-- 把第五格的資料列（記錄）"格子"，變成紅色。
                e.Row.Cells(4).Font.Bold = True
            End If

            If CInt(e.Row.Cells(5).Text) < 60 Then
                e.Row.Cells(5).ForeColor = Drawing.Color.Red   '-- 把第六格的資料列（記錄）"格子"，變成紅色。
                e.Row.Cells(5).Font.Bold = True
            End If

            '*********************************************************
            '*** 重點！！請您瞭解這個事件的參數 e代表什麼意思？ ***
            '*********************************************************
            '-- 這個事件的「e」，代表 GridView產生畫面上的「每一列」。
        End If

    End Sub

End Class
