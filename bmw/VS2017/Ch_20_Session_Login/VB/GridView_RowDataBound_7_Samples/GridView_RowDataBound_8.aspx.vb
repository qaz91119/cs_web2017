﻿
Partial Class Book_Sample_Ch11_Program_GridView_RowDataBound_7_Samples_GridView_RowDataBound_8
    Inherits System.Web.UI.Page

    '*****************************************************
    '** 參考資料：http://www.allenkuo.com/GenericArticle/view1374.aspx
    '*****************************************************
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

        Dim cf As CommandField = New CommandField()
        cf.ShowEditButton = True

        GridView1.Columns.Add(cf)

    End Sub
End Class
