﻿
Partial Class VS2010_Book_Sample_B12_Member_Login_Session_User_Name_2_PlaceHolder_GridView
    Inherits System.Web.UI.Page


    Protected Sub GridView1_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        'If e.Row.RowType = DataControlRowType.DataRow Then

        '    '*********************************************************
        '    '**  失敗！！程式寫在這裡，「編輯」按鈕按下去沒有反應！
        '    If e.Row.RowState <> DataControlRowState.Edit Then
        '        '**  重點！！！進入「編輯」模式以後，不執行這一段，就不會報錯！(start)
        '        '*********************************************************

        '        Dim ph As PlaceHolder = CType(e.Row.FindControl("PlaceHolder1"), PlaceHolder)
        '        '== 將會在 PlaceHolder裡面，動態產生 Button「編輯」按鈕

        '        '==================================================
        '        '==  動態加入 Button到 PlaceHolder裡面，無法觸發 GridView編輯模式 ==
        '        '==  失敗！！程式寫在這裡，「編輯」按鈕按下去沒有反應！
        '        '==================================================
        '        Dim b1 As New Button
        '        b1.ID = "Button_Edit_1"
        '        b1.CommandName = "Edit"  '-- 重點！！這樣才能觸發 GridView編輯模式
        '        b1.Text = "MIS2000 Lab.「編輯」按鈕"
        '        '--   動態加入 GridView的「編輯」按鈕！ *** 重點！！*** 跟上一個範例不同。

        '        ph.Controls.Add(b1)    '-- 在 PlaceHolder裡面，動態產生 Button「編輯」按鈕
        '        '-- .Controls.Add()  動態加入控制項。

        '        '---------------------------------------------------------------------------------
        '        Dim b2 As New Button
        '        b2.ID = "Button_Delete_2"
        '        b2.CommandName = "Delete"  '-- 重點！！這樣才能觸發 GridView刪除事件
        '        b2.Text = "MIS2000 Lab.「刪除」按鈕"
        '        ph.Controls.Add(b2)
        '        '=================================================
        '    End If

        'End If   '**  重點！！！進入「編輯」模式以後，不執行這一段，就不會報錯！(end)
    End Sub


    Protected Sub GridView1_RowCreated(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then

            '*********************************************************
            If e.Row.RowState <> DataControlRowState.Edit Then
                '**  重點！！！進入「編輯」模式以後，不執行這一段，就不會報錯！(start)
                '*********************************************************

                Dim ph As PlaceHolder = CType(e.Row.FindControl("PlaceHolder1"), PlaceHolder)
                '== 將會在 PlaceHolder裡面，動態產生 Button「編輯」按鈕


                '==================================================
                '==  動態加入 Button到 PlaceHolder裡面，無法觸發 GridView編輯模式 ==
                '==================================================
                Dim b1 As New Button
                b1.ID = "Button_Edit_1"
                b1.CommandName = "Edit"  '-- 重點！！這樣才能觸發 GridView編輯模式
                b1.Text = "MIS2000 Lab.「編輯」按鈕"
                '--   動態加入 GridView的「編輯」按鈕！ *** 重點！！*** 跟上一個範例不同。

                ph.Controls.Add(b1)    '-- 在 PlaceHolder裡面，動態產生 Button「編輯」按鈕
                '-- .Controls.Add()  動態加入控制項。

                '---------------------------------------------------------------------------------
                'Dim b2 As New Button
                'b2.ID = "Button_Delete_2"
                'b2.CommandName = "Delete"  '-- 重點！！這樣才能觸發 GridView刪除事件
                'b2.Text = "MIS2000 Lab.「刪除」按鈕"
                'ph.Controls.Add(b2)
                '=================================================
            End If

        End If   '**  重點！！！進入「編輯」模式以後，不執行這一段，就不會報錯！(end)
    End Sub

End Class
