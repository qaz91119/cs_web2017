﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----


Partial Class _Book_App_Session_Session_Login_DB
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub




    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '----上面已經事先寫好 Imports System.Web.Configuration ----
        '---- (連結資料庫)----
        Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString.ToString())
        Conn.Open()

        Dim dr As SqlDataReader = Nothing
        Dim cmd As SqlCommand = New SqlCommand("select name, password from db_user where name = '" & TextBox1.Text & "' and password = '" & TextBox2.Text & "'", Conn)
        '*** 請注意資料隱碼攻擊（SQL Injection攻擊）***

        dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料

        '*****************************
        If Not dr.Read() Then
            '*****************************
            Response.Write("帳號或是密碼有錯！")

            cmd.Cancel()
            '----關閉DataReader之前，一定要先「取消」SqlCommand
            dr.Close()
            Conn.Close()
            Conn.Dispose()

            Response.End()   '--程式暫停。或是寫成 Exit Sub，脫離這個事件。
        Else
            Session("Login") = "OK"
            '--帳號密碼驗證成功，才能獲得這個 Session("Login") = "OK" 鑰匙

            Session("u_name") = dr("name")
            Session("u_passwd") = dr("password")

            cmd.Cancel()
            dr.Close()
            Conn.Close()
            Conn.Dispose()

            Response.Redirect("Session_Login_end.aspx")
            '--帳號密碼驗證成功，導向另一個網頁。
        End If
    End Sub
End Class
