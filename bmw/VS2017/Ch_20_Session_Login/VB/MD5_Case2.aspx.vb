﻿'==自己寫的==
Imports System.Security.Cryptography   '-- MD5專用
Imports System.Text                            '-- StringBuilder專用


Partial Class _Book_App_Session_MD5_Case2
    Inherits System.Web.UI.Page


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim source As String = TextBox1.Text

        Dim hash As String = getMd5Hash(source)  '--加密的 function寫在下面。

        Label1.Text = "原本的字串「<font color=blue>" + source + "</font>」，經過加密後變成：&nbsp;&nbsp; " + hash
    End Sub

    '===================================================================
    ' Hash an input string and return the hash as a 32 character hexadecimal string.
    ' 資料來源：http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.md5.aspx 

    Function getMd5Hash(ByVal uInput As String) As String
        ' Create a new instance of the MD5 object.
        ' MD5必須搭配 System.Security.Cryptography命名空間
        Dim md5Hasher As MD5 = MD5.Create()

        ' Convert the input string to a byte array and compute the hash.
        Dim MD5data() As Byte = md5Hasher.ComputeHash(Encoding.Default.GetBytes(uInput))

        ' Create a new Stringbuilder to collect the bytes
        ' and create a string.
        ' StringBuilder必須搭配 System.Text命名空間
        Dim sBuilder As New StringBuilder()

        ' Loop through each byte of the hashed data 
        ' and format each one as a hexadecimal string.
        For i As Integer = 0 To MD5data.Length - 1
            sBuilder.Append(MD5data(i).ToString("x2"))  '--變成十六進位
        Next i

        ' Return the hexadecimal string.
        Return sBuilder.ToString()

    End Function

    '*****************************************************************************
    '***  MD5加密後的資料進行驗證
    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        '-- 輸入兩個字串，進行驗證比對。
        '        第一個字串，是原始的輸入字串（未加密）。
        '        第二個字串，是經過 MD5加密之後的字串。
        If verifyMd5Hash(TextBox1.Text, TextBox2.Text) Then
            Label2.Text = "驗證成功！"
        Else
            Label2.Text = "驗證失敗！！您輸入的 MD5碼，跟一開始輸入的「原始字串」不符合！The hashes are not same."
        End If

    End Sub


    ' Verify a hash against a string.
    '        第一個字串，是原始的輸入字串（未加密）。
    '        第二個字串，是經過 MD5加密之後的字串。
    Function verifyMd5Hash(ByVal Input_Source As String, ByVal Input_MD5 As String) As Boolean
        ' Hash the input.
        Dim hashOfInput As String = getMd5Hash(Input_Source)

        ' Create a StringComparer an comare the hashes.
        Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

        If comparer.Compare(hashOfInput, Input_MD5) = 0 Then
            Return True
        Else
            Return False
        End If

    End Function




End Class
