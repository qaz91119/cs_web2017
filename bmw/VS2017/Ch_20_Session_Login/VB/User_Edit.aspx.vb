﻿
Partial Class _Book_App_Session_User_Edit
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If CInt(Session("u_rank")) <> 3 Then
            '-- RANK=3表示最高等級。我們的使用者權限是依照數字來區分，數字越大，權限越大。

            Dim u_author As TextBox = FormView1.FindControl("authorTextBox")

            If Right(u_author.Text, 3) <> Trim(Session("u_name").ToString()) Then
                '-- 權限不夠的話。再次比對作者姓名，必須相同才能修改。
                Response.Write("<h2><font colore=red>抱歉，您權限不夠，也不是本文作者，因此不得修改！</font></h2>")
                Response.End()
            End If
        End If

    End Sub

    Protected Sub FormView1_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewUpdatedEventArgs) Handles FormView1.ItemUpdated
        Response.Write("<h2><font colore=blue>謝謝，修改已經成功！</font></h2>")
    End Sub
End Class
