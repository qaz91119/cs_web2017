﻿
Partial Class VS2010_Book_Sample__Book_App_Session_User_Name_1_List
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            '*********************************************************
            If e.Row.RowState <> DataControlRowState.Edit Then
                '**  重點！！！進入「編輯」模式以後，不執行這一段，就不會報錯！(start)
                '*********************************************************

                Dim lb As Label = CType(e.Row.FindControl("Label1"), Label)

                Dim b As Button = CType(e.Row.FindControl("Button1"), Button)
                '--   抓到 GridView的「編輯」按鈕！！ ***重點！！

                If InStr(1, lb.Text, Session("u_realname").ToString()) = 0 Then
                    '============================
                    '== 作者名稱相同，才可以修改這篇文章。如果名字不同，就會把「編輯」按鈕隱藏起來！！
                    b.Visible = False
                    '-- 這段不會動！ GridView1.AutoGenerateEditButton = True
                    '============================
                End If

            End If   '**  重點！！！進入「編輯」模式以後，不執行這一段，就不會報錯！(end)

        End If
    End Sub


    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        Session.Abandon()
        '-- 登出，把自己的 Session清空。

        Response.Redirect("User_Name_1_Login.aspx")
    End Sub

End Class
