﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient

Imports System.Security.Cryptography   '-- MD5專用
Imports System.Text                            '-- StringBuilder專用
'----自己寫的----


Partial Class _Book_App_Session_MD5_Case4_Login
    Inherits System.Web.UI.Page


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Check_SQLInjection(TextBox1.Text, TextBox2.Text) <> 0 Then
            '-- 自訂函數。為了避免SQL Injection攻擊，發現可疑字將會立刻阻擋！
            Response.End()    '-- 程式終止。或是寫成 Exit Sub
        End If

        Dim hash As String = getMd5Hash(TextBox2.Text)  '-- MD5加密的 function寫在下面。


        '----上面已經事先寫好 Imports System.Web.Configuration ----
        '---- (連結資料庫)----
        Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString.ToString())
        Conn.Open()

        Dim dr As SqlDataReader = Nothing
        Dim cmd As SqlCommand = New SqlCommand("select name, rank from db_user where name = '" & TextBox1.Text & "' and password = '" & hash & "'", Conn)

        dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料

        If Not dr.Read() Then
            Response.Write("<h2><font color=red>帳號或是密碼有錯！</font></h2>")

            cmd.Cancel()      '----關閉DataReader之前，一定要先「取消」SqlCommand
            dr.Close()
            Conn.Close()
            Conn.Dispose()

            Response.End()   '--程式終止。或是寫成 Exit Sub，脫離這個事件。
        Else
            Session("Login") = "OK"
            '--帳號密碼驗證成功，才能獲得這個 Session("Login") = "OK" 鑰匙

            Session("u_name") = dr("name")
            Session("u_rank") = dr("rank")

            cmd.Cancel()
            dr.Close()
            Conn.Close()
            Conn.Dispose()

            Response.Write("<h2><font color=blue>帳號 / 密碼驗證成功</font></h2>")
            'Response.Redirect("Session_Login_end.aspx")
            '--帳號密碼驗證成功，導向另一個網頁。
        End If
    End Sub


    '******************************************************************
    '***  避免SQL Injection攻擊
    Function Check_SQLInjection(ByVal str_1 As String, ByVal str_2 As String) As Integer
        '-- 自訂函數。為了避免SQL Injection攻擊，發現可疑字將會立刻阻擋！
        Dim i As Integer = 0
        If (InStr(1, str_1, "--") <> 0) Or (InStr(1, str_1, "1=1") <> 0) Then
            Response.Write("<h2>「真實姓名」的欄位發現可疑字  立刻阻擋！</h2>")
            i = 1
        Else
            i = 0  '--通過檢查
        End If

        If (InStr(1, str_2, "--") <> 0) Or (InStr(1, str_2, "1=1") <> 0) Then
            Response.Write("<h2>「帳號」的欄位發現可疑字  立刻阻擋！</h2>")
            i = 1
        Else
            i = 0  '--通過檢查
        End If

        '*** 其他相關的攻擊字串，可以仿造上面寫法，繼續增加。 ***

        Return i   '--返回值。檢查結果（1表示失敗，沒通過。）
    End Function


    '********************************************************************
    '***  MD5加密
    Function getMd5Hash(ByVal uInput As String) As String
        ' Create a new instance of the MD5 object.
        ' MD5必須搭配 System.Security.Cryptography命名空間
        Dim md5Hasher As MD5 = MD5.Create()

        ' Convert the input string to a byte array and compute the hash.
        Dim MD5data() As Byte = md5Hasher.ComputeHash(Encoding.Default.GetBytes(uInput))

        ' Create a new Stringbuilder to collect the bytes
        ' and create a string.
        ' StringBuilder必須搭配 System.Text命名空間
        Dim sBuilder As New StringBuilder()

        ' Loop through each byte of the hashed data 
        ' and format each one as a hexadecimal string.
        For i As Integer = 0 To MD5data.Length - 1
            sBuilder.Append(MD5data(i).ToString("x2"))  '--變成十六進位
        Next i

        ' Return the hexadecimal string.
        Return sBuilder.ToString()

    End Function
End Class
