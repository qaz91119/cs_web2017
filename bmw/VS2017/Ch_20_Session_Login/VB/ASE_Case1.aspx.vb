﻿'==== 自己宣告 ==================
Imports System.IO
Imports System.Security.Cryptography
'資料來源： http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.aes.aspx


Partial Class ASE_Case1
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim original As String = TextBox1.Text

            ' Create a new instance of the Aes class. This generates a new key and initialization vector (IV).
            Using myAes As Aes = Aes.Create()
                ' Encrypt the string to an array of bytes().
                Dim encrypted As Byte() = EncryptStringToBytes_Aes(original, myAes.Key, myAes.IV)

                ' Decrypt the bytes() to a string.
                Dim roundtrip As String = DecryptStringFromBytes_Aes(encrypted, myAes.Key, myAes.IV)

                'Display the original data and the decrypted data.
                Label1.Text = String.Format("您輸入的字串（準備加密）：   {0} <hr />", original)
                Label1.Text &= String.Format("解密之後（還原）： {0}", roundtrip)
            End Using
        Catch ex As Exception
            Response.Write("Error:" & ex.Message)
        End Try
    End Sub


    '==== 加密 ================================
    Shared Function EncryptStringToBytes_Aes(ByVal plainText As String, ByVal Key() As Byte, ByVal IV() As Byte) As Byte()
        ' Check arguments.
        If plainText Is Nothing OrElse plainText.Length <= 0 Then
            Throw New ArgumentNullException("plainText--您沒有輸入任何字串")
        End If
        If Key Is Nothing OrElse Key.Length <= 0 Then
            Throw New ArgumentNullException("Key")
        End If
        If IV Is Nothing OrElse IV.Length <= 0 Then
            Throw New ArgumentNullException("IV Key")
        End If

        Dim encrypted() As Byte

        ' Create an Aes object with the specified key and IV.
        Using aesAlg As Aes = Aes.Create()
            aesAlg.Key = Key
            aesAlg.IV = IV

            ' Create a decrytor to perform the stream transform.
            Dim encryptor As ICryptoTransform = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV)
            ' Create the streams used for encryption.
            Using msEncrypt As New MemoryStream()
                Using csEncrypt As New CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write)
                    Using swEncrypt As New StreamWriter(csEncrypt)
                        'Write all data to the stream.
                        swEncrypt.Write(plainText)
                    End Using
                End Using
                encrypted = msEncrypt.ToArray()
            End Using
        End Using

        ' Return the encrypted bytes() from the memory stream.
        Return encrypted
    End Function 'EncryptStringToBytes_Aes


    '==== 解密 ================================
    Shared Function DecryptStringFromBytes_Aes(ByVal cipherText() As Byte, ByVal Key() As Byte, ByVal IV() As Byte) As String
        ' Check arguments.
        If cipherText Is Nothing OrElse cipherText.Length <= 0 Then
            Throw New ArgumentNullException("cipherText--沒有輸入「需要解密」的字串")
        End If
        If Key Is Nothing OrElse Key.Length <= 0 Then
            Throw New ArgumentNullException("Key")
        End If
        If IV Is Nothing OrElse IV.Length <= 0 Then
            Throw New ArgumentNullException("IV Key")
        End If
        ' Declare the string used to hold the decrypted text.
        Dim plaintext As String = Nothing

        ' Create an Aes object with the specified key and IV.
        Using aesAlg As Aes = Aes.Create()
            aesAlg.Key = Key
            aesAlg.IV = IV

            ' Create a decrytor to perform the stream transform.
            Dim decryptor As ICryptoTransform = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV)

            ' Create the streams used for decryption.
            Using msDecrypt As New MemoryStream(cipherText)
                Using csDecrypt As New CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read)
                    Using srDecrypt As New StreamReader(csDecrypt)
                        ' Read the decrypted bytes() from the decrypting stream
                        ' and place them in a string.
                        plaintext = srDecrypt.ReadToEnd()
                    End Using
                End Using
            End Using
        End Using

        Return plaintext

    End Function 'DecryptStringFromBytes_Aes

End Class
