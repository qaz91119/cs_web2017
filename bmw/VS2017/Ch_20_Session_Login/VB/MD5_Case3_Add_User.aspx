﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MD5_Case3_Add_User.aspx.vb" Inherits="_Book_App_Session_MD5_Case3_Add_User" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
            font-weight: bold;
        }
        .style2
        {
            font-weight: bold;
            background-color: #66FFFF;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        利用 MD5 雜湊演算法，將使用者的「密碼」進行加密。</div>
    <p>
        <span class="style2">請輸入會員資料（範例：db_user資料表）</span></p>
    <p>
        真實姓名：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    </p>
    <p>
        <span class="style1">*</span>帳&nbsp; 號：<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            ControlToValidate="TextBox2" ErrorMessage="帳號，必填欄位！"></asp:RequiredFieldValidator>
    </p>
    <p>
        <span class="style1">*</span>密&nbsp; 碼：<asp:TextBox ID="TextBox3" 
            runat="server" TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ControlToValidate="TextBox3" ErrorMessage="密碼，必填欄位！"></asp:RequiredFieldValidator>
    </p>
    <p>
        性&nbsp; 別：
        <asp:RadioButtonList ID="RadioButtonList1" runat="server">
            <asp:ListItem Selected="True">男</asp:ListItem>
            <asp:ListItem>女</asp:ListItem>
        </asp:RadioButtonList>
    </p>
    <p>
        E-Mail：<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     
        <asp:Button ID="Button1" runat="server" Text="Submit" />
    </p>
    </form>
    </body>
</html>
