<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Session_Login.aspx.vb" Inherits="_Book_App_Session_Session_Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Session最常用在會員登入的身分檢查上面</title>
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            background-color: #FFFF00;
        }
        .style2
        {
            color: #FF0000;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <p>
        <span class="style1">Session </span>&nbsp;<span class="style2">最常用在會員登入的身分檢查上面</span></p>
    <p>
        本範例的 帳號 與 密碼 都是 123</p>
    <form id="form1" runat="server">
    <p>
        帳號：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    </p>
    <p>
        密碼：<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
&nbsp;&nbsp;
        <asp:Button ID="Button1" runat="server" Text="Login..." />
    </p>
    </form>
 
</body>
</html>
