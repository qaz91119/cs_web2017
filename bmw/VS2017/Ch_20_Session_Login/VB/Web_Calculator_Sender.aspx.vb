﻿
Partial Class VS2010_Book_Sample_Ch16_Program__Book_App_Session_Web_Calculator_Sender
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            '--第一次執行程式
            ViewState("cal_input") = ""      '-- 使用者輸入的數字
            ViewState("cal_singal") = "0"   '-- 有無按下運算符號（+ / - / * / %）
            ViewState("cal_sum") = 0       '-- 運算後的結果
        End If

        '-- 寫程式的時候，我會即時把[變數值]列印到銀幕上，觀察程式有無寫錯。
        'Response.Write(ViewState("cal_input").ToString() & "<br>")
        'Response.Write(ViewState("cal_singal").ToString() & "<br>")
        'Response.Write(ViewState("cal_sum").ToString() & "<br>")
    End Sub


    Protected Sub ButtonCE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCE.Click
        ViewState("cal_input") = ""
        ViewState("cal_singal") = "0"
        ViewState("cal_sum") = 0
        '-- 按下 [CE清除]按鍵，數字歸零

        TextBox1.Text = "0"    '-- 畫面歸零"
        Label1.Text = "歸零"
    End Sub


    Protected Sub ButtonEnd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonEnd.Click
        '-- 運算完畢，輸入「=」計算出結果。
        Label1.Text = "運算完畢 !!"

        If ViewState("cal_singal").ToString() <> "+" Then
            ViewState("cal_sum") = Convert.ToInt32(ViewState("cal_sum")) + Convert.ToInt32(ViewState("cal_input"))
            TextBox1.Text = ViewState("cal_sum").ToString()
        End If

        '--運算完畢，按下「=」符號後，如果要繼續運算的話....
        ViewState("cal_input") = Convert.ToInt32(ViewState("cal_sum"))
        ViewState("cal_singal") = "0"
    End Sub


    Protected Sub Button_x1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button_x1.Click
        '--加法--
        Label1.Text = "加法（＋）運算"
        TextBox1.Text = ViewState("cal_input").ToString()

        If ViewState("cal_singal").ToString() = "0" Then
            '-- 第一次按下「+ 加法」按鈕
            ViewState("cal_sum") = Convert.ToInt32(ViewState("cal_input"))
        Else
            ViewState("cal_sum") = Convert.ToInt32(ViewState("cal_sum")) + Convert.ToInt32(ViewState("cal_input"))
        End If

        TextBox1.Text = ViewState("cal_sum").ToString()  '--畫面更新

        ViewState("cal_singal") = "+"  '-- 「非」第一次按下「+ 加法」按鈕
        ViewState("cal_input") = ""      '--數字歸零，等待下一次輸入
    End Sub




    '== 數 字 ============================================================
    '== 以下程式的重複性太高，可以濃縮在一起。請看下一個範例 Web_Calculator_sender.aspx。

    Protected Sub Button_Click(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles Button0.Click, Button1.Click, Button2.Click, Button3.Click, Button4.Click, Button5.Click, Button6.Click, Button7.Click, Button8.Click, Button9.Click
        '*** 上面這一列（Handles）是VB語法的重點，代表所有的數字按鈕，都會觸發同一個 Button_click事件！！

        Dim btn As Button = CType(sender, Button)

        ViewState("cal_input") = ViewState("cal_input").ToString() & btn.CommandArgument
        TextBox1.Text = ViewState("cal_input").ToString()
    End Sub



End Class
