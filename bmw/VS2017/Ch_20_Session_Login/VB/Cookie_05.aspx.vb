﻿Imports System.Text  '-- StringBuilder需要用到


Partial Class VS2010_Book_Sample_Ch16_Program__Book_App_Session_Cookie_05
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim output As StringBuilder = New StringBuilder()
        Dim aCookie As HttpCookie

        For i As Integer = 0 To (Request.Cookies.Count - 1)
            aCookie = Request.Cookies(i)
            output.Append("Cookie name is <font color=red>" & Server.HtmlEncode(aCookie.Name) & "</font><br />")
            output.Append("Cookie value is <font color=red>" & Server.HtmlEncode(aCookie.Value) & "</font><hr />")
        Next

        Label1.Text = output.ToString()
    End Sub
End Class
