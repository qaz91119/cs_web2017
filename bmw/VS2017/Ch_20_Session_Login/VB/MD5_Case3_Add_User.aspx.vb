﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient

Imports System.Security.Cryptography   '-- MD5專用
Imports System.Text                            '-- StringBuilder專用
'----自己寫的----

Partial Class _Book_App_Session_MD5_Case3_Add_User
    Inherits System.Web.UI.Page


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Check_SQLInjection(TextBox1.Text, TextBox2.Text, TextBox3.Text, TextBox4.Text) <> 0 Then
            '-- 自訂函數。為了避免SQL Injection攻擊，發現可疑字將會立刻阻擋！
            Response.End()    '-- 程式終止。或是寫成 Exit Sub
        End If

        '======================================================
        '==  以參數的方式，進行資料存取，比較不會受到 SQL Injection攻擊。
        '==  自己寫程式透過 SqlDataSource來做會員資料「新增」的功能。
        '======================================================
        Dim SqlDataSource2 As SqlDataSource = New SqlDataSource

        '== 連結資料庫的連接字串 ConnectionString  ==
        SqlDataSource2.ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString

        '*******************************************************************************
        '== 撰寫SQL指令(Insert Into) == 
        '== (使用參數來做，避免 SQL Injection攻擊)
        SqlDataSource2.InsertParameters.Add("myRealName", TextBox1.Text)
        SqlDataSource2.InsertParameters.Add("myName", TextBox2.Text)

        '--會員的「密碼」，會透過 MD5做加密的動作。
        '-- MD5的 function寫在下面。
        Dim hash As String = getMd5Hash(TextBox3.Text)
        SqlDataSource2.InsertParameters.Add("myPasswd", hash)

        SqlDataSource2.InsertParameters.Add("mySex", RadioButtonList1.SelectedValue.ToString())
        SqlDataSource2.InsertParameters.Add("myEmail", TextBox4.Text)

        SqlDataSource2.InsertCommand = "Insert into db_user(real_name, name, password, sex, email, rank) values(@myRealName, @myName, @myPasswd, @mySex, @myEmail, '1')"
        '*******************************************************************************

        '== 執行SQL指令 / 新增 .Insert() ==
        Dim aff_row As Integer = SqlDataSource2.Insert()

        If aff_row = 0 Then
            Response.Write("資料新增失敗！")
        Else
            Response.Write("資料新增成功！")
        End If
    End Sub


    '******************************************************************
    '***  避免SQL Injection攻擊
    Function Check_SQLInjection(ByVal str_1 As String, ByVal str_2 As String, ByVal str_3 As String, ByVal str_4 As String) As Integer
        '-- 自訂函數。為了避免SQL Injection攻擊，發現可疑字將會立刻阻擋！
        Dim i As Integer = 0
        If (InStr(1, str_1, "--") <> 0) Or (InStr(1, str_1, "1=1") <> 0) Then
            Response.Write("<h2>「真實姓名」的欄位發現可疑字  立刻阻擋！</h2>")
            i = 1
        Else
            i = 0  '--通過檢查
        End If

        If (InStr(1, str_2, "--") <> 0) Or (InStr(1, str_2, "1=1") <> 0) Then
            Response.Write("<h2>「帳號」的欄位發現可疑字  立刻阻擋！</h2>")
            i = 1
        Else
            i = 0  '--通過檢查
        End If

        If (InStr(1, str_3, "--") <> 0) Or (InStr(1, str_3, "1=1") <> 0) Then
            Response.Write("<h2>「密碼」的欄位發現可疑字  立刻阻擋！</h2>")
            i = 1
        Else
            i = 0  '--通過檢查
        End If

        If (InStr(1, str_4, "--") <> 0) Or (InStr(1, str_4, "1=1") <> 0) Then
            Response.Write("<h2>「E-Mail」的欄位發現可疑字  立刻阻擋！</h2>")
            i = 1
        Else
            i = 0  '--通過檢查
        End If
        '*** 其他相關的攻擊字串，可以仿造上面寫法，繼續增加。 ***

        Return i   '--返回值。檢查結果（1表示失敗，沒通過。）
    End Function


    Function getMd5Hash(ByVal uInput As String) As String
        ' Create a new instance of the MD5 object.
        ' MD5必須搭配 System.Security.Cryptography命名空間
        Dim md5Hasher As MD5 = MD5.Create()

        ' Convert the input string to a byte array and compute the hash.
        Dim MD5data() As Byte = md5Hasher.ComputeHash(Encoding.Default.GetBytes(uInput))

        ' Create a new Stringbuilder to collect the bytes
        ' and create a string.
        ' StringBuilder必須搭配 System.Text命名空間
        Dim sBuilder As New StringBuilder()

        ' Loop through each byte of the hashed data 
        ' and format each one as a hexadecimal string.
        For i As Integer = 0 To MD5data.Length - 1
            sBuilder.Append(MD5data(i).ToString("x2"))   '--變成十六進位
        Next i

        ' Return the hexadecimal string.
        Return sBuilder.ToString()

    End Function
End Class
