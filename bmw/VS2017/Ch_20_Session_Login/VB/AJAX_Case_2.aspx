<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AJAX_Case_2.aspx.vb" Inherits="_Book_App_Session_AJAX_Case_2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>聊天室[AJAX版]，應用Application與Session的最好範例。</title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
            font-weight: bold;
            background-color: #FFFF66;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <p>&lt;&lt;聊 天 室 <span class="style1">AJAX版本</span>&gt;&gt;<hr />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            
            <% =Now()%>  /  每0.5秒透過AJAX更新一次畫面<hr />
            
            <asp:Label ID="Label1" runat="server"></asp:Label>
            <asp:Timer ID="Timer1" runat="server" Interval="500">
            </asp:Timer>
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
    <hr />
    
        請輸入聊天內容：<asp:TextBox ID="TextBox1" runat="server" Width="348px"></asp:TextBox>
        &nbsp;<asp:Button ID="Button1" runat="server" Text="Submit/送出" />
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
        ControlToValidate="TextBox1" ErrorMessage="不可以留空白！" style="text-align: center"></asp:RequiredFieldValidator>
    <br />
    <br />
    <asp:Button ID="Button2" runat="server" Text="不玩了，掰掰∼∼Exit" />
    </p>
    </form>
 
</body>
</html>

