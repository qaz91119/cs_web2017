﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RSA_Case1.aspx.vb" Inherits="_Book_App_Session_RSA_Case1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
            font-weight: bold;
        }
        .style2
        {
            color: #009900;
            font-weight: bold;
        }
        .style3
        {
            color: #FFFF00;
            font-weight: bold;
            background-color: #000099;
        }
    </style>
</head>
<body>
    <p>
        RSA 加/解密 --
        <a href="http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.rsacryptoserviceprovider.aspx">
        http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.rsacryptoserviceprovider.aspx</a>
    </p>
<p class="style1">
        命名空間： System.Security.Cryptography</p>
    <form id="form1" runat="server">
    <div>
    
        請輸入一段字，讓<span class="style3"> RSA演算法</span>為您加密：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
&nbsp;&nbsp;
        <asp:Button ID="Button1" runat="server" Text="Submit" />
        <hr />
        <br />
       
        <br />
        RSA <span class="style1">加密</span>後的成果（256）：<asp:Label ID="Label1" runat="server" 
            style="font-weight: 700; color: #FF0066" ></asp:Label>

        
    
        <br />
        <br />
        RSA <span class="style2">解密</span>後的成果&nbsp; ： 
        <asp:Label ID="Label2" runat="server" 
            style="font-weight: 700; color: #009900" ></asp:Label>

        
    
    </div>
    </form>
</body>
</html>
