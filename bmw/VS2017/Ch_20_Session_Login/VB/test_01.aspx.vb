﻿
Partial Class VS2010_Book_Sample_Ch16_Program_test_01
    Inherits System.Web.UI.Page

    Dim i As Integer

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            i = 0
            Response.Write("<font color=blue>i = " & i & "</font><br />")
        End If
    End Sub


    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        Label1.Text = CInt(Label1.Text) + 1

        Response.Write("i = " & (i + 1) & "<br />")
    End Sub


End Class
