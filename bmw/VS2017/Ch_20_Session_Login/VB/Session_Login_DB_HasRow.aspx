﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Session_Login_DB_HasRow.aspx.vb" Inherits="VS2008_Book_Sample__Book_App_Session_Session_Login_DB_HasRow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Session最常用在會員登入的身分檢查上面（搭配DB）#2</title>
    <style type="text/css">
        .style2
        {
            color: #FFFF00;
            font-weight: bold;
            background-color: #000099;
        }
    </style>
</head>
<body>
    <p>
        <b>Session -- 最常用在會員登入的身分檢查上面 #2</b></p>
    <p>
        <span class="style2">（搭配DB），使用 dr.HasRow()的寫法</span></p>
    <p>
        &nbsp;</p>
    <p>
        本範例的 帳號 與 密碼 都是 123</p>
    <form id="form1" runat="server">
    <p>
        帳號：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    </p>
    <p>
        密碼：<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
&nbsp;&nbsp;
        <asp:Button ID="Button1" runat="server" Text="Login..." />
    </p>
    </form>
</body>
</html>
