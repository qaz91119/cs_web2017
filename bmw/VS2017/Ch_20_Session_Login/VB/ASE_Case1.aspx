﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ASE_Case1.aspx.vb" Inherits="ASE_Case1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .style1
        {
            font-size: small;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <p>
            <span class="style1">資料來源：<a href="http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.aes.aspx">http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.aes.aspx</a> </span>
            <br class="style1" />
        </p>
        <p class="style1">
            命名空間： System.Security.Cryptography</p>
        <div>
            請輸入一段字，讓 <strong>AES演算法</strong>為您加密：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            &nbsp;&nbsp;
            <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Submit" />
            <hr />
            <br />
            <br />
            AES加密後的成果：<asp:Label ID="Label1" runat="server" style="font-weight: 700; color: #009933; background-color: #CCFFCC;"></asp:Label>
        </div>
        <br />
        <br />
        <br />
       

    
    </div>
    </form>
</body>
</html>
