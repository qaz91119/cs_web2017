﻿
Partial Class _Book_App_Session_ViewState_1
    Inherits System.Web.UI.Page

    '--參考資料：http://msdn.microsoft.com/zh-tw/library/ms227551(VS.80).aspx
    Dim PageArrayList As ArrayList   '—註解：公用變數


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '—註解：您可以將資訊儲存在ViewState中，
        '   在下次將網頁要求（Request）重新傳送至伺服器時，可以在Page_Load()事件期間存取ViewState。

        If (ViewState("my_ViewState") Is Nothing) Then
            Button1.Visible = True


            PageArrayList = New ArrayList(4)
            '—註解：如果 ViewState裡面空無一物，我們就先設定一個。

            PageArrayList.Add("item 1（一號）")
            PageArrayList.Add("item 2（二號）")
            PageArrayList.Add("item 3（三號）")
            PageArrayList.Add("item 4（四號）")

            ViewState.Add("my_ViewState", PageArrayList)    '把資料寫進 ViewState
            '-- 也可以寫成這樣  ViewState("my_ViewState") = PageArrayList;
        Else
            '—註解：如果 ViewState有東西在裡面，那麼就透過 ListBox呈現出來
            PageArrayList = CType(ViewState("my_ViewState"), ArrayList)

            ListBox1.DataSource = PageArrayList
            ListBox1.DataBind()

            Button1.Visible = False
        End If
    End Sub


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Response.Write("您已經按下按鈕！！")
        '--按下按鈕，將會導致 PostBack的動作，也就是「將網頁要求傳送至伺服器」
    End Sub
End Class
