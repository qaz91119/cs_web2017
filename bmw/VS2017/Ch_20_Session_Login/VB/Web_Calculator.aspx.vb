﻿
Partial Class _Book_App_Session_Web_Calculator
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            '--第一次執行程式
            ViewState("cal_input") = ""      '-- 使用者輸入的數字
            ViewState("cal_singal") = "0"   '-- 有無按下運算符號（+ / - / * / %）
            ViewState("cal_sum") = 0       '-- 運算後的結果
        End If

        '-- 寫程式的時候，我會即時把[變數值]列印到銀幕上，觀察程式有無寫錯。
        'Response.Write(ViewState("cal_input").ToString() & "<br>")
        'Response.Write(ViewState("cal_singal").ToString() & "<br>")
        'Response.Write(ViewState("cal_sum").ToString() & "<br>")
    End Sub


    Protected Sub ButtonCE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCE.Click
        ViewState("cal_input") = ""
        ViewState("cal_singal") = "0"
        ViewState("cal_sum") = 0
        '-- 按下 [CE清除]按鍵，數字歸零

        TextBox1.Text = "0"    '-- 畫面歸零"
        Label1.Text = "歸零"
    End Sub


    Protected Sub ButtonEnd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonEnd.Click
        '-- 運算完畢，輸入「=」計算出結果。
        Label1.Text = "運算完畢 !!"

        If ViewState("cal_singal").ToString() <> "+" Then
            ViewState("cal_sum") = Convert.ToInt32(ViewState("cal_sum")) + Convert.ToInt32(ViewState("cal_input"))
            TextBox1.Text = ViewState("cal_sum").ToString()
        End If

        '--運算完畢，按下「=」符號後，如果要繼續運算的話....
        ViewState("cal_input") = Convert.ToInt32(ViewState("cal_sum"))
        ViewState("cal_singal") = "0"
    End Sub


    Protected Sub Button_x1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button_x1.Click
        '--加法--
        Label1.Text = "加法（＋）運算"
        TextBox1.Text = ViewState("cal_input").ToString()

        If ViewState("cal_singal").ToString() = "0" Then
            '-- 第一次按下「+ 加法」按鈕
            ViewState("cal_sum") = Convert.ToInt32(ViewState("cal_input"))
        Else
            ViewState("cal_sum") = Convert.ToInt32(ViewState("cal_sum")) + Convert.ToInt32(ViewState("cal_input"))
        End If

        TextBox1.Text = ViewState("cal_sum").ToString()  '--畫面更新

        ViewState("cal_singal") = "+"  '-- 「非」第一次按下「+ 加法」按鈕
        ViewState("cal_input") = ""      '--數字歸零，等待下一次輸入
    End Sub




    '== 數 字 ============================================================
    '== 以下程式的重複性太高，可以濃縮在一起。請看下一個範例 Web_Calculator_sender.aspx。

    Protected Sub Button0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button0.Click
        ViewState("cal_input") = ViewState("cal_input").ToString() & "0"
        TextBox1.Text = ViewState("cal_input").ToString()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ViewState("cal_input") = ViewState("cal_input").ToString() & "1"
        TextBox1.Text = ViewState("cal_input").ToString()
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        ViewState("cal_input") = ViewState("cal_input").ToString() & "2"
        TextBox1.Text = ViewState("cal_input").ToString()
    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        ViewState("cal_input") = ViewState("cal_input").ToString() & "3"
        TextBox1.Text = ViewState("cal_input").ToString()
    End Sub

    Protected Sub Button4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button4.Click
        ViewState("cal_input") = ViewState("cal_input").ToString() & "4"
        TextBox1.Text = ViewState("cal_input").ToString()
    End Sub

    Protected Sub Button5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button5.Click
        ViewState("cal_input") = ViewState("cal_input").ToString() & "5"
        TextBox1.Text = ViewState("cal_input").ToString()
    End Sub

    Protected Sub Button6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button6.Click
        ViewState("cal_input") = ViewState("cal_input").ToString() & "6"
        TextBox1.Text = ViewState("cal_input").ToString()
    End Sub

    Protected Sub Button7_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button7.Click
        ViewState("cal_input") = ViewState("cal_input").ToString() & "7"
        TextBox1.Text = ViewState("cal_input").ToString()
    End Sub

    Protected Sub Button8_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button8.Click
        ViewState("cal_input") = ViewState("cal_input").ToString() & "8"
        TextBox1.Text = ViewState("cal_input").ToString()
    End Sub

    Protected Sub Button9_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button9.Click
        ViewState("cal_input") = ViewState("cal_input").ToString() & "9"
        TextBox1.Text = ViewState("cal_input").ToString()
    End Sub

End Class
