﻿
Partial Class Book_Sample_Ch16_Program_DropDownList_DropDownList_02
    Inherits System.Web.UI.Page

    Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownList1.SelectedIndexChanged
        Session("SelectedIndex") = DropDownList1.SelectedIndex
        ' 或是寫成  ViewState("SelectedIndex") = DropDownList1.SelectedIndex

        Response.Write("您剛剛挑選了--" & Session("SelectedIndex").ToString() & "（從零算起）<br>")
    End Sub

    Protected Sub SqlDataSource2_Updated(sender As Object, e As SqlDataSourceStatusEventArgs) Handles SqlDataSource2.Updated
        DropDownList1.DataSourceID = "SqlDataSource1"

        Dim i As Integer = Convert.ToInt32(Session("SelectedIndex"))

        'DropDownList1.Items[0].Selected = true  '*** 發生錯誤！！
        DropDownList1.SelectedIndex = i   '***正確***
    End Sub
End Class
