<%
‘--註解：第一種寫法。 針對第一個 Cookie，會直接設定 Cookies 集合的值。
Response.Cookies("userName").Value = "MIS2000 Lab."
Response.Cookies("userName").Expires = DateTime.Now.AddDays(1)

‘--註解：第二種寫法。
‘--建立 HttpCookie型別的物件執行個體、設定其屬性，然後經由 Add方法將其加入 Cookies集合。
Dim aCookie As New HttpCookie("lastVisit")
aCookie.Value = DateTime.Now.ToString()
aCookie.Expires = DateTime.Now.AddDays(1)
Response.Cookies.Add(aCookie)

Response.Write("完成！")
%>