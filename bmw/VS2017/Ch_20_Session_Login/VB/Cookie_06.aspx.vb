﻿Imports System.Text  '-- StringBuilder需要用到


Partial Class VS2010_Book_Sample_Ch16_Program__Book_App_Session_Cookie_06
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        '本範例將會讀取 Cookie_02.aspx的成果，請您務必「先執行」Cookie_02.aspx一次！
        '========================================================
        Dim output As StringBuilder = New StringBuilder()
        Dim aCookie As HttpCookie
        Dim subkeyName, subkeyValue As String

        For i As Integer = 0 To (Request.Cookies.Count - 1)
            aCookie = Request.Cookies(i)
            output.Append("Cookie’s Name is " & aCookie.Name & "<br />")

            If (aCookie.HasKeys) Then

                For j As Integer = 0 To (aCookie.Values.Count - 1)
                    subkeyName = Server.HtmlEncode(aCookie.Values.AllKeys(j))
                    subkeyValue = Server.HtmlEncode(aCookie.Values(j))
                    output.Append("Subkey name is " & subkeyName & "<br />")
                    output.Append("Subkey value is " & subkeyValue & "<hr />")
                Next

            Else
                output.Append("Value is " & Server.HtmlEncode(aCookie.Value) & "<hr />")
            End If
        Next

        Label1.Text = output.ToString()
    End Sub
End Class
