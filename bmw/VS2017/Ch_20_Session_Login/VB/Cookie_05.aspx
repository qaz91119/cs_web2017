﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Cookie_05.aspx.vb" Inherits="VS2010_Book_Sample_Ch16_Program__Book_App_Session_Cookie_05" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title> 未命名頁面 </title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
   
        如果 Cookie裡面有「子索引鍵」，就會將子索引鍵顯示為
        <b> 單一 <span class="style1"> 名稱/值 </span> 字串 </b> 。  
        <br />
        （建議您事先執行過前面的範例，如 Cookie_02.aspx）<br />
        <br />
        <br />
   
    </div>
    <asp:Label ID="Label1" runat="server"></asp:Label>

    </form>


 
</body>
</html>

