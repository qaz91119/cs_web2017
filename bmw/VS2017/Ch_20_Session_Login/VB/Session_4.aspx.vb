﻿
Partial Class _Book_App_Session_Session_4
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("u_name") = Nothing Then
            Panel1.Visible = True
            Panel2.Visible = False
            '--註解：如果SESSION裡面有值，就把資料呈現出來。

            Label1.Text = Session("u_name")
            Label2.Text = Session("u_corp")

            Session.Abandon()
        Else
            Panel1.Visible = False
            Panel2.Visible = True
            '--註解：如果SESSION裡面沒有值，就進入資料輸入畫面。
        End If
    End Sub


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Session("u_name") = TextBox1.Text
        Session("u_corp") = TextBox2.Text

        Response.Redirect("Session_4.aspx")
    End Sub
End Class
