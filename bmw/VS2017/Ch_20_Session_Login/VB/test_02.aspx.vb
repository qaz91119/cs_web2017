﻿
Partial Class VS2010_Book_Sample_Ch16_Program_test_02
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            '-- 兩種作法任選其一
            'Session("i") = 0
            'Response.Write("<font color=blue>Session(""i"") = " & Session("i") & "</font><br />")

            ViewState("i") = 0
            Response.Write("<font color=blue>Session(""i"") = " & ViewState("i") & "</font><br />")
        End If
    End Sub


    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        Label1.Text = CInt(Label1.Text) + 1

        'Session("i") = CInt(Session("i")) + 1
        'Response.Write("Session(""i"") = " & Session("i").ToString() & "<br />")
        ViewState("i") = CInt(ViewState("i")) + 1
        Response.Write("ViewState(""i"") = " & ViewState("i").ToString() & "<br />")
    End Sub

End Class
