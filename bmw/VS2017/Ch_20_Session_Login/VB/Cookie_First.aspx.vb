﻿
Partial Class _Book_App_Session_Cookie_First
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            If Request.QueryString("AcceptsCookies") Is Nothing Then
                '—註解：判斷是否接受 Cookie 的一種方法，是嘗試寫入 Cookie然後再次嘗試讀取 Cookie。
                Response.Cookies("TestCookie").Value = "ok"

                Response.Cookies("TestCookie").Expires = DateTime.Now.AddMinutes(1)
                '—註解：讓 Cookie過期時間為一分鐘之後，就會失效。

                Response.Redirect("Cookie_Second.aspx?redirect=" & Server.UrlEncode(Request.Url.ToString))
                '—導向到第二個網頁(檔名Cookie_Second.aspx)。
            Else
                Label1.Text = "接受 Cookie，且Cookie的值為：" & Server.UrlEncode(Request.QueryString("AcceptsCookies"))
            End If

        End If

    End Sub
End Class
