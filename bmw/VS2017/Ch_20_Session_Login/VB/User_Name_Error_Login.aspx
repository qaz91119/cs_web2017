﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="User_Name_Error_Login.aspx.vb" Inherits="VS2010_Book_Sample__Book_App_Session_User_Name_Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>搭配使用者姓名，才可 編輯/更新？（有錯誤）</title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
        .style2
        {
            font-weight: bold;
            color: #FFFF66;
            background-color: #006600;
        }
    </style>
</head>
<body bgcolor="#ccff99">
    <form id="form1" runat="server">
    <div>
        以 Session來作「權限控管」<span class="style2">搭配使用者姓名，才可 編輯/更新？</span>（有錯誤）<br />
        <br />
        &nbsp;&nbsp;&nbsp; 當使用者登入之後，會有不同的權限。<br />
        <b>&nbsp;&nbsp;&nbsp; 權限夠大</b>或是<b>文章發表人</b>，才能修改資料。</div>
    <p>
        <span class="style1">*</span>Name :
        <asp:TextBox ID="TextBox1" runat="server" Style="margin-bottom: 0px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="必填！不可以留空白！"
            Style="font-weight: 700" ControlToValidate="TextBox1"></asp:RequiredFieldValidator>
    </p>
    <p>
        Passwd :
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        &nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button1_login" runat="server" Text="Login（登入 / 使用者姓名）（有錯誤）" />
    </p>
    <hr />
    <p align="center">
        <asp:Button ID="Button2_email_passwd" runat="server" Text="忘記密碼，請E-Mail給我～" />
    </p>
    </form>
</body>
</html>
