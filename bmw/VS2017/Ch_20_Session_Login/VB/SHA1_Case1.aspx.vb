﻿'==自己寫的==
Imports System.Security.Cryptography   '-- SHA1專用
Imports System.Text                            '-- StringBuilder專用



Partial Class _Book_App_Session_SHA1_Case1
    Inherits System.Web.UI.Page
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim source As String = TextBox1.Text

        Dim hash As String = getSHA1Hash(source)  '--加密的 function寫在下面。

        Label1.Text = "原本的字串「<font color=blue>" + source + "</font>」，經過加密後變成：&nbsp;&nbsp; " + hash
    End Sub


    '===================================================================
    ' 資料來源：http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.sha1.aspx 
    '
    '== 底下這部分的程式，就算讀者不懂也可以使用它。照著做就對了。
    '== 只要知道我們輸入一段字串，它就會用 SHA1進行加密後，傳回「加密後的字串」。

    Function getSHA1Hash(ByVal uInput As String) As String

        Dim SHA1Hasher As SHA1 = SHA1.Create()
        '-- SHA1必須搭配 System.Security.Cryptography命名空間

        Dim data() As Byte = SHA1Hasher.ComputeHash(Encoding.Default.GetBytes(uInput))
        '-- SHA1的 .ComputeHash(Byte[]) 方法，計算指定位元組陣列的雜湊值。

        Dim result() As Byte
        Dim sha1SP As New SHA1CryptoServiceProvider()
        '-- 使用密碼編譯服務提供者 (CSP) 所提供之實作，計算輸入資料的 SHA1 雜湊值。
        '-- http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.sha1cryptoserviceprovider.aspx
        result = sha1SP.ComputeHash(data)

        Dim sBuilder As New StringBuilder()
        '-- StringBuilder必須搭配 System.Text命名空間

        ' Loop through each byte of the hashed data 
        ' and format each one as a hexadecimal string.
        For i As Integer = 0 To data.Length - 1
            sBuilder.Append(data(i).ToString("x2"))  '--變成十六進位
        Next i

        Return sBuilder.ToString()

    End Function

End Class
