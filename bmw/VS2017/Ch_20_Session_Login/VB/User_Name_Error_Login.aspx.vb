﻿'----自己寫的（宣告）----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient

Imports System.Net
Imports System.Net.Mail    '--發信必備的NameSpace，用來取代 System.Web.Mail
'----自己寫的（宣告）----


Partial Class VS2010_Book_Sample__Book_App_Session_User_Name_Login
    Inherits System.Web.UI.Page

    Protected Sub Button1_login_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1_login.Click

        If Check_SQLInjection(TextBox1.Text, TextBox2.Text) <> 0 Then
            '-- 自訂函數。為了避免SQL Injection攻擊，發現可疑字將會立刻阻擋！
            Response.Write("<h2>發現可疑字  立刻阻擋！</h2>")
            Response.End()    '--程式終止。或是寫成 Exit Sub
        End If

        Dim Conn As SqlConnection = New SqlConnection
        '----上面已經事先寫好 Imports System.Web.Configuration ----
        Conn.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString
        '----(連結資料庫)----
        Dim dr As SqlDataReader = Nothing
        Dim cmd As SqlCommand = New SqlCommand("select * from db_user where name = '" & TextBox1.Text & "' and password = '" & TextBox2.Text & "'", Conn)

        Try
            Conn.Open()   '---- 這時候才連結DB
            dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料
            If dr.HasRows Then
                dr.Read()
                '**********************************************
                Session("u_name") = dr.Item("name").ToString()
                Session("u_realname") = dr.Item("real_name").ToString()  '--真實姓名。
                '**********************************************

                Session("Login") = "OK"   '--通過帳號與密碼的認證，就獲得 Session。

                cmd.Cancel()
                dr.Close()
                Conn.Close()
                Conn.Dispose()   '--先關閉資源後，下面再來轉換網頁
                Response.Redirect("User_Name_Error_List.aspx")  '--通過帳號與密碼的認證，就可以進入後端的管理區。
            Else
                Response.Write("<h2>帳號/密碼有誤！</h2>")
                cmd.Cancel()
                dr.Close()
                Conn.Close()
                Conn.Dispose()
                Exit Sub  '--程式終止
            End If

        Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<HR/>")
        End Try
    End Sub


    Protected Sub Button2_email_passwd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2_email_passwd.Click

        If Check_SQLInjection(TextBox1.Text, TextBox2.Text) <> 0 Then
            '-- 自訂函數。為了避免SQL Injection攻擊，發現可疑字將會立刻阻擋！
            Response.End()
        End If

        Dim Conn As SqlConnection = New SqlConnection
        '----上面已經事先寫好 Imports System.Web.Configuration ----
        Conn.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString
        '----(連結資料庫)----

        Dim dr As SqlDataReader = Nothing
        Dim cmd As SqlCommand = New SqlCommand("select [email], [password] from db_user where name = '" & TextBox1.Text & "'", Conn)

        Try
            Conn.Open()   '---- 這時候才連結DB
            dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料
            If dr.HasRows Then
                dr.Read()
                '************************************************************
                '--通過帳號的認證，就可以收到E-Mail。
                '--參考網址：http://msdn.microsoft.com/zh-tw/library/system.net.mail.mailmessage(VS.80).aspx

                Dim u_Mail As New MailMessage("admin@xxx.com.tw", dr.Item("email").ToString(), "信件標題", "您好，您的密碼是：" & dr("password").ToString())
                '--四個參數分別是：發信人。收信人。標題。信件內容。

                Dim mail_client As SmtpClient = New SmtpClient("127.0.0.1")
                mail_client.Credentials = CredentialCache.DefaultNetworkCredentials
                '--用於驗證 (Authentication) 的認證 (如果 SMTP 伺服器需要的話)。
                '--NameSpace為「System.Net」
                mail_client.Send(u_Mail)
                '************************************************************
            Else
                Response.Write("<h2>帳號有誤！無此人</h2>")
            End If
            cmd.Cancel()
            dr.Close()
            Conn.Close()
            Conn.Dispose()

        Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<HR/>")
        End Try
    End Sub


    '******************************************************************
    '***  避免SQL Injection攻擊
    Function Check_SQLInjection(ByVal str_1 As String, ByVal str_2 As String) As Integer
        '-- 自訂函數。為了避免SQL Injection攻擊，發現可疑字將會立刻阻擋！

        Dim i As Integer = 1

        '==============================================
        '== 方法一 ==
        'If (InStr(1, str_1, "--") <> 0) Or (InStr(1, str_1, "1=1") <> 0) Then
        '    Response.Write("<h2>發現可疑字  立刻阻擋！</h2>")
        '    i = 1
        'Else
        '  If (InStr(1, str_2, "--") <> 0) Or (InStr(1, str_2, "1=1") <> 0) Then
        '       Response.Write("<h2>發現可疑字  立刻阻擋！</h2>")
        '       i = 1
        '   Else
        '       i = 0  '--通過檢查
        '          'If ... then
        '          'Else
        '          'End If
        '   End If
        '    i = 0  '--通過檢查
        'End If

        'Return i   '--返回值。檢查結果（1表示失敗，沒通過。）


        '==============================================
        '== 方法二 ==
        'If (InStr(1, str_1, "--") <> 0) Or (InStr(1, str_1, "1=1") <> 0) Then
        '    'Response.Write("<h2>發現可疑字  立刻阻擋！</h2>")
        '    Return 1    '--返回值。檢查結果（1表示失敗，沒通過。）
        'End If

        'If (InStr(1, str_2, "--") <> 0) Or (InStr(1, str_2, "1=1") <> 0) Then
        '    'Response.Write("<h2>發現可疑字  立刻阻擋！</h2>")
        '    Return 1    '--返回值。檢查結果（1表示失敗，沒通過。）
        'End If

        'Return 0   '--返回值。通過。


        '==============================================
        '== 方法三 ==
        '== 請您參閱「重構（Refactoring）」一書的  Ch.9-5節
        If (InStr(1, str_1, "--") <> 0) Then Return 1 '--返回值。檢查結果（1表示失敗，沒通過。）
        If (InStr(1, str_1, "1=1") <> 0) Then Return 1 '--返回值。檢查結果（1表示失敗，沒通過。）

        If (InStr(1, str_2, "--") <> 0) Then Return 1 '--返回值。檢查結果（1表示失敗，沒通過。）
        If (InStr(1, str_2, "1=1") <> 0) Then Return 1 '--返回值。檢查結果（1表示失敗，沒通過。）

        Return 0   '--返回值。通過。

    End Function

End Class
