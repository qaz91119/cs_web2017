﻿Imports Microsoft.VisualBasic

Public Class Class1

    Public Shared Sub defense()
        HttpContext.Current.Response.Write("<br /><hr />")
        HttpContext.Current.Response.Write("<h3>此為網站管理區，外人莫入！</h3>")

        If Not HttpContext.Current.Session("Login") Is Nothing Then
            ' 或是寫成 If HttpContext.Current.Session("Login") <> Nothing Then
            HttpContext.Current.Response.Write("<h3><font color=red>嚴重警告！</font>您的帳號、密碼錯誤！是非法使用者～</h3>")
            HttpContext.Current.Response.End()     '--註解：程式立刻終止！        
        End If

        '============================================
        '== Session如果是 Nothing，一使用就會報錯。所以要用上面的判別式來預防。
        '============================================
        If HttpContext.Current.Session("Login") = "OK" Then
            HttpContext.Current.Response.Write("<h3>恭喜您，您成功登入，才會看見這一頁！</h3><hr />")
            HttpContext.Current.Response.Write("<br />您的個人資料是----<br />")
            HttpContext.Current.Response.Write("<br />    帳號=>  " + HttpContext.Current.Session("u_name"))
            'HttpContext.Current.Response.Write("<br />    密碼=>  " + HttpContext.Current.Session("u_passwd"))
        Else
            HttpContext.Current.Response.Write("<h3><font color=red>嚴重警告！</font>您的帳號、密碼錯誤！是非法使用者～</h3>")
            HttpContext.Current.Response.End()     '--註解：程式立刻終止！
        End If


        '==============================================================
        '==  上面的寫法跟下面完全相等  ==========================================
        '==============================================================

        'With HttpContext.Current
        '    .Response.Write("<br /><hr />")
        '    .Response.Write("<h3>此為網站管理區，外人莫入！</h3>")

        '    If Not .Session("Login") Is Nothing Then
        '        ' 或是寫成 If .Session("Login") <> Nothing Then
        '        .Response.Write("<h3><font color=red>嚴重警告！</font>您的帳號、密碼錯誤！是非法使用者～</h3>")
        '        .Response.End()     '--註解：程式立刻終止！        
        '    End If

        '    '============================================
        '    '== Session如果是 Nothing，一使用就會報錯。所以要用上面的判別式來預防。
        '    '============================================
        '    If .Session("Login") = "OK" Then
        '        .Response.Write("<h3>恭喜您，您成功登入，才會看見這一頁！</h3><hr />")
        '        .Response.Write("<br />您的個人資料是----<br />")
        '        .Response.Write("<br />    帳號=>  " + .Session("u_name"))
        '        '.Response.Write("<br />    密碼=>  " + .Session("u_passwd"))
        '    Else
        '        .Response.Write("<h3><font color=red>嚴重警告！</font>您的帳號、密碼錯誤！是非法使用者～</h3>")
        '        .Response.End()     '--註解：程式立刻終止！
        '    End If
        'End With

    End Sub

End Class
