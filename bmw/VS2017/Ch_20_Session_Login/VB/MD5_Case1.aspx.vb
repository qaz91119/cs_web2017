﻿'==自己寫的==
Imports System.Security.Cryptography   '-- MD5專用
Imports System.Text                            '-- StringBuilder專用



Partial Class _Book_App_Session_MD5_Case1
    Inherits System.Web.UI.Page


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim source As String = TextBox1.Text

        Dim hash As String = getMd5Hash(source)  '--加密的 function寫在下面。

        Label1.Text = "原本的字串「<font color=blue>" + source + "</font>」，經過加密後變成：&nbsp;&nbsp; " + hash
    End Sub


    '===================================================================
    ' Hash an input string and return the hash as a 32 character hexadecimal string.
    ' 資料來源：http://msdn.microsoft.com/zh-tw/library/system.security.cryptography.md5.aspx 
    '
    '== 底下這部分的程式，就算讀者不懂也可以使用它。照著做就對了。
    '== 只要知道我們輸入一段字串，它就會用MD5進行加密後，傳回「加密後的字串」。

    Function getMd5Hash(ByVal uInput As String) As String
        ' Create a new instance of the MD5 object.
        ' MD5必須搭配 System.Security.Cryptography命名空間
        Dim md5Hasher As MD5 = MD5.Create()

        ' Convert the input string to a byte array and compute the hash.
        Dim MD5data() As Byte = md5Hasher.ComputeHash(Encoding.Default.GetBytes(uInput))

        ' Create a new Stringbuilder to collect the bytes
        ' and create a string.
        ' StringBuilder必須搭配 System.Text命名空間
        Dim sBuilder As New StringBuilder()

        ' Loop through each byte of the hashed data 
        ' and format each one as a hexadecimal string.
        For i As Integer = 0 To MD5data.Length - 1
            sBuilder.Append(MD5data(i).ToString("x2"))  '--變成十六進位
        Next i

        ' Return the hexadecimal string.
        Return sBuilder.ToString()

    End Function

End Class
