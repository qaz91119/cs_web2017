﻿
Partial Class _Book_App_Session_Cookie_04
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '--註解：第一種寫法。
        If Not Request.Cookies("userName") Is Nothing Then
            Label1.Text = Server.HtmlEncode(Request.Cookies("userName").Value)
            '--註解：使用進行 HtmlEnCode編碼，比較安全。。
        End If

        '--註解：第二種寫法。
        If Not Request.Cookies("userName") Is Nothing Then
            Dim aCookie As HttpCookie = Request.Cookies("userName")
            Label2.Text = Server.HtmlEncode(aCookie.Value)
        End If

    End Sub
End Class
