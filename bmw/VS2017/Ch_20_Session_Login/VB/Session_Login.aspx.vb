﻿
Partial Class _Book_App_Session_Session_Login
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        If TextBox1.Text = "123" And TextBox2.Text = "123" Then
            Session("u_name") = "123"
            Session("u_passwd") = "123"

            Session("Login") = "OK"
            '-- 註解：只有通過帳號、密碼的檢查，才會得到這個 Session(“Login”) = “OK” 的鑰匙！
        End If

        Response.Redirect("Session_Login_end.aspx")
        '-- 註解：沒有獲得 Session(“OK”)，就算連到這網頁也沒用！
    End Sub

End Class
