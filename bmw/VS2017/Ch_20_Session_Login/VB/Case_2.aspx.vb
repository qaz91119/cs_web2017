﻿
Partial Class _Book_App_Session_Case_2
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("my_name") = "" Then
            '====防止有人沒註冊就闖入
            Response.Write("抱歉，請用正當程序，進行登入！......<a href=Case_1_Login.aspx>登入畫面</a>")
            Response.End()
        End If

        If Not Page.IsPostBack Then
            List_All()    '====第一次進到聊天室，會立刻出現目前所有人的談話內容

            '====歡迎新夥伴加入====
            Application.Lock()

            Dim my_Label As New StringBuilder
            For i = 15 To 2 Step -1
                Application("A" & i) = Application("A" & (i - 1))
                my_Label.Append(Application("A" & i).ToString)
            Next
            '==== 上面這段FOR迴圈，是聊天室程式裡面的重點！ ====
            '  每當有人發表一則留言，原本畫面上的留言，就會向上推擠。
            '  最新的留言，總會出現在畫面下方的最後一筆。
            '  為了良好的效率，這裡我們改用 StringBuilder來處理字串。

            Application("A1") = Session("my_name") & "  " & FormatDateTime(Now, DateFormat.LongTime) & "  說： <b>====Hello~Everybody~歡迎新夥伴加入！！====</b><br />"
            my_Label.Append(Application("A1"))
            Label1.Text = my_Label.ToString

            Application.UnLock()
        End If

    End Sub


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '====送出留言
        Application.Lock()

        Dim my_Label As New StringBuilder
        For i = 15 To 2 Step -1
            Application("A" & i) = Application("A" & (i - 1))
            my_Label.Append(Application("A" & i).ToString)
        Next

        Application("A1") = Session("my_name") & "  " & FormatDateTime(Now, DateFormat.LongTime) & "  說： <font color=" & Session("my_color") & ">" & TextBox1.Text & "</font><br />"

        my_Label.Append(Application("A1"))
        Label1.Text = my_Label.ToString

        Application.UnLock()
    End Sub


    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        '====離開聊天室====
        Application.Lock()

        Dim my_Label As New StringBuilder
        For i = 15 To 2 Step -1
            Application("A" & i) = Application("A" & (i - 1))
            my_Label.Append(Application("A" & i).ToString)
        Next
        Application("A1") = Session("my_name") & "  " & FormatDateTime(Now, DateFormat.LongTime) & "  說： <font color=" & Session("my_color") & ">掰掰～各位好朋友，再見了</font><br />"
        my_Label.Append(Application("A1"))
        Label1.Text = my_Label.ToString

        Application.UnLock()

        Response.Redirect("Case_1_Login.aspx")
    End Sub


    Sub List_All()    '====列出「目前」聊天室的所有談話內容====
        Dim chatroom As New StringBuilder
        For i = 15 To 1 Step -1
            chatroom.Append(Application("A" & i).ToString)
        Next

        Label1.Text = chatroom.ToString
    End Sub

End Class
