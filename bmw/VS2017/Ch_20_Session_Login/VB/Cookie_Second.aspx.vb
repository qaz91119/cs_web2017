﻿
Partial Class _Book_App_Session_Cookie_Second
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim redirect As String = Request.QueryString("redirect")
        Dim acceptsCookies As String

        If Request.Cookies("TestCookie") Is Nothing Then
            acceptsCookies = "no"
        Else
            acceptsCookies = "yes"
            '—註解：讓 Cookie過期失效。
            Response.Cookies("TestCookie").Expires = DateTime.Now.AddDays(-1)
        End If

        Response.Redirect(redirect & "?AcceptsCookies=" & acceptsCookies, True)
        '== 又回到第一個網頁 Cookie_First.aspx ==

    End Sub
End Class
