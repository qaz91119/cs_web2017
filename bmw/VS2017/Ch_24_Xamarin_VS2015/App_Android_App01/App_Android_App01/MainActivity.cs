﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace App_Android_App01
{
    [Activity(Label = "App_Android_App01", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.MyButton);
            button.Click += delegate { button.Text = string.Format("您按下 {0} 次!", count++); };

            Button button2 = FindViewById<Button>(Resource.Id.MyButton2);
            button2.Click += delegate { button2.Text = string.Format("今天是 {0}", DateTime.Now.ToLongDateString()); };
        }
    }
}

