﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListBox_Add.aspx.cs" Inherits="Ch03_WebControls_ListBox_Add" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>手動新增一個選項（只能單選）</title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
            font-weight: bold;
        }
        .style2
        {
            color: #FF0000;
        }
        .style3
        {
            background-color: #FFFF00;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <br />
        <br />

        <asp:ListBox ID="ListBox1" runat="server" Height="125px" Width="85px" OnSelectedIndexChanged="ListBox1_SelectedIndexChanged" ForeColor="#6699FF" SelectionMode="Multiple">
            <asp:ListItem>4</asp:ListItem>
            <asp:ListItem>5</asp:ListItem>
            <asp:ListItem>6</asp:ListItem>
             <asp:ListItem>7</asp:ListItem>
            <asp:ListItem>8</asp:ListItem>
            <asp:ListItem>9</asp:ListItem>
             <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
        </asp:ListBox>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

        <asp:ListBox ID="ListBox2" runat="server" Height="125px" Width="85px" ForeColor="#FF66FF" SelectionMode="Multiple">
            <asp:ListItem>1_1111</asp:ListItem>
            <asp:ListItem>1_2222</asp:ListItem>
            <asp:ListItem>1_3333</asp:ListItem>
            <asp:ListItem>1_4444</asp:ListItem>
            <asp:ListItem>2_1111</asp:ListItem>
            <asp:ListItem>2_2222</asp:ListItem>
            <asp:ListItem>2_3333</asp:ListItem>
            <asp:ListItem>2_4444</asp:ListItem>
            <asp:ListItem>3_1111</asp:ListItem>
            <asp:ListItem>3_2222</asp:ListItem>
            <asp:ListItem>3_3333</asp:ListItem>
            <asp:ListItem>3_4444</asp:ListItem>
        </asp:ListBox>

        <br />
        <asp:Button ID="Button1" runat="server" Text="右移" 
            onclick="Button1_Click" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button2" runat="server" Text="左移" 
            onclick="Button2_Click" />
        <br />
        <br />
        <asp:Label ID="Label1" runat="server"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label2" runat="server"></asp:Label>
        <br />
        </div>
    </form>
 
</body>
</html>