﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _01_Postback_01 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            TextBox1.Text = "李小明";
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        int i = 3;

        //Label1.Text = "您修改後的名字 -- " + TextBox1.Text;
        Label1.Text = i.ToString();
    }

    
}