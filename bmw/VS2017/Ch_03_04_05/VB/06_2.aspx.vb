﻿
Partial Class WebControls_6_2
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '-- 註解：輸入在TextBox控制項裡面的文字，就會新增為下拉式選單的子選項！
        DropDownList1.Items.Add(TextBox1.Text)
    End Sub


    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Label1.Text = DropDownList1.SelectedItem.Text
        Label2.Text = DropDownList1.SelectedItem.Value
    End Sub
End Class
