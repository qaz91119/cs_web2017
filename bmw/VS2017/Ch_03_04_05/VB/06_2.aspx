﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="06_2.aspx.vb" Inherits="WebControls_6_2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> Ch 3</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            文章分類：
            <asp:DropDownList ID="DropDownList1" runat="server">
	            <asp:ListItem Selected="True">政治</asp:ListItem>
	            <asp:ListItem>教育</asp:ListItem>
	            <asp:ListItem>娛樂</asp:ListItem>
        </asp:DropDownList>    
    &nbsp;<asp:Button ID="Button2" runat="server" Text="Button2_選好了～" />
            <br />
            <br />
    </div>
    &nbsp;&nbsp;&nbsp;
    您剛剛點選的選項 Text：&nbsp; <asp:Label ID="Label1" runat="server" ForeColor="#FF3300"></asp:Label>
    <br />&nbsp;&nbsp;&nbsp;
    您剛剛點選的選項 Value：<asp:Label ID="Label2" runat="server" ForeColor="#FF3300"></asp:Label>
    <br />
    <hr />
    <br />
    在此新增 Item文字，就會加入上方的DropDownList控制項裡面--<br />
    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
&nbsp;<asp:Button ID="Button1" runat="server" Text="Button1_增加一個選項" />
    </form>
</body>
</html>
