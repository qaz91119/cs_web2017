﻿
Partial Class _Book_WebControls_11_FindControl_2
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim Button1 As New Button
        PlaceHolder1.Controls.Add(Button1)
        Button1.Text = "程式裡頭，動態產生的按鈕"
        Button1.ID = "Button_DynaAdd"


        Dim TextBox1 As New TextBox
        PlaceHolder1.Controls.Add(TextBox1)
        TextBox1.Text = "程式裡頭，動態產生的文字輸入方塊"
        TextBox1.ID = "TextBox_DynaAdd"


        '--------------------------------------------------------------------------------------------------
        '== 透過 FindControl抓取 PlaceHolder裡面的「子控制項」
        Dim getTextBox As New TextBox
        getTextBox = CType(PlaceHolder1.FindControl("TextBox_DynaAdd"), TextBox)

        Label1.Text = getTextBox.Text


        '***********************************************
        '*** 當您修改「動態產生的Text」身體裡面的值，再按下一次按鈕。
        '*** ......還能抓到您修改後的文字（值）嗎？
        Label2.Text = Request(getTextBox.UniqueID)
    End Sub

End Class
