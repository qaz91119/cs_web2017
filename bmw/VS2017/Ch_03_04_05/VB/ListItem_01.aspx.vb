﻿
Partial Class _Book_New_Samples_ListItemCollection_ListItem_01
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        '== 網頁第一次執行
        If Not Page.IsPostBack Then

            Dim listBoxData As New ListItemCollection()    ' Create a new ListItemCollection.

            listBoxData.Add(New ListItem("apples"))    '== 寫法一。加入新的子選項（ListItem型態）。
            listBoxData.Add(New ListItem("bananas"))
            listBoxData.Add(New ListItem("cherries"))
            listBoxData.Add("grapes")    '== 寫法二。加入新的子選項的「文字」（Text屬性）。
            listBoxData.Add("mangos")
            listBoxData.Add("oranges")
            '== 以下是 DataBinding（資料繫結、數據綁定）
            ListBox1.DataSource = listBoxData
            ListBox1.DataBind()

            '=========================================
            Dim ddBoxData As New ListItemCollection()    ' Create a new ListItemCollection.

            '== 以下是 DataBinding（資料繫結、數據綁定）
            DropDownList1.DataSource = ddBoxData
            DropDownList1.DataBind()
        End If

        ' 以下只是「防呆」，沒選項了，怕人家按下去出錯。
        If ListBox1.Items.Count = 0 Then
            moveButton1.Enabled = False
        End If
        If DropDownList1.Items.Count = 0 Then
            moveButton2.Enabled = False
        End If
    End Sub


    Protected Sub moveButton1_Click(sender As Object, e As System.EventArgs) Handles moveButton1.Click
        DropDownList1.SelectedIndex = -1  '-- 沒有預設「被選取」的子選項。

        ' Add the selected item to DropDownList1.
        DropDownList1.Items.Add(ListBox1.SelectedItem)
        ' Delete the selected item from ListBox1.
        ListBox1.Items.Remove(ListBox1.SelectedItem)

        ' 以下只是「防呆」，沒選項了，怕人家按下去出錯。
        If ListBox1.Items.Count = 0 Then
            moveButton1.Enabled = False
        Else
            moveButton2.Enabled = True
        End If
    End Sub



    Protected Sub moveButton2_Click(sender As Object, e As System.EventArgs) Handles moveButton2.Click
        ListBox1.SelectedIndex = -1  '-- 沒有預設「被選取」的子選項。

        ' Add the selected item to ListBox1.
        ListBox1.Items.Add(DropDownList1.SelectedItem)
        ' Delete the selected item from DropDownList1.
        DropDownList1.Items.Remove(DropDownList1.SelectedItem)

        ' 以下只是「防呆」，沒選項了，怕人家按下去出錯。
        If DropDownList1.Items.Count = 0 Then
            moveButton2.Enabled = False
        Else
            moveButton1.Enabled = True
        End If
    End Sub

End Class
