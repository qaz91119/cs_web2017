﻿
Partial Class _Book_WebControls_TextBox_Calculator
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim sum As Integer = CInt(TextBox1.Text) + CInt(TextBox2.Text) + CInt(TextBox3.Text)

        Label1.Text = String.Format("{0:C}", sum)
        '--轉換成貨幣格式
        '-- 也可以寫成這樣 -- Label1.Text = String.Format("{0:NT$ #,### ; -NT$ #,###}", sum)

    End Sub
End Class
