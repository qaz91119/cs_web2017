﻿
Partial Class VS2010_Book_Sample_Ch03_Program__Book_WebControls_TextBox_DefaultValue_TextChanged
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        'TextBox1.Text = "@@@ Page_Init事件給予的預設值 @@@" & DateTime.Now.ToLongTimeString()
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            '-- 第一次執行網頁
            TextBox1.Text = "Page_Load事件，「第一次」給予的預設值" & DateTime.Now.ToLongTimeString()
        Else
            TextBox1.Text = "=== Page_Load事件，「第N次」給予的預設值 ===" & DateTime.Now.ToLongTimeString()
        End If
    End Sub


    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        '-- 按下按鈕，輸入一個 TextBox的預設值
        TextBox1.Text = "*** Button給予的預設值 ***" & DateTime.Now.ToLongTimeString()
    End Sub


    Protected Sub TextBox1_TextChanged(sender As Object, e As System.EventArgs) Handles TextBox1.TextChanged
        Label1.Text = "*** TextBox1_TextChanged觸發事件！！***" & DateTime.Now.ToLongTimeString()
    End Sub


End Class
