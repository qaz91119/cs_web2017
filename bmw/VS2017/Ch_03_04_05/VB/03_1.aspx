﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="03_1.aspx.vb" Inherits="WebControls_3_1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title> 按鈕 -- CommandName #2</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    <asp:Button ID="Button1" runat="server" 
        CommandArgument="哈囉！您剛剛按下的按鈕是[測試（test）]" Text="Button / 測試(test)" 
        CommandName="ABC" />
    </form>
 
</body>
</body>
</html>
