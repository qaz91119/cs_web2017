﻿
Partial Class Book_Sample_Ch03_Program__Book_WebControls_11_Dynamic_Generate_Controls_01
    Inherits System.Web.UI.Page

    Protected Sub RadioButtonList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadioButtonList1.SelectedIndexChanged
        If RadioButtonList1.Items(0).Selected Then
            RadioButtonList2.Visible = True
            CheckBoxList1.Visible = False
            TextBox1.Visible = False
        End If

        If RadioButtonList1.Items(1).Selected Then
            RadioButtonList2.Visible = False
            CheckBoxList1.Visible = True
            TextBox1.Visible = False
        End If

        If RadioButtonList1.Items(2).Selected Then
            RadioButtonList2.Visible = False
            CheckBoxList1.Visible = False
            TextBox1.Visible = True
        End If

        '也可以改成 swich... case的寫法，更簡潔。
    End Sub


    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Response.Write("<h2>" & RadioButtonList2.SelectedValue & "</h2>")  '--單選，被User點選的結果

        Dim str As String = Nothing
        For i As Integer = 0 To (CheckBoxList1.Items.Count - 1)
            If CheckBoxList1.Items(i).Selected Then
                str &= CheckBoxList1.Items(i).Value
            End If
        Next

        Response.Write("<h2>" & str & "</h2>")  '--複選，被User點選的結果

        Response.Write("<h2>" & TextBox1.Text & "</h2>")  '--文字輸入
    End Sub
End Class
