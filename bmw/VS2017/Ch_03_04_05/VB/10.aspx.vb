﻿
Partial Class WebControls_10
    Inherits System.Web.UI.Page

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles Calendar1.SelectionChanged

        TextBox1.Text = Calendar1.SelectedDate

        TextBox2.Text = Calendar1.SelectedDates.Count
        '-- 註解： Count可以用來計算被選定的日期區間一共有幾天？

        Label1.Text = Calendar1.TodaysDate.DayOfWeek
        '-- 註解： 今天，是本週的第幾天（從零算起）？
    End Sub

End Class
