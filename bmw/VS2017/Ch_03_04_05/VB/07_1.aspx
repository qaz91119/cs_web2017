﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="07_1.aspx.vb" Inherits="WebControls_7_1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> Ch 3</title>
    <style type="text/css">
        .style1
        {
            background-color: #FFFF00;
        }
        .style2
        {
            background-color: #00FF99;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        本範例並未搭配 SqlDataSource，完全是自己動手撰寫。<br />
        完整的程式，放在「後置程式碼」裡面，以DataReader方式撰寫。<br />
        <br />
    <asp:ListBox ID="ListBox1" runat="server" DataTextField="title" DataValueField="id"></asp:ListBox>
        <br />
        <br />
        雖然如此，還是請您看看 ListBox控制項的基礎設定，<br />
        兩個重要的屬性，請設定如下：<br />
&nbsp;&nbsp; Data<span class="style1">Text</span>Field=&quot;title&quot;
        <br />
&nbsp;&nbsp; Data<span class="style2">Value</span>Field=&quot;id&quot;</div>
    </form>
</body>
</html>
