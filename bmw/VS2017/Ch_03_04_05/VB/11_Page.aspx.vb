﻿
Partial Class _Book_WebControls_11_Page
    Inherits System.Web.UI.Page


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '==========================
        '== 本網頁"沒有"PlaceHolder控制項
        '== (直接加入網頁裡面)
        '==========================

        Dim Button1 As New Button
        Page.Form.Controls.Add(Button1)
        Button1.Text = "程式裡頭，動態產生的按鈕"


        Dim TextBox1 As New TextBox
        Page.Form.Controls.Add(TextBox1)
        TextBox1.Text = "程式裡頭，動態產生的文字輸入方塊"

    End Sub

End Class
