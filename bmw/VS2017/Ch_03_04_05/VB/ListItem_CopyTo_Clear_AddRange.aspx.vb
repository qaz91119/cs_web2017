﻿
Partial Class VS2010_Book_Sample_Ch03_Program__Book_WebControls_ListItem_CopyTo
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        '-- (1). 宣告一個 ListItem陣列
        Dim myListItemArray(ListBox1.Items.Count - 1) As ListItem
        ListBox1.Items.CopyTo(myListItemArray, 0)
        '-- 將全部子選項的集合，複製到陣列裡面。

        '-- (2). 刪除 ListBox的所有子選項。
        ListBox1.Items.Clear()


        Dim listResults As [String] = ""

        For Each myItem2 As ListItem In myListItemArray
            listResults = listResults & myItem2.Text & "<br />"
        Next myItem2

        Label1.Text = "子選項已經全部刪除，如下：<br />" & listResults


        DropDownList1.Items.AddRange(myListItemArray)
        '-- (3). 透過陣列，批次搬移所有的 Item。
        '-- 資料來源：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.listitemcollection.addrange.aspx
    End Sub
End Class
