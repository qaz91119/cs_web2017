﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Button_JavaScript_Alert.aspx.vb" Inherits="VS2010_Book_Sample__Book_WebControls_Button_JavaScript_Alert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>按下此按鈕，就會出現一個 JavaScript寫的警告視窗。</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Button ID="Button1" runat="server" 
            onclientclick="window.alert(&quot;警告視窗！&quot;);" Text="Button" />
        <br />
        <br />
        <br />
        按下此按鈕，就會出現一個 JavaScript寫的警告視窗。</div>
    </form>
</body>
</html>
