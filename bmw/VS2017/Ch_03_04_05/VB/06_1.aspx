﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="06_1.aspx.vb" Inherits="WebControls_6_1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> Ch 2</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            文章分類：<asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True">
	            <asp:ListItem Selected="True">政治</asp:ListItem>
	            <asp:ListItem>教育</asp:ListItem>
	            <asp:ListItem>娛樂</asp:ListItem>
        </asp:DropDownList>
            <br />
            <br />
            不需要Submit按鈕。<br />
            透過下拉式選單的AutoPostBack屬性，就能完成。</div>
    </form>
</body>
</html>
