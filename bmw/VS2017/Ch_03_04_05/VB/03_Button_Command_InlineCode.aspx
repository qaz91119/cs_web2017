﻿<%@ Page Language="VB" AutoEventWireup="false"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Button控制項，CommandName屬性的範例（Inline Code）</title>

        <script runat="server">
        Sub CommandBtn_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
            Select Case e.CommandName
                Case "Sort"
                    ' –註解： CType(A, B) 把A 轉成B型態
                    Sort_List(CType(e.CommandArgument, String))
                Case Else
                    Message.Text = "你按下哪個按鈕？我不認得～."
            End Select
        End Sub

        Sub Sort_List(ByVal CommandArgument As String)
            Select Case CommandArgument
                Case "Ascending"
                    Message.Text = "你按下「排序」的按鈕！CommandArgument為 Ascending "
                Case "Descending"
                    Message.Text = "你按下「反排序」的按鈕！ CommandArgument為 Descending "
            End Select
        End Sub
        </script>
        
    <style type="text/css">
        .style1 {
            background-color: #FFFF00;
        }
    </style>
        
</head>

<body>
   <form id="form2" runat="server">
  <h3>按下不同的按鈕，會出現不同的狀態<span class="style1">（Inline Code）</span></h3>
  <br /><br />
  <asp:Button id="Button1" runat="server"
   Text="排序（由大到小）"
   CommandName="Sort" CommandArgument="Ascending"
   OnCommand="CommandBtn_Click" />
       <!-- 註解： onCommand 表示按下這個按鈕，將會執行的程式名稱？
                        例如：將會執行 CommandBtn_Click() 這支程式 -->
      &nbsp;

  <asp:Button id="Button2" runat="server"
   Text="反排序（由小到大）"
   CommandName="Sort" CommandArgument="Descending"
   OnCommand="CommandBtn_Click" />
       <!-- 註解： 本範例的兩個按鈕，執行同一支 CommandBtn_Click() 這支程式 -->
      <br /><br />
  <asp:Label id="Message" runat="server"/>
   </form>
</body>
</html>

