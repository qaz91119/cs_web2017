﻿
Partial Class Book_Sample_Ch03_Program__Book_WebControls_ListBox_Add_Multiple_0_Easy
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '== 右移 ==
        Dim a As Integer = 0
        If ListBox1.Items.Count = 0 Then
            Label1.Text = "<font color=red>警告！ListBox1已經沒有子選項</font>"
            Exit Sub
        End If

        '**********************************************************
        '*** 改用反迴圈（由大到小）來執行，即可。
        '**********************************************************
        For i As Integer = (ListBox1.Items.Count - 1) To 0 Step -1
            If ListBox1.Items(i).Selected Then  '==判定哪一個子選項被點選了。
                ListBox2.Items.Add(ListBox1.Items(i))
                a = a + 1

                ListBox1.Items.Remove(ListBox1.Items(i))
                '==被搬移走了，這個子選項就該移除！
            End If
        Next

        If a = 0 Then
            Label1.Text = "<font color=red>警告！您未點選任何一個子選項</font>"
        Else
            Label1.Text = "<font color=blue>移動成功</font>"
        End If

    End Sub


    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        '== 左移 ==
        Dim b As Integer = 0
        If ListBox2.Items.Count = 0 Then
            Label2.Text = "<font color=red>警告！ListBox2已經沒有子選項</font>"
            Exit Sub
        End If

        '**********************************************************
        '*** 改用反迴圈（由大到小）來執行，即可。
        '**********************************************************
        For j As Integer = (ListBox2.Items.Count - 1) To 0 Step -1
            If ListBox2.Items(j).Selected Then  '==判定哪一個子選項被點選了。
                ListBox1.Items.Add(ListBox2.Items(j))
                b = b + 1

                ListBox2.Items.Remove(ListBox2.Items(j))
                '==被搬移走了，這個子選項就該移除！

            End If
        Next

        If b = 0 Then
            Label2.Text = "<font color=red>警告！您未點選任何一個子選項</font>"
        Else
            Label2.Text = "<font color=green>移動成功</font>"
        End If

    End Sub

End Class
