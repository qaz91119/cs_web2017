﻿
Partial Class VS2010_Book_Sample_Ch03_Program__Book_WebControls_dropdownlist_Hyperlink_02
    Inherits System.Web.UI.Page

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
        Response.Redirect("http://" & DropDownList1.SelectedValue)
        '--建議寫成  DropDownList1.SelectedValue.ToString() 是最標準的寫法！
    End Sub

End Class
