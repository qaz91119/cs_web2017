﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="10.aspx.vb" Inherits="WebControls_10" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <br />
        請點選下面的日曆控制項，來輸入日期：<asp:TextBox ID="TextBox1" runat="server" ReadOnly="True"></asp:TextBox>
        <br />
        注意！這個 TextBox文字輸入方塊，是「唯讀」的。禁止自己輸入文字！<br />
        <asp:Calendar ID="Calendar1" runat="server" BackColor="#FFFFCC" 
            BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Full" 
            Font-Names="Verdana" Font-Size="10pt" ForeColor="#663399" Height="200px" 
            SelectionMode="DayWeekMonth" ShowGridLines="True" Width="220px">
            <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
            <SelectorStyle BackColor="#FFCC66" />
            <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
            <OtherMonthDayStyle ForeColor="#CC9966" />
            <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
            <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
            <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" 
                ForeColor="#FFFFCC" />
        </asp:Calendar>
        <br />
        計算被選定的日期區間一共有幾天？<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <br />
        <hr />
        今天<%=FormatDateTime(Now, DateFormat.ShortDate)%>，是本週的第幾天（從零算起）？<asp:Label ID="Label1" runat="server"></asp:Label>
    
    </div>
    
    </form>
</body>
</html>
