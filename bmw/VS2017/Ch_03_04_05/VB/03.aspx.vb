﻿
Partial Class WebControls_3
    Inherits System.Web.UI.Page

    Protected Sub Button1_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles Button1.Command

        Response.Write("<br />1.  Command事件*** 您剛剛按下的Button按鈕，其內建的CommandArgument是----")
            Response.Write(e.CommandArgument)
    End Sub


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        
        ' Response.Write("<br />2.  Click事件*** 您剛剛按下的Button按鈕，其內建的CommandArgument是----")
        ' Response.Write(e.CommandArgument)    '-- 錯誤！！在 Click事件，不能使用 e.CommandArgument。

        '*************** 上面的兩句錯誤程式！！ ***************
        '-- 因為Button控制項的CommandName、CommandArgument這兩個屬性，
        '-- 只能夠搭配Button1_Command( )事件副程式來撰寫程式而已。請切記此重點！！

        Response.Write("<br />2.  Click事件*** 您剛剛按下的Button按鈕，其內建的CommandArgument是----")
        Response.Write(Button1.CommandArgument)    '-- 改寫成這樣，就正確！
    End Sub
End Class
