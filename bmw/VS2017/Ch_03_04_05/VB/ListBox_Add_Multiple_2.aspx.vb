﻿
Partial Class VS2010_Book_Sample_Ch03_Program_ListBox_Add_Multiple_2
    Inherits System.Web.UI.Page

    '========================================
    '== 這兩個事件的程式，非常雷同。您可以彙整起來，
    '== 改寫成一個 Sub副程式或是 Function，讓大家重複呼叫「這些類似的程式碼」。
    '========================================

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '== 右移 ==
        Dim a As Integer = 0
        Dim ItemA(ListBox1.Items.Count) As String  '-- 陣列

        If ListBox1.Items.Count = 0 Then
            Label1.Text = "<font color=red>警告！ListBox1已經沒有子選項</font>"
        End If
        For i As Integer = 0 To (ListBox1.Items.Count - 1)
            If ListBox1.Items(i).Selected Then  '==判定哪一個子選項被點選了。
                ListBox2.Items.Add(ListBox1.Items(i).Text)

                ItemA(a) = ListBox1.Items(i).Text  '==把要移除的子項目，先暫存在陣列裡面。
                a = a + 1
            End If
        Next

        '== 批次刪除 子項目 =======================
        For K As Integer = 0 To (a - 1)
            ListBox1.Items.Remove(ItemA(K))
            '==被搬移走了，這個子選項就該移除！
        Next
        '====================================

        If a = 0 Then
            Label1.Text = "<font color=red>警告！您未點選任何一個子選項</font>"
        Else
            Label1.Text = "<font color=blue>移動成功</font>"
        End If

    End Sub


    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        '== 左移 ==
        Dim b As Integer = 0
        Dim ItemB(ListBox2.Items.Count) As String  '-- 陣列

        If ListBox2.Items.Count = 0 Then
            Label2.Text = "<font color=red>警告！ListBox2已經沒有子選項</font>"
        End If

        For j As Integer = 0 To (ListBox2.Items.Count - 1)
            If ListBox2.Items(j).Selected Then  '==判定哪一個子選項被點選了。
                ListBox1.Items.Add(ListBox2.Items(j).Text)

                ItemB(b) = ListBox2.Items(j).Text  '==把要移除的子項目，先暫存在陣列裡面。
                b = b + 1
            End If
        Next

        '== 批次刪除 子項目 =======================
        For k As Integer = 0 To (b - 1)
            ListBox2.Items.Remove(ItemB(k))
            '==被搬移走了，這個子選項就該移除！
        Next
        '====================================
        If b = 0 Then
            Label2.Text = "<font color=red>警告！您未點選任何一個子選項</font>"
        Else
            Label2.Text = "<font color=green>移動成功</font>"
        End If

    End Sub



End Class
