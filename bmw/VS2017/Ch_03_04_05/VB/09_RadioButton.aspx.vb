﻿
Partial Class VS2010_Book_Sample__Book_WebControls_9_RadioButton
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Response.Write("共有" & RadioButtonList1.Items.Count & "個選項<hr>")

        Dim my_class_text As String = ""
        Dim my_class_value As String = ""

        For i As Integer = 0 To (RadioButtonList1.Items.Count - 1)
            ' 註解：控制項的 Items是從「零」算起的
            If (RadioButtonList1.Items(i).Selected) Then
                my_class_text &= RadioButtonList1.Items(i).Text & " ， "
                my_class_value &= RadioButtonList1.Items(i).Value & " ， "
            End If

        Next

        Label1.Text = my_class_text
        Label2.Text = my_class_value
    End Sub
End Class
