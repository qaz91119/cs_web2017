﻿
Partial Class Book_Sample_Ch03_Program__Book_WebControls_dropdownlist_Request
    Inherits System.Web.UI.Page

    Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownList1.SelectedIndexChanged
        Response.Write("<h2>")

        Response.Write(Request("DropDownList1") & "<br />")
        '-- 即使IIS編譯之後，轉成標準HTML表單的下拉式選項，ID名稱仍為 DropDownList1。

        '-- 建議使用 DropDownList1.UniqueID（獨一無二的ID，專門給JavaScript使用）會更好！
        Response.Write(Request(DropDownList1.UniqueID))

        Response.Write("</h2>")
    End Sub
End Class
