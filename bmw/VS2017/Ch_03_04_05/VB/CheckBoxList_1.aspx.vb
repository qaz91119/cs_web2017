﻿
Partial Class _Book_WebControls_CheckBoxList_1
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim u_summary As Integer = 0

        Label1.Text = ""
        For i As Integer = 0 To (CheckBoxList1.Items.Count - 1)
            If CheckBoxList1.Items(i).Selected Then
                If CheckBoxList1.Items(i).Value <> "0" Then
                    Label1.Text = "<font color=darkgreen>答對！</font>"
                    u_summary += CInt(CheckBoxList1.Items(i).Value)
                Else
                    Label1.Text = "<font color=red>有錯誤喔～</font>"
                    u_summary = 0  '-- 有錯誤選項，立刻把分數歸零！2011/12/19修正。
                    Exit For
                End If
            End If
        Next
        '----------------------------------------------------------------------------
        Label2.Text = ""
        For j As Integer = 0 To (CheckBoxList2.Items.Count - 1)
            If CheckBoxList2.Items(j).Selected Then
                If CheckBoxList2.Items(j).Value <> "0" Then
                    Label2.Text = "<font color=darkgreen>答對！</font>"
                    u_summary += CInt(CheckBoxList2.Items(j).Value)
                Else
                    Label2.Text = "<font color=red>有錯誤喔～</font>"
                    u_summary = 0  '-- 有錯誤選項，立刻把分數歸零！2011/12/19修正。
                    Exit For
                End If
            End If
        Next

        '--------------------------------------------------------------
        '---- 第三題裡面，必須複選兩個才算正確 ----
        '--------------------------------------------------------------
        Label3.Text = ""
        Dim u_summary_3 As Integer = 0

        For k As Integer = 0 To (CheckBoxList3.Items.Count - 1)
            If CheckBoxList3.Items(k).Selected Then
                If CheckBoxList3.Items(k).Value <> "0" Then
                    u_summary_3 += CInt(CheckBoxList3.Items(k).Value)
                Else
                    Label3.Text = "<font color=red>有錯誤喔～</font>"
                    u_summary_3 = 0  '-- 有錯誤選項，立刻把分數歸零！2011/12/19修正。
                    Exit For
                End If
            End If
        Next

        If u_summary_3 = 6 Then
            Label3.Text = "<font color=darkgreen>答對！</font>"
        End If

        u_summary += CInt(u_summary_3)

        '----------------------------------------------------------------------------
        Label_summary.Text = u_summary
    End Sub

End Class
