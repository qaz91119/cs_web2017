﻿
Partial Class Book_Sample_Ch03_Program__Book_WebControls_11_Dynamic_Generate_Controls_05_Nest
    Inherits System.Web.UI.Page

    '**** Init事件 ************************************************
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

        Dim TB As TextBox = New TextBox()
        TB.ID = "TextBox1"
        TB.Text = "動態加入的TextBox"

        Dim PL As Panel = New Panel()
        PL.ID = "Panel1"
        PL.Controls.Add(TB)

        '**** 最後才加入頁面。這樣便是「巢狀」動態加入控制項！ ****
        Page.Form.Controls.Add(PL)

    End Sub

End Class
