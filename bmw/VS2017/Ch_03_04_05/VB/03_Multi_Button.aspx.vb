﻿
Partial Class VS2010_Book_Sample_Ch03_Program__Book_WebControls_03_Multi_Button
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click, Button2.Click
        '-- 重點(1)：共用同一個事件。VB語法必須寫在後面的「Handles」，用逗點(,)區隔。

        Dim btn As Button = sender
        Response.Write("<hr />您剛剛按下的按鈕，其CommandArgument屬性為：" & btn.CommandArgument & "<hr />")


    End Sub
End Class
