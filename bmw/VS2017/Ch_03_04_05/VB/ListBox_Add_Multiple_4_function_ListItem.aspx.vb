﻿
Partial Class VS2010_Book_Sample_Ch03_Program__Book_WebControls_ListBox_Add_Multiple_4_function_ListItem
    Inherits System.Web.UI.Page


    '========================================
    '== 這兩個事件的程式，非常雷同。您可以彙整起來，
    '== 改寫成一個 Sub副程式或是 Function，讓大家重複呼叫「這些類似的程式碼」。
    '========================================

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '== 右移 ==
        MoveItem(ListBox1, ListBox2)

    End Sub


    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        '== 左移 ==
        MoveItem(ListBox2, ListBox1)

    End Sub


    '*****************************************************************
    '*** 從哪個 ListBox（LB_A）移動到另外一個 ListBox（LB_B）
    Sub MoveItem(LB_A As ListBox, LB_B As ListBox)
        Dim a As Integer = 0
        '-------------------------------------------------------
        Dim myList As List(Of ListItem) = New List(Of ListItem)  '-- 改用 List(Of T)取代原本的陣列
        '-------------------------------------------------------

        If LB_A.Items.Count = 0 Then
            Response.Write("<Script Language=""JavaScript"">window.alert(""警告！" & LB_A.ID & "已經沒有子選項"");</Script>")
            Exit Sub
        End If

        For i As Integer = 0 To (LB_A.Items.Count - 1)
            If LB_A.Items(i).Selected Then  '==判定哪一個子選項被點選了。

                LB_B.Items.Add(LB_A.Items(i))
                '-- .Add()方法，在此是加入一個「子選項(ListItem)」，而非上一個範例的「字串」。
                '-- 參考資料：http://msdn.microsoft.com/zh-tw/library/e7s6873c.aspx

                '-------------------------------------------------------
                myList.Add(LB_A.Items(i))  '==把要移除的子項目，先暫存在 List(Of T)裡面。
                '-------------------------------------------------------
                a = a + 1
            End If
        Next

        '== 批次刪除 子項目 =======================
        For K As Integer = 0 To (myList.Count - 1)
            LB_A.Items.Remove(myList(K))
            '==被搬移走了，這個子選項就該移除！
        Next
        '====================================

        If a = 0 Then
            Response.Write("<Script Language=""JavaScript"">window.alert(""警告！您未點選" & LB_A.ID & "任何一個子選項"");</Script>")
        Else
            Response.Write("<Script Language=""JavaScript"">window.alert(""移動成功"");</Script>")
        End If
    End Sub

End Class
