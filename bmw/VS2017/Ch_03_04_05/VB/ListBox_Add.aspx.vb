﻿
Partial Class _Book_WebControls_ListBox_Add
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '== 右移 ==
        Dim a As Integer = 0
        If ListBox1.Items.Count = 0 Then
            Label1.Text = "<font color=red>警告！ListBox1已經沒有子選項</font>"
            Exit Sub
        End If

        For i As Integer = 0 To (ListBox1.Items.Count - 1)

            If ListBox1.Items(i).Selected Then  '==判定哪一個子選項被點選了。
                ListBox2.Items.Add(ListBox1.Items(i).Text)
                a = a + 1

                ListBox1.Items.Remove(ListBox1.Items(i).Text)
                '==被搬移走了，這個子選項就該移除！
                Exit For
                '************************
                '** 重點！沒有這一段程式的話，中間的子選項被移走，就會報錯！
                '** 中間的子選項突然變少（臨時被移走），所以迴圈次數又少一個，因此報錯！
            End If
        Next

        If a = 0 Then
            Label1.Text = "<font color=red>警告！您未點選任何一個子選項</font>"
        Else
            Label1.Text = "<font color=blue>移動成功</font>"
        End If

    End Sub


    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        '== 左移 ==
        Dim b As Integer = 0
        If ListBox2.Items.Count = 0 Then
            Label2.Text = "<font color=red>警告！ListBox2已經沒有子選項</font>"
            Exit Sub
        End If

        For j As Integer = 0 To (ListBox2.Items.Count - 1)
            If ListBox2.Items(j).Selected Then  '==判定哪一個子選項被點選了。
                ListBox1.Items.Add(ListBox2.Items(j).Text)
                b = b + 1

                ListBox2.Items.Remove(ListBox2.Items(j).Text)
                '==被搬移走了，這個子選項就該移除！
                Exit For 
                '************************
                '** 重點！沒有這一段程式的話，中間的子選項被移走，就會報錯！
                '** 中間的子選項突然變少（臨時被移走），所以迴圈次數又少一個，因此報錯！
            End If
        Next

        If b = 0 Then
            Label2.Text = "<font color=red>警告！您未點選任何一個子選項</font>"
        Else
            Label2.Text = "<font color=green>移動成功</font>"
        End If

    End Sub
End Class
