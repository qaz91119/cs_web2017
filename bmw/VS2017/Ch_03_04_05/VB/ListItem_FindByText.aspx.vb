﻿
Partial Class VS2010_Book_Sample_Ch03_Program__Book_WebControls_ListItem_FindByText
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        Dim crItem As ListItem
        Dim searchText As String = TextBox1.Text

        If (TextBox1.Text <> "") Then
            searchText = TextBox1.Text

            crItem = ListBox1.Items.FindByText(searchText)
            '== 注意！！.FindByText()方法。
            '== (1). 區分大小寫但不區分文化特性的比較，不執行部分搜尋或萬用字元搜尋。
            '== (2). 如果使用這個準則無法在集合中找到項目，則會傳回 Nothing。
            '== (3). 傳回值，是一個子選項（ListItem）。

            If crItem Is Nothing Then
                Label1.Text = "<h3>找不到，不符合（有區分大小寫）。</h3>"
            Else
                Label1.Text = "<h3> ***找到了！*** </h3>"
                Label1.Text &= "<br />被搜尋到的子選項，「Text」屬性為 -- " & crItem.Text
                Label1.Text &= "<br />被搜尋到的子選項，「Value」屬性為 -- " & crItem.Value
            End If

        Else
            Label1.Text = "<h3>Sorry，您沒有輸入關鍵字....Orz。</h3>"
        End If

        '-- 被搜尋到子選項，設為「已選取」。
        For i As Integer = 0 To (ListBox1.Items.Count - 1)
            If ListBox1.Items(i).Text = searchText Then
                ListBox1.Items(i).Selected = True
            End If
        Next

    End Sub
End Class
