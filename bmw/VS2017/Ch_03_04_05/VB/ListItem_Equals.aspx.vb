﻿
Partial Class VS2010_Book_Sample_Ch03_Program__Book_WebControls_ListItem_Equals
    Inherits System.Web.UI.Page

    Protected Sub addButton_Click(sender As Object, e As System.EventArgs) Handles addButton.Click
        '--參考來源： http://msdn.microsoft.com/zh-tw/library/bsc2ak47.aspx

        If ListBox1.Items.Equals(DropDownList1.SelectedItem) Then
            Response.Write("<Script Language=JavaScript>window.alert(""這個 Object相等"")</Script>")
        Else
            Response.Write("<Script Language=JavaScript>window.alert("" ***抱歉，這個 Object不相等*** "")</Script>")
            '-- Items（子選項的集合）與 SelectItem（子選項）並不是同一個 Object。
            '-- Items（子選項的集合）是 ListItemCollection類別。
            '-- SelectItem（子選項）是	ListItem類別。
        End If

    End Sub

End Class
