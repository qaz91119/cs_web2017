﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="07.aspx.vb" Inherits="WebControls_7" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ListBox</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:ListBox ID="ListBox1" runat="server" DataSourceID="SqlDataSource1" 
            DataTextField="title" DataValueField="id"></asp:ListBox>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testConnectionString4 %>" 
            SelectCommand="SELECT [id], [title] FROM [test]"></asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>
