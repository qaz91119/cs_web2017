﻿
Partial Class VS2010_Book_Sample_Ch03_Program__Book_WebControls_0_Page_Load_2
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        '--務必把 Label1的文字（Text屬性）預設為零。

        Label1.Text = CInt(Label1.Text) + 1
        '-- 也可以寫成跟C#語法一樣的。  Label1.Text = Convert.ToInt32(Label1.Text) + 1
        '*********************************************
        '-- 跟上一隻程式不同，第一次執行，Label就會變成 1。
    End Sub
End Class
