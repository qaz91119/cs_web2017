﻿
Partial Class Book_Sample_Ch03_Program__Book_WebControls_TextBox_TextMode_Password_04_AJAX
    Inherits System.Web.UI.Page

    Protected Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged
        If (TextBox1.Text <> TextBox2.Text) Then

            Label1.Text = "<big>密碼錯誤，請重新輸入</big>" & System.DateTime.Now.ToLongTimeString()
            '---- 寫成 JavaScript不會動。
            'Label1.Text = "<script language=javascript>window.alert('密碼錯誤，請重新輸入');</script>"
            TextBox1.Focus()
        Else
            Label1.Text = "<hr />您輸入的密碼 -- " & TextBox1.Text & System.DateTime.Now.ToLongTimeString()
        End If
    End Sub
End Class
