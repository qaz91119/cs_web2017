﻿
Partial Class Book_Sample_Ch03_Program__Book_WebControls_Calendar_Birthday_DropDownList
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim myYear As Integer =System.DateTime.Now.Year
        '==寫在 for迴圈外面，避免每一次迴圈都要調用 System.DateTime，以節省資源。

        For i As Integer = 0 To 10
            ''*** myYear = myYear - i  '***這裡出了大錯！！***
            '*** DropDownList1.Items.Add(myYear)  '***這裡出了大錯！！***

            DropDownList1.Items.Add(myYear - i)
            '資料來源  http://msdn.microsoft.com/zh-tw/library/system.datetime.addyears.aspx
        Next
    End Sub


    Protected Sub DropDownList2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownList2.SelectedIndexChanged
        ''== 方法一 ==：
        'Calendar1.TodaysDate = New DateTime(DropDownList1.SelectedValue, DropDownList2.SelectedValue, 1)
        ''Calendar1.TodaysDate = New DateTime(Convert.ToInt32(DropDownList1.SelectedValue), Convert.ToInt32(DropDownList2.SelectedValue), 1)
        ''DateTime裡面的日期（年/月/日），只能接受「整數」。

        ''== 方法二 ==：
        ''**** 建議改用 .VisibleDate屬性 更好。
        ''**** 只會出現「當月月份」！
        Calendar1.VisibleDate = New DateTime(DropDownList1.SelectedValue, DropDownList2.SelectedValue, 1)
        ''Calendar1.VisibleDate = New DateTime(Convert.ToInt32(DropDownList1.SelectedValue), Convert.ToInt32(DropDownList2.SelectedValue), 1)
        ''如果日曆控制項的 VisibleDate屬性未設定，所指定的日期 TodaysDate屬性會決定哪個月份會顯示在Calendar控制項。
    End Sub


    Protected Sub Calendar1_SelectionChanged(sender As Object, e As EventArgs) Handles Calendar1.SelectionChanged
        Label1.Text = "您剛剛挑選的生日---" & Calendar1.SelectedDate
    End Sub
End Class
