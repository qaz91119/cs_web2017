﻿'----自己（宣告）寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己（宣告）寫的----
Partial Class Book_Sample_Ch03_Program__Book_WebControls_CheckBoxList_DB
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            DBinit()
        End If
    End Sub


    Sub DBinit()
        '=======微軟SDK文件的範本=======
        Dim Conn As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)
        '----上面已經事先寫好 Imports System.Web.Configuration ----
        '----(連結資料庫)----

        Dim dr As SqlDataReader = Nothing
        Dim cmd As SqlCommand = New SqlCommand("select * from CheckBoxList_Table", Conn)

        Try     '==== 以下程式，只放「執行期間」的指令！=====================
            Conn.Open()   '---- 這時候才連結DB

            dr = cmd.ExecuteReader()   '---- 這時候執行SQL指令，取出資料

            '*******************************************************
            '** 輸入資料表的「欄位名稱」
            CheckBoxList1.DataTextField = "TheText"
            CheckBoxList1.DataValueField = "TheValue"
            '*******************************************************

            CheckBoxList1.DataSource = dr
            CheckBoxList1.DataBind()

        Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
            '---- http://www.dotblogs.com.tw/billchung/archive/2009/03/31/7779.aspx
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<hr />")

        Finally
            '---- Always call Close when done reading.
            If Not (dr Is Nothing) Then
                cmd.Cancel()
                '----關閉DataReader之前，一定要先「取消」SqlCommand
                '參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
                dr.Close()
            End If
            '---- Close the connection when done with it.
            If (Conn.State = ConnectionState.Open) Then
                Conn.Close()
                Conn.Dispose() '---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
            End If

        End Try
    End Sub
End Class
