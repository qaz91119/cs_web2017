﻿
Partial Class WebControls_6_1
    Inherits System.Web.UI.Page

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged

        '--註解：所謂的「SelectedIndexChanged()」事件，就是有人選取了下拉式選單裡面，任何一個選項。
        '--          就會啟動這個「SelectedIndexChanged()」事件。
        Response.Write("您剛剛選擇了----" & DropDownList1.SelectedItem.Text)
    End Sub
End Class
