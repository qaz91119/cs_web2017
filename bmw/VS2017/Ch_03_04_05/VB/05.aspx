﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="05.aspx.vb" Inherits="WebControls_5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ch 3</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        當使用者按下不同的按鈕，畫面上面的「HyperLink」控制項就會依照不同的按鈕，變化成不同的超連結！</div>
    <br />
    <br />
    <asp:Button ID="Button1" runat="server" Text="台灣Yahoo!網站 www.yahoo.com.tw" />
&nbsp;&nbsp;
    <br />
    <asp:Button ID="Button2" runat="server" Text="資策會FIND網站 www.find.org.tw" />
&nbsp;&nbsp;
    <br />
    <asp:Button ID="Button3" runat="server" 
        Text="MIS2000 Lab.網站  www.taconet.com.tw/mis2000lab/" />
    <br />
    <br />
    <asp:HyperLink ID="HyperLink1" runat="server">HyperLink</asp:HyperLink>
    </div>
    </form>
</body>
</html>
