﻿
Partial Class VS2010_Book_Sample_4_2_JavaScript
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Label1.Text = "1. 程式會首先執行 Page_Load()這一區喔！"
        Label1.Visible = True
        '-- 如果不把 Label的 Text清空，就必須用這種方法把他隱形。畫面才好看！
    End Sub


    Protected Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        Label2.Text = "<h3>2. 謝謝您輸入資料！～～～Bye Bye</h3>"
        Label2.Visible = True
        '-- 如果不把 Label的 Text清空，就必須用這種方法把他隱形。畫面才好看！
    End Sub


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Label3.Text = "<font color=blue>"
        Label3.Text &= "3. 您按下按鈕囉！<Script Language=""Javascript"">window.alert(""警告視窗！"");</Script>您輸入的文字是----" & TextBox1.Text
        Label3.Text &= "</font>"

        ' 註解：要輸出雙引號（"），請寫成「""」。
    End Sub

End Class
