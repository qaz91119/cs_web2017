﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="08.aspx.vb" Inherits="_8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> 一年一班的班長選舉 </title>
    <style type="text/css">
        .style1
        {
            color: #0000FF;
        }
        .style2
        {
            font-weight: bold;
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <b><span class="style1">2010年 台灣小學，</span><br class="style1" />
        <span class="style1">&nbsp;&nbsp;&nbsp; 一年一班的班長選舉</span></b><br />
        <br />
        <span class="style2">班長候選人</span>：<asp:CheckBoxList ID="CheckBoxList1" 
            runat="server">
            <asp:ListItem>李小輝</asp:ListItem>
            <asp:ListItem>連小戰</asp:ListItem>
            <asp:ListItem>宋小魚</asp:ListItem>
            <asp:ListItem>馬小九</asp:ListItem>
            <asp:ListItem>謝小夫</asp:ListItem>
            <asp:ListItem>呂小蓮</asp:ListItem>
            <asp:ListItem>電火球</asp:ListItem>
        </asp:CheckBoxList>
        <asp:Button ID="Button1" runat="server" Text="Button--投票！可複選！" />
        <br />
        <br />
        您剛剛投票給哪些人？&nbsp;&nbsp; 
        <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
    
    </div>
    </form>
</body>
</html>
