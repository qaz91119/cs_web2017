﻿
Partial Class _Book_WebControls_11_FindControl_1
    Inherits System.Web.UI.Page


    '--正確運作。
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Dim Button1 As New Button
        PlaceHolder1.Controls.Add(Button1)
        Button1.Text = "程式裡頭，動態產生的按鈕（按下之後，會執行自訂的事件--myButton1EventHandler）"
        Button1.ID = "Button_DynaAdd"

        '=========================================
        AddHandler Button1.Click, AddressOf myButton1EventHandler
        '-- AddressOf後面的事件，請按下警告訊息就會自動產生。
        '-- 自訂的事件。建議寫在 Page_Init()裡面。
        '--資料來源：http://msdn.microsoft.com/zh-tw/library/t3d01ft1%28v=VS.100%29.aspx
        '=========================================

        Dim TextBox1 As New TextBox
        PlaceHolder1.Controls.Add(TextBox1)
        TextBox1.Text = "程式裡頭，動態產生的文字輸入方塊"
        TextBox1.ID = "TextBox_DynaAdd"

    End Sub


    '--正確運作。不建議使用！！請改用 Page_Init事件來作！！
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim Button1 As New Button
        'PlaceHolder1.Controls.Add(Button1)
        'Button1.Text = "程式裡頭，動態產生的按鈕（按了也沒用）"
        'Button1.ID = "Button_DynaAdd"


        'Dim TextBox1 As New TextBox
        'PlaceHolder1.Controls.Add(TextBox1)
        'TextBox1.Text = "程式裡頭，動態產生的文字輸入方塊"
        'TextBox1.ID = "TextBox_DynaAdd"

    End Sub


    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        '== 透過 FindControl抓取 PlaceHolder裡面的「子控制項」
        Dim getTextBox As New TextBox
        getTextBox = CType(PlaceHolder1.FindControl("TextBox_DynaAdd"), TextBox)

        Label1.Text = getTextBox.Text
    End Sub


    '=========================================
    Private Sub myButton1EventHandler(sender As Object, e As EventArgs)
        Response.Write("<script language=javascript>window.alert(""自訂的事件-- myButton1EventHandler"");</script>")
    End Sub
    '=========================================

End Class
