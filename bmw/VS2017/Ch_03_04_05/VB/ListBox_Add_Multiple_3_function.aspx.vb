﻿
Partial Class VS2010_Book_Sample_Ch03_Program__Book_WebControls_ListBox_Add_Multiple_3_function
    Inherits System.Web.UI.Page

    '========================================
    '== 這兩個事件的程式，非常雷同。您可以彙整起來，
    '== 改寫成一個 Sub副程式或是 Function，讓大家重複呼叫「這些類似的程式碼」。
    '========================================

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '== 右移 ==
        MoveItem(ListBox1)

    End Sub


    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        '== 左移 ==
        MoveItem(ListBox2)

    End Sub


    '**** 重複的程式，可以做成一個副程式 *****************************************************
    Sub MoveItem(myListBox As ListBox)
        Dim a As Integer = 0
        Dim ItemA(myListBox.Items.Count) As String  '-- 陣列

        If myListBox.Items.Count = 0 Then
            Label1.Text = "<font color=red>警告！" & myListBox.ID & "已經沒有子選項</font>"
            Exit Sub
        End If

        For i As Integer = 0 To (myListBox.Items.Count - 1)
            If myListBox.Items(i).Selected Then  '==判定哪一個子選項被點選了。
                If myListBox.ID = "ListBox1" Then
                    ListBox2.Items.Add(myListBox.Items(i).Text)
                Else
                    ListBox1.Items.Add(myListBox.Items(i).Text)
                End If

                ItemA(a) = myListBox.Items(i).Text  '==把要移除的子項目，先暫存在陣列裡面。
                a = a + 1
            End If
        Next

        '== 批次刪除 子項目 =======================
        For K As Integer = 0 To (a - 1)
            myListBox.Items.Remove(ItemA(K))
            '==被搬移走了，這個子選項就該移除！
        Next
        '====================================

        If a = 0 Then
            Label1.Text = "<font color=red>警告！您未點選" & myListBox.ID & "任何一個子選項</font>"
        Else
            Label1.Text = "<font color=blue>移動成功</font>"
        End If
    End Sub

End Class
