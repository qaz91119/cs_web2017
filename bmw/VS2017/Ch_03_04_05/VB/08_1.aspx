﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="08_1.aspx.vb" Inherits="WebControls_8_1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> CheckBoxList </title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:CheckBoxList ID="CheckBoxList1" runat="server" 
            DataSourceID="SqlDataSource1" DataTextField="title" DataValueField="id">
        </asp:CheckBoxList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testConnectionString %>" 
            SelectCommand="select top 7 id,title from test"></asp:SqlDataSource>
    
            <br />
    
            <asp:Button ID="Button1" runat="server" Text="Button--想看哪幾篇文章？可複選！" />
        <br />
        <br />
        您想看這幾篇文章（ID編號）？&nbsp;&nbsp; 
        <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
    </div>
    </form>
</body>
</html>
