﻿'----自己寫的（宣告）----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的（宣告）----


Partial Class VS2010_Book_Sample__Book_WebControls_6_DropDownList_TwoField_02
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Conn As SqlConnection = New SqlConnection
        '----上面已經事先寫好 Imports System.Web.Configuration ----
        Conn.ConnectionString = WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString

        Dim dr As SqlDataReader = Nothing
        Dim cmd As New SqlCommand("select id, title, author from test", Conn)

        Try     '==== 以下程式，只放「執行期間」的指令！=====================
            Conn.Open()   '---- 這時候才連結DB

            '---- 這時候執行SQL指令，取出資料。
            dr = cmd.ExecuteReader()

            Dim i As Integer = 0
            '====== 重  點！！======================
            '== http://www.blueshop.com.tw/board/FUM20041006161839LRJ/BRD20101114072706MJM.html

            While dr.Read()
                DropDownList1.Items.Add(dr("title") & " / " & dr("author"))
                DropDownList1.Items(i).Value = dr("id")
                i = i + 1
            End While
            '===================================

        Catch ex As Exception   '---- 如果程式有錯誤或是例外狀況，將執行這一段
            Response.Write("<b>Error Message----  </b>" + ex.ToString() + "<HR/>")
        Finally
            '---- Always call Close when done reading.
           If Not (dr Is Nothing) Then
                cmd.Cancel()
                '----關閉DataReader之前，一定要先「取消」SqlCommand
                '參考資料： http://blog.darkthread.net/blogs/darkthreadtw/archive/2007/04/23/737.aspx
                dr.Close()
           End If

            '---- Close the connection when done with it.
            If (Conn.State = ConnectionState.Open) Then
                Conn.Close()
                Conn.Dispose() '---- 一開始宣告有用到 New的,最後必須以 .Dispose()結束
            End If
        End Try
    End Sub
End Class
