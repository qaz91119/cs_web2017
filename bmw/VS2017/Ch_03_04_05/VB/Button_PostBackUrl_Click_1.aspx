﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Button_PostBackUrl_Click_1.aspx.vb" Inherits="Book_Sample_Ch03_Program__Book_WebControls_Button_PostBackUrl_Click_1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Button控制項的「PostBackUrl屬性」與「Click事件」能否一起做到？#1
        </div>
        <p>
            <asp:Button ID="Button1" runat="server" 
                PostBackUrl="http://www.dotblogs.com.tw/mis2000lab/" Text="Button_已設定PostBackUrl屬性" />
        </p>
    </form>
</body>
</html>
