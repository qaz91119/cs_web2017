﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="03_Button_JavaScript.aspx.vb" Inherits="VS2010_Book_Sample__Book_WebControls_3_Button_JavaScript" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <br />
        Button與 JavaScript<br />
    
        <br />
        <asp:Button ID="Button2" runat="server" 
            onclientclick="javascript:window.alert(&quot;Hello!The World!&quot;)" 
            Text="Button" />
        <br />
        <br />
        <a href="3_Button_HistoryBack.aspx">連結到下一頁</a><br />
        <br />
        
    
    </div>
    </form>
</body>
</html>
