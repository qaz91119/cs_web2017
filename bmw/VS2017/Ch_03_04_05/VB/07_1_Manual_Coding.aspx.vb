﻿Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient


Partial Class _Book_New_Samples_DropDownList_ListBox_7_1_Manual_Coding
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        '--註解：SqlDataSource的設定畫面，幫我們處理掉下面這一大串程式碼。
        '--    本程式的執行結果，跟上面 7_1.aspx完全相同！
        Dim Conn As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString)
        Conn.Open()   '--第一、連結資料庫

        Dim cmd As SqlCommand = New SqlCommand("select id,title from test", Conn)
        Dim dr As SqlDataReader = cmd.ExecuteReader()   '--第二、執行SQL指令，取出資料

        '--註解：執行SQL指令之後，把資料庫撈出來的結果，
        '--    交由ListBox控制項來呈現。
        ListBox1.DataSource = dr
        ListBox1.DataBind()   '--第三、資料繫結

        '--註解：注意！！這裡的 .DataSource與另外一個「DataSourceID」屬性
        '--    完全不同。兩者也不可同時使用！本書後續會深入解說……

        cmd.Cancel()
        dr.Close()
        Conn.Close()  '--第四、關閉資料庫的連接與相關資源

    End Sub
End Class
