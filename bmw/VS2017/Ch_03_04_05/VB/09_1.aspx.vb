﻿
Partial Class _Book_WebControls_9_1
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            '--註解：日曆
            If Calendar1.SelectedDates.Count >= 1 Then
                TextBox1.Text = Calendar1.SelectedDate
            Else
                Throw New Exception("尚未選擇生日！")
            End If


            '--註解：不填寫資料，則出現警告視窗
        Catch ex As Exception
            Dim myMsg As New Literal()
            myMsg.Text = "<script>alert('" & ex.Message.ToString & "')</script>"

            Me.Page.Form.Controls.Add(myMsg)
            '—註解：把上面的 JavaScript警告視窗，臨時加入頁面（Page）裡頭。
        End Try

    End Sub
End Class
