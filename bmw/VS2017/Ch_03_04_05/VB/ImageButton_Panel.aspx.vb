﻿
Partial Class VS2010_Book_Sample_Ch03_Program__Book_WebControls_ImageButton_01
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        '== 方法一 ==
        'If Not Page.IsPostBack Then
        '    Session("click") = 1
        'End If

    End Sub



    Protected Sub ImageButton1_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        '== 方法一 ==
        'If (CInt(Session("click")) Mod 2) = 0 Then
        '    Panel1.Visible = True
        'Else
        '    Panel1.Visible = False
        'End If

        'Session("click") = CInt(Session("click")) + 1

        '==============================
        '== 方法二 ==
        If Panel1.Visible = False Then
            Panel1.Visible = True
        Else
            Panel1.Visible = False
        End If

    End Sub

End Class
