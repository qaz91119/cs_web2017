﻿'----自己寫的----
Imports System
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
'----自己寫的----

Partial Class WebControls_7_1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Conn As New SqlConnection(WebConfigurationManager.ConnectionStrings("testConnectionString").ConnectionString.ToString)
        Conn.Open()   '---- 連結DB

        Dim cmd As SqlCommand = New SqlCommand("select id,title from test", Conn)
        Dim dr As SqlDataReader = cmd.ExecuteReader()   '---- 執行SQL指令，取出資料

        '-- 註解：執行SQL指令之後，把資料庫撈出來的結果，交由ListBox控制項來呈現()
        ListBox1.DataSource = dr
        ListBox1.DataBind()

        cmd.Cancel()
        dr.Close()
        Conn.Close()

    End Sub
End Class
