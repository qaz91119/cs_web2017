﻿
Partial Class Book_Sample_Ch03_Program__Book_WebControls_TextBox_TextMode_Password_02
    Inherits System.Web.UI.Page

    Protected Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged

        If (TextBox1.Text <> TextBox2.Text) Then
            Response.Write("<script language=javascript>window.alert(""密碼錯誤，請重新輸入"");</script>")
            '--使用兩個雙引號，表示字串內有一個「雙引號」。
            TextBox1.Focus()
        Else
            Response.Write("您輸入的密碼 -- " & TextBox1.Text)
        End If

    End Sub
End Class
