﻿
Partial Class _Book_WebControls_Food_Menu
    Inherits System.Web.UI.Page

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
        Label1.Text = DropDownList1.SelectedItem.Text

        '********************************************************************
        '-- 避免畫面一開啟，就出現菜名。必須點選上面的「食物種類」之後，才會出現菜名。
        '-- 這個作法，就是所謂的「DataBinding」（資料繫結、綁定）
        ListBox1.DataSourceID = "SqlDataSource2"
    End Sub



    Protected Sub ListBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        Label2.Text = ListBox1.SelectedItem.Text
    End Sub
End Class
