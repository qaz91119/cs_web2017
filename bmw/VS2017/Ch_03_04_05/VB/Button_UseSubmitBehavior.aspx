<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Button_UseSubmitBehavior.aspx.vb" Inherits="_Book_WebControls_Button_UseSubmitBehavior" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>未命名頁面</title>
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            color: #FFFFCC;
            background-color: #CC0000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        底下的送出按鈕，只能使用一次！<span class="style1">（建議使用 FireFox瀏覽器來測試）</span><br />
        <br />
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Submit / 送出" OnClientClick="javascript:this.disabled=true;this.document.form1.submit();" UseSubmitBehavior="false"   />
    
    </div>
    </form>
 
</body>
</html>
