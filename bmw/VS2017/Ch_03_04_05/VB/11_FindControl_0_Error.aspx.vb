﻿
Partial Class _Book_WebControls_11_FindControl
    Inherits System.Web.UI.Page


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim Button1 As New Button
        Button1.Text = "程式裡頭，動態產生的按鈕"
        Button1.ID = "Button_DynaAdd"
        PlaceHolder1.Controls.Add(Button1)

        Dim TextBox1 As New TextBox
        TextBox1.Text = "程式裡頭，動態產生的文字輸入方塊"
        TextBox1.ID = "TextBox_DynaAdd"
        PlaceHolder1.Controls.Add(TextBox1)


    End Sub



    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        '== 透過 FindControl抓取 PlaceHolder裡面的「子控制項」
        Dim getTextBox As New TextBox
        getTextBox = CType(PlaceHolder1.FindControl("TextBox_DynaAdd"), TextBox)

        Label1.Text = getTextBox.Text

    End Sub
End Class
