﻿
Partial Class Book_Sample_Ch03_Program__Book_WebControls_11_Dynamic_Generate_Controls_02_Error
    Inherits System.Web.UI.Page

    Protected Sub RadioButtonList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadioButtonList1.SelectedIndexChanged
        If RadioButtonList1.Items(0).Selected Then
            Dim RB As RadioButtonList = New RadioButtonList()
            RB.ID = "RadioButtonList2"
            RB.Items.Add(New ListItem("單選一Text", "單選一Value"))
            RB.Items.Add(New ListItem("單選二Text", "單選二Value"))
            RB.Items.Add(New ListItem("單選三Text", "單選三Value"))

            PlaceHolder1.Controls.Add(RB)
        End If

        If RadioButtonList1.Items(1).Selected Then
            Dim CB As CheckBoxList = New CheckBoxList()
            CB.ID = "CheckBoxList1"
            CB.Items.Add(New ListItem("複選一Text", "複選一Value"))
            CB.Items.Add(New ListItem("複選二Text", "複選二Value"))
            CB.Items.Add(New ListItem("複選三Text", "複選三Value"))

            PlaceHolder1.Controls.Add(CB)
        End If

        If RadioButtonList1.Items(2).Selected Then
            Dim TB As TextBox = New TextBox()
            TB.ID = "TextBox1"

            PlaceHolder1.Controls.Add(TB)
        End If

        ' 也可以改成 swich... case的寫法，更簡潔。
    End Sub


    '*******************************************************************************
    '*** 需要透過 .FindControl()方法才能作。請參閱上集第十章。
    '*******************************************************************************
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Dim RB As RadioButtonList = PlaceHolder1.FindControl("RadioButtonList2")
        '--標準寫法：  Dim RB As RadioButtonList = Ctype(PlaceHolder1.FindControl("RadioButtonList2"), RadioButtonList)
        Response.Write("<h2>" + RB.SelectedValue + "</h2>")  '單選，被User點選的結果
        '==========================================================

        Dim CB As CheckBoxList = PlaceHolder1.FindControl("CheckBoxList1")
        Dim str As String = Nothing
        For i As Integer = 0 To (CB.Items.Count - 1)
            If CB.Items(i).Selected Then
                str &= CB.Items(i).Value
            End If
        Next
        Response.Write("<h2>" & str & "</h2>")  '複選，被User點選的結果

        '==========================================================

        Dim TB As TextBox = PlaceHolder1.FindControl("TextBox1")
        Response.Write("<h2>" & TB.Text & "</h2>")  '文字輸入
    End Sub
End Class
