﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="03.aspx.vb" Inherits="WebControls_3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> 按鈕 -- CommandArgument  #1</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    <asp:Button ID="Button1" runat="server" 
        CommandArgument="哈囉！您剛剛按下的按鈕是[測試（test）]" Text="Button / 測試(test)" />
    </form>
</body>
</html>
