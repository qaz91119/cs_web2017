﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="09_RadioButton.aspx.vb" Inherits="VS2010_Book_Sample__Book_WebControls_9_RadioButton" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
            DataSourceID="SqlDataSource1" DataTextField="title" DataValueField="id">
        </asp:RadioButtonList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testConnectionString %>" 
            SelectCommand="select top 7 id,title from test"></asp:SqlDataSource>
        <br />
        <asp:Button ID="Button1" runat="server" Text="Button，請點選！" />
        <br />
        <br />
        <br />
        您剛剛選擇的文章標題：<asp:Label ID="Label1" runat="server" ForeColor="#3333CC"></asp:Label>
        <br />
        您剛剛選擇的文章（資料表裡頭的ID編號）：<asp:Label ID="Label2" runat="server" ForeColor="#009900"></asp:Label>
    
    </div>
    </form>
</body>
</html>
