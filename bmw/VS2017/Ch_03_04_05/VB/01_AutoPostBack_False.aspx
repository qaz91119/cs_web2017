﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="01_AutoPostBack_False.aspx.vb" Inherits="VS2010_Book_Sample_Ch03_Program__Book_WebControls_01_AutoPostBack_False" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title> Ch3 TextBox控制項 </title>
    </head>
<body>
    <form id="form1" runat="server">
    <div>
    
        欄位一： <asp:TextBox ID="TextBox1" runat="server" >預設值</asp:TextBox>
        &nbsp; (設定 AutoPostBack=&quot;<strong>False</strong>&quot; 。) 畫面上改用Button按鈕來作 Submit<br />
        <asp:Button ID="Button1" runat="server" Text="Button" />
        <br />
        <br />
    </div>
    </form>
 
</body>
</html>
