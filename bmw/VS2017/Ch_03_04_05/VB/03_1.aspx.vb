﻿
Partial Class WebControls_3_1
    Inherits System.Web.UI.Page

    Protected Sub Button1_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles Button1.Command

        If e.CommandName = "ABC" Then
            Response.Write("<br>1.  您剛剛按下的Button按鈕，其內建的CommandArgument是----")
            Response.Write(e.CommandArgument)
        Else
            Response.Write(e.CommandName.ToString())
        End If
        
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Response.Write("<br> 2. Button按鈕 的 .Click()事件")
        ' 註解： Button1_Click()事件會被優先執行！
    End Sub
End Class
