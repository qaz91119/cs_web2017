﻿
Partial Class VS2010_Book_Sample_Ch03_Program_0_Page_Load_4
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        '--務必把 Label1的文字（Text屬性）預設為零。

        If Not Page.IsPostBack Then
            '-- 網頁，第一次執行。
            Response.Write("網頁<b>第一次執行</b>....Page_Load事件......<br />")
        Else
            Label1.Text = CInt(Label1.Text) + 1
            '-- 也可以寫成跟C#語法一樣的。  Label1.Text = Convert.ToInt32(Label1.Text) + 1
            '*********************************************
            '-- 跟上一隻程式不同（第一次執行，Label就會變成 1）。
            '-- 本範例第一次執行，Label仍然保持 0。必須按下按鈕才會加一。

            Response.Write("網頁重新PostBack（回傳）....Page_Load事件......<br />")
        End If
    End Sub


    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        Response.Write("按下[按鈕]....Button的 Click事件......<br />")
    End Sub
End Class
