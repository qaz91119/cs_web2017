﻿'=======================================
Imports System.Data   '==請自己動手加入命名空間。
'=======================================


Partial Class _Book_New_Samples_ListItemCollection_ListItem_02
    Inherits System.Web.UI.Page

    '== 全域變數 ==
    Private dv As DataView
    Private dt As New DataTable()


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        ListBox1.Rows = ListBox1.Items.Count ' If the DataTable is already stored in the Web form's default
        ' HttpSessionState variable, then don't recreate the DataTable.
        If Session("data") Is Nothing Then
            ' Add columns to the DataTable.
            dt.Columns.Add(New DataColumn("Item"))
            dt.Columns.Add(New DataColumn("Price"))
            ' Store the DataTable in the Session variable so it can be ' accessed again later.
            Session("data") = dt

            ' Use the table to create a DataView, because the DataGrid' can only bind to a data source that implements IEnumerable.
            dv = New DataView(dt)

            ' 以下是 DataBinding（資料繫結、數據綁定）
            GridView1.DataSource = dv
            GridView1.DataBind()
        End If
    End Sub


    Protected Sub addButton_Click(sender As Object, e As System.EventArgs) Handles addButton.Click
        ' Add the items selected in ListBox1 to DataGrid1.
        '-- 參考資料：http://msdn.microsoft.com/zh-tw/library/system.web.ui.webcontrols.listitem.aspx
        Dim item As ListItem

        For Each item In ListBox1.Items

            If item.Selected Then ' Add the item to the GridView.
                ' First, get the DataTable from the Session variable.
                dt = CType(Session("data"), DataTable)

                If Not (dt Is Nothing) Then ' Create a new DataRow in the DataTable.
                    Dim dr As DataRow
                    dr = dt.NewRow()
                    ' Add the item to the new DataRow.
                    dr("Item") = item.Text
                    ' Add the item's value to the DataRow.
                    dr("Price") = item.Value
                    ' Add the DataRow to the DataTable.
                    dt.Rows.Add(dr)

                    ' 重新作一次DataBinding（資料繫結、數據綁定）
                    dv = New DataView(dt)

                    GridView1.DataSource = dv
                    GridView1.DataBind()
                End If
            End If

        Next item

    End Sub
End Class
