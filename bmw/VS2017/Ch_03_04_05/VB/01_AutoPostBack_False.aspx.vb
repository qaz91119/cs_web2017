﻿
Partial Class VS2010_Book_Sample_Ch03_Program__Book_WebControls_01_AutoPostBack_False
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Write("程式會首先執行 Page_Load()這一區喔！<br />" & DateTime.Now.ToLongTimeString())
    End Sub



    Protected Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        Response.Write("<h3>謝謝您輸入資料！～～～Bye Bye</h3>" & DateTime.Now.ToLongTimeString())
        '--重點！ TextBox的屬性 AutoPostBack="True"，必須先設定好！
    End Sub

End Class
