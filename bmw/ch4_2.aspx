﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ch4_2.aspx.cs" Inherits="ch4_2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            這是第一個乘法器<br />
            <br />
            請輸入資料，再按確定按鈕<br />
            <br />
            <asp:TextBox ID="TextBox1" runat="server" Height="15px" Width="100px"></asp:TextBox>
            *<asp:TextBox ID="TextBox2" runat="server" Height="15px" Width="100px"></asp:TextBox>
            *<asp:TextBox ID="TextBox3" runat="server" Height="15px" Width="100px"></asp:TextBox>
&nbsp;=
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="開始計算" />
        </div>
    </form>
</body>
</html>
 