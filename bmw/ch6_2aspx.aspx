﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ch6_2aspx.aspx.cs" Inherits="ch6_2aspx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            另一個DetailsViews控制項的應用<br />
            <br />
            <asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" AutoGenerateRows="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="test_id" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="Vertical" Height="50px" Width="534px">
                <AlternatingRowStyle BackColor="White" />
                <EditRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                <Fields>
                    <asp:BoundField DataField="test_id" HeaderText="test_id" InsertVisible="False" ReadOnly="True" SortExpression="test_id" />
                    <asp:BoundField DataField="test_guid" HeaderText="test_guid" SortExpression="test_guid" />
                    <asp:BoundField DataField="test_time" HeaderText="test_time" SortExpression="test_time">
                    <HeaderStyle Height="10px" Width="10px" />
                    <ItemStyle Height="10px" Width="10px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="test_class" HeaderText="test_class" SortExpression="test_class" />
                    <asp:BoundField DataField="title" HeaderText="標題" SortExpression="title" />
                    <asp:TemplateField HeaderText="內容" SortExpression="summary">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Height="87px" Text='<%# Bind("summary") %>' TextMode="MultiLine" Width="424px"></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("summary") %>'></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("summary") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="文章" SortExpression="article">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Height="87px" Text='<%# Bind("article") %>' TextMode="MultiLine" Width="431px"></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("article") %>'></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("article") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="author" HeaderText="作者" SortExpression="author" />
                    <asp:BoundField DataField="hit_no" HeaderText="hit_no" SortExpression="hit_no" />
                    <asp:BoundField DataField="get_no" HeaderText="get_no" SortExpression="get_no" />
                    <asp:BoundField DataField="email_no" HeaderText="email_no" SortExpression="email_no" />
                    <asp:BoundField DataField="approved" HeaderText="approved" SortExpression="approved" />
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
                </Fields>
                <FooterStyle BackColor="#CCCC99" />
                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                <RowStyle BackColor="#F7F7DE" />
            </asp:DetailsView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Data Source=F502-1;Integrated Security=True" DeleteCommand="DELETE FROM [News_Test] WHERE [test_id] = @test_id" InsertCommand="INSERT INTO [News_Test] ([test_guid], [test_time], [test_class], [title], [summary], [article], [author], [hit_no], [get_no], [email_no], [approved]) VALUES (@test_guid, @test_time, @test_class, @title, @summary, @article, @author, @hit_no, @get_no, @email_no, @approved)" ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM [News_Test]" UpdateCommand="UPDATE [News_Test] SET [test_guid] = @test_guid, [test_time] = @test_time, [test_class] = @test_class, [title] = @title, [summary] = @summary, [article] = @article, [author] = @author, [hit_no] = @hit_no, [get_no] = @get_no, [email_no] = @email_no, [approved] = @approved WHERE [test_id] = @test_id">
                <DeleteParameters>
                    <asp:Parameter Name="test_id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="test_guid" Type="Object" />
                    <asp:Parameter Name="test_time" Type="DateTime" />
                    <asp:Parameter Name="test_class" Type="String" />
                    <asp:Parameter Name="title" Type="String" />
                    <asp:Parameter Name="summary" Type="String" />
                    <asp:Parameter Name="article" Type="String" />
                    <asp:Parameter Name="author" Type="String" />
                    <asp:Parameter Name="hit_no" Type="Int32" />
                    <asp:Parameter Name="get_no" Type="Int32" />
                    <asp:Parameter Name="email_no" Type="Int32" />
                    <asp:Parameter Name="approved" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="test_guid" Type="Object" />
                    <asp:Parameter Name="test_time" Type="DateTime" />
                    <asp:Parameter Name="test_class" Type="String" />
                    <asp:Parameter Name="title" Type="String" />
                    <asp:Parameter Name="summary" Type="String" />
                    <asp:Parameter Name="article" Type="String" />
                    <asp:Parameter Name="author" Type="String" />
                    <asp:Parameter Name="hit_no" Type="Int32" />
                    <asp:Parameter Name="get_no" Type="Int32" />
                    <asp:Parameter Name="email_no" Type="Int32" />
                    <asp:Parameter Name="approved" Type="String" />
                    <asp:Parameter Name="test_id" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <br />
        </div>
    </form>
</body>
</html>
